let mix = require('laravel-mix');
require('laravel-mix-tailwind');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
	// .copy('resources/assets/less/vendor/mdc-get-colors-mixin.less', 'resources/assets/bower/material-design-color-palette/less/mixins/get-colors.less')
	// .less('resources/assets/bower/material-design-color-palette/less/material-design-color-palette.less', '../resources/assets/bower/material-design-color-palette/css')
	.styles([
		'resources/assets/bower/normalize-css/normalize.css',
		'resources/assets/bower/leaflet/dist/leaflet.css',
		'resources/assets/bower/angular-loading-bar/build/loading-bar.min.css',
		'resources/assets/bower/angular-material/angular-material.css',
		'resources/assets/js/vendor/DataTables/datatables.css',
		'resources/assets/bower/highcharts/css/highcharts.css',
		'resources/assets/bower/highcharts-ng/dist/highcharts-ng.css',
		'resources/assets/bower/fullcalendar/dist/fullcalendar.min.css',
		'resources/assets/bower/angular-datatables/dist/css/angular-datatables.min.css',
		'resources/assets/bower/material-design-color-palette/css/material-design-color-palette.css',
		'resources/assets/bower/mdi/css/materialdesignicons.min.css',
		'resources/assets/bower/animate.css/animate.min.css',
	], 'public/css/vendor.css')

	.scripts([
		'resources/assets/bower/lodash/dist/lodash.min.js',
		'resources/assets/bower/wow/dist/wow.min.js',
		'resources/assets/bower/animejs/anime.min.js',
		'resources/assets/bower/moment/min/moment-with-locales.min.js',
		'resources/assets/bower/jquery/dist/jquery.min.js',
		'resources/assets/bower/chartist/dist/chartist.min.js',
		'resources/assets/js/vendor/DataTables/datatables.min.js',
		'resources/assets/js/vendor/DataTables/datatables-sum.js',
		'resources/assets/js/vendor/DataTables/accent-neutralise.js',
		'resources/assets/js/vendor/DataTables/datetime-moment.js',
		'resources/assets/bower/jszip/dist/jszip.min.js',
		'resources/assets/bower/pdfmake/build/pdfmake.min.js',
		'resources/assets/bower/pdfmake/build/vfs_fonts.js',
		'resources/assets/js/vendor/DataTables/Buttons-1.2.4/js/buttons.html5.js',
		'resources/assets/bower/highcharts/js/highstock.js',
		'resources/assets/bower/highcharts/js/modules/series-label.js',
		'resources/assets/bower/highcharts/js/themes/grid-light.src.js',
		'resources/assets/bower/angular/angular.min.js',
		'resources/assets/bower/angular-loading-bar/build/loading-bar.min.js',
		'resources/assets/bower/angular-animate/angular-animate.min.js',
		'resources/assets/bower/angular-aria/angular-aria.min.js',
		'resources/assets/bower/angular-cookies/angular-cookies.min.js',
		'resources/assets/bower/angular-messages/angular-messages.min.js',
		'resources/assets/bower/angular-moment/angular-moment.min.js',
		'resources/assets/bower/angular-resource/angular-resource.min.js',
		'resources/assets/bower/angular-local-storage/dist/angular-local-storage.min.js',
		'resources/assets/bower/angular-ui-router/release/angular-ui-router.min.js',
		'resources/assets/bower/highcharts-ng/dist/highcharts-ng.js',
		'resources/assets/bower/angular-material/angular-material.min.js',
		'resources/assets/bower/angular-datatables/dist/angular-datatables.min.js',
		'resources/assets/bower/angular-datatables/dist/plugins/buttons/angular-datatables.buttons.min.js',
		'resources/assets/bower/ui-leaflet/dist/ui-leaflet.min.js',
		'resources/assets/bower/angularjs-slider/dist/rzslider.min.js',
		'resources/assets/bower/ng-file-upload/ng-file-upload.min.js',
		'resources/assets/bower/fullcalendar/dist/fullcalendar.min.js',
		'resources/assets/bower/fullcalendar/dist/locale/en-gb.js',
		'resources/assets/bower/fullcalendar/dist/locale/es.js',
	], 'public/js/vendor.js')

	.copyDirectory('resources/assets/fonts', 'public/fonts')
	.copyDirectory('resources/assets/bower/angular-i18n', 'public/js/locales')
	.copyDirectory('resources/assets/bower/mdi/fonts', 'public/fonts')
	.copyDirectory('resources/assets/bower/leaflet/dist/images', 'public/css/images')
	.copyDirectory('resources/assets/js/vendor/DataTables/DataTables-1.10.13/images', 'public/css/images')
	.copyDirectory('resources/assets/js/vendor/DataTables/Buttons-1.2.4/swf', 'public/js/swf')

	// APPLICATION
	.sass('resources/assets/sass/app.scss', 'public/css/app.css').tailwind()

	.coffee([
		'resources/assets/coffee/app.coffee',
		'resources/assets/coffee/components/app-config.coffee',
		'resources/assets/coffee/components/app-factories.coffee',
		'resources/assets/coffee/components/app-routes.coffee',
		'resources/assets/coffee/components/app-services.coffee',
		'resources/assets/coffee/components/c-candidate-followup.coffee',
		'resources/assets/coffee/components/c-chart-candidates-stats.coffee',
		'resources/assets/coffee/components/c-chart-commercials.coffee',
		'resources/assets/coffee/components/c-chart-commercials-contracts.coffee',
		'resources/assets/coffee/components/c-chart-commercials-contracts-per-day.coffee',
		'resources/assets/coffee/components/c-chart-commercials-contracts-per-month.coffee',
		'resources/assets/coffee/components/c-chart-commercials-monetary.coffee',
		'resources/assets/coffee/components/c-chart-commercials-productivity.coffee',
		'resources/assets/coffee/controllers/_headerCtrl.coffee',
		'resources/assets/coffee/controllers/_notificationsCtrl.coffee',
		'resources/assets/coffee/controllers/_rootCtrl.coffee',
		'resources/assets/coffee/controllers/activities/activitiesCtrl.coffee',
		'resources/assets/coffee/controllers/candidates/candidateCreateCtrl.coffee',
		'resources/assets/coffee/controllers/candidates/candidateFollowupCtrl.coffee',
		'resources/assets/coffee/controllers/candidates/candidatesCtrl.coffee',
		'resources/assets/coffee/controllers/candidates/candidateShowCtrl.coffee',
		'resources/assets/coffee/controllers/commercials/commercialCreateCtrl.coffee',
		'resources/assets/coffee/controllers/commercials/commercialsAbsencesCtrl.coffee',
		'resources/assets/coffee/controllers/commercials/commercialsAbsencesListCtrl.coffee',
		'resources/assets/coffee/controllers/commercials/commercialsCtrl.coffee',
		'resources/assets/coffee/controllers/commercials/commercialShowCtrl.coffee',
		'resources/assets/coffee/controllers/commercials/commercialsReportsCtrl.coffee',
		'resources/assets/coffee/controllers/dashboard/dashboardCtrl.coffee',
		'resources/assets/coffee/controllers/managements/managementCreateCtrl.coffee',
		'resources/assets/coffee/controllers/managements/managementsCtrl.coffee',
		'resources/assets/coffee/controllers/managements/managementShowCtrl.coffee',
		'resources/assets/coffee/controllers/managements/managementStrategiesCtrl.coffee',
		'resources/assets/coffee/controllers/productivity/productivityCtrl.coffee',
		'resources/assets/coffee/controllers/productivity/productivityLowCtrl.coffee',
		'resources/assets/coffee/controllers/settings/profileCtrl.coffee',
		'resources/assets/coffee/controllers/settings/settingsCtrl.coffee',

	], 'public/js/app.js')

	.version()
;
