importScripts('https://storage.googleapis.com/workbox-cdn/releases/4.3.1/workbox-sw.js');
if (workbox) {

	// top-level routes we want to precache
	workbox.precaching.precacheAndRoute(['/']);

	// injected assets by Workbox CLI
	workbox.precaching.precacheAndRoute([
  {
    "url": "css/app.css",
    "revision": "1d3cbea47a540f5f98087db2aa0dbe3b"
  },
  {
    "url": "css/vendor.css",
    "revision": "0659803d381004c1f5192a4176ee4d63"
  },
  {
    "url": "js/angular-locale_es-co.js",
    "revision": "fbb1e3ead87c1b233c0a3f74c108946c"
  },
  {
    "url": "js/angular-locale_es-es.js",
    "revision": "41df311f4b8636ddfdb00c208fe45807"
  },
  {
    "url": "js/app.js",
    "revision": "88861a7a8194a9ac83c83d186117a8ce"
  },
  {
    "url": "js/vendor.js",
    "revision": "3ef399b9ddd1aefe67741095779f52a6"
  },
  {
    "url": "statics/app-logo-128x128.png",
    "revision": "1a888fb5ad2f65f549fe93cbd0e602e9"
  },
  {
    "url": "statics/icons/apple-icon-120x120.png",
    "revision": "dcadec4306e24b7e11aad57153f8afa0"
  },
  {
    "url": "statics/icons/apple-icon-152x152.png",
    "revision": "2b0eb5d1cc0389b8ae47bd3cc7c4cc77"
  },
  {
    "url": "statics/icons/apple-icon-167x167.png",
    "revision": "b7a91833eea75a083a24c3ce5c41c7ca"
  },
  {
    "url": "statics/icons/apple-icon-180x180.png",
    "revision": "1f9babbbb8638a1b17755e2a9ee09dc8"
  },
  {
    "url": "statics/icons/favicon-16x16.png",
    "revision": "332495aa4bd888a8faf8dec4397b3be2"
  },
  {
    "url": "statics/icons/favicon-32x32.png",
    "revision": "e4a0ea42b412befac425e5950608da80"
  },
  {
    "url": "statics/icons/favicon-96x96.png",
    "revision": "64734ec7398109d59db3db69e7ae618f"
  },
  {
    "url": "statics/icons/icon-128x128.png",
    "revision": "1a888fb5ad2f65f549fe93cbd0e602e9"
  },
  {
    "url": "statics/icons/icon-192x192.png",
    "revision": "fac48b67b4028b6d56afd3a58d08c190"
  },
  {
    "url": "statics/icons/icon-256x256.png",
    "revision": "5c311acbcf337852d885cbd485311313"
  },
  {
    "url": "statics/icons/icon-384x384.png",
    "revision": "8983d9524a67de996d7ff1362134485b"
  },
  {
    "url": "statics/icons/icon-512x512.png",
    "revision": "b3331cb5fbbd5cecc7685fda64c59697"
  },
  {
    "url": "statics/icons/ms-icon-144x144.png",
    "revision": "42abce2cce47b7bf7b2b0a033fd3abae"
  }
]);

	// js/css files
	workbox.routing.registerRoute(
		/\.(?:js|css)$/,
		new workbox.strategies.StaleWhileRevalidate({
			cacheName: 'static-resources',
		})
	);

	// images
	workbox.routing.registerRoute(
		// Cache image files.
		/\.(?:png|jpg|jpeg|svg|gif)$/,
		// Use the cache if it's available.
		new workbox.strategies.CacheFirst({
			// Use a custom cache name.
			cacheName: 'image-cache',
			plugins: [
				new workbox.expiration.Plugin({
					// Cache upto 50 images.
					maxEntries: 50,
					// Cache for a maximum of a week.
					maxAgeSeconds: 7 * 24 * 60 * 60,
				})
			],
		})
	);

}
