<?php

use App\Mail\MailSender;
use App\Mail\NewCandidate as NewCandidateMail;
use App\Models\Candidate;
use App\Models\CandidatesFile;
use App\Models\CandidatesOrigin;
use App\Models\EmailsSent;
use App\Models\External\TProvincias;
use App\Models\Management;
use App\Models\User;
use Carbon\Carbon;

$apiExtRouteParams = ['middleware' => 'custom-api-login', 'namespace' => 'API', 'prefix' => 'api/ext'];

Route::group($apiExtRouteParams, function () {
	Route::post('test', function () {
		return auth()->id();
	});

	Route::get('comprod/{commercial}', 'Commercials\ProductivityController@commercial');
});

Route::post('landing/store', function () {
	$data = request()->validate([
		'name'     => 'required',
		'surname'  => 'sometimes',
		'lastname' => 'sometimes',
		'phone'    => 'required',
		'email'    => 'sometimes',
		'city'     => 'sometimes',
		'location' => 'required',
		'offer'    => 'required',
	]);

	$data['comment'] = 'Oferta: ' . $data['location'];
	$data['management_id'] = Management::where('company_id', 5)->where('name', 'Reclutamiento')->first()->id;
	$data['petitioned_at'] = Carbon::now();
	$data['origin_id'] = CandidatesOrigin::where('company_id', 5)->where('name', 'Landing Page')->first()->id;
	$data['status_id'] = 1;
	$data['user_id'] = User::where('username', 'Amparo')->first()->id;

	if (isset($data['city'])) {
		$data['comment'] .= ' | Ciudad: ' . $data['city'];
	}

	if (isset($data['offer'])) {
		$data['comment'] .= ' | Vacante: ' . $data['offer'];
	}

	unset($data['city']);
	unset($data['location']);
	unset($data['offer']);

	$candidate = Candidate::create($data);

	MailSender::send($candidate, new NewCandidateMail(auth()->user(), $candidate));

	EmailsSent::create([
		'candidate_id'  => $candidate->id,
		'user_id'       => 21,
		'email_type_id' => 1,
	]);

	return ['id' => $candidate->id];
});
Route::post('landing/store-cv', function () {
	request()->validate([
		'candidate_id' => 'required',
		'file'         => 'required|file',
	]);

	$candidateId = request()->candidate_id;
	$file = request()->file;

	$filename = $file->getClientOriginalName();
	$path = $file->store('public/documents');

	$fileCV = CandidatesFile::create([
		'candidate_id' => $candidateId,
		'filename'     => $filename,
		'path'         => $path,
		'type_id'      => 39,
		'user_id'      => 21,
	]);

	return ['id' => $fileCV->id];
});

/*
Route::get('landing/{any}', function () {
	return view('templates.landing.index');
})->where('any', '.*');
*/

Route::get('landing/mx', function () {

	return view('templates.landing.index')->with('provinces',TProvincias::on('mexico')->get());
});
Route::get('landing/pt', function () {

	return view('templates.landing.indexpt')->with('provinces',TProvincias::on('portugal')->get());
});
Route::get('me', function () {
	return auth()->user();
});
Route::post('login', 'Auth\LoginController@login');

Route::group(['middleware' => 'guest'], function () {
	Route::get('login', function () {
		return view('auth.login');
	})->name('login');
});


Route::group(['middleware' => 'auth'], function () {
	Route::get('logout', 'Auth\LoginController@logout');

	Route::group(['namespace' => 'API', 'prefix' => 'api/v1'], function () {
		Route::group(['namespace' => 'Candidates', 'prefix' => 'candidates'], function () {
			Route::apiResource('appointments', 'AppointmentController')->only(['store', 'update']);
			Route::apiResource('files/types', 'FilesTypeController')->except(['show']);
			Route::apiResource('files', 'FileController')->only(['store', 'show', 'destroy']);
			Route::apiResource('origins', 'OriginController')->only(['index']);
			Route::get('stats', 'StatsController');
			Route::apiResource('statuses', 'StatusController')->only(['index']);

		});

		Route::get('candidates/external/{external_id}', 'CandidateController@external');
		Route::get('candidates/checkIsValid/{commercial}/{type}/{phone}', 'CandidateController@checkIsValid');
        Route::get('candidates/checkHireAvailability/{candidate}', 'CandidateController@checkHireAvailability');
		Route::apiResource('candidates', 'CandidateController');

		Route::group(['namespace' => 'Administration', 'prefix' => 'administration'], function () {

			Route::get('contractsTypes/{contractType}', 'ContractsTypesController@show');
			Route::get('contractsTypes/', 'ContractsTypesController@index');
			Route::delete('contractsTypes/{contractType}', 'ContractsTypesController@destroy');
			Route::post('contractsTypes/', 'ContractsTypesController@store');
			Route::put('contractsTypes/{contractType}', 'ContractsTypesController@update');

			Route::get('rates/', 'RatesController@index');
			Route::get('paymentMethods/', 'PaymentMethodsController@index');
			Route::get('storages/', 'StoragesController@index');

			Route::get('ivas/{iva}', 'IvasController@show');
			Route::get('ivas/', 'IvasController@index');
			Route::delete('ivas/{iva}', 'IvasController@destroy');
			Route::post('ivas/', 'IvasController@store');
			Route::put('ivas/{iva}', 'IvasController@update');


			Route::get('itaus/{itau}', 'ItausController@show');
			Route::get('itaus/', 'ItausController@index');
			Route::delete('itaus/{itau}', 'ItausController@destroy');
			Route::post('itaus/', 'ItausController@store');
			Route::put('itaus/{itau}', 'ItausController@update');


			Route::get('irpfs/{irpf}', 'IrpfsController@show');
			Route::get('irpfs/', 'IrpfsController@index');
			Route::delete('irpfs/{irpf}', 'IrpfsController@destroy');
			Route::post('irpfs/', 'IrpfsController@store');
			Route::put('irpfs/{irpf}', 'IrpfsController@update');




			Route::get('contractsTypesStretches/{stretch}', 'ContractsTypesStretchesController@show');
			Route::get('contractsTypesStretches/', 'ContractsTypesStretchesController@index');
			Route::delete('contractsTypesStretches/{stretch}', 'ContractsTypesStretchesController@destroy');
			Route::post('contractsTypesStretches/', 'ContractsTypesStretchesController@store');
			Route::put('contractsTypesStretches/{stretch}', 'ContractsTypesStretchesController@update');


			Route::get('feesGroup/{group}', 'FeesGroupsController@show');
			Route::get('feesGroup/', 'FeesGroupsController@index');
			Route::delete('feesGroup/{group}', 'FeesGroupsController@destroy');
			Route::post('feesGroup/', 'FeesGroupsController@store');
			Route::put('feesGroup/{group}', 'FeesGroupsController@update');

			Route::get('feesProducts/{product}', 'FeesGroupProductsController@show');
			Route::get('feesProducts/', 'FeesGroupProductsController@index');
			Route::delete('feesProducts/{product}', 'FeesGroupProductsController@destroy');
			Route::post('feesProducts/', 'FeesGroupProductsController@store');
			Route::put('feesProducts/{product}', 'FeesGroupProductsController@update');


			Route::get('feesProductFees/{fee}', 'FeesGroupProductFeesController@show');
			Route::get('feesProductFees/', 'FeesGroupProductFeesController@index');
			Route::delete('feesProductFees/{fee}', 'FeesGroupProductFeesController@destroy');
			Route::post('feesProductFees/', 'FeesGroupProductFeesController@store');
			Route::put('feesProductFees/{fee}', 'FeesGroupProductFeesController@update');

			Route::get('feesManagementFees/{fee}', 'FeesGroupManagementFeesController@show');
			Route::get('feesManagementFees/', 'FeesGroupManagementFeesController@index');
			Route::delete('feesManagementFees/{fee}', 'FeesGroupManagementFeesController@destroy');
			Route::post('feesManagementFees/', 'FeesGroupManagementFeesController@store');
			Route::put('feesManagementFees/{fee}', 'FeesGroupManagementFeesController@update');



			Route::get('feesArticles/{article}', 'FeesGroupArticlesController@show');
			Route::get('feesArticles/', 'FeesGroupArticlesController@index');
			Route::delete('feesArticles/{article}', 'FeesGroupArticlesController@destroy');
			Route::post('feesArticles/', 'FeesGroupArticlesController@store');
			Route::put('feesArticles/{article}', 'FeesGroupArticlesController@update');

			Route::get('feesManagements/{management}', 'FeesGroupManagementsController@show');
			Route::get('feesManagements/', 'FeesGroupManagementsController@index');
			Route::delete('feesManagements/{management}', 'FeesGroupManagementsController@destroy');
			Route::post('feesManagements/', 'FeesGroupManagementsController@store');
			Route::put('feesManagements/{management}', 'FeesGroupManagementsController@update');


			Route::get('feesGroupFees/{fee}', 'FeesGroupFeesController@show');
			Route::get('feesGroupFees/', 'FeesGroupFeesController@index');
			Route::delete('feesGroupFees/{fee}', 'FeesGroupFeesController@destroy');
			Route::post('feesGroupFees/', 'FeesGroupFeesController@store');
			Route::put('feesGroupFees/{fee}', 'FeesGroupFeesController@update');

			Route::get('feesGroupEfectivityFees/{fee}', 'FeesGroupEfectivityFeesController@show');
			Route::get('feesGroupEfectivityFees/', 'FeesGroupEfectivityFeesController@index');
			Route::delete('feesGroupEfectivityFees/{fee}', 'FeesGroupEfectivityFeesController@destroy');
			Route::post('feesGroupEfectivityFees/', 'FeesGroupEfectivityFeesController@store');
			Route::put('feesGroupEfectivityFees/{fee}', 'FeesGroupEfectivityFeesController@update');

			Route::get('managementStructures/{management_id}', 'ManagementStructuresController@index');
			Route::delete('managementStructures/{management_id}/{management_structure_id}', 'ManagementStructuresController@destroy');
			Route::post('managementStructures/{management_id}/', 'ManagementStructuresController@store');
			Route::put('managementStructures/{management}/{structure}', 'ManagementStructuresController@update');


			Route::delete('settlementconcepts/{concept}', 'SettlementConceptsController@destroy');

			Route::get('settlements/getManagementSettlements/{management}', 'SettlementsController@getManagementSettlements');
			Route::get('settlements/getCommercialSettlements/{commercial}', 'SettlementsController@getCommercialSettlements');

			Route::get('settlements/pdfManagementSettlement/{management}/{settlement}', 'SettlementsController@pdfManagementSettlement');
			Route::get('settlements/pdfCommercialSettlement/{commercialSettlement}', 'SettlementsController@pdfCommercialSettlement');
			Route::get('settlements/pdfSettlement/{settlement}', 'SettlementsController@pdfSettlement');

			Route::get('settlements/{settlement}', 'SettlementsController@show');
			Route::get('settlements/', 'SettlementsController@index');
			Route::delete('settlements/{settlement}', 'SettlementsController@destroy');
			Route::post('settlements/', 'SettlementsController@store');
			Route::put('settlements/{settlement}', 'SettlementsController@update');




			/*
			Route::apiResource('feesGroupProduct', 'FeesGroupProductController');
			Route::apiResource('feesGroupManagement', 'FeesGroupManagementController');
			Route::apiResource('feesGroupManagementFees', 'FeesGroupManagementFeesController');*/

		});


		Route::group(['namespace' => 'Commercials', 'prefix' => 'commercials'], function () {
			Route::apiResource('absences/motives', 'AbsencesMotiveController');
			Route::apiResource('absences', 'AbsenceController');
			Route::apiResource('campaigns', 'CampaignController');

			Route::get('managements/stats', 'ManagementController@stats');
			Route::get('managements/types-contracts', 'ManagementController@types_contracts');
			Route::get('managements/types-contracts-subtype', 'ManagementController@types_contracts_subtype');
			Route::get('managements/leaders/{id_gerencia}', 'ManagementController@leaders');
			Route::apiResource('managements', 'ManagementController');

			Route::apiResource('organizations', 'OrganizationController')->only(['index']);

			Route::get('productivity', 'ProductivityController@index');
			Route::get('productivity/commercial/{commercial}', 'ProductivityController@commercial');
			Route::get('productivity/commercials', 'ProductivityController@commercials');
			Route::get('productivity/low', 'ProductivityController@low');
			Route::get('productivity/managements', 'ProductivityController@managements');
			Route::get('productivity/stats', 'ProductivityController@stats');

			Route::apiResource('reports/motives', 'ReportsMotiveController');
			Route::apiResource('reports/statuses', 'ReportsStatusController');
			Route::apiResource('reports', 'ReportController');
			Route::apiResource('strategies', 'StrategyController');

			Route::get('managementAddresses/{address}/{management}', 'ManagementAddressController@show');
			Route::get('managementAddresses/', 'ManagementAddressController@index');
			Route::delete('managementAddresses/{address}/{management}', 'ManagementAddressController@destroy');
			Route::post('managementAddresses/', 'ManagementAddressController@store');
			Route::put('managementAddresses/{address}/{management}', 'ManagementAddressController@update');




		});
		Route::delete('productsStretches/{stretch}', 'ProductsStretchesController@destroy');
		Route::get('products/{product}', 'ProductsController@show');
		Route::get('products/', 'ProductsController@index');
		Route::delete('products/{product}', 'ProductsController@destroy');
		Route::post('products/', 'ProductsController@store');
		Route::put('products/{product}', 'ProductsController@update');


		Route::get('articles/{article}', 'ArticlesController@show');
		Route::get('articles/', 'ArticlesController@index');
		Route::delete('articles/{article}', 'ArticlesController@destroy');
		Route::post('articles/', 'ArticlesController@store');
		Route::put('articles/{article}', 'ArticlesController@update');

		Route::get('provinces/{province}', 'ProvincesController@show');
		Route::get('provinces/', 'ProvincesController@index');



		Route::delete('commercials/{commercial}/structures/{id_estructura}', 'Commercials\CommercialStructureController@destroy');
		Route::post('commercials/{commercial}/structures/', 'Commercials\CommercialStructureController@store');
		Route::put('commercials/{commercial}/structures/{id_estructura}', 'Commercials\CommercialStructureController@update');

		Route::delete('commercials/{commercial}/company_contracts/{id_comercial_contrato}', 'Commercials\CommercialCompanyContractsController@destroy');
		Route::post('commercials/{commercial}/company_contracts/', 'Commercials\CommercialCompanyContractsController@store');
		Route::put('commercials/{commercial}/company_contracts/{id_comercial_contrato}', 'Commercials\CommercialCompanyContractsController@update');


		Route::post('commercials/{commercial}/avatar', 'Commercials\CommercialAvatarController@store');
		Route::delete('commercials/{commercial}/avatar', 'Commercials\CommercialAvatarController@destroy');

		Route::post('commercials/{commercial}/campaigns', 'CommercialController@campaigns');
		Route::get('commercials/contracts/{commercial_id}', 'CommercialController@contracts');
		Route::get('commercials/internal/{external_id}', 'CommercialController@internal');
        Route::get('commercials/managers', 'CommercialController@managers');

		Route::get('commercials/checkIsValid/{commercial}/{type}/{phone}', 'CommercialController@checkIsValid');


		Route::apiResource('commercials', 'CommercialController');
		Route::apiResource('companies', 'CompanyController')->only(['index']);

		Route::apiResource('files/types', 'Commercials\FilesTypeController');

		Route::apiResource('emails/types', 'EmailsTypeController');
		Route::apiResource('emails', 'EmailController');

		Route::apiResource('managements', 'ManagementController');
		Route::apiResource('user', 'UserController');
	});


	Route::any('views/{controller}/{view}', function ($controller, $view) {
		return view('templates' . '.' . $controller . '.' . $view);
	})->where("path", ".+");

	Route::any('{path?}', function () {
		return view("home");
	})->where("path", ".*");
});
