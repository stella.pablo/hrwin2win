<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Company;
use App\Models\Candidate;
use App\Models\EmailsType;
use App\Models\Management;
use App\Models\CandidatesOrigin;
use App\Models\CandidatesFilesType;
use App\Models\CandidatesStatus;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		Company::create([
			'name' => 'Giving International',
			'database' => 'giving',
			'locale' => 'es-es'
		]);

		Company::create([
			'name' => 'Colombia',
			'database' => 'colombia',
			'locale' => 'es-co'
		]);

		Management::create([
			'name' => 'Gerencia 1',
			'company_id' => 1,
			'external_id' => '0038'
		]);

		Management::create([
			'name' => 'Gerencia 1',
			'company_id' => 2,
			'external_id' => '0038'
		]);

		User::create([
			'name' => 'Carolina',
			'username' => 'Carolina',
			'email' => 'carolina@company.com',
			'password' => Hash::make('k0z72V'),
			'management_id' => 1,
			'level' => 1,
			'pin' => '6431'
		]);

		User::create([
			'name' => 'Elena',
			'username' => 'Elena',
			'email' => 'elena@company.com',
			'password' => Hash::make('huVHR1'),
			'management_id' => 1,
			'level' => 1,
			'pin' => '8936'
		]);

		User::create([
			'name' => 'Fernando',
			'username' => 'Fernando',
			'email' => 'fernando@company.com',
			'password' => Hash::make('5P0i8C'),
			'management_id' => 1,
			'level' => 2,
			'pin' => '9731'
		]);

		User::create([
			'name' => 'Dani',
			'username' => 'Dani',
			'email' => 'reclutamiento@givinginternational.es',
			'password' => Hash::make('27IJ92'),
			'management_id' => 1,
			'level' => 3,
			'pin' => '4638'
		]);

		User::create([
			'name' => 'Eduardo',
			'username' => 'Eduardo',
			'email' => 'eduardo@colombia.com',
			'password' => Hash::make('z83VsD'),
			'management_id' => 2,
			'level' => 2,
			'pin' => '1234'
		]);

		CandidatesStatus::create(['name' => 'Abierto']);
		CandidatesStatus::create(['name' => 'No contesta']);
		CandidatesStatus::create(['name' => 'Mensaje dejado']);
		CandidatesStatus::create(['name' => 'Retención']);

		CandidatesFilesType::create(['name' => 'CV']);
		CandidatesFilesType::create(['name' => 'DNI']);
		CandidatesFilesType::create(['name' => 'Contrato']);
		CandidatesFilesType::create(['name' => 'Autónomo']);
		CandidatesFilesType::create(['name' => 'Baja médica']);
		CandidatesFilesType::create(['name' => 'Otros']);

		$origins = ["Adtriboo", "CareerBuilder", "CornerJob", "Domestika", "El País / Monster", "Empleomarketing", "Indeed", "Infoempleo", "Infojobs", "Informáticos", "Jobandtalent", "Jobeeper", "Jobijoba", "JobToday", "Laboris", "Linkmyjob", "Monster", "Plataforma de empleo", "Primerempleo", "RP - Reclutamiento Personal", "Simplyhired", "Studentjob", "Tecnoempleo", "Ticjob", "Trabajamos.net"];

		foreach ($origins as $origin) {
			CandidatesOrigin::create(['name' => $origin]);
		}

		EmailsType::create(['name' => 'Bienvenid@']);
		EmailsType::create(['name' => 'Candidato no contesta']);
		EmailsType::create(['name' => 'Candidato citado']);
		EmailsType::create(['name' => 'Candidato contratado']);

        // factory(CandidatesOrigin::class, 3)->create();
		// factory(Candidate::class, 10)->create();
		//
		// factory(CandidatesFilesType::class, 3)->create();
		// factory(EmailsType::class, 3)->create();
    }
}
