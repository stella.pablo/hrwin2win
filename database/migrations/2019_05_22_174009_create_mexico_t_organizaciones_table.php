<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMexicoTOrganizacionesTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::connection('mexico')->create('t_organizaciones', function (Blueprint $table) {
			$table->increments('id');
			$table->string('id_comercial')->nullable()->comment('El comercial gerente de esta organizacion');
			$table->string('organizacion');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::connection('mexico')->dropIfExists('t_organizaciones');
	}
}
