<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Schema;

class AddFechaAltaToTGerenciasTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		$databases = array_keys(Arr::except(config('database.connections'), 'mysql'));

		foreach ($databases as $database) {
			Schema
				::connection($database)
				->table('t_gerencias', function (Blueprint $table) {
					$table->dateTime('fecha_alta')->after('id_estado')->nullable();
				});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		$databases = array_keys(Arr::except(config('database.connections'), 'mysql'));

		foreach ($databases as $database) {
			Schema
				::connection($database)
				->table('t_gerencias', function (Blueprint $table) {
					$table->dropColumn('fecha_alta');
				});
		}
	}
}
