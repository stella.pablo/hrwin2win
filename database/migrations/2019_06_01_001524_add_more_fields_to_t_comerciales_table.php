<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMoreFieldsToTComercialesTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::connection('mexico')->table('t_comerciales', function (Blueprint $table) {
			$table->string('nacionalidad', 1)->nullable()->comment('N si es Nacional o E si es Extranjero');
			$table->string('rfc')->nullable();
			$table->string('curp')->nullable();
			$table->string('permiso')->nullable();
			$table->string('fm12')->nullable();
			$table->string('pasaporte')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::connection('mexico')->table('t_comerciales', function (Blueprint $table) {
			$table->dropColumn([
				'nacionalidad',
				'rfc',
				'curp',
				'permiso',
				'fm12',
				'pasaporte',
			]);
		});
	}
}
