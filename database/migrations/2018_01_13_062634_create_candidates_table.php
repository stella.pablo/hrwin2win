<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCandidatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candidates', function (Blueprint $table) {
            $table->increments('id');
			$table->string('name');
			$table->string('surname')->nullable();
			$table->string('lastname')->nullable();
			$table->string('email')->nullable();
			$table->string('phone')->nullable();
			$table->boolean('phone_not_answered')->nullable();
			$table->date('petitioned_at')->nullable();
			$table->text('comment')->nullable();

			$table->boolean('preapproved')->nullable();
			$table->dateTime('preapproved_at')->nullable();
			$table->unsignedInteger('preapproved_user_id')->nullable();
			$table->boolean('approved')->nullable();
			$table->dateTime('approved_at')->nullable();
			$table->unsignedInteger('approved_user_id')->nullable();
			$table->boolean('hired')->nullable();
			$table->dateTime('hired_at')->nullable();
			$table->unsignedInteger('hired_user_id')->nullable();

			$table->string('external_id')->nullable();

			$table->unsignedInteger('management_id');
			$table->unsignedInteger('origin_id')->nullable();
			$table->unsignedInteger('status_id')->default(1);
			$table->unsignedInteger('user_id');

			$table->softDeletes();
            $table->timestamps();

			$table->foreign('management_id')->references('id')->on('managements');
			$table->foreign('origin_id')->references('id')->on('candidates_origins');
			$table->foreign('status_id')->references('id')->on('candidates_statuses');
			$table->foreign('user_id')->references('id')->on('users');
			$table->foreign('preapproved_user_id')->references('id')->on('users');
			$table->foreign('approved_user_id')->references('id')->on('users');
			$table->foreign('hired_user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('candidates');
    }
}
