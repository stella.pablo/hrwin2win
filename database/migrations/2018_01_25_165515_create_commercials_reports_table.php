<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommercialsReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commercials_reports', function (Blueprint $table) {
            $table->increments('id');
			$table->unsignedInteger('management_id');
			$table->string('external_id');
			$table->tinyInteger('motive_id');
			$table->tinyInteger('status_id')->nullable();

            $table->timestamps();

			$table->foreign('management_id')->references('id')->on('managements');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('commercials_reports');
    }
}
