<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailsSentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emails_sent', function (Blueprint $table) {
            $table->increments('id');
			$table->unsignedInteger('candidate_id')->nullable();
			$table->string('external_id')->nullable();
			$table->unsignedInteger('user_id');
			$table->unsignedInteger('email_type_id');
            $table->timestamps();

			$table->foreign('candidate_id')->references('id')->on('candidates');
			$table->foreign('user_id')->references('id')->on('users');
			$table->foreign('email_type_id')->references('id')->on('emails_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emails_sent');
    }
}
