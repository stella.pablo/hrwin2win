<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIdOrganizacionToTGerenciasTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::connection('mexico')->table('t_gerencias', function (Blueprint $table) {
			$table->string('id_gerencia_externa')->after('id_gerencia')->nullable();
			$table->unsignedInteger('id_organizacion')->nullable()->comment('Una gerencia puede pertenecer a una organizacion');

			$table->foreign('id_organizacion')->references('id')->on('t_organizaciones');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::connection('mexico')->table('t_gerencias', function (Blueprint $table) {
			$table->dropColumn([
				'id_gerencia_externa',
				'id_organizacion',
			]);
		});
	}
}
