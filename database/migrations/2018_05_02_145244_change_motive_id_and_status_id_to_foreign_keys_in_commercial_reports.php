<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeMotiveIdAndStatusIdToForeignKeysInCommercialReports extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('commercials_reports', function (Blueprint $table) {
            $table->unsignedInteger('motive_id')->change();
			$table->unsignedInteger('status_id')->nullable()->change();

			$table->foreign('motive_id')->references('id')->on('reports_motives');
			$table->foreign('status_id')->references('id')->on('reports_statuses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('commercials_reports', function (Blueprint $table) {
            //
        });
    }
}
