<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCandidatesAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candidates_appointments', function (Blueprint $table) {
            $table->increments('id');
			$table->unsignedInteger('candidate_id');
			$table->tinyInteger('number'); // appointment 1, 2, 3....
			$table->dateTime('date');
			$table->tinyInteger('status')->default(1); // 0 = did not come; 1 = appointed / awaiting; 2 = did come
			$table->string('reason')->nullable(); // reason if did not come
            $table->timestamps();

			$table->foreign('candidate_id')->references('id')->on('candidates');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('candidates_appointments');
    }
}
