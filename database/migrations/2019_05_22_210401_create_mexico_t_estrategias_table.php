<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMexicoTEstrategiasTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::connection('mexico')->create('t_estrategias', function (Blueprint $table) {
			$table->increments('id');
			$table->string('id_gerencia');
			$table->string('id_estrategia');
			$table->string('estrategia');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::connection('mexico')->dropIfExists('t_estrategias');
	}
}
