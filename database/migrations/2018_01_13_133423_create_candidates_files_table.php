<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCandidatesFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candidates_files', function (Blueprint $table) {
            $table->increments('id');
			$table->unsignedInteger('candidate_id')->nullable();
			$table->string('external_id')->nullable();
			$table->unsignedInteger('type_id');
			$table->unsignedInteger('user_id');
			$table->string('filename');
			$table->string('path');
			$table->unsignedInteger('absence_id')->nullable();
            $table->timestamps();

			$table->foreign('candidate_id')->references('id')->on('candidates');
			$table->foreign('type_id')->references('id')->on('candidates_files_types');
			$table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('candidates_files');
    }
}
