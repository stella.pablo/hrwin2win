<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeReasonToMotiveIdForCommercialsAbsences extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('commercials_absences', function (Blueprint $table) {
            $table->dropColumn(['commercial_id', 'reason']);
			$table->string('external_id')->after('id');
			$table->unsignedInteger('motive_id')->after('date');

			$table->foreign('motive_id')->references('id')->on('absences_motives');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('commercials_absences', function (Blueprint $table) {
            //
        });
    }
}
