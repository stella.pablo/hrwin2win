<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdEstrategiaToTComercialesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mexico')->table('t_comerciales', function (Blueprint $table) {
            $table->unsignedInteger('id_estrategia')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mexico')->table('t_comerciales', function (Blueprint $table) {
            $table->dropColumn('id_estrategia');
        });
    }
}
