<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
			$table->unsignedInteger('management_id');
            $table->string('name');
            $table->string('email')->unique();
			$table->string('username')->unique();
            $table->string('password');
			$table->string('level');
			$table->string('pin', 10);
            $table->rememberToken();
            $table->timestamps();

			$table->foreign('management_id')->references('id')->on('managements');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
