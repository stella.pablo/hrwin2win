<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Candidate::class, function (Faker $faker) {
	$dates = [
		'2018-01-01',
		'2018-01-02',
		'2018-01-03',
		'2018-01-04',
		'2018-01-05',
		'2018-01-06',
	];
    return [
        'name' => $faker->firstName,
		'surname' => $faker->lastName,
		'email' => $faker->unique()->safeEmail,
		'phone' => $faker->phoneNumber,
		'petitioned_at' => $dates[rand(0, count($dates) - 1)],
		'origin_id' => rand(1, 3),
		'user_id' => 3
    ];
});
