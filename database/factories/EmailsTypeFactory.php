<?php

use Faker\Generator as Faker;

$factory->define(App\Models\EmailsType::class, function (Faker $faker) {
    return [
        'name' => ucfirst($faker->word)
    ];
});
