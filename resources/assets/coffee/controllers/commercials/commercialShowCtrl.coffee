window.app.controller 'commercialShowCtrl', (
	$scope,
	$state,
	$rootScope,
	$http,
	$mdDialog,
	$mdMedia,
	$window,
	$timeout,
	$compile,
	Candidates,
	Commercials,
	commercial,
	CommercialsReports,
	CommercialsStructures,
	filesTypes,
	Productivity,
	ReportsMotives,
	ReportsStatuses,
	Toast,
	Upload,
	ViewsUrl,
	DTOptionsBuilder,
	DTColumnBuilder
) ->

	$scope.$mdMedia = $mdMedia

	$scope.avatarUpBarStyle =
		visibility: 'hidden'
	$scope.avatarUpProgress = 0

	$scope.candidate = Candidates.external({ id: commercial.id_comercial })
	$scope.commercial = commercial
	$scope.commercialStats = {}
	$scope.filesTypes = filesTypes

	$scope.fileType = null
	$scope.fileUpBarStyle =
		visibility: 'hidden'
	$scope.fileUpProgress = 0

	$scope.moment = moment

	$scope.stats = null

	Productivity.commercial {id: commercial.id_comercial}, (result) ->
		$scope.stats = result
		return

#	$scope.filters =
#		date_from: moment().startOf('month').format('YYYY-MM-DD')
#		date_to: moment().format('YYYY-MM-DD')
#
	$scope.contractsObjective = if $scope.commercial.management_internal then $scope.commercial.management_internal.monthly_objective else 10
	$scope.calcData =
		contractsObjective: $scope.contractsObjective
		contractsObjectivePercentage: null
		contractsObjectivePerDay: $scope.contractsObjective / moment().daysInMonth()
		currentDay: moment()
		monthStart: moment().startOf('month')
		monthEnd: moment().endOf('month') # not used right now

	$scope.calcData.daysDiff = $scope.calcData.currentDay.diff($scope.calcData.monthStart, 'days') + 1

	$scope.commercial.numero_contratos_total = $scope.commercial.contracts_ok_count + $scope.commercial.contracts_p_count + $scope.commercial.contracts_ko_count
	$scope.commercial.contratos_por_dia = $scope.commercial.numero_contratos_total / $scope.calcData.daysDiff
	$scope.commercial.contratos_adiadehoy_debe = Math.round($scope.calcData.contractsObjectivePerDay * $scope.calcData.daysDiff)
	$scope.commercial.contratos_adiadehoy_tiene = $scope.commercial.numero_contratos_total
	$scope.commercial.contratos_adiadehoy_porcentaje = Math.round(($scope.commercial.contratos_adiadehoy_tiene / $scope.commercial.contratos_adiadehoy_debe) * 100)
	$scope.commercial.contratos_prevision = Math.round($scope.calcData.contractsObjective * ($scope.commercial.contratos_adiadehoy_porcentaje / 100))
	$scope.commercial.contratos_prevision_porcentaje = Math.round(($scope.commercial.contratos_prevision / $scope.calcData.contractsObjective) * 100)
	$scope.commercialStats = $scope.commercial

	$scope.fileUpBarStyle =
		visibility: 'hidden'
	$scope.fileUpProgress = 0

	$scope.table =
		reports:
			dtInstance: null
			dtInstanceCallback: (dtInstance) ->
				$scope.table.reports.dtInstance = dtInstance
				return
			dtOptions:
				dom: 't'
				drawCallback: (settings) ->
					$timeout (() ->
						angular.element(window).trigger('resize')
						return
					), 500
					return
				paginationType: 'full_numbers'
				scrollX: true


	$scope.upload = (file) ->
		Upload.upload(
			url: '/api/v1/candidates/files'
			data:
				external_id: $scope.commercial.id_comercial
				file: file
				type_id: $scope.fileType
		).then ((resp) ->
			$scope.commercial.files.push resp.data
			$scope.fileUpBarStyle.visibility = 'hidden'
			return
		), ((resp) ->
			return
		), (evt) ->
			$scope.fileUpBarStyle.visibility = 'visible'
			$scope.fileUpProgress = parseInt(100.0 * evt.loaded / evt.total)
			return
		return

	$scope.uploadAvatar = (file) ->
		Upload.upload(
			url: '/api/v1/commercials/' + $scope.commercial.id_comercial + '/avatar'
			data:
				file: file
		).then ((resp) ->
			$scope.commercial.url_imagen_perfil = resp.data.url_imagen_perfil
			$scope.avatarUpBarStyle.visibility = 'hidden'
			Toast.show 'Imagen de perfil subida con éxito'
			return
		), ((resp) ->
			return
		), (evt) ->
			$scope.avatarUpBarStyle.visibility = 'visible'
			$scope.avatarUpProgress = parseInt(100.0 * evt.loaded / evt.total)
			return
		return

	$scope.deleteAvatar = () ->
		route = '/api/v1/commercials/' + $scope.commercial.id_comercial + '/avatar'
		$http.delete(route).then (response) ->
			$scope.commercial.url_imagen_perfil = response.data.url_imagen_perfil
			Toast.show 'Imagen de perfil eliminada con éxito'
			return
		return

	$scope.deleteFile = (file, index, event) ->
		confirm = $mdDialog.confirm()
			.title('Confirmación')
			.textContent('¿Está seguro/a de querer eliminar este archivo?')
			.ariaLabel('Delete file')
			.targetEvent(event)
			.ok('Eliminar')
			.cancel('Cancelar')

		$mdDialog.show(confirm).then (->
			$http.delete('/api/v1/candidates/files/' + file.id).then (response) ->
				if response.data.status
					$scope.commercial.files.splice(index, 1)
				return
			return
		), ->
			console.log 'not deleting file...'
			return
		return

	$scope.campaignsSet = () ->
		$mdDialog.show(
			controller: ($scope, $mdDialog, Commercials, CommercialsCampaigns, commercial, Toast) ->
				$scope.campaigns = CommercialsCampaigns.query()
				$scope.commercial = commercial
				$scope.commercialCampaigns = []

				if $scope.commercial.campaigns.length
					$scope.commercialCampaigns = []

					for c in $scope.commercial.campaigns
						$scope.commercialCampaigns.push c.id

				$scope.cancel = () ->
					$mdDialog.cancel()
					return

				$scope.submit = () ->
					if not $scope.commercialCampaigns.length
						Toast.show 'El comercial debe tener acceso a por lo menos 1 campaña'
						return

					Commercials.campaigns {id_comercial: $scope.commercial.id_comercial, campaigns: $scope.commercialCampaigns}, (result) ->
						Toast.show 'Acceso a campañas modificado con éxito'
						$mdDialog.hide()
						return
					return
				return
			templateUrl: ViewsUrl + 'commercials/campaigns'
			parent: angular.element(document.body)
			targetEvent: event
			locals:
				commercial: $scope.commercial
			clickOutsideToClose: true
			fullscreen: false
		).then (() ->
			$state.reload()
			return
		), () ->
			return

		return


	$scope.alterStructures = (id) ->

		if id == null
			action = 'save'
		else
			action = 'update'

		$mdDialog.show(
			controller: ($scope, $mdDialog, commercial, Toast, Commercials, CommercialsManagements, action, id, CommercialsStructures) ->
				$scope.commercial = commercial
				$scope.enableCommercials = true
				$scope.action = action
				$scope.mgr = {}
				##maxid se eliminará cuando se cierre el erp
				if (!$scope.commercial.structures.length)
					$scope.maxid = 0

				else
					$scope.maxid = _.maxBy($scope.commercial.structures,'id_estructura').id_estructura
					if action == 'save'
						Commercials.managers (result) ->
							$scope.managers = result
							##delete managers from scope where already in structures or invalid
							angular.forEach $scope.commercial.structures, ($structure) ->
								angular.forEach $scope.managers, (($manager)->
									if $structure.id_gerencia_estructura =='003'
										$scope.enableCommercials = false
									if $manager.id_comercial == $structure.id_comercial_estructura
										deleteIndex = _.findIndex($scope.managers, {'id_comercial' : $structure.id_comercial_estructura}   )
										$scope.managers[deleteIndex].enabled = false
									else if	$manager.id_comercial == $scope.commercial.id_comercial
										deleteIndex = _.findIndex($scope.managers, {'id_comercial' : $scope.commercial.id_comercial}   )
										$scope.managers[deleteIndex].enabled = false
									return
								),$structure
								return

						CommercialsManagements.leaders {id_gerencia: $scope.commercial.id_gerencia},(result) ->
							$scope.leaders = result
							##delete managers from scope where already in structures or invalid
							angular.forEach $scope.commercial.structures, ($structure) ->
								angular.forEach $scope.leaders, (($leader)->
									if $structure.id_gerencia_estructura =='003'
										$scope.enableCommercials = false
									if $leader.id_comercial == $structure.id_comercial_estructura
										deleteIndex = _.findIndex($scope.leaders, {'id_comercial' : $structure.id_comercial_estructura}   )
										$scope.leaders[deleteIndex].enabled = false
									else if	$leader.id_comercial == $scope.commercial.id_comercial
										deleteIndex = _.findIndex($scope.leaders, {'id_comercial' : $scope.commercial.id_comercial}   )
										$scope.leaders[deleteIndex].enabled = false
									return
								),$structure
								return

							return

					else
						Commercials.managers (result) ->
							$scope.managers = result
							angular.forEach $scope.commercial.structures, ($structure) ->
								if $structure.id_estructura == parseInt(id)
									$scope.structure = $structure
								return
							angular.forEach $scope.managers, ($manager,$index)->
								if $manager.id_comercial != $scope.structure.id_comercial_estructura
									$scope.managers.splice($index,1)
								return

						CommercialsManagements.leaders {id_gerencia: $scope.commercial.id_gerencia},(result) ->
							$scope.leaders = result
							##delete managers from scope where already in structures or invalid
							angular.forEach $scope.commercial.structures, ($structure) ->
								if $structure.id_estructura == parseInt(id)
									$scope.structure = $structure
								return
							angular.forEach $scope.leaders, ($leader,$index)->
								if $leader.id_comercial != $scope.structure.id_comercial_estructura
									$scope.leaders.splice($index,1)
								return

				$scope.cancel = () ->
					$mdDialog.cancel()
					return

				$scope.submit = () ->
					if action =='save'
						$scope.structure.id_comercial = $scope.commercial.id_comercial
						$scope.structure.id_gerencia = $scope.commercial.id_gerencia
						$scope.structure.id_empresa = $scope.commercial.id_empresa
						$scope.structure.suggestedId = $scope.maxid+1
					CommercialsStructures[action] $scope.structure, (structure) ->
						if structure.id_estructura
							Toast.show 'Estructura almacenada con éxito'
							$mdDialog.hide()
						else if structure.updated
							Toast.show 'Estructura editada con éxito'
							$mdDialog.hide()
						else
							Toast.show 'Error en acción'


						return
					return
				return
			templateUrl: ViewsUrl + '_components/commercial-create-structure'
			parent: angular.element(document.body)
			targetEvent: event
			locals:
				commercial: $scope.commercial
				action: action
				id: id
			clickOutsideToClose: true
			fullscreen: false
		).then (() ->
			$state.reload()
			return
		), () ->
			return

		return



	$scope.removeStructure = (structure_id) ->

		angular.forEach $scope.commercial.structures, ($structure)->
			if $structure.id_estructura == parseInt(structure_id)
				$scope.structure = $structure
			return


		$mdDialog.show(
			controller: ($scope, $mdDialog, commercial, Toast, structure, CommercialsStructures) ->
				$scope.commercial = commercial
				$scope.structure = structure


				$scope.cancel = () ->
					$mdDialog.cancel()
					return

				$scope.submit = () ->

					CommercialsStructures.delete angular.merge({},id_comercial:$scope.structure.id_comercial,id_estructura:$scope.structure.id_estructura), (result) ->
						Toast.show 'Eliminada estructura con éxito'
						$mdDialog.hide()
						return
					return
				return
			templateUrl: ViewsUrl + '_components/commercial-delete-structure'
			parent: angular.element(document.body)
			targetEvent: event
			locals:
				commercial: $scope.commercial
				structure : $scope.structure
			clickOutsideToClose: true
			fullscreen: false
		).then (() ->
			$state.reload()
			return
		), () ->
			return

		return


	render = (data) ->
		if $rootScope.user.level < 3
			return ' <a style="color:lightcoral" ng-click="removeStructure(\'' + data.id_estructura + '\');"> ' + 'Eliminar' + '</a>'
		else
			return ''

	renderUpdate =  (data) ->
		if $rootScope.user.level < 3
			return ' <a style="color:lightblue" ng-click="alterStructures(\'' + data.id_estructura + '\');"> ' + 'Editar' + '</a>'
		else
			return ''





	$scope.table =
# Instance
		instance: null
	###	showName : showName###
		instanceCallback: (instance) ->
			$scope.table.instance = instance
			return


		dtOptions: DTOptionsBuilder.fromFnPromise(Promise.resolve($scope.commercial.structures) ) \
			.withPaginationType('full_numbers') \
			.withOption('aaData',$scope.commercial.structures)\
			.withOption('createdRow', (row) ->
			$compile(angular.element(row).contents()) $scope
			return)\
			.withOption('rowCallback', $scope.rowCallbackSale) \
			.withOption('fnDrawCallback', $scope.fnDrawCallback) \
			.withOption('pageLength', 50) \
			.withOption('sale', [[ 0, "desc" ]]) \
			.withDOM('t')
		dtColumns: [
			DTColumnBuilder.newColumn('#').withTitle('#').renderWith (data, type, full) ->
				full.id_estructura
			DTColumnBuilder.newColumn('id_gerencia_estructura').withTitle('Tipo de estructura comercial').renderWith (data, type, full) ->
				if full.id_gerencia_estructura == '001' then 'Gerencia' else if full.id_gerencia_estructura=='002' then 'Estructura' else 'Comercial'
			DTColumnBuilder.newColumn('id_comercial_estructura').withTitle('Gerente / Comercial').renderWith (data, type, full) ->
				full.commercial_name
			DTColumnBuilder.newColumn('porcentaje_cobro').withTitle('% cobro estructura').renderWith (data, type, full) ->
				if full.porcentaje_cobro then full.porcentaje_cobro.toFixed(2) else 0
			DTColumnBuilder.newColumn('fecha_desde').withTitle('Fecha inicio').renderWith (data, type, full) ->
				moment(full.fecha_desde).format('D-MMM-YYYY')
			DTColumnBuilder.newColumn('fecha_hasta').withTitle('Fecha fin').renderWith (data, type, full) ->
				if full.fecha_hasta then moment(full.fecha_hasta).format('D-MMM-YYYY') else 'Permanente'
			DTColumnBuilder.newColumn('editar').withTitle('').renderWith (data,type,full)->
				renderUpdate(full)
			DTColumnBuilder.newColumn('borrar').withTitle('').renderWith (data,type,full)->
				render(full)


		]

	$scope.submit = () ->
		Commercials.update $scope.commercial, (result) ->
			$scope.commercial = result
			Toast.show 'Comercial editado con éxito'
			return
		return

	$scope.enableCommercial = () ->
		$scope.commercial.fecha_baja = null
		$scope.commercial.id_estado = 0
		$scope.submit()
		return

	$scope.disableCommercial = () ->
		$scope.commercial.fecha_baja = moment().format('YYYY-MM-DD HH:mm:ss')
		$scope.commercial.id_estado = 1
		$scope.submit()
		return

	return
