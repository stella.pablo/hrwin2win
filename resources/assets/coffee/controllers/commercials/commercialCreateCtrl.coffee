app.controller 'commercialCreateCtrl', (
	$scope,
	$rootScope,
	$state,
	$stateParams,
	$timeout,
	$mdDialog,
	campaigns,
	Candidates,
	Commercials,
	CommercialsCampaigns,
	CommercialsManagements,
	CommercialsStrategies,
	managements,
	Toast,
	ContractsTypes,
	ViewsUrl,
) ->

	$scope.campaigns = campaigns
	$scope.candidate = {}
	$scope.commercial = {}
	$scope.commercialContract = {}
	$scope.commercialHire = false
	$scope.managements = managements
	$scope.moment = moment
	$scope.strategies = []
	$scope.managementStructures = []
	$scope.typesContracts = ContractsTypes.query()
	$scope.commercial = {}
	$scope.commercial.company_contracts = []
	$scope.serverManagements = CommercialsManagements.query()



	$scope.loadStrategies = () ->
		filter = {}

		if $scope.commercial.id_gerencia
			filter.management_id = $scope.commercial.id_gerencia

		CommercialsStrategies.query filter, (result) ->
			$scope.strategies = result
			return
		$scope.alterManagement($scope.commercial.id_gerencia)
		return


	if $stateParams.id_comercial
		Commercials.get { id_comercial: $stateParams.id_comercial }, (result) ->
			$scope.commercial = result
			$scope.loadStrategies()
			managementIndex = _.findIndex($scope.serverManagements, {id_gerencia: $scope.commercial.id_gerencia})
			$scope.managementStructures = $scope.serverManagements[managementIndex].structures
			return


	else
		if $scope.campaigns.length is 1
			$scope.commercial.campaigns = []
			$scope.commercial.campaigns.push $scope.campaigns[0]

		if $scope.typesContracts.length is 1
			$scope.commercial.id_tipo_contrato_comercial = $scope.typesContracts[0].id_tipo_contrato_comercial
###			$scope.loadContractsSubtypes()###


	if $stateParams.candidate_id
		Candidates.get { id: $stateParams.candidate_id }, (result) ->
			if not result.external_id
				$scope.candidate = result
				$scope.commercial.candidate_id = $scope.candidate.id
				$scope.commercial.nombre = $scope.candidate.name
				$scope.commercial.apellido1 = $scope.candidate.surname
				$scope.commercial.apellido2 = $scope.candidate.lastname
				$scope.commercial.email = $scope.candidate.email
				$scope.commercial.telefono_movil = $scope.candidate.phone

			return

	$scope.alterManagement = ($management_id) ->

		managementIndex = _.findIndex($scope.serverManagements, {id_gerencia: $management_id})
		$scope.managementStructures = $scope.serverManagements[managementIndex].structures



		return
	$scope.cancel = () ->
		window.history.back()
		return

	$scope.checkIsValid = (event) ->
		if $scope.commercial.id_comercial
			##update
			Commercials.checkIsValid ({ id_comercial : $scope.commercial.id_comercial, tipo :'update', tele_movil: $scope.commercial.telefono_movil}) ,(result) ->
				if result.valid
					$scope.submit(event)
				else
					$scope.repeatedEmail(event)
				return
		else
			##save
			Commercials.checkIsValid ({ id_comercial : 'false', tipo :'save', tele_movil: $scope.commercial.telefono_movil}) ,(result) ->
				if result.valid
					$scope.submit(event)
				else
					$scope.repeatedEmail(event)
				return
		return


	$scope.save = () ->
		action = if $scope.commercial.id_comercial then 'update' else 'save'
		if $scope.commercial.company_contracts.length < 1
			Toast.show 'Debe indicar al menos un contrato para el comercial'
			##controlo que haya contrato
			return

		Commercials[action] $scope.commercial, (commercial) ->
			if commercial.id_comercial
				Toast.show 'Comercial guardado con éxito'
				if $scope.candidate.id
					$scope.candidate.hired = true
					$scope.candidate.hired_at = moment().format('YYYY-MM-DD HH:mm:ss')
					$scope.candidate.hired_user_id = $rootScope.user.id
					$scope.candidate.external_id = commercial.id_comercial

					Candidates.update $scope.candidate, (candidate) ->
						Toast.show 'Enlazado comercial con ficha de candidato'



				$state.go 'commercials.show', { id_comercial: commercial.id_comercial }, { reload: true }
			else
				Toast.show commercial.error

			return
		return

	$scope.submit = (event) ->
		confirm = $mdDialog
			.prompt()
			.title('Confirmación')
			.textContent('Por favor, confirme esta operación con su Código PIN personal.')
			.placeholder('Código PIN')
			.ariaLabel('Código PIN')
			.targetEvent(event)
			.required(true)
			.ok('Confirmar')
			.cancel('Cancelar')

		$mdDialog.show(confirm).then ((result) ->
			if result is $rootScope.user.pin
				$scope.save()
			else
				Toast.show 'Error. Código PIN incorrecto'
				return
			return
		), ->
			return


	$scope.alterContracts = (contract_id, index) ->
		if contract_id == null
			action = 'save'
		else
			action = 'update'

		$mdDialog.show(
			controller: ($scope, $mdDialog, commercial, Toast, ContractsTypes, action, types, contract_id, index, originalScope,CommercialsCompanyContracts) ->
				$scope.switchStretches = () ->

					angular.forEach $scope.contractTypes, (type) ->
						if type.id_tipo_contrato_comercial == $scope.contract.id_tipo_contrato_comercial
							$scope.stretches = type.stretches
						return
					return
				$scope.commercial = commercial
				$scope.action = action
				$scope.contractTypes = types
				$scope.strecthes = {}
				if contract_id
					angular.forEach $scope.commercial.company_contracts, (contract) ->
						if contract.id_comercial_contrato == contract_id
							$scope.contract = contract
							$scope.switchStretches()
						return
				else
					if index  != null
						$scope.contract = $scope.commercial.company_contracts[index]
						$scope.switchStretches()
					else
						$scope.contract  = {}




				$scope.cancel = () ->
					$mdDialog.cancel()
					return
				$scope.submit = () ->
					if $scope.commercial.id_comercial
						$scope.contract.id_comercial = $scope.commercial.id_comercial
						$scope.contract.id_empresa = $scope.commercial.id_empresa
						CommercialsCompanyContracts[action] $scope.contract, (contract) ->
							console.dir(contract)
							if contract.id_comercial_contrato
								Toast.show 'Contrato añadido con éxito'
								originalScope.commercial.company_contracts.push contract
								$mdDialog.hide()
							else if contract.updated
								Toast.show 'Contrato editado con éxito'
								$mdDialog.hide()
							else
								Toast.show 'Error en acción'


							return
					else
						if action =='save'
							angular.forEach $scope.contractTypes, (type) ->
								$scope.contract.contract_type = type
								return
							angular.forEach $scope.stretches, (stretch) ->
								$scope.contract.contract_stretch = stretch
								return
							originalScope.commercial.company_contracts.push $scope.contract
							$mdDialog.hide()
						else
							$mdDialog.hide()
					return
				return
			templateUrl: ViewsUrl + '_components/commercial-create-company_contract'
			parent: angular.element(document.body)
			targetEvent: event
			locals:
				commercial: $scope.commercial
				action: action
				types: $scope.typesContracts
				contract_id : contract_id
				index : index
				originalScope: $scope
				preservescope: true

			clickOutsideToClose: true
			fullscreen: false
		).then (() ->
			return
		), () ->
			return

		return



	$scope.removeContract = (contract_id,index) ->

		angular.forEach $scope.commercial.company_contracts, (company_contract)->
			if company_contract.id_comercial_contrato == contract_id
				$scope.company_contract = company_contract
			return

		if $scope.commercial.company_contracts.length < 2
			Toast.show('No puede eliminar el ultimo contrato del comercial. Añada otro antes')
			return

		$mdDialog.show(
			controller: ($scope, $mdDialog, commercial, Toast, companyContract, CommercialsCompanyContracts, originalScope, index) ->
				$scope.commercial = commercial
				$scope.contract = companyContract
				$scope.cancel = () ->
					$mdDialog.cancel()
					return

				$scope.submit = () ->

					if $scope.commercial.id_comercial

						CommercialsCompanyContracts.delete angular.merge({},id_comercial:companyContract.id_comercial,id_comercial_contrato:companyContract.id_comercial_contrato), (result) ->
							Toast.show 'Contrato eliminado con éxito'
							originalScope.commercial.company_contracts.splice(index,1)
							$mdDialog.hide()
							return
					else
						originalScope.commercial.company_contracts.splice(index,1)
						$mdDialog.hide()
					return
				return
			templateUrl: ViewsUrl + '_components/commercial-delete-company_contract'
			parent: angular.element(document.body)
			targetEvent: event
			locals:
				commercial: $scope.commercial
				companyContract : $scope.company_contract
				originalScope : $scope
				preservescope : true
				index : index

			clickOutsideToClose: true
			fullscreen: false
		).then (() ->
		), () ->
			return

		return

	$scope.repeatedEmail = (evt) ->
		console.log('repeatesd')
		$mdDialog.show(
			controller: ($scope, $mdDialog, commercial, Toast, evt) ->
				$scope.commercial = commercial
				$scope.cancel = () ->
					$mdDialog.cancel()
					return

				$scope.submit = () ->

					$mdDialog.hide()
					###originalScope.submit(evt)###
					return
				return
			templateUrl: ViewsUrl + '_components/commercial-repeated_email'
			parent: angular.element(document.body)
			targetEvent: event
			locals:
				commercial: $scope.commercial
				evt: evt
			clickOutsideToClose: true
			fullscreen: false
		).then (() ->
			$scope.submit(evt)
		), () ->
			Toast.show('Cancelado')
			return

		return


	$scope.table =
		contracts:
			dtInstance: null
			dtInstanceCallback: (dtInstance) ->
				$scope.table.contracts.dtInstance = dtInstance
				return
			dtOptions:
				dom: 't'
				drawCallback: (settings) ->
					$timeout (() ->
						angular.element(window).trigger('resize')
						return
					), 500
					return
				paginationType: 'full_numbers'
				scrollX: true





	return
