window.app.controller 'commercialsCtrl', (
	$scope,
	Commercials,
	CommercialsCampaigns,
	CommercialsManagements,
	CommercialsOrganizations,
	CommercialsStrategies,
	CdnUrl,
	localStorageService,
	$timeout
) ->

	$scope.avatarUrl = null
	$scope.commercials = []
	$scope.campaigns = CommercialsCampaigns.query()
	$scope.managements = []
	$scope.organizations = CommercialsOrganizations.query()
	$scope.strategies = []

	$scope.loadManagements = () ->
		CommercialsManagements.query { organization_id: $scope.filters.organization_id }, (result) ->
			$scope.managements = result
			return
		return

	$scope.loadStrategies = () ->
		CommercialsStrategies.query { management_id: $scope.filters.management_id }, (result) ->
			$scope.strategies = result
			return
		return

	$scope.filter = (removeFilters) ->
		if removeFilters
			localStorageService.remove 'commercials-filters'
		else
			localStorageService.set 'commercials-filters', $scope.filters

		Commercials.query $scope.filters, (result) ->
			$scope.commercials = result
			return
		return

	$scope.clearFilters = (removeFilters) ->
		if localStorageService.get 'commercials-filters' and not removeFilters
			$scope.filters = localStorageService.get 'commercials-filters'

			if $scope.filters.organization_id
				$scope.loadManagements()
				$scope.loadStrategies()

			if $scope.filters.management_id
				$scope.loadStrategies()

		else
			$scope.filters =
				date_from: null
				date_to: null
				campaign_id: null
				management_id: null
				organization_id: null
				strategy_id: null

		$scope.filter(removeFilters)
		$scope.loadManagements()
		$scope.loadStrategies()

		return

	$scope.clearFilters()

	$scope.table =
		dtInstance: null
		dtInstanceCallback: (dtInstance) ->
			$scope.table.dtInstance = dtInstance
			return
		dtOptions:
			order: [
				if $scope.user.company.database is 'mexico' then 7 else 5,
				'desc'
			]
			pageLength: 25
			paginationType: 'full_numbers'
			scrollX: true

	$scope.$watchCollection 'filters', (newCollection, oldCollection) ->
		if newCollection.organization_id and newCollection.organization_id isnt oldCollection.organization_id
			$scope.loadManagements()
			$scope.loadStrategies()

		if newCollection.management_id and newCollection.management_id isnt oldCollection.management_id
			$scope.loadStrategies()

		return

#	$('.table-with-avatars').on('mouseenter', 'tbody tr', () ->
#		row = $scope.table.dtInstance.DataTable.row(this)
#		imgBox = img = $(row.selector.rows.children).find('div.avatar-hover')[0]
#		console.dir imgBox
#		img = $(imgBox).find('img.mini-avatar')[0]
#		pos = img.getBoundingClientRect()
#
#		$(imgBox).css('top', (pos.top - 106) + 'px')
#
#		$scope.avatarUrl = img.src
#		return
#	)

	$scope.getPaymentProofByWeek = (commercial, week) ->
		paymentUrl = false

		if commercial.paymentproofs.length
			hasPayment = _.filter(commercial.paymentproofs, (item) ->
				return moment(item.fecha, 'YYYY-MM-DD').isBetween(
					moment().week(week).startOf('week').subtract(1, 'hour'),
					moment().week(week).endOf('week')
				)
			)

			if hasPayment.length
				paymentUrl = CdnUrl + hasPayment[0].fichero_url

		return paymentUrl

	return
