window.app.controller 'commercialsAbsencesListCtrl', ($scope, $timeout, CommercialsAbsences) ->

	$scope.commercialsAbsences = []
	$scope.moment = moment

	$scope.filters =
		month: moment().startOf('month').toDate()

	$scope.filter = () ->
		filters =
			date_from: moment($scope.filters.month).format('YYYY-MM-DD')
			date_to: moment($scope.filters.month).endOf('month').format('YYYY-MM-DD')

		CommercialsAbsences.query filters, (result) ->
			$scope.commercialsAbsences = result
			return
		return

	$scope.setDatepickerText = () ->
		text = moment($scope.filters.month).format('MMMM-YYYY')
		$timeout () ->
			angular.element('#month-select input').val(text)
			return
		return

	$scope.setDatepickerText()

	$scope.table =
		dtInstance: null
		dtInstanceCallback: (dtInstance) ->
			$scope.table.dtInstance = dtInstance
			return
		dtOptions:
			buttons: ['excelHtml5', 'pdfHtml5']
			dom: 'Bfrtip'
			drawCallback: (settings) ->
				$timeout (() ->
					angular.element(window).trigger('resize')
					angular.element('.dt-buttons a').removeClass('dt-button').addClass('md-button md-raised md-primary margin-0 margin-bottom-10 margin-right-10')
					return
				), 500
				return
			order: [0, 'asc']
			pageLength: 100
			paginationType: 'full_numbers'
			scrollX: true


	$scope.$watch 'selectedView', (newVal, oldVal) ->
		if newVal
			$scope.filter()
		return

	return
