window.app.controller 'commercialsReportsCtrl', ($scope, $rootScope, $timeout, Commercials, CommercialsReports, ReportsMotives, ReportsStatuses, Toast) ->

	$scope.commercials = Commercials.query()
	$scope.commercialsReports = []

	$scope.moment = moment

	$scope.reportsDailyStats = []
	$scope.reportsDays = []
	$scope.typesOfMotivesStats = []

	$scope.filtersDates = {}

	$scope.filter = () ->
		filtersDates =
			date_from: moment($scope.filtersDates.date_from).format('YYYY-MM-DD')
			date_to: moment($scope.filtersDates.date_to).format('YYYY-MM-DD')

		CommercialsReports.query filtersDates, (result) ->
			$scope.commercialsReports = result

			$scope.reports =
				motives: ReportsMotives.query (result) ->
					$scope.typesOfMotivesStats.splice(0, $scope.typesOfMotivesStats.length)
					for motive in result
						motiveCount = 0

						for report in $scope.commercialsReports
							if report.motive_id is motive.id
								motiveCount++

						data =
							name: motive.name
							y: Math.floor((motiveCount / result.length) * 100)

						$scope.typesOfMotivesStats.push data

					return
				statuses: ReportsStatuses.query()

			$scope.getReportsDailyChartData()

			return
		return

	$scope.clearFilters = () ->
		$scope.filtersDates =
			date_from: moment().startOf('month').toDate()
			date_to: moment().endOf('month').toDate()
		$scope.filters =
			show: true

		$scope.filter()

		return

	$scope.clearFilters()

	$scope.answerReport = (report) ->
		if report.status_id
			_report = angular.copy report
			_commercial = angular.copy report.commercial

			delete _report.commercial
			delete _report.motive
			delete _report.show
			delete _report.status
			CommercialsReports.update _report, (result) ->
				Toast.show 'Actualizado el estado del aviso de ' + _commercial.nombre_completo
				return
		return

	$scope.table =
		reports:
			dtInstance: null
			dtInstanceCallback: (dtInstance) ->
				$scope.table.reports.dtInstance = dtInstance
				return
			dtOptions:
				drawCallback: (settings) ->
					$timeout (() ->
						angular.element(window).trigger('resize')
						return
					), 500
					return
				pageLength: 100
				paginationType: 'full_numbers'
				scrollX: true

	$scope.chartsConfig =
		reportsDaily:
			chart:
				type: 'column'
			series: [],
			title:
				text: 'Avisos este mes'
			xAxis:
				categories: []
			yAxis:
				title:
					text: 'Avisos',
			plotOptions:
				line:
					marker:
						enabled: true
					enableMouseTracking: true
				series:
					label:
						connectorAllowed: false
		typesOfMotives:
			chart:
				plotBackgroundColor: null
				plotBorderWidth: null
				plotShadow: false
				type: 'pie'
			title:
				text: 'Motivos de aviso'
			tooltip:
				pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
			plotOptions: pie:
				allowPointSelect: true
				cursor: 'pointer'
				dataLabels:
					enabled: true
					format: '<b>{point.name}</b>: {point.percentage:.1f} %'
					style: color: Highcharts.theme and Highcharts.theme.contrastTextColor or 'black'
			series: [
				{
					name: 'Porcentaje'
					colorByPoint: true
					data: $scope.typesOfMotivesStats
				}
			]

	$scope.getReportsDailyChartData = () ->
		categories = []
		data = []
		for report in $scope.commercialsReports
			date = moment(report.created_at).format('YYYY-MM-DD')
			if categories.indexOf(date) is -1
				categories.push date

		for category, index in categories
			for report in $scope.commercialsReports
				date = moment(report.created_at).format('YYYY-MM-DD')

				if category is date
					if not data[index]?
						data[index] = 1
					else
						data[index]++

		$scope.chartsConfig.reportsDaily.xAxis.categories = categories

		$scope.chartsConfig.reportsDaily.series.push {
			name: 'Avisos'
			data: data
		}

		return

	return
