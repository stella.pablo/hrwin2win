window.app.controller 'commercialsAbsencesCtrl', ($scope, $mdDialog, $timeout, commercials, CommercialsAbsences, commercialsAbsences, ViewsUrl) ->

	$scope.commercials = commercials
	$scope.commercialsAbsences = commercialsAbsences

	$scope.dateToday = moment().toDate()

	$scope.dateObj = null
	$scope.date = null

	$scope.tomorrowBtnStyle =
		visibility: 'hidden'

	$scope.setDate = (date) ->
		if date
			$scope.dateObj = date
		else
			$scope.dateObj = $scope.dateToday

		$scope.date = moment($scope.dateObj).format('YYYY-MM-DD')
		return

	$scope.setDate()

	$scope.setDateYesterday = () ->
		$scope.setDate(moment($scope.dateObj).subtract(1, 'day').toDate())
		$scope.calcAbsences()
		return

	$scope.setDateTomorrow = () ->
		$scope.setDate(moment($scope.dateObj).add(1, 'day').toDate())
		$scope.calcAbsences()
		return

	$scope.isDateToday = () ->
		isDateToday = moment($scope.date).format('YYYY-MM-DD') is moment($scope.dateToday).format('YYYY-MM-DD')
		$scope.tomorrowBtnStyle.visibility = if isDateToday then 'hidden' else 'visible'
		isDateToday

	$scope.calcAbsences = () ->
		# Reset status
		for absence in $scope.commercialsAbsences
			for commercial in $scope.commercials
				commercial.absence = false
				commercial.absence_id = undefined
				commercial.absence_reason = undefined

		# Recalc status
		for absence in $scope.commercialsAbsences
			for commercial in $scope.commercials
				if absence.external_id is commercial.id_comercial and absence.date is $scope.date
					commercial.absence = true
					commercial.absence_id = absence.id
					commercial.absence_reason = absence.motive.name

		$scope.isDateToday()
		return

	$scope.calcAbsences()

	$scope.absence = (commercial) ->
		if commercial.absence
			$mdDialog.show(
				controller: ($scope, $mdDialog, AbsencesMotives, commercial, CandidatesFilesTypes, date, Toast, Upload) ->
					$scope.absence =
						external_id: commercial.id_comercial
						date: moment(date).format('YYYY-MM-DD')
						motive_id: null

					$scope.disableSave = false
					$scope.filesTypes = CandidatesFilesTypes.query()
					$scope.fileType = null

					$scope.motives = AbsencesMotives.query()

					$scope.commercial = commercial
					$scope.date = date
					$scope.moment = moment

					$scope.cancel = () ->
						$mdDialog.cancel()
						return

					$scope.submit = () ->
						CommercialsAbsences.save $scope.absence, (result) ->
							if $scope.file?
								$scope.disableSave = false

								Upload.upload(
									url: '/api/v1/candidates/files'
									data:
										external_id: $scope.commercial.id_comercial
										file: $scope.file
										type_id: $scope.fileType
								).then ((resp) ->
									return
								), ((resp) ->
									return
								), (evt) ->
									$scope.fileUpProgress = parseInt(100.0 * evt.loaded / evt.total)
									return

							Toast.show commercial.nombre_completo + ' marcado como ausente el ' + moment(date).format('DD-MMM-YYYY')
							$mdDialog.hide(result)
							return
						return

					return
				templateUrl: ViewsUrl + 'commercials-absences/store'
				parent: angular.element(document.body)
				targetEvent: event
				locals:
					commercial: commercial
					date: $scope.date
				clickOutsideToClose: true
				fullscreen: false
			).then ((absence) ->
				$scope.commercialsAbsences.push absence
				$scope.calcAbsences()
				return
			), () ->
				commercial.absence = false
				return
		else
			$mdDialog.show(
				controller: ($scope, $mdDialog, commercial, date, Toast) ->
					$scope.absence =
						id: commercial.absence_id
					$scope.commercial = commercial
					$scope.date = date
					$scope.moment = moment

					$scope.cancel = () ->
						$mdDialog.cancel()
						return

					$scope.submit = () ->
						CommercialsAbsences.remove $scope.absence, (result) ->
							Toast.show commercial.nombre_completo + ' marcado como presente el ' + moment(date).format('DD-MMM-YYYY')
							$mdDialog.hide($scope.absence.id)
							return
						return

					return
				templateUrl: ViewsUrl + 'commercials-absences/delete'
				parent: angular.element(document.body)
				targetEvent: event
				locals:
					commercial: commercial
					date: $scope.date
				clickOutsideToClose: true
				fullscreen: false
			).then ((absence_id) ->
				for absence, index in $scope.commercialsAbsences
					if absense.id is absence_id
						$scope.commercialsAbsences.splice(index, 1)
						break
				$scope.calcAbsences()
				return
			), () ->
				commercial.absence = true
				return
		return

	$scope.$watch 'dateObj', (newVal, oldVal) ->
		$scope.setDate(newVal)
		$scope.calcAbsences()
		return

	return
