window.app.controller 'dashboardCtrl', (
	$scope,
	$rootScope,
	$state,
	$mdDialog,
	$timeout,
	Candidates,
	Commercials,
	Productivity,
	Toast,
	ViewsUrl
) ->

	$scope.moment = moment
	$scope.user = $rootScope.user

	$scope.stats = null
	$scope.statsThisMonth =
		daily: {}
	$scope.statsThisMonthBadges =
		ok: 0
		p: 0
		ko: 0
		total: 0

	Productivity.stats (result) ->
		$scope.stats = result
		return

	statsThisMonthFilters =
		daily: true
		date_from: moment().startOf('month').hours(0).minutes(0).seconds(0).toDate()
		date_to: moment().toDate()

	Productivity.stats statsThisMonthFilters, (result) ->
		$scope.statsThisMonth = result

		if Object.keys(result.daily).length
			for day in Object.keys(result.daily)
				$scope.statsThisMonthBadges.ok += result.daily[day].ok
				$scope.statsThisMonthBadges.p += result.daily[day].p
				$scope.statsThisMonthBadges.ko += result.daily[day].ko

			$scope.statsThisMonthBadges.total = $scope.statsThisMonthBadges.ok + $scope.statsThisMonthBadges.p + $scope.statsThisMonthBadges.ko
		return

	$scope.candidates = Candidates.query {latest: true, take: 10}
	$scope.commercials = Commercials.query {latest: true, take: 10}

	$scope.table =
		candidates:
			dtInstance: null
			dtInstanceCallback: (dtInstance) ->
				$scope.table.candidates.dtInstance = dtInstance
				return
			dtOptions:
				dom: 't'
				drawCallback: (settings) ->
					$timeout (() ->
						angular.element(window).trigger('resize')
						return
					), 500
					return
				order: [2, 'desc']
				paginationType: 'full_numbers'
				scrollX: true
		commercials:
			dtInstance: null
			dtInstanceCallback: (dtInstance) ->
				$scope.table.commercials.dtInstance = dtInstance
				return
			dtOptions:
				dom: 't'
				drawCallback: (settings) ->
					$timeout (() ->
						angular.element(window).trigger('resize')
						return
					), 500
					return
				order: [3, 'desc']
				paginationType: 'full_numbers'
				scrollX: true
		reports:
			dtInstance: null
			dtInstanceCallback: (dtInstance) ->
				$scope.table.reports.dtInstance = dtInstance
				return
			dtOptions:
				dom: 't'
				drawCallback: (settings) ->
					$timeout (() ->
						angular.element(window).trigger('resize')
						return
					), 500
					return
				paginationType: 'full_numbers'
				scrollX: true

	return
