window.app.controller 'productivityCtrl', (
	$scope,
	$rootScope,
	$locale,
	$mdDialog,
	$state,
	$timeout,
	Commercials,
	CommercialsCampaigns,
	CommercialsManagements,
	CommercialsOrganizations,
	Productivity
) ->

	$scope.campaign = {}
	$scope.campaigns = CommercialsCampaigns.query()
	$scope.campaignsChart = []
	$scope.commercials = []
	$scope.management = {}
	$scope.managements = []
	$scope.managementsCampaigns = []
	$scope.organization = {}
	$scope.organizations = []
	$scope.stats = null
	$scope.typesOfObjective = []

	$scope.clearFilters = () ->
		$scope.filters =
			date_from: moment.utc().startOf('month').toDate()
			date_to: moment().toDate()
			organization_id: null
			management_id: null
			campaign_id: null

		return

	$scope.clearFilters()

	$scope.getMonthlyObjective = (id_gerencia) ->
		monthlyObjective = 0

		for management in window.UserData.managements
			if management.external_id is id_gerencia and management.company_id is window.UserData.company_id
				monthlyObjective = management.monthly_objective

		monthlyObjective

	$scope.loadOrganizations = () ->
		$scope.organizations = []

		filters = $scope.filters
		filters.stats = true

		CommercialsOrganizations.query filters, (result) ->
			$scope.organizations = result

			for organization in $scope.organizations
				avg = if (organization.stats.c_total and organization.stats.commercials) then (organization.stats.c_total / organization.stats.commercials) else 0
				$scope.chartsConfig.contractsAvgPerOrg.series.push {
					name: organization.organizacion
					data: [avg]
				}

				$scope.chartsConfig.contractsPerOrg.series.push {
					name: organization.organizacion
					data: [organization.stats.c_total]
				}
			return
		return

	$scope.loadManagements = () ->
		Productivity.managements $scope.filters, (result) ->
			$scope.managements = result
			$scope.managementsCampaigns = []

			if $scope.campaignsChart.length
				$scope.campaignsChart.splice(0, $scope.campaignsChart.length)

			if $scope.managements.length
				$scope.managementsCampaigns = Object.keys($scope.managements[0].campaigns)

				for campaign in $scope.managementsCampaigns
					campaignTotal = 0

					for management in $scope.managements
						contractsTotal = parseInt(management.campaigns[campaign].c_ok) + parseInt(management.campaigns[campaign].c_p) + parseInt(management.campaigns[campaign].c_ko)
						campaignTotal += contractsTotal

					$scope.campaignsChart.push {
						name: campaign
						y: campaignTotal
					}

				if $scope.chartsConfig.contractsPerMng.series.length
					$scope.chartsConfig.contractsPerMng.series.splice(0, $scope.chartsConfig.contractsPerMng.series.length)

				for management in $scope.managements
					totalContrats = 0

					for campaign in $scope.managementsCampaigns
						totalContrats += management.campaigns[campaign].c_total

					$scope.chartsConfig.contractsPerMng.series.push {
						name: management.gerencia
						data: [totalContrats]
					}
			return
		return

	$scope.loadCommercials = () ->
		$scope.commercials = []
		daysInMonth = moment($scope.filters.date_from).daysInMonth()
		daysDiff = moment($scope.filters.date_to).diff($scope.filters.date_from, 'days') + 1

		Productivity.stats $scope.filters, (result) ->
			$scope.stats = result
			return

		Productivity.query $scope.filters, (result) ->
			$scope.commercials = result

			objectivesCount =
				red: 0
				orange: 0
				green: 0

			for commercial in $scope.commercials
				contractsObjective = $scope.getMonthlyObjective(commercial.id_gerencia)
				contractsObjectivePerDay = contractsObjective / daysInMonth

				commercial.numero_contratos_total = parseInt(commercial.numero_contratos_ok) + parseInt(commercial.numero_contratos_p) + parseInt(commercial.numero_contratos_ko)
				commercial.contratos_por_dia = commercial.numero_contratos_total / daysDiff
				commercial.contratos_adiadehoy_debe = contractsObjectivePerDay * daysDiff
				commercial.contratos_adiadehoy_tiene = commercial.numero_contratos_total

				if commercial.contratos_adiadehoy_debe
					commercial.contratos_adiadehoy_porcentaje = Math.round((commercial.contratos_adiadehoy_tiene / commercial.contratos_adiadehoy_debe) * 100)
				else
					commercial.contratos_adiadehoy_porcentaje = 0

				isStartDateUntouched = moment($scope.filters.date_from).isSame(moment.utc().startOf('month'), 'date')
				isEndDateInThisMonth = moment($scope.filters.date_to).isSame(moment(), 'month')
				if isStartDateUntouched and isEndDateInThisMonth
					commercial.contratos_prevision = Math.round(contractsObjective * (commercial.contratos_adiadehoy_porcentaje / 100))
					commercial.contratos_prevision_porcentaje = Math.round((commercial.contratos_prevision / contractsObjective) * 100)
				else
					commercial.contratos_prevision = '---'
					commercial.contratos_prevision_porcentaje = '---'

				if commercial.contratos_adiadehoy_porcentaje < 50
					objectivesCount.red++
				else if commercial.contratos_adiadehoy_porcentaje >= 50 and commercial.contratos_adiadehoy_porcentaje < 80
					objectivesCount.orange++
				else if commercial.contratos_adiadehoy_porcentaje >= 80
					objectivesCount.green++

			$scope.typesOfObjective.splice(0, $scope.typesOfObjective.length)

			$scope.typesOfObjective.push {
				name: 'No llegan a objetivo'
				y: Math.floor((objectivesCount.red / $scope.commercials.length) * 100)
			}

			$scope.typesOfObjective.push {
				name: 'Probable'
				y: Math.floor((objectivesCount.orange / $scope.commercials.length) * 100)
			}

			$scope.typesOfObjective.push {
				name: 'Sí llegan a objetivo'
				y: Math.floor((objectivesCount.green / $scope.commercials.length) * 100)
			}

			$timeout (() ->
				window.dispatchEvent(new Event('resize'))
				return
			), 100

			return
		return

	$scope.loadCampaigns = () ->
		return

	# Check if we're embedding this controller inside managements.show
	if $scope.$parent.management
		$timeout (() ->
			$scope.management = $scope.$parent.management
			$scope.filters.management_id = $scope.management.id_gerencia

			if $scope.user.company.database is 'mexico'
				$scope.filters.organization_id = $scope.management.organization.id

			$scope.loadCommercials()
		), 1000

	$scope.$watchCollection 'filters', (newCollection, oldCollection) ->
		if $scope.user.company.database is 'mexico'
			if newCollection.organization_id and newCollection.organization_id isnt oldCollection.organization_id
				if newCollection.organization_id
					$scope.organization = _.find($scope.organizations, {id: newCollection.organization_id})
				else
					$scope.organization = {}

				$scope.loadManagements()
			else if not newCollection.organization_id
				$scope.loadOrganizations()
				return

		if newCollection.management_id and newCollection.management_id isnt oldCollection.management_id
			if newCollection.management_id
				$scope.management = _.find($scope.managements, {id_gerencia: newCollection.management_id})
			else
				$scope.management = {}

			$scope.loadCampaigns()
			$scope.loadCommercials()
		else if not newCollection.management_id
			$scope.loadManagements()
			return

		$scope.loadCommercials()

		return


	$scope.chartsConfig =
		campaigns:
			chart:
				plotBackgroundColor: null
				plotBorderWidth: null
				plotShadow: false
				type: 'pie'
			title:
				text: 'Producción por campaña'
			tooltip:
				pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
			plotOptions:
				pie:
					allowPointSelect: true
					cursor: 'pointer'
					dataLabels:
						enabled: true
						format: '<b>{point.name}</b>: {point.percentage:.1f} %'
						style:
							color: Highcharts.theme and Highcharts.theme.contrastTextColor or 'black'
			series: [
				{
					name: 'Porcentaje'
					colorByPoint: true
					data: $scope.campaignsChart
				}
			]
		contractsAvgPerOrg:
			chart:
				type: 'column'
			title:
				text: 'Media de contratos por comerciales activos'
			xAxis:
				categories: ['Organizaciones']
				title:
					text: null
			yAxis:
				title:
					text: 'Contratos'
			tooltip:
				valueSuffix: ' contratos de media'
			plotOptions:
				bar:
					dataLabels:
						enabled: true
			series: []
		contractsPerOrg:
			chart:
				type: 'column'
			title:
				text: 'Contratos por organización'
			xAxis:
				categories: ['Organizaciones']
				title:
					text: null
			yAxis:
				title:
					text: 'Contratos'
			tooltip:
				valueSuffix: ' contratos'
			plotOptions:
				bar:
					dataLabels:
						enabled: true
			series: []
		contractsPerMng:
			chart:
				type: 'column'
			title:
				text: 'Contratos por gerencia'
			xAxis:
				categories: ['Gerencias']
				title:
					text: null
			yAxis:
				title:
					text: 'Contratos'
			tooltip:
				valueSuffix: ' contratos'
			plotOptions:
				bar:
					dataLabels:
						enabled: true
			series: []
		typesOfObjective:
			chart:
				plotBackgroundColor: null
				plotBorderWidth: null
				plotShadow: false
				type: 'pie'
			title:
				text: 'Objetivos'
			tooltip:
				pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
			plotOptions:
				pie:
					allowPointSelect: true
					cursor: 'pointer'
					dataLabels:
						enabled: true
						format: '<b>{point.name}</b>: {point.percentage:.1f} %'
						style:
							color: Highcharts.theme and Highcharts.theme.contrastTextColor or 'black'
			series: [
				{
					name: 'Porcentaje'
					colorByPoint: true
					data: $scope.typesOfObjective
				}
			]

	$scope.tableCommercials =
		dtInstance: null
		dtInstanceCallback: (dtInstance) ->
			$scope.tableCommercials.dtInstance = dtInstance
			return
		dtOptions:
			order: [5, 'desc']
			pageLength: 10
			paginationType: 'full_numbers'
			scrollX: true

	$scope.tableManagements =
		dtInstance: null
		dtInstanceCallback: (dtInstance) ->
			$scope.tableManagements.dtInstance = dtInstance
			return
		dtOptions:
			dom: 't'
			pageLength: 1000
			scrollX: true

	$scope.tableOrganizations =
		dtInstance: null
		dtInstanceCallback: (dtInstance) ->
			$scope.tableOrganizations.dtInstance = dtInstance
			return
		dtOptions:
			dom: 't'
#			'footerCallback': (row, data, start, end, display) ->
#				api = @api()
#				api.columns('.sum', page: 'current').every ->
#					sum = @data().reduce(((a, b) ->
#						x = parseFloat(a) or 0
#						y = parseFloat(b) or 0
#						x + y
#					), 0)
#					console.log sum
#					#alert(sum);
#					$(@footer()).html sum
#					return
#				return
			order: []
			pageLength: 1000
			scrollX: true


	$scope.sumBy = (arr, val) ->
		_.sumBy arr, (o) ->
			o.stats[val]

	$scope.sumByCom = (arr, val) ->
		_.sumBy arr, (o) ->
			o[val]

	return
