window.app.controller 'productivityLowCtrl', (
	$scope,
	Commercials,
	CommercialsCampaigns,
	CommercialsManagements,
	Productivity
) ->

	$scope.campaigns = CommercialsCampaigns.query()
	$scope.commercials = []
	$scope.days = [...Array(30).keys()]
	$scope.managements = CommercialsManagements.query()

	$scope.filter = () ->
		$scope.commercials = []
		$scope.filters.date_from = moment($scope.filters.date_to)
			.subtract($scope.filters.days, 'days')
			.hours(0)
			.minutes(0)
			.seconds(0)
			.toDate()

		Productivity.low $scope.filters, (result) ->
			$scope.commercials = result
			return
		return

	$scope.clearFilters = () ->
		$scope.filters =
			date_from: null
			date_to: moment().toDate()
			days: 3
			low: true

		$scope.filter()

		return

	$scope.clearFilters()

	$scope.table =
		dtInstance: null
		dtInstanceCallback: (dtInstance) ->
			$scope.table.dtInstance = dtInstance
			return
		dtOptions:
			order: [4, 'desc']
			pageLength: 10
			paginationType: 'full_numbers'
			scrollX: true

	return
