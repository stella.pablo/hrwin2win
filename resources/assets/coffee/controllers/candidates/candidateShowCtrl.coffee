window.app.controller 'candidateShowCtrl', (
	$scope,
	$rootScope,
	$http,
	$mdMenu,
	$state,
	$mdDialog,
	Candidates,
	candidate,
	CandidatesAppointments,
	CandidatesFilesTypes,
	CommercialsManagements,
	CandidatesStatuses,
	Commercials,
	Toast,
	Upload,
	ViewsUrl
) ->

	$scope.$mdMenu = $mdMenu

	$scope.candidate = candidate
	$scope.candidatesFilesTypes = CandidatesFilesTypes.query()
	$scope.candidatesStatuses = CandidatesStatuses.query()
	$scope.moment = moment

	$scope.managers = CommercialsManagements.query()

	$scope.getManager = () ->
		managerName = ''
		for manager in $scope.managers
			if $scope.candidate.manager_id is manager.id_gerencia
				managerName = manager.manager.nombre + ' ' + manager.manager.apellido1 + ' ' + manager.manager.apellido2
		managerName

	$scope.fileType = null
	$scope.fileUpBarStyle =
		visibility: 'hidden'
	$scope.fileUpProgress = 0

	$scope.doesNotAnswer = () ->
		if $scope.candidate.status_id is 1
			$scope.candidate.status_id = 2
		else if $scope.candidate.status_id is 2
			$scope.candidate.status_id = 3

		$scope.setStatus()
		return

	$scope.pause = () ->
		$scope.candidate.status_id = 4
		$scope.setStatus()
		return

	$scope.delete = () ->
		Candidates.delete { id: $scope.candidate.id }, (result) ->
			Toast.show 'El candidato ha sido retirado'
			$scope.candidate.deleted_at = result.deleted_at
			return
		return

	$scope.unDelete = () ->
		Candidates.update { id: $scope.candidate.id, deleted_at: null }, (result) ->
			Toast.show 'El candidato ha sido habilitado'
			$scope.candidate.deleted_at = null
			return
		return

	$scope.setComment = () ->
		Candidates.update { id: $scope.candidate.id, comment: $scope.candidate.comment }, (result) ->
			Toast.show 'El comentario ha sido modificado'
			return
		return

	$scope.setStatus = () ->
		Candidates.update { id: $scope.candidate.id, status_id: $scope.candidate.status_id }, (result) ->
			Toast.show 'El estado del candidato ha sido modificado'
			$scope.candidate.status = result.status
			return
		return

	$scope.appointments =
		store: (number, candidate, event) ->
			$mdDialog.show(
				controller: ($scope, $mdDialog, CandidatesAppointments, Candidates, candidate, number, Toast) ->
					$scope.appointment =
						candidate_id: candidate.id
						number: number
						status: 1
					$scope.candidate = candidate
					$scope.moment = moment
					$scope.today = moment().toDate()

					$scope.cancel = () ->
						$mdDialog.cancel()
						return

					$scope.submit = () ->
						appointment = angular.copy $scope.appointment
						time = null

						if appointment.time_only
							time = appointment.time_only.split(':')
						else
							Toast.show 'La hora es obligatoria'
							return

						if time.length isnt 2
							Toast.show 'El formato de la hora no es correcto, use HH:mm'
							return

						appointment.date = moment(appointment.date_only).format('YYYY-MM-DD') + ' ' + moment().hours(time[0]).minutes(time[1]).format('HH:mm:ss')
						delete appointment.date_only
						delete appointment.time_only
						delete appointment.hide

						CandidatesAppointments.save appointment, (result) ->
							Toast.show 'Cita añadida con éxito'
							$mdDialog.hide(result)
							return
						return

					return
				templateUrl: ViewsUrl + 'candidates/appointments-store'
				parent: angular.element(document.body)
				targetEvent: event
				locals:
					candidate: candidate
					number: number
				clickOutsideToClose: true
				fullscreen: false
			).then ((appointment) ->
				if number is 1
					candidate.appointmentfirst = appointment
				else if number is 2
					candidate.appointmentsecond = appointment

				data =
					id: $scope.candidate.id
					status_id: 1

				Candidates.update data, (result) ->
					$scope.candidate = result
					return

				return
			), () ->
				return
			return

		update: (appointment, candidate, event) ->
			$mdDialog.show(
				controller: ($scope, $mdDialog, appointment, CandidatesAppointments, Candidates, candidate, Toast) ->
					$scope.appointment = appointment
					$scope.candidate = candidate
					$scope.moment = moment
					$scope.today = moment().toDate()

					$scope.appointment.date_only = moment($scope.appointment.date).toDate()
					$scope.appointment.time_only = moment($scope.appointment.date).format('HH:mm')

					$scope.cancel = () ->
						$mdDialog.cancel()
						return

					$scope.submit = () ->
						appointment = angular.copy $scope.appointment

						appointment.date = moment(appointment.date_only).format('YYYY-MM-DD') + ' ' + appointment.time_only
						delete appointment.date_only
						delete appointment.time_only
						delete appointment.hide

						CandidatesAppointments.update appointment, (result) ->
							Toast.show 'Cita editada con éxito'
							$mdDialog.hide(result)
							return
						return

					return
				templateUrl: ViewsUrl + 'candidates/appointments-store'
				parent: angular.element(document.body)
				targetEvent: event
				locals:
					appointment: appointment
					candidate: candidate
				clickOutsideToClose: true
				fullscreen: false
			).then ((appointmentUpdated) ->
				if appointment.number is 1
					candidate.appointmentfirst = appointmentUpdated
				else if appointment.number is 2
					candidate.appointmentsecond = appointmentUpdated
				return
			), () ->
				return
			return

		updateFast: (appointment, candidate, data) ->
			angular.merge(appointment, data)
			appointment.hide = undefined

			CandidatesAppointments.update appointment, (result) ->
				if appointment.status
					Toast.show 'Guardado con éxito. Ha acudido a la cita'
				else
					Toast.show 'Guardado con éxito. NO ha acudido a la cita'

				# $state.reload()
				return
			return

	$scope.promote = () ->

		if $rootScope.user.level >= 3 and $scope.candidate.preapproved_user_id and $rootScope.company.id isnt 5
			Toast.show 'No tiene suficientes permisos'
			return
		if $rootScope.user.level is 2 and $scope.candidate.approved_user_id and $rootScope.company.id isnt 5
			Toast.show 'No tiene suficientes permisos'
			return

		$mdDialog.show(
			controller: (
				$scope,
				$state,
				$mdDialog,
				$rootScope,
				Candidates,
				candidate,
				Toast
			) ->

				$scope.candidate = angular.copy(candidate)
				$scope.pinCode = null

				$scope.getCandidateNextState = () ->
					state = ''
					if $scope.candidate.approved_user_id and $scope.candidate.approved
						state = 'Contratado'
					else if $scope.candidate.preapproved_user_id and $scope.candidate.preapproved
						state = 'Aprobado'
					else
						state = 'Preaprobado'

					state

				$scope.update = () ->
					Candidates.update $scope.candidate, (result) ->
						Toast.show 'Candidato promocionado con éxito'
						$mdDialog.hide(result)
						return
					return

				$scope.preapprove = () ->
					$scope.candidate.preapproved = true
					$scope.candidate.preapproved_at = moment().format('YYYY-MM-DD HH:mm:ss')
					$scope.candidate.preapproved_user_id = $rootScope.user.id

					$scope.update()
					return

				$scope.approve = () ->
					$scope.candidate.approved = true
					$scope.candidate.approved_at = moment().format('YYYY-MM-DD HH:mm:ss')
					$scope.candidate.approved_user_id = $rootScope.user.id

					$scope.update()
					return

				$scope.hire = () ->
					Candidates.checkHireAvailability( { id: $scope.candidate.id }, (result)->
						if result.available
							$state.go 'commercials.create', { candidate_id: $scope.candidate.id }
							$mdDialog.cancel()
						else
							Toast.show result.error
					)

					return

				$scope.cancel = () ->
					$mdDialog.cancel()
					return

				$scope.submit = () ->
					if $scope.candidate.approved_user_id and $scope.candidate.approved
						# We don't require pincode because we're redirecting to commercials.create
					else
						if $scope.pinCode isnt $rootScope.user.pin
							Toast.show 'Error. Código PIN incorrecto'
							return

					if $rootScope.user.level is 3
						$scope.preapprove()
					else if $rootScope.user.level is 2
						if $scope.candidate.preapproved_user_id and $scope.candidate.preapproved
							$scope.approve()
						else
							$scope.preapprove()
					else if $rootScope.user.level is 1
						if $scope.candidate.approved_user_id and $scope.candidate.approved
							$scope.hire()
						else if $scope.candidate.preapproved_user_id and $scope.candidate.preapproved
							$scope.approve()
						else
							$scope.preapprove()

					return

				return
			templateUrl: ViewsUrl + 'candidates/promote'
			parent: angular.element(document.body)
			targetEvent: event
			locals:
				candidate: $scope.candidate
			clickOutsideToClose: true
			fullscreen: false
		).then ((candidate) ->
			$scope.candidate = candidate
			return
		), () ->
			return
		return

	$scope.upload = (file) ->
		Upload.upload(
			url: '/api/v1/candidates/files'
			data:
				candidate_id: $scope.candidate.id
				file: file
				type_id: $scope.fileType
		).then ((resp) ->
			$scope.candidate.files.push resp.data
			$scope.fileUpBarStyle.visibility = 'hidden'
			return
		), ((resp) ->
			return
		), (evt) ->
			$scope.fileUpBarStyle.visibility = 'visible'
			$scope.fileUpProgress = parseInt(100.0 * evt.loaded / evt.total)
			return
		return

	$scope.deleteFile = (file, index, event) ->
		confirm = $mdDialog.confirm()
			.title('Confirmación')
			.textContent('¿Está seguro/a de querer eliminar este archivo?')
			.ariaLabel('Delete file')
			.targetEvent(event)
			.ok('Eliminar')
			.cancel('Cancelar')

		$mdDialog.show(confirm).then (->
			$http.delete('/api/v1/candidates/files/' + file.id).then (response) ->
				if response.data.status
					$scope.candidate.files.splice(index, 1)
				return
			return
		), ->
			console.log 'not deleting file...'
			return
		return

	return
