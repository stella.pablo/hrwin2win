window.app.controller 'candidatesCtrl', (
	$scope,
	$filter,
	$state,
	$timeout,
	$mdDialog,
	$mdMedia,
	$mdSidenav,
	$window,
	Candidates,
	CandidatesAppointments,
	CandidatesOrigins,
	CandidatesStatuses,
	DTColumnBuilder,
	DTOptionsBuilder,
	localStorageService,
	Toast,
	ViewsUrl,
	managements
) ->

	# $.fn.dataTable.moment('DD-MMM-YYYY HH:mm')
	# $.fn.dataTable.moment('DD-MMM-YYYY')
	$scope.managements = managements
	$scope.candidate = null
	$scope.candidates = []
	$scope.candidatesStatuses = CandidatesStatuses.query()
	# $scope.dates = []
	if localStorageService.get 'candidates-filters'
		$scope.filters = localStorageService.get 'candidates-filters'
	else
		$scope.filters =
			date_from: moment().subtract(1, 'month').hours(0).minutes(0).seconds(0).toDate()
			date_to: moment().toDate()

	$scope.moment = moment
	$scope.origins = CandidatesOrigins.query()

	# $scope.getDates = () ->
	# 	for candidate in $scope.candidates
	# 		date = moment(candidate.petitioned_at).format('YYYY-MM-DD')
	#
	# 		if $scope.dates.indexOf(date) is -1
	# 			$scope.dates.push date
	#
	# 	$scope.dates.sort()
	#
	# 	return
	#
	# $scope.getDates()

	$scope.loadData = () ->
		filters = angular.copy $scope.filters

		if filters.date_from
			filters.date_from = moment(filters.date_from).format('YYYY-MM-DD')
		if filters.date_to
			filters.date_to = moment(filters.date_to).format('YYYY-MM-DD')

		Candidates.query(filters).$promise

	$scope.filter = (removeFilters) ->
		if removeFilters
			localStorageService.remove 'candidates-filters'
		else
			localStorageService.set 'candidates-filters', $scope.filters

		$scope.table.instance.changeData($scope.loadData())
		return

	$scope.clearFilters = () ->
		$scope.filters =
			date_from: moment().subtract(1, 'month').hours(0).minutes(0).seconds(0).toDate()
			date_to: moment().toDate()

		$scope.filter(true)

		return

	$scope.appointments =
		store: (number, candidate, event) ->
			$mdDialog.show(
				controller: ($scope, $mdDialog, CandidatesAppointments, Candidates, candidate, number, Toast) ->
					$scope.appointment =
						candidate_id: candidate.id
						number: number
						status: 1
					$scope.candidate = candidate
					$scope.moment = moment
					$scope.today = moment().toDate()

					$scope.cancel = () ->
						$mdDialog.cancel()
						return

					$scope.submit = () ->
						appointment = angular.copy $scope.appointment
						time = null
						if appointment.time_only
							time = appointment.time_only.split(':')

						if time.length isnt 2
							Toast.show 'El formato de la hora no es correcto'
							return

						appointment.date = moment(appointment.date_only).format('YYYY-MM-DD') + ' ' + moment().hours(time[0]).minutes(time[1]).format('HH:mm:ss')
						delete appointment.date_only
						delete appointment.time_only
						delete appointment.hide

						CandidatesAppointments.save appointment, (result) ->
							Toast.show 'Cita añadida con éxito'
							$mdDialog.hide(result)
							return
						return

					return
				templateUrl: ViewsUrl + 'candidates/appointments-store'
				parent: angular.element(document.body)
				targetEvent: event
				locals:
					candidate: candidate
					number: number
				clickOutsideToClose: true
				fullscreen: false
			).then ((appointment) ->
				if number is 1
					candidate.appointmentfirst = appointment
				else if number is 2
					candidate.appointmentsecond = appointment
				return
			), () ->
				return
			return

		update: (appointment, candidate, event) ->
			$mdDialog.show(
				controller: ($scope, $mdDialog, appointment, CandidatesAppointments, Candidates, candidate, Toast) ->
					$scope.appointment = appointment
					$scope.candidate = candidate
					$scope.moment = moment
					$scope.today = moment().toDate()

					$scope.appointment.date_only = moment($scope.appointment.date).toDate()
					$scope.appointment.time_only = moment($scope.appointment.date).format('HH:mm')

					$scope.cancel = () ->
						$mdDialog.cancel()
						return

					$scope.submit = () ->
						appointment = angular.copy $scope.appointment

						appointment.date = moment(appointment.date_only).format('YYYY-MM-DD') + ' ' + appointment.time_only
						delete appointment.date_only
						delete appointment.time_only
						delete appointment.hide

						CandidatesAppointments.update appointment, (result) ->
							Toast.show 'Cita editada con éxito'
							$mdDialog.hide(result)
							return
						return

					return
				templateUrl: ViewsUrl + 'candidates/appointments-store'
				parent: angular.element(document.body)
				targetEvent: event
				locals:
					appointment: appointment
					candidate: candidate
				clickOutsideToClose: true
				fullscreen: false
			).then ((appointmentUpdated) ->
				if appointment.number is 1
					candidate.appointmentfirst = appointmentUpdated
				else if appointment.number is 2
					candidate.appointmentsecond = appointmentUpdated
				return
			), () ->
				return
			return

		updateFast: (appointment, candidate, data) ->
			angular.merge(appointment, data)
			appointment.hide = undefined

			CandidatesAppointments.update appointment, (result) ->
				if appointment.status
					Toast.show 'Guardado con éxito. Ha acudido a la cita'
				else
					Toast.show 'Guardado con éxito. NO ha acudido a la cita'

				# $state.reload()
				return
			return

	$scope.drawCallback = () ->
		interval = setInterval(( ->
			element = angular.element('.buttons-colvis')[0]

			if element and not angular.element(element).hasClass('md-button')
				angular.element('.buttons-colvis') \
					.removeClass('dt-button') \
					.addClass('md-button') \
					.prepend('<md-icon class="material-icons mdi mdi-table-row-plus-after mdi-rotate-270 margin-right-10" style="line-height: 1;"></md-icon>')
				clearInterval(interval)
		), 100)

	$scope.rowCallback = (nRow, aData, iDisplayIndex, iDisplayIndexFull) ->
		$('td', nRow).unbind 'click'
		$('td', nRow).bind 'click', (event) ->
			$scope.$apply () ->
				$scope.candidate = aData
				$mdSidenav('right').toggle()
				return
			return
		nRow

	$scope.closeFollowup = () ->
		$mdSidenav('right').toggle()
		return


	$scope.table =
		# Columns, we'll fill them later
		columns: [
			DTColumnBuilder.newColumn('id').withTitle('ID').notVisible()
			DTColumnBuilder.newColumn('petitioned_at').withTitle('Fecha Petición').renderWith (data, type, full) ->
				if ( type is 'display' or type is 'filter' ) then moment(data).format('DD-MMM-YYYY') else data
			DTColumnBuilder.newColumn('fullname').withTitle('Nombre completo')
			DTColumnBuilder.newColumn('email').withTitle('Email')
			DTColumnBuilder.newColumn('phone').withTitle('Teléfono')
			DTColumnBuilder.newColumn('origin').withTitle('Origen').renderWith (data, type, full) ->
				data.name
			DTColumnBuilder.newColumn('status').withTitle('Estado').renderWith (data, type, full) ->
				data.name
			DTColumnBuilder.newColumn('comment').withTitle('Comentario')
			DTColumnBuilder.newColumn('appointmentfirst').withTitle('1ª Entrevista').renderWith (data, type, full) ->
				value = ''

				if type is 'display' or type is 'filter'
					if data
						colorClass = ''
						formattedDate = moment(data.date).format('DD-MMM-YYYY HH:mm')

						if data.status is 2
							colorClass = 'mdc-text-green'
						else if data.status is 0
							colorClass = 'mdc-text-red'

						value = '<span class="' + colorClass + '">' + formattedDate + '</span>'
				else
					value = if data then data.date else '0000-00-00 00:00'

				value
			DTColumnBuilder.newColumn('appointmentsecond').withTitle('2ª Entrevista').renderWith (data, type, full) ->
				value = ''

				if type is 'display' or type is 'filter'
					if data
						colorClass = ''
						formattedDate = moment(data.date).format('DD-MMM-YYYY HH:mm')

						if data.status is 2
							colorClass = 'mdc-text-green'
						else if data.status is 0
							colorClass = 'mdc-text-red'

						value = '<span class="' + colorClass + '">' + formattedDate + '</span>'
				else
					value = if data then data.date else '0000-00-00 00:00'

				value
			DTColumnBuilder.newColumn('preapproved').withTitle('Preaprobado').notVisible().renderWith (data, type, full) ->
				if data then 'Sí' else 'No'
			DTColumnBuilder.newColumn('approved').withTitle('Aprobado').notVisible().renderWith (data, type, full) ->
				if data then 'Sí' else 'No'
			DTColumnBuilder.newColumn('hired').withTitle('Contratado').notVisible().renderWith (data, type, full) ->
				if data then 'Sí' else 'No'
		]
		# Instance
		instance: null
		instanceCallback: (instance) ->
			$scope.table.instance = instance
			return
		# Define the dataset table options
		options: DTOptionsBuilder.fromFnPromise($scope.loadData()) \
			.withPaginationType('numbers') \
			.withOption('order', [1, 'asc']) \
			.withOption('pageLength', 100) \
			.withOption('scrollX', true) \
			.withOption('drawCallback', $scope.drawCallback) \
			.withOption('rowCallback', $scope.rowCallback) \
			.withButtons([
				{ extend: 'colvis', 'text': 'Columnas' }
			]) \
			.withDOM('<"layout-row layout-xs-column layout-align-space-between-center"lfB>rtip')


	$scope.doesNotAnswer = () ->
		if $scope.candidate.status_id is 1
			$scope.candidate.status_id = 2
		else if $scope.candidate.status_id is 2
			$scope.candidate.status_id = 3

		$scope.setStatus()
		return

	$scope.setStatus = () ->
		Candidates.update { id: $scope.candidate.id, status_id: $scope.candidate.status_id }, (result) ->
			Toast.show 'El estado del candidato ha sido modificado'
			$scope.candidate.status = result.status
			return
		return

	$scope.pause = () ->
		$scope.candidate.status_id = 4
		$scope.setStatus()
		return

	return
