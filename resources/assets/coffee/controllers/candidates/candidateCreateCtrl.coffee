window.app.controller 'candidateCreateCtrl', ($scope,
	$state,
	$stateParams,
	$mdMedia,
	$mdDialog,
	Candidates,
	CandidatesOrigins,
	CommercialsManagements,
	EmailsTypes,
	Toast,
	ViewsUrl) ->

	$scope.candidate =
		petitioned_at: moment().toDate()
	$scope.emailsTypes = EmailsTypes.query()
	$scope.managements = CommercialsManagements.query()
	$scope.origins = CandidatesOrigins.query()

	if $stateParams.id
		$scope.candidate = Candidates.get({ id: $stateParams.id })

	$scope.cancel = () ->
		$state.go('candidates')
		return

	$scope.checkIsValid = (event) ->
		if $scope.candidate.id
			##update
			Candidates.checkIsValid ({ id : $scope.candidate.id, tipo :'update', tele_movil: $scope.candidate.phone}) ,(result) ->
				if result.valid
					$scope.submit(event)
				else
					$scope.repeatedEmail(event)
				return
		else
			##save
			Candidates.checkIsValid ({ id : 'false', tipo :'save', tele_movil: $scope.candidate.phone}) ,(result) ->
				if result.valid
					$scope.submit(event)
				else
					$scope.repeatedEmail(event)
				return
		return

	$scope.repeatedEmail = (evt) ->
		console.log('repe')
		console.log(evt)
		$mdDialog.show(
			controller: ($scope, $mdDialog, candidate, Toast, evt) ->
				console.log('show')
				$scope.candidate = candidate
				$scope.cancel = () ->
					$mdDialog.cancel()
					return

				$scope.submit = () ->

					$mdDialog.hide()
					###originalScope.submit(evt)###
					return
				return
			templateUrl: ViewsUrl + '_components/candidate-repeated_email'
			parent: angular.element(document.body)
			targetEvent: event
			locals:
				candidate: $scope.candidate
				evt: evt
			clickOutsideToClose: true
			fullscreen: false
		).then (() ->
			$scope.submit(evt)
		), () ->
			Toast.show('Cancelado')
			return

		return


	$scope.submit = () ->
		action = if $scope.candidate.id then 'update' else 'save'
		candidate = angular.copy($scope.candidate)
		candidate.petitioned_at = moment($scope.candidate.petitioned_at).format('YYYY-MM-DD')

		Candidates[action] candidate, (result) ->
			if result.id?
				Toast.show 'Candidato guardado con éxito'
				$state.go('candidates.show', {id: result.id}, {reload: true})
			return
		return

	return
