window.app.controller 'settingsCtrl', ($scope,
	$rootScope,
	$mdDialog,
	$timeout,
	$window,
	AbsencesMotives,
	CandidatesFilesTypes,
	Companies,
	FilesTypes,
	CommercialsManagements,
	Managements,
	ReportsMotives,
	ReportsStatuses,
	Toast,
	ContractsTypes,
	ViewsUrl,
	Ivas,
	Irpfs,
	FeesGroups,
	FeesProducts,
	FeesManagements,
	FeesGroupsFees,
	ManagementStructures,
	Products,
	Articles,
	Settlements) ->
	$scope.user = $rootScope.user

	$scope.contractsTypes = {}

	$scope.contractsTypes = ContractsTypes.query()
	$scope.ivas = Ivas.query()
	$scope.irpfs = Irpfs.query()
	$scope.products = Products.query()
	$scope.articles = Articles.query()

	$scope.feeGroups = FeesGroups.query()
	$scope.managements = CommercialsManagements.query()

	$scope.absencesMotives = AbsencesMotives.query {company_id: $scope.user.company_id}
	$scope.newAbsenceMotive = {}
	$scope.newManagementStructure = {}
	$scope.settlements = Settlements.query()

	$scope.fileTypes = CandidatesFilesTypes.query {company_id: $scope.user.company_id}
	$scope.newFileType = {}

	$scope.fileContractTypes = FilesTypes.query {company_id: $scope.user.company_id}
	$scope.newFileContractType = {}

	$scope.reportsMotives = ReportsMotives.query {company_id: $scope.user.company_id}

	$scope.reportsStatuses = ReportsStatuses.query {company_id: $scope.user.company_id}
	$scope.newReportStatus = {}

	# Absences
	$scope.createAbsenceMotive = () ->
		$scope.newAbsenceMotive.company_id = $scope.user.company_id
		AbsencesMotives.save $scope.newAbsenceMotive, (result) ->
			Toast.show 'Guardado con éxito'
			$scope.absencesMotives.push result
			$scope.newAbsenceMotive = {}
		return

	$scope.updateAbsenceMotive = (motive) ->
		AbsencesMotives.update motive, (result) ->
			Toast.show 'Guardado con éxito'
			return
		return

	$scope.deleteAbsenceMotive = (motive, index) ->
		AbsencesMotives.delete {id: motive.id}, (result) ->
			if result.status
				Toast.show 'Eliminado con éxito'
				$scope.absencesMotives.splice(index, 1)
			else
				Toast.show 'No se puede eliminar'
			return
		return

	# File types
	$scope.createFileType = () ->
		$scope.newFileType.company_id = $scope.user.company_id
		CandidatesFilesTypes.save $scope.newFileType, (result) ->
			Toast.show 'Guardado con éxito'
			$scope.fileTypes.push result
			$scope.newFileType = {}
		return

	$scope.updateFileType = (fileType) ->
		CandidatesFilesTypes.update fileType, (result) ->
			Toast.show 'Guardado con éxito'
			return
		return

	$scope.deleteFileType = (fileType, index) ->
		CandidatesFilesTypes.delete {id: fileType.id}, (result) ->
			if result.status
				Toast.show 'Eliminado con éxito'
				$scope.fileTypes.splice(index, 1)
			else
				Toast.show 'No se puede eliminar'
			return
		return

	# File types contracts
	$scope.createFileContractType = () ->
		$scope.newFileContractType.company_id = $scope.user.company_id
		FilesTypes.save $scope.newFileContractType, (result) ->
			Toast.show 'Guardado con éxito'
			$scope.fileContractTypes.push result
			$scope.newFileContractType = {}
		return

	$scope.updateFileContractType = (fileContractType) ->
		FilesTypes.update fileContractType, (result) ->
			Toast.show 'Guardado con éxito'
			return
		return

	$scope.deleteFileContractType = (fileContractType, index) ->
		FilesTypes.delete {id_tipo_fichero: fileContractType.id_tipo_fichero}, (result) ->
			if result.status
				Toast.show 'Eliminado con éxito'
				$scope.fileContractTypes.splice(index, 1)
			else
				Toast.show 'No se puede eliminar'
			return
		return

	# Statuses
	$scope.createReportStatus = () ->
		$scope.newReportStatus.company_id = $scope.user.company_id
		ReportsStatuses.save $scope.newReportStatus, (result) ->
			Toast.show 'Guardado con éxito'
			$scope.reportsStatuses.push result
			$scope.newReportStatus = {}
		return

	$scope.updateReportStatus = (status) ->
		ReportsStatuses.update status, (result) ->
			Toast.show 'Guardado con éxito'
			return
		return

	$scope.deleteReportStatus = (status, index) ->
		ReportsStatuses.delete {id: status.id}, (result) ->
			if result.status
				Toast.show 'Eliminado con éxito'
				$scope.reportsStatuses.splice(index, 1)
			else
				Toast.show 'No se puede eliminar'
			return
		return


	# ContractTypes
	$scope.alterContractsType = (contract_id, index) ->
		if contract_id == null
			action = 'save'
		else
			action = 'update'

		$mdDialog.show(
			controller: ($scope, $mdDialog, $q, Toast, ContractsTypes, ContractsTypesStretches, action, contract_id, originalScope, ivas, irpfs) ->
				$scope.promises = []
				$scope.action = action
				$scope.ivas = ivas
				$scope.irpfs = irpfs

				if contract_id

					ContractsTypes.get({id_tipo_contrato_comercial: contract_id}, (result)->
						$scope.contractType = result
					)
				else
					$scope.contractType = {}
					$scope.contractType.stretches = []

				$scope.removeStretch = (stretch, index) ->
					if stretch

						ContractsTypesStretches.delete {id_tramo: stretch}, (result)->
							if result.status
								Toast.show 'Eliminado con éxito'
								$scope.contractType.stretches.splice(index, 1)
							else
								Toast.show 'No se puede eliminar'
							return
					else
						$scope.contractType.stretches.splice(index, 1)
					return


				$scope.addStretch = () ->
					$scope.contractType.stretches.push {}
					return
				$scope.cancel = () ->
					$mdDialog.cancel()
					return

				$scope.submit = () ->
					if $scope.contractType.stretches.length < 1
						Toast.show 'Debe indicar al menos un tramo'
						return
					ContractsTypes[action] $scope.contractType, (contractType) ->
						if contractType.created
							Toast.show 'Tipo de contrato añadido con éxito'
							originalScope.contractsTypes.push contractType.contractType
							$mdDialog.hide()
						else if contractType.updated
							angular.forEach originalScope.contractsTypes, (originalContractType, $index) ->
								if originalContractType.id_tipo_contrato_comercial == contractType.contractType.id_tipo_contrato_comercial
									originalScope.contractsTypes.splice($index, 1)
									originalScope.contractsTypes.push contractType.contractType
								return

							Toast.show 'Tipo de contrato editado con éxito'
							$mdDialog.hide()
							return
						else
							Toast.show 'Error en acción'
							return


					return


				$scope.stretchTable =
					stretches:
						dtInstance: null
						dtInstanceCallback: (dtInstance) ->
							$scope.stretchTable.stretches.dtInstance = dtInstance
							return
						dtOptions:
							dom: 't'
							drawCallback: (settings) ->
								$timeout (() ->
									angular.element(window).trigger('resize')
									return
								), 500
								return
							paginationType: 'full_numbers'
							scrollX: true

				return
			templateUrl: ViewsUrl + '_components/settings-create-contract_type'
			parent: angular.element(document.body)
			targetEvent: event
			locals:
				action: action
				types: $scope.contractsTypes
				contract_id: contract_id
				index: index
				ivas: $scope.ivas
				irpfs: $scope.irpfs
				originalScope: $scope
				preservescope: true

			clickOutsideToClose: false
			fullscreen: true
		).then (() ->
			return
		), () ->
			return

		return



	$scope.deleteContractsType = (contract_id, name) ->
		$mdDialog.show(
			controller: ($scope, $mdDialog, Toast, ContractsTypes, contract_id, originalScope, name) ->
				$scope.name = name
				$scope.cancel = () ->
					$mdDialog.cancel()
					return

				$scope.submit = () ->
					ContractsTypes.delete {id_tipo_contrato_comercial: contract_id}, (response) ->
						if response.deleted
							angular.forEach originalScope.contractsTypes, (originalContractType, $index) ->
								if originalContractType.id_tipo_contrato_comercial == contract_id
									originalScope.contractsTypes.splice($index, 1)
								return
							Toast.show 'Tipo de contrato eliminado con éxito'
							$mdDialog.hide()
						else
							Toast.show 'Error al eliminar'
						return

					return


				return
			templateUrl: ViewsUrl + '_components/settings-delete-contract_type'
			parent: angular.element(document.body)
			targetEvent: event
			locals:
				contract_id: contract_id
				name: name
				originalScope: $scope
				preservescope: true
			clickOutsideToClose: true
			fullscreen: false
		).then (() ->
			return
		), () ->
			return


	#settlements


	$scope.settlementPdf = (id) ->

		$window.open('/api/v1/administration/settlements/pdfSettlement/' + id  , '_blank');

		return

	$scope.alterSettlements = (id, index) ->

		if id == null
			action = 'save'
		else
			action = 'update'

		$mdDialog.show(
			controller: ($scope, $mdDialog, $q, Toast, Commercials, ContractsTypes, Settlements, SettlementConcepts, ManagementStructures, action, id, originalScope, managements) ->
				$scope.promises = []
				$scope.managements = managements
				$scope.settlementControls = []

				$scope.action = action
				$scope.structures = []
				$scope.availableSales = []
				$scope.availableContracts = []


				$scope.contractsTypes = ContractsTypes.query()






				$scope.selectCommercialSettlement = (id) ->



					$scope.commercialSettlementIndex = _.findIndex($scope.settlement.commercial_settlements, {id: id})

					$scope.commercialConcepts = $scope.settlement.commercial_settlements[$scope.commercialSettlementIndex].concepts

					$scope.selectedCommercialSettlement = $scope.settlement.commercial_settlements[$scope.commercialSettlementIndex]

					if $scope.commercialSettlementIndex >= 0
						commercial = $scope.settlement.commercial_settlements[$scope.commercialSettlementIndex].commercial
					if commercial
						$scope.selectedCommercialName = commercial.nombre_completo


					$scope.resetSales()
					$scope.resetContracts()
					return


				$scope.resetStructures = ()->
					angular.forEach managements, (management)->

						angular.forEach management.structures, (structure)->
							find = _.findIndex($scope.structures, {id_gerencia_estructura: structure.id_gerencia_estructura})

							if find < 0
								$scope.structures.push(structure)
							return
						return
					return


				$scope.removeSettlementConcept = (id, index)->

					if id

						SettlementConcepts.delete {id: id}, (result)->
							if result.status
								Toast.show 'Eliminado con éxito'
								$scope.settlement.commercial_settlements[$scope.commercialSettlementIndex].concepts.splice(index, 1)

							else
								Toast.show 'No se puede eliminar'
							return
					if !id
						$scope.settlement.commercial_settlements[$scope.commercialSettlementIndex].concepts.splice(index, 1)
					return


###
				$scope.fixDate = (field) ->

					$scope.settlement[field] = new Date($scope.settlement[field]).toLocaleDateString()
					console.log(moment($scope.settlement[field]))
					return
###

				$scope.resetSales = () ->

					angular.forEach $scope.commercialConcepts, (concept)->
						if concept.sale
							find = _.findIndex($scope.availableSales, {id_venta: concept.id_venta})
							if find < 0
								$scope.availableSales.push(concept.sale)
						return
					return

				$scope.resetContracts = () ->
					angular.forEach $scope.commercialConcepts, (concept)->
						if concept.contract
							find = _.findIndex($scope.availableContracts, {id_contrato: concept.id_contrato})
							if find < 0
								$scope.availableContracts.push(concept.contract)
						return
					return

				if id
					Settlements.get({id: id}, (result)->
						$scope.settlement = result
###						if result.id_gerencia
							$scope.structures = result.management###

					)
					$scope.settlementControls.push {}


				else
					$scope.settlement = {}
					$scope.structures = []
					$scope.settlementControls.push {}
					$scope.resetStructures()



				$scope.switchManagement = (managementId) ->
					Commercials.query({management_id: managementId}, (result)->
						$scope.commercials = result

					)
					ManagementStructures.query({id_gerencia: managementId},(result)->
						$scope.settlement.id_estructura_comercial = null
						$scope.structures = result
					)
					return


				$scope.addSettlementConcept = () ->


					$scope.settlement.commercial_settlements[$scope.commercialSettlementIndex].concepts.push {id_liquidacion: $scope.settlement.id, id_comercial: $scope.selectedCommercialSettlement.id_comercial, tipo_linea:1, id_liquidacion_comercial: $scope.selectedCommercialSettlement.id}

					return

				$scope.cancel = () ->
					$mdDialog.cancel()
					return

				$scope.submit = () ->
					$scope.settlement.fecha_desde = moment($scope.settlement.fecha_desde).format('YYYY-MM-DD')
					$scope.settlement.fecha_hasta = moment($scope.settlement.fecha_hasta).format('YYYY-MM-DD')
					$scope.settlement.fecha_liquidacion = moment($scope.settlement.fecha_liquidacion).format('YYYY-MM-DD')

					Settlements[action] $scope.settlement, (settlement) ->
						if settlement.created
							Toast.show 'Liquidacion generada con éxito'
							originalScope.settlements.push settlement.settlement
							$mdDialog.hide()
						else if settlement.updated
							angular.forEach originalScope.settlements, (originalsettlement, $index) ->
								if originalsettlement.id == settlement.settlement.id
									originalScope.settlements.splice($index, 1)
									originalScope.settlements.push settlement.settlement
								return

							Toast.show 'Liquidación editada con éxito'
							$mdDialog.hide()
							return
						else
							Toast.show 'Error en acción'
							return


					return


				$scope.settlementTable =
					settlements:
						dtInstance: null
						dtInstanceCallback: (dtInstance) ->
							$scope.settlementTable.settlements.dtInstance = dtInstance
							return
						dtOptions:
							dom: 't'
							drawCallback: (settings) ->
								$timeout (() ->
									angular.element(window).trigger('resize')
									return
								), 500
								return
							paginationType: 'full_numbers'
							scrollX: true


				$scope.settlementCommercialsTable =
					settlementCommercials:
						dtInstance: null
						dtInstanceCallback: (dtInstance) ->
							$scope.settlementCommercialsTable.settlementCommercials.dtInstance = dtInstance
							return
						dtOptions:
							dom: 't'
							drawCallback: (settings) ->
								$timeout (() ->
									angular.element(window).trigger('resize')
									return
								), 500
								return
							paging: false
							scrollX: true


				$scope.settlementCommercialConceptsTable =
					settlementCommercialConcepts:
						dtInstance: null
						dtInstanceCallback: (dtInstance) ->
							$scope.settlementCommercialConceptsTable.settlementCommercialConcepts.dtInstance = dtInstance
							return
						dtOptions:
							dom: 't'
							drawCallback: (settings) ->
								$timeout (() ->
									angular.element(window).trigger('resize')
									return
								), 500
								return
							paging: false
							###paginationType: 'full_numbers'###
							scrollX: true

				return
			templateUrl: ViewsUrl + '_components/settings-create-settlement'
			parent: angular.element(document.body)
			targetEvent: event
			locals:
				action: action
				managements: $scope.managements
				commercials: $scope.commercials
				id: id
				index: index
				originalScope: $scope
				preservescope: true

			clickOutsideToClose: false
			fullscreen: true
		).then (() ->
			return
		), () ->
			return

		return


	$scope.deleteSettlement = (settlement_id, name) ->
		$mdDialog.show(
			controller: ($scope, $mdDialog, Toast, Settlements, settlement_id, originalScope, name) ->
				$scope.name = name
				$scope.cancel = () ->
					$mdDialog.cancel()
					return

				$scope.submit = () ->
					Settlements.delete {id: settlement_id}, (response) ->
						if response.deleted
							angular.forEach originalScope.settlements, (originalSettlement, $index) ->
								if originalSettlement.id == settlement_id
									originalScope.settlements.splice($index, 1)
								return
							Toast.show 'Liquidación eliminada con éxito'
							$mdDialog.hide()
						else
							Toast.show 'Error al eliminar'
						return

					return


				return
			templateUrl: ViewsUrl + '_components/settings-delete-settlement'
			parent: angular.element(document.body)
			targetEvent: event
			locals:
				settlement_id: settlement_id
				name: name
				originalScope: $scope
				preservescope: true
			clickOutsideToClose: true
			fullscreen: false
		).then (() ->
			return
		), () ->
			return



	# Products
	$scope.alterProducts = (product_id, index) ->
		if product_id == null
			action = 'save'
		else
			action = 'update'

		$mdDialog.show(
			controller: ($scope, $mdDialog, $q, Toast, Products, ProductsStretches, action, product_id, originalScope) ->
				$scope.promises = []

				$scope.action = action

				if product_id

					Products.get({id_producto: product_id}, (result)->
						$scope.product = result
					)
				else
					$scope.product = {}
					$scope.product.stretches = []

				$scope.removeStretch = (stretch, index) ->
					if stretch
						ProductsStretches.delete {id_tramo: stretch}, (result)->
							if result.status
								Toast.show 'Eliminado con éxito'
								$scope.product.stretches.splice(index, 1)
							else
								Toast.show 'No se puede eliminar'
							return
					else
						$scope.product.stretches.splice(index, 1)
					return


				$scope.addStretch = () ->
					$scope.product.stretches.push {}
					return
				$scope.cancel = () ->
					$mdDialog.cancel()
					return

				$scope.submit = () ->
					if $scope.product.stretches.length < 1
						Toast.show 'Debe indicar al menos un tramo'
						return
					Products[action] $scope.product, (product) ->
						if product.created
							Toast.show 'Producto añadido con éxito'
							originalScope.products.push product.product
							$mdDialog.hide()
						else if product.updated
							angular.forEach originalScope.products, (originalProduct, $index) ->
								if originalProduct.id_producto == product.product.id_producto
									originalScope.products.splice($index, 1)
									originalScope.products.push product.product
								return

							Toast.show 'Producto editado con éxito'
							$mdDialog.hide()
							return
						else
							Toast.show 'Error en acción'
							return


					return


				$scope.productStretchTable =
					stretches:
						dtInstance: null
						dtInstanceCallback: (dtInstance) ->
							$scope.productStretchTable.stretches.dtInstance = dtInstance
							return
						dtOptions:
							dom: 't'
							drawCallback: (settings) ->
								$timeout (() ->
									angular.element(window).trigger('resize')
									return
								), 500
								return
							paginationType: 'full_numbers'
							scrollX: true

				return
			templateUrl: ViewsUrl + '_components/settings-create-product'
			parent: angular.element(document.body)
			targetEvent: event
			locals:
				action: action
				products: $scope.products
				product_id: product_id
				index: index
				originalScope: $scope
				preservescope: true

			clickOutsideToClose: false
			fullscreen: true
		).then (() ->
			return
		), () ->
			return

		return


	$scope.deleteProduct = (product_id, name) ->
		$mdDialog.show(
			controller: ($scope, $mdDialog, Toast, Products, product_id, originalScope, name) ->
				$scope.name = name
				$scope.cancel = () ->
					$mdDialog.cancel()
					return

				$scope.submit = () ->
					Products.delete {id_producto: product_id}, (response) ->
						if response.deleted
							angular.forEach originalScope.products, (originalProduct, $index) ->
								if originalProduct.id_producto == product_id
									originalScope.products.splice($index, 1)
								return
							Toast.show 'Producto eliminado con éxito'
							$mdDialog.hide()
						else
							Toast.show 'Error al eliminar'
						return

					return


				return
			templateUrl: ViewsUrl + '_components/settings-delete-product'
			parent: angular.element(document.body)
			targetEvent: event
			locals:
				product_id: product_id
				name: name
				originalScope: $scope
				preservescope: true
			clickOutsideToClose: true
			fullscreen: false
		).then (() ->
			return
		), () ->
			return


	#structures


	$scope.createStructure = () ->
		ManagementStructures.save $scope.newManagementStructure, (result) ->
			Toast.show 'Guardado con éxito'
			index = _.findIndex($scope.managements, {id_gerencia: $scope.newManagementStructure.id_gerencia})
			$scope.managements[index].structures.push result
			$scope.newManagementStructure = {}
		return

	$scope.updateStructure = (structure) ->
		ManagementStructures.update structure, (result) ->
			Toast.show 'Guardado con éxito'
			return
		return

	$scope.deleteStructure = (structure, index) ->
		ManagementStructures.delete {id_gerencia: structure.id_gerencia, id_gerencia_estructura: structure.id_gerencia_estructura,}, (result) ->
			if result.status
				Toast.show 'Eliminado con éxito'
				indexM = _.findIndex($scope.managements, {id_gerencia: structure.id_gerencia})
				$scope.managements[indexM].structures.splice(index, 1)
			else
				Toast.show 'No se puede eliminar'
			return
		return


	# Fees
	$scope.alterFeeGroup = (fee_id, index) ->
		if fee_id == null
			action = 'save'
		else
			action = 'update'

		$mdDialog.show(
			controller: ($scope, $mdDialog, Toast, FeesGroups, FeesProductFees, FeesProducts,  FeesArticles, FeesManagements, FeesManagementFees, FeesGroupsFees, FeesGroupsEfectivityFees, action, fee_id, originalScope, products, managements, articles, contractsTypes) ->
				$scope.valid = true
				$scope.promises = []

				$scope.action = action
				$scope.availableProducts = products
				$scope.availableArticles = articles

				angular.forEach $scope.availableArticles, (article, $index)->
					$scope.availableArticles[$index].packages.push {definicion: 'Unitario', id_articulo: article.id_articulo, id_empresa: article.id_empresa, unidades_lote:1}
					return

				$scope.controls = []
				$scope.productsControls = []
				$scope.articlesControls = []
				$scope.feeControls = []

				$scope.managements = managements

				$scope.contractsTypes = contractsTypes
				$scope.structures = []
				$scope.selectedProductIndex = null
				$scope.selectedProductName = null
				$scope.selectedStretchName = null
				$scope.selectedManagementIndex = null
				$scope.selectedManagementName = null
				$scope.selectedProductType = null


				$scope.selectSettlementProduct = (product_id, stretch_id, index) ->
					$scope.selectedProductIndex = index
					find = _.findIndex($scope.availableProducts, {id_producto: product_id})
					stretch = _.findIndex($scope.availableProducts[find].stretches, {id: stretch_id})
					$scope.selectedProductName = $scope.availableProducts[find].producto
					$scope.selectedStretchName = $scope.availableProducts[find].stretches[stretch].descripcion
					if $scope.availableProducts[find].tipo_contrato == 'H'
						$scope.selectedProductType= 'Hogar'
					else if $scope.availableProducts[find].tipo_contrato == 'N'
						$scope.selectedProductType =  'Negocio'
					else if $scope.availableProducts[find].tipo_contrato == 'A'
						$scope.selectedProductType = 'Hogar y Negocio'
					return

				$scope.selectManagement = (management_id, index) ->
					$scope.selectedManagementIndex = index
					find = _.findIndex($scope.managements, {id_gerencia: management_id})
					$scope.selectedManagementName = $scope.managements[find].gerencia
					return

				if fee_id
					FeesGroups.get({id: fee_id}, (result)->
						$scope.fee = result
						$scope.productsControls = [{}]
						$scope.feesControls = [{}]
						$scope.articlesControls = [{}]
						$scope.controls = [{}]
						if !$scope.fee.fee_articles
							$scope.fee.fee_articles = []
						if !$scope.fee.fee_products
							$scope.fee.fee_products = []


						angular.forEach $scope.fee.fee_managements, (management)->
							angular.forEach management.management.structures, (structure)->
								find = _.findIndex($scope.structures, {id_gerencia_estructura: structure.id_gerencia_estructura})
								if find < 0
									$scope.structures.push(structure)
								return

							return
					)
				else
					$scope.fee = {}
					$scope.fee.fee_products = []
					$scope.fee.fee_managements = []
					$scope.fee.efectivity_fees = []
					$scope.fee.fees = []
					$scope.fee.fee_articles = []






				$scope.removeFeeProduct = (id, index) ->
					#validate
					counter = 0
					if $scope.fee.id
						#existing fee
						angular.forEach $scope.fee.fee_products, (product) ->
							if product.id
								counter++
							return

						if counter < 2 and id
							Toast.show 'No se puede eliminar el último producto guardado. Almacene completamente una antes'
							return
					if id and counter > 1

						FeesProducts.delete {id: id}, (result)->
							if result.status
								Toast.show 'Eliminado con éxito'
								$scope.fee.fee_products.splice(index, 1)
							else
								Toast.show 'No se puede eliminar'
							return
					if !id
						$scope.fee.fee_products.splice(index, 1)
					return
				$scope.removeProductFee = (id, index) ->
					#validate

					if id

						FeesProductFees.delete {id: id}, (result)->
							if result.status
								Toast.show 'Eliminado con éxito'
								$scope.fee.fee_products[$scope.selectedProductIndex].fees.splice(index, 1)
							else
								Toast.show 'No se puede eliminar'
							return
					if !id
						$scope.fee.fee_products[$scope.selectedProductIndex].fees.splice(index, 1)
					return
				$scope.removeManagement = (id, index) ->
					counter = 0
					#validate
					if $scope.fee.id
						#existing fee
						angular.forEach $scope.fee.fee_managements, (management) ->
							if management.id
								counter++
							return

						if counter < 2 and id
							Toast.show 'No se puede eliminar la ultima gerencia guardada. Almacene completamente una antes'
							return


					if id and counter > 1
						FeesManagements.delete {id: id}, (result)->
							if result.status
								Toast.show 'Eliminado con éxito'
								$scope.fee.fee_managements.splice(index, 1)
							else
								Toast.show 'No se puede eliminar'
							return
					else
						$scope.fee.fee_managements.splice(index, 1)
					return




				$scope.removeManagementFee = (id, index) ->
					#validate

					if id

						FeesManagementFees.delete {id: id}, (result)->
							if result.status
								Toast.show 'Eliminado con éxito'
								$scope.fee.fee_managements[$scope.selectedManagementIndex].fees.splice(index, 1)
							else
								Toast.show 'No se puede eliminar'
							return
					if !id
						$scope.fee.fee_managements[$scope.selectedManagementIndex].fees.splice(index, 1)
					return

				$scope.removeFeeArticle = (id, index) ->
					#validate
					counter = 0
					if $scope.fee.id
						#existing fee
						angular.forEach $scope.fee.fee_articles, (article) ->
							if article.id
								counter++
							return

						if counter < 2 and id
							Toast.show 'No se puede eliminar el último artículo guardado. Almacene completamente una antes'
							return
					if id and counter > 1

						FeesArticles.delete {id: id}, (result)->
							if result.status
								Toast.show 'Eliminado con éxito'
								$scope.fee.fee_articles.splice(index, 1)
							else
								Toast.show 'No se puede eliminar'
							return
					if !id
						$scope.fee.fee_articles.splice(index, 1)
					return

				$scope.removeFee = (fee_id, index) ->
					#validate
					counter = 0
					if $scope.fee.id
						#existing fee
						angular.forEach $scope.fee.fees, (fee) ->
							if fee.id
								counter++
							return

						if counter < 2 and id
							Toast.show 'No se puede eliminar la ultima comisión guardada. Almacene completamente una antes'
							return

					if fee_id and counter > 1

						FeesGroupsFees.delete {id: fee_id}, (result)->
							if result.status
								Toast.show 'Eliminado con éxito'
								$scope.fee.fees.splice(index, 1)
							else
								Toast.show 'No se puede eliminar'
							return

					if not fee_id
						$scope.fee.fees.splice(index, 1)
					return

				$scope.removeEfectivityFee = (id, index) ->
					#validate
					if id
						FeesGroupsEfectivityFees.delete {id: id}, (result)->
							if result.status
								Toast.show 'Eliminado con éxito'
								$scope.fee.efectivity_fees.splice(index, 1)
							else
								Toast.show 'No se puede eliminar'
							return
					if !id
						$scope.fee.efectivity_fees.splice(index, 1)
					return


				$scope.addFeeProduct = () ->
					$scope.fee.fee_products.push {fees:[]}
					$scope.productsControls = [{}]
					return

				$scope.addProductFee = (index) ->

					$scope.fee.fee_products[index].fees.push {}

					return

				$scope.addFeeProductsAll = () ->
					angular.forEach $scope.availableProducts, (product)->
						angular.forEach product.stretches, (stretch)->
							$scope.fee.fee_products.push {id_producto: product.id_producto, id_tramo: stretch.id, fees:[]}
							$scope.productsControls = [{}]
							return
						return
					return
				$scope.addFeeManagement = () ->
					index = $scope.fee.fee_managements.push {fees:[]}
					index -= 1
					$scope.controls = [{}]
					return

				$scope.addManagementFee = (index) ->

					$scope.fee.fee_managements[index].fees.push {}

					return

				$scope.addFeeArticle = () ->
					$scope.fee.fee_articles.push {}
					$scope.articlesControls = [{}]
					return

				$scope.addFee = () ->
					$scope.fee.fees.push {}
					$scope.feesControls = [{}]
					return

				$scope.addEfectivityFee = () ->
					$scope.fee.efectivity_fees.push {}
					return

				$scope.refreshStructures = (management) ->
					angular.forEach $scope.managements, (manag) ->
						if manag.id_gerencia == management
							angular.forEach manag.structures, (structure)->
								find = _.findIndex($scope.structures, {id_gerencia_estructura: structure.id_gerencia_estructura})
								if find < 0
									$scope.structures.push(structure)
								return
						return
					return

				$scope.cancel = () ->
					$mdDialog.cancel()
					return


				$scope.submit = () ->

					$scope.fee.fecha_desde = moment($scope.fee.fecha_desde).format('YYYY-MM-DD')
					$scope.fee.fecha_hasta = moment($scope.fee.fecha_hasta).format('YYYY-MM-DD')

					if $scope.fee.id_tipo == 1 or $scope.fee.id_tipo == 3
						if $scope.fee.fee_products
							if $scope.fee.fee_products.length < 1
								Toast.show 'Debe indicar al menos un producto'
								return
						else
							Toast.show 'Debe indicar al menos un producto'
							return


					if $scope.fee.fee_managements.length < 1
						Toast.show 'Debe indicar al menos una gerencia'
						return


					if $scope.fee.id_tipo == 2 or $scope.fee.id_tipo == 3
						if $scope.fee.fee_articles
							if $scope.fee.fee_articles.length < 1
								Toast.show 'Debe indicar al menos un articulo'
								return
						else
							Toast.show 'Debe indicar al menos un articulo'
							return


					if $scope.fee.fees.length < 1
						Toast.show 'Debe indicar al menos una comision al grupo'
						return

					FeesGroups[action] $scope.fee, (fee) ->
						if fee.created
							Toast.show 'Grupo de comisionamiento añadido con éxito'
							originalScope.feeGroups.push fee.feeGroup
							$mdDialog.hide()
						else if fee.updated
							angular.forEach originalScope.fees, (originalFee, $index) ->
								if originalFee.id == fee.fee.id
									originalScope.feeGroups.splice($index, 1)
									originalScope.feeGroups.push fee.fee
								return

							Toast.show 'Grupo de comisionamiento editado con éxito'
							$mdDialog.hide()
							return
						else
							Toast.show 'Error en acción'
							return
					return


				$scope.feeProducstTable =
					feeProducts:
						dtInstance: null
						dtInstanceCallback: (dtInstance) ->
							$scope.feeProducstTable.feeProducts.dtInstance = dtInstance
							return
						dtOptions:
							dom: 't'
							drawCallback: (settings) ->
								$timeout (() ->
									angular.element(window).trigger('resize')
									return
								), 500
								return
							paging: false
							scrollX: true
				$scope.feeProductFeesTable =
					feeProductsFees:
						dtInstance: null
						dtInstanceCallback: (dtInstance) ->
							$scope.feeProductFeesTable.fees.dtInstance = dtInstance
							return
						dtOptions:
							dom: 't'
							drawCallback: (settings) ->
								$timeout (() ->
									angular.element(window).trigger('resize')
									return
								), 500
								return
							paging: false
							scrollX: true
				$scope.feeProductEfectivityFeesTable =
					efectivityFees:
						dtInstance: null
						dtInstanceCallback: (dtInstance) ->
							$scope.feeProductEfectivityFeesTable.efectivityFees.dtInstance = dtInstance
							return
						dtOptions:
							dom: 't'
							drawCallback: (settings) ->
								$timeout (() ->
									angular.element(window).trigger('resize')
									return
								), 500
								return
							paging: false
							scrollX: true
				$scope.feeArticlesTable =
					feeArticles:
						dtInstance: null
						dtInstanceCallback: (dtInstance) ->
							$scope.feeArticlesTable.feeArticles.dtInstance = dtInstance
							return
						dtOptions:
							dom: 't'
							drawCallback: (settings) ->
								$timeout (() ->
									angular.element(window).trigger('resize')
									return
								), 500
								return
							paging: false
							scrollX: true
				$scope.feeManagementsTable =
					feeManagements:
						dtInstance: null
						dtInstanceCallback: (dtInstance) ->
							$scope.feeManagementsTable.feeManagements.dtInstance = dtInstance
							return
						dtOptions:
							dom: 't'
							drawCallback: (settings) ->
								$timeout (() ->
									angular.element(window).trigger('resize')
									return
								), 500
								return
							paging: false
							scrollX: true
				###numbers : [{number :1}, {number: 2}, {number: 3}, {number: 4}, {number: 5}, {number:6}]###
				$scope.feeManagementFeesTable =
					feeManagementFees:
						dtInstance: null
						dtInstanceCallback: (dtInstance) ->
							$scope.feeManagementFeesTable.feeManagementFees.dtInstance = dtInstance
							return
						dtOptions:
							dom: 't'
							drawCallback: (settings) ->
								$timeout (() ->
									angular.element(window).trigger('resize')
									return
								), 500
								return
							paging: false
							scrollX: true

				return
			templateUrl: ViewsUrl + '_components/settings-create-fee_group'
			parent: angular.element(document.body)
			targetEvent: event
			locals:
				action: action
				fees: $scope.fees
				products: $scope.products
				articles: $scope.articles
				contractsTypes: $scope.contractsTypes
				managements: $scope.managements
				fee_id: fee_id
				index: index
				originalScope: $scope
				preservescope: true



			clickOutsideToClose: false
			fullscreen: true
		).then (() ->
			return
		), () ->
			return

		return


	$scope.deleteFeeGroup = (fee_id, name) ->
		$mdDialog.show(
			controller: ($scope, $mdDialog, Toast, FeesGroups, FeesProducts, FeesManagements, FeesGroupsFees, fee_id, originalScope, name) ->
				$scope.name = name
				$scope.cancel = () ->
					$mdDialog.cancel()
					return

				$scope.submit = () ->
					FeesGroups.delete {id: fee_id}, (response) ->
						if response.deleted
							angular.forEach originalScope.feeGroups, (originalFee, $index) ->
								if originalFee.id == fee_id
									originalScope.feeGroups.splice($index, 1)
								return
							Toast.show 'Grupo de comisionamiento eliminado con éxito'
							$mdDialog.hide()
						else
							Toast.show 'Error al eliminar'
						return

					return


				return
			templateUrl: ViewsUrl + '_components/settings-delete-fee_group'
			parent: angular.element(document.body)
			targetEvent: event
			locals:
				fee_id: fee_id
				name: name
				originalScope: $scope
				preservescope: true
			clickOutsideToClose: true
			fullscreen: false
		).then (() ->
			return
		), () ->
			return

	return
