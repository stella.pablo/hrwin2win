window.app.controller 'activitiesCtrl', ($scope, Activities, Clients) ->

	$scope.activities = null
	$scope.clients = Clients.query()
	$scope.dates = []
	$scope.dateFilter = undefined

	$scope.moment = moment

	$scope.resetFilters = () ->
		$scope.clientFilter = undefined
		$scope.dateFilter = undefined
		$scope.typeFilter = undefined
		return

	Activities.query (result) ->
		$scope.activities = result

		angular.forEach $scope.activities, (activity, index) ->
			activity.date = moment(activity.created_at).format('YYYY-MM-DD')


			if $scope.dates.indexOf(activity.date) is -1
				$scope.dates.push activity.date

			return

		return

	return
