app.controller 'managementStrategiesCtrl', (
	$scope,
	$mdDialog,
	CommercialsStrategies,
	management,
	Toast
) ->

	$scope.management = angular.copy management
	$scope.strategy = {}

	$scope.close = () ->
		$mdDialog.hide($scope.management)
		return

	$scope.setForm = (strategy) ->
		$scope.strategy = strategy
		return

	$scope.clearForm = () ->
		$scope.strategy = {}
		$scope.strategiesForm.$setPristine()
		$scope.strategiesForm.$setUntouched()
		return

	$scope.submit = () ->
		action = if $scope.strategy.id then 'update' else 'save'

		if not $scope.strategy.id_gerencia
			$scope.strategy.id_gerencia = $scope.management.id_gerencia

		CommercialsStrategies[action] $scope.strategy, (result) ->
			Toast.show 'Estrategia guardada con éxito'
			$scope.clearForm()

			if action is 'save'
				$scope.management.strategies.push result
			else
				index = _.findIndex($scope.management.strategies, { id: result.id })
				$scope.management.strategies[index] = result
			return
		return

	$scope.delete = (index, strategy) ->
		CommercialsStrategies.delete { id: strategy.id }, (result) ->
			if result.status
				Toast.show 'Estrategia eliminada con éxito'
				$scope.management.strategies.splice(index, 1)
			return
		return

	return
