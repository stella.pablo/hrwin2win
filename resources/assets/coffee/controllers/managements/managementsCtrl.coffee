window.app.controller 'managementsCtrl', (
	$scope,
	CommercialsCampaigns,
	CommercialsManagements,
	CommercialsOrganizations,
	CommercialsStrategies,
	localStorageService
) ->

	$scope.campaigns = CommercialsCampaigns.query()
	$scope.managements = []
	$scope.organizations = CommercialsOrganizations.query()
	$scope.strategies = []

	$scope.filter = (removeFilters) ->
		if removeFilters
			localStorageService.remove 'managements-filters'
		else
			localStorageService.set 'managements-filters', $scope.filters

		CommercialsManagements.query $scope.filters, (result) ->
			$scope.managements = result
			$scope.strategies = CommercialsStrategies.query $scope.filters
			return
		return

	$scope.clearFilters = (removeFilters) ->
		if localStorageService.get 'managements-filters' and not removeFilters
			$scope.filters = localStorageService.get 'managements-filters'
		else
			$scope.filters =
				organization_id: null
				strategy_id: null
				with_commercials_count: true
				with_organization: window.UserData.company.database is 'mexico'
				with_trashed: true

		$scope.filter(removeFilters)
		return

	$scope.clearFilters()

	$scope.table =
		dtInstance: null
		dtInstanceCallback: (dtInstance) ->
			$scope.table.dtInstance = dtInstance
			return
		dtOptions:
			order: [[0, 'asc'], [1, 'asc']]
			pageLength: 25
			paginationType: 'full_numbers'
			scrollX: true

	$scope.$watchCollection 'filters', (newCollection, oldCollection) ->
		$scope.filter()
		return

#	$('.table-with-avatars').on('mouseenter', 'tbody tr', () ->
#		row = $scope.table.dtInstance.DataTable.row(this)
#		imgBox = img = $(row.selector.rows.children).find('div.avatar-hover')[0]
#		console.dir imgBox
#		img = $(imgBox).find('img.mini-avatar')[0]
#		pos = img.getBoundingClientRect()
#
#		$(imgBox).css('top', (pos.top - 106) + 'px')
#
#		$scope.avatarUrl = img.src
#		return
#	)

	return
