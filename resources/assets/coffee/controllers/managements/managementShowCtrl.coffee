window.app.controller 'managementShowCtrl', (
	$scope,
	$mdDialog,
	$mdMedia,
	$window,
	$timeout,
	Candidates,
	Commercials,
	CommercialsManagements,
	management,
	Productivity,
	Toast,
	Settlements,
	ViewsUrl,
	Provinces
) ->

	$scope.$mdMedia = $mdMedia

	$scope.contractsObjective = management.internal.monthly_objective * management.commercials_count
	$scope.calcData =
		contractsObjective: $scope.contractsObjective
		contractsObjectivePercentage: null
		contractsObjectivePerDay: $scope.contractsObjective / moment().daysInMonth()
		currentDay: moment()
		monthStart: moment().startOf('month')
		monthEnd: moment().endOf('month') # not used right now


	$scope.calcData.daysDiff = $scope.calcData.currentDay.diff($scope.calcData.monthStart, 'days') + 1

	$scope.management = management
	$scope.managementStats = {}

	$scope.moment = moment


	$scope.provinces = Provinces.query()

	$scope.addressTypes = [{id_tipo_direccion:0,tipo_direccion:'ENTREGA'}, {id_tipo_direccion:1,tipo_direccion:'POSTAL'}, {id_tipo_direccion:2,tipo_direccion:'BASE'}]

	$scope.settlements = Settlements.getManagementSettlements {id : $scope.management.id_gerencia}

	$scope.productivityFilters =
		date_from: moment().startOf('month').toDate()
		date_to: moment().add(1, 'day').toDate()
		management_id: $scope.management.id_gerencia

	Productivity.managements $scope.productivityFilters, (result) ->
		$scope.managementStats.numero_contratos_total = 0
		$scope.managementStats.numero_contratos_ok = 0
		$scope.managementStats.numero_contratos_p = 0
		$scope.managementStats.numero_contratos_ko = 0

		if result.length
			stats = result[0]
		else
			return

		$scope.managementsCampaigns = Object.keys(stats.campaigns)
		for campaign in $scope.managementsCampaigns
			$scope.managementStats.numero_contratos_ok += stats.campaigns[campaign].c_ok
			$scope.managementStats.numero_contratos_p += stats.campaigns[campaign].c_p
			$scope.managementStats.numero_contratos_ko += stats.campaigns[campaign].c_ko
			$scope.managementStats.numero_contratos_total += stats.campaigns[campaign].c_total

		$scope.managementStats.contratos_por_dia = $scope.managementStats.numero_contratos_total / $scope.calcData.daysDiff
		$scope.managementStats.contratos_adiadehoy_debe = Math.round($scope.calcData.contractsObjectivePerDay * $scope.calcData.daysDiff)
		$scope.managementStats.contratos_adiadehoy_tiene = $scope.managementStats.numero_contratos_total
		$scope.managementStats.contratos_adiadehoy_porcentaje = Math.round(($scope.managementStats.contratos_adiadehoy_tiene / $scope.managementStats.contratos_adiadehoy_debe) * 100)
		$scope.managementStats.contratos_prevision = Math.round($scope.calcData.contractsObjective * ($scope.managementStats.contratos_adiadehoy_porcentaje / 100))
		$scope.managementStats.contratos_prevision_porcentaje = Math.round(($scope.managementStats.contratos_prevision / $scope.calcData.contractsObjective) * 100)

		return

	$scope.strategiesSet = (event) ->
		console.log 'ok'

		$mdDialog.show(
			controller: 'managementStrategiesCtrl'
			templateUrl: ViewsUrl + 'managements/strategies'
			parent: angular.element(document.body)
			targetEvent: event
			locals:
				management: $scope.management
			clickOutsideToClose: false
			fullscreen: true
		).then ((management) ->
			$scope.management = management
			return
		), () ->
			return
		return

	$scope.submit = () ->
		CommercialsManagements.update $scope.management, (result) ->
			Toast.show 'Gerencia editada con éxito'
			return
		return

	$scope.enableManagement = () ->
		$scope.management.fecha_baja = null
		$scope.submit()
		return

	$scope.disableManagement = () ->
		$scope.management.fecha_baja = moment().format('YYYY-MM-DD HH:mm:ss')
		$scope.submit()
		return



	$scope.statsProd = null
	$scope.statsThisMonth =
		daily: {}
	$scope.statsThisMonthBadges =
		ok: 0
		p: 0
		ko: 0

	Productivity.stats { management_id: $scope.management.id_gerencia }, (result) ->
		$scope.statsProd = result
		return

	statsThisMonthFilters =
		daily: true
		date_from: moment().startOf('month').hours(0).minutes(0).seconds(0).toDate()
		date_to: moment().toDate()
		management_id: $scope.management.id_gerencia

	Productivity.stats statsThisMonthFilters, (result) ->
		$scope.statsThisMonth = result

		if Object.keys(result.daily).length
			for day in Object.keys(result.daily)
				$scope.statsThisMonthBadges.ok += result.daily[day].ok
				$scope.statsThisMonthBadges.p += result.daily[day].p
				$scope.statsThisMonthBadges.ko += result.daily[day].ko
		return

	$scope.candidates = Candidates.query {latest: true, take: 10, management_id: $scope.management.internal.id}
	$scope.commercials = Commercials.query {latest: true, take: 10, management_id: $scope.management.id_gerencia}

	$scope.table =
		candidates:
			dtInstance: null
			dtInstanceCallback: (dtInstance) ->
				$scope.table.candidates.dtInstance = dtInstance
				return
			dtOptions:
				dom: 't'
				drawCallback: (settings) ->
					$timeout (() ->
						angular.element(window).trigger('resize')
						return
					), 500
					return
				order: [2, 'desc']
				paginationType: 'full_numbers'
				scrollX: true
		commercials:
			dtInstance: null
			dtInstanceCallback: (dtInstance) ->
				$scope.table.commercials.dtInstance = dtInstance
				return
			dtOptions:
				dom: 't'
				drawCallback: (settings) ->
					$timeout (() ->
						angular.element(window).trigger('resize')
						return
					), 500
					return
				order: [3, 'desc']
				paginationType: 'full_numbers'
				scrollX: true



	$scope.managementPdf = (settlement_id) ->

		$window.open('/api/v1/administration/settlements/pdfManagementSettlement/' + $scope.management.id_gerencia + '/' + settlement_id  , '_blank')



		return

	$scope.showSettlement = (id) ->

		index = _.findIndex($scope.settlements, {id: id})

		$scope.settlement = $scope.settlements[index]



		$mdDialog.show(
			controller: ($scope, $window, $mdDialog, Settlements,$q, Toast, settlement) ->

				$scope.moment = moment
				$scope.settlement = settlement

				$scope.structures = []


				$scope.selectCommercialSettlement = (id) ->

					$scope.commercialSettlementIndex = _.findIndex($scope.settlement.commercial_settlements, {id: id})
					$scope.commercialConcepts = $scope.settlement.commercial_settlements[$scope.commercialSettlementIndex].concepts
					$scope.selectedCommercialSettlement = $scope.settlement.commercial_settlements[$scope.commercialSettlementIndex]

					if $scope.commercialSettlementIndex >= 0
						commercial = $scope.settlement.commercial_settlements[$scope.commercialSettlementIndex].commercial
					if commercial
						$scope.selectedCommercialName = commercial.nombre_completo

					return

				$scope.cancel = () ->
					$mdDialog.cancel()
					return

				$scope.commercialPdf  = () ->
					$window.open('/api/v1/administration/settlements/pdfCommercialSettlement/' + $scope.selectedCommercialSettlement.id  , '_blank')

					return


				$scope.submit = () ->

					Settlements.pdfSettlement  $scope.settlement


					return


				$scope.settlementTable =
					settlements:
						dtInstance: null
						dtInstanceCallback: (dtInstance) ->
							$scope.settlementTable.settlements.dtInstance = dtInstance
							return
						dtOptions:
							dom: 't'
							drawCallback: (settings) ->
								$timeout (() ->
									angular.element(window).trigger('resize')
									return
								), 500
								return
							paginationType: 'full_numbers'
							scrollX: true


				$scope.settlementCommercialsTable =
					settlementCommercials:
						dtInstance: null
						dtInstanceCallback: (dtInstance) ->
							$scope.settlementCommercialsTable.settlementCommercials.dtInstance = dtInstance
							return
						dtOptions:
							dom: 't'
							drawCallback: (settings) ->
								$timeout (() ->
									angular.element(window).trigger('resize')
									return
								), 500
								return
							paging: false
							scrollX: true


				$scope.settlementCommercialConceptsTable =
					settlementCommercialConcepts:
						dtInstance: null
						dtInstanceCallback: (dtInstance) ->
							$scope.settlementCommercialConceptsTable.settlementCommercialConcepts.dtInstance = dtInstance
							return
						dtOptions:
							dom: 't'
							drawCallback: (settings) ->
								$timeout (() ->
									angular.element(window).trigger('resize')
									return
								), 500
								return
							paging: false
						###paginationType: 'full_numbers'###
							scrollX: true

				return
			templateUrl: ViewsUrl + '_components/managements-show-settlement'
			parent: angular.element(document.body)
			targetEvent: event
			locals:
				settlement: $scope.settlement

			clickOutsideToClose: false
			fullscreen: true
		).then (() ->
			return
		), () ->
			return

		return




	$scope.addressTable =
		addresses:
			dtInstance: null
			dtInstanceCallback: (dtInstance) ->
				$scope.addressTable.addresses.dtInstance = dtInstance
				return
			dtOptions:
				dom: 't'
				drawCallback: (settings) ->
					$timeout (() ->
						angular.element(window).trigger('resize')
						return
					), 500
					return
				paginationType: 'full_numbers'
				scrollX: true



	$scope.removeAddress = (address_id,management_id,index) ->

		angular.forEach $scope.management.addresses, ($adress)->
			if $adress.id_direccion == parseInt(address_id)
				$scope.address = $adress
			return


		$mdDialog.show(
			controller: ($scope, $mdDialog,originalScope, address, index, Toast, management,  ManagementAddresses) ->

				$scope.address = address
				$scope.index = index

				$scope.cancel = () ->
					$mdDialog.cancel()
					return

				$scope.submit = () ->

					ManagementAddresses.delete {id_gerencia: management, id_direccion: address.id_direccion },(result) ->

						if result
							originalScope.management.addresses.splice(index, 1)
							Toast.show('elimninada direccion con éxito')
							$mdDialog.hide()
						else
							Toast.show('No se ha podido eliminar')


						return
					return
				return
			templateUrl: ViewsUrl + '_components/management-delete-address'
			parent: angular.element(document.body)
			targetEvent: event
			locals:
				address: $scope.address
				management: management_id
				index: index
				originalScope: $scope

			clickOutsideToClose: true
			fullscreen: false
		).then (() ->
			$state.reload()
			return
		), () ->
			return

		return



	$scope.alterAddress = (address_id, management_id, index) ->

		if address_id
			action = 'update'
		###	if address.id_direccion == null
				action = 'save'
			else
				action = 'update'###
		else
			action = 'save'

		$mdDialog.show(
			controller: ($scope, $mdDialog, management_id, ManagementAddresses, provinces, Toast, action, index, originalScope, address_id, addressTypes) ->

				$scope.provinces = provinces
				$scope.management_id = management_id
				$scope.action = action
				$scope.addressTypes = addressTypes
				$scope.address_id = address_id



				if $scope.address_id
					$scope.address = ManagementAddresses.get {id_direccion: address_id, id_gerencia: management_id}
				else
					$scope.address = {}

				$scope.cancel = () ->
					$mdDialog.cancel()
					return
				$scope.submit = () ->


					if action is 'save'
						$scope.address.id_gerencia = $scope.management.id_gerencia
						$scope.address.id_empresa = $scope.management.id_empresa
					ManagementAddresses[action] $scope.address, (address) ->
						if address.id_direccion
							Toast.show 'Dirección añadida con éxcito'
							originalScope.management.addresses.push address
							$mdDialog.hide()
						else if address.updated
							console.log(address.address)
							console.log(index)
							originalScope.management.addresses[index] = address.address
							Toast.show 'Dirección editada con éxito'
							$mdDialog.hide()
						else
							Toast.show 'Error en acción'


						return

					return
				return
			templateUrl: ViewsUrl + '_components/managements-create-address'
			parent: angular.element(document.body)
			targetEvent: event
			locals:
				management_id: $scope.management.id_gerencia
				action: action
				address_id: address_id
				index : index
				originalScope: $scope
				preservescope: true
				provinces: $scope.provinces
				addressTypes : $scope.addressTypes
			clickOutsideToClose: true
			fullscreen: false
		).then (() ->
			return
		), () ->
			return


		return



	return

