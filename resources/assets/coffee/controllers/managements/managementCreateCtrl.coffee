app.controller 'managementCreateCtrl', (
	$scope,
	$rootScope,
	$state,
	$stateParams,
	$mdDialog,
	ViewsUrl,
	$timeout,
	Commercials,
	CommercialsManagements,
	CommercialsOrganizations,
	Managements,
	Toast,
	Provinces,
	Rates,
	PaymentMethods,
	Ivas,
	Itaus,
	Storages,
) ->

	$scope.commercials = []
	$scope.management = {}
	$scope.rates = {}
	$scope.paymentMethods = {}
	$scope.storages = {}

	if $stateParams.id_gerencia
		CommercialsManagements.get { id_gerencia: $stateParams.id_gerencia }, (result) ->
			$scope.management = result
			return
	else
		$scope.management.internal = {}

	Rates.query (result)->
		$scope.rates = result
		return

	PaymentMethods.query (result)->
		$scope.paymentMethods = result
		return

	Ivas.query (result)->
		$scope.ivas = result
		return
	Itaus.query (result)->
		$scope.itaus = result
		return

	Storages.query (result)->
		$scope.storages = result
		return



	Commercials.query (result) ->
		$scope.commercials = result
		return


	if $rootScope.user.company.database is 'mexico' || $rootScope.user.company.database is 'mexico_test'
		$scope.organizations = []
		CommercialsOrganizations.query (result) ->
			$scope.organizations = result
			return

	$scope.saved = (management_id) ->
		Toast.show 'Gerencia guardada con éxito'
		$state.go 'managements.show', { id_gerencia: management_id }, { reload: true }
		return

	$scope.save = () ->
		action = if $scope.management.id_gerencia then 'update' else 'save'

		CommercialsManagements[action] $scope.management, (management) ->
			if management.id_gerencia
				if action is 'save'
					$scope.management.internal.id = management.internal.id

				$scope.management.internal.name = $scope.management.gerencia
				Managements.update $scope.management.internal, (result) ->
					$scope.saved(management.id_gerencia)
					return
			else if management.error
				Toast.show(management.error)
			return
		return

	$scope.submit = (event) ->
		confirm = $mdDialog
			.prompt()
			.title('Confirmación')
			.textContent('Por favor, confirme esta operación con su Código PIN personal.')
			.placeholder('Código PIN')
			.ariaLabel('Código PIN')
			.targetEvent(event)
			.required(true)
			.ok('Confirmar')
			.cancel('Cancelar')

		$mdDialog.show(confirm).then ((result) ->
			if result is $rootScope.user.pin
				$scope.save()
			else
				Toast.show 'Error. Código PIN incorrecto'
				return
			return
		), ->
			return

	$scope.cancel = () ->
		if $stateParams.id_gerencia
			$state.go 'managements.show', { id_gerencia: $stateParams.id_gerencia }
		else
			$state.go 'managements'
		return

	return
