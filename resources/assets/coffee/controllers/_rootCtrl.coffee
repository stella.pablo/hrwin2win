window.app.controller 'rootCtrl', ($scope, $rootScope, $mdDialog, $mdMedia, $mdSidenav, $timeout, ViewsUrl) ->

	$scope.$mdMedia = $mdMedia
	$scope.hiddenFilters = $mdMedia('xs')
	$scope.isLockedOpen = $mdMedia('gt-sm')

	$scope.changeCompany = (event) ->
		$mdDialog.show
			controller: ($scope, $rootScope, $mdDialog, $state, Companies, Toast, User) ->
				$scope.companies = Companies.query()
				$scope.user = $rootScope.user

				$scope.cancel = () ->
					$mdDialog.cancel()
					return

				$scope.setCompany = (company) ->
					User.update { id: $scope.user.id, company_id: company.id }, (result) ->
						$rootScope.user = result
						Toast.show 'Empresa cambiada a: ' + company.name

						window.location.href = '/'
						$mdDialog.hide()
						return
					return

				return
			templateUrl: ViewsUrl + 'settings/company_change'
			parent: angular.element(document.body)
			targetEvent: event
			fullscreen: true

		return

	$scope.toggleMenu = () ->
		if $mdMedia('gt-sm')
			$scope.isLockedOpen = not $scope.isLockedOpen
		else
			$mdSidenav('left').toggle()

		$timeout (() ->
			window.dispatchEvent(new Event('resize'))
			return
		), 100

		$timeout (() ->
			window.dispatchEvent(new Event('resize'))
			return
		), 500

		$timeout (() ->
			window.dispatchEvent(new Event('resize'))
			return
		), 1000

		return

	return
