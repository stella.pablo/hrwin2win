componentChartCommercialsMonetaryCtrl = (CommercialsManagements) ->
	ctrl = this

	ctrl.chart =
		chart:
			type: 'line'
		series: [],
		title:
			text: 'Volumen'
		xAxis:
			categories: []
		yAxis:
			title:
				text: 'Volumen',
		tooltip:
			pointFormat: '<span style="color:{point.color}">\u25CF</span> {series.name}: <b>{point.y}€</b><br/>'
		plotOptions:
			line:
				marker:
					enabled: true
				enableMouseTracking: true
			series:
				label:
					connectorAllowed: false

	ctrl.populate = () ->
		ctrl.chart.xAxis.categories = []

		monetary = []

		for stat in ctrl.stats
			ctrl.chart.xAxis.categories.push moment.utc(stat.fecha_desde).format('MMM-YYYY')

			if not stat.valor_donaciones
				stat.valor_donaciones = 0

			monetary.push parseFloat(stat.valor_donaciones)

		ctrl.chart.series.push {
			name: 'Volumen'
			data: monetary
		}

		return

	CommercialsManagements.stats (result) ->
		if result
			ctrl.stats = result
			ctrl.populate()

		return

	return


window.app.component 'chartCommercialsMonetary', {
	bindings: {}
	controller: componentChartCommercialsMonetaryCtrl
	templateUrl: '/views/_components/chart-commercials-monetary'
}
