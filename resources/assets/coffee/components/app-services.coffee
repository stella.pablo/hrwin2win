window.app.service 'UserService', ($rootScope, User) ->
	this.user = {}

	this.get = () ->
		User.get (result) ->
			this.user = $rootScope.user = window.userData = result
			$rootScope.$broadcast 'user-loaded'
			$rootScope.$emit 'user-loaded'
			return

	this.set = () ->
		this.user = $rootScope.user = window.UserData
		return

	this

# window.app.service 'Utils', () ->
# 	this.timelineLabels = (startTime, endTime, interval, period) ->
# 		periodsInADay = moment.duration(1, 'day').as(period)
# 		timeLabels = []
# 		startTimeMoment = moment(startTime, 'HH:mm')
# 		i = 0
# 		while i <= periodsInADay
# 			interval
# 			startTimeMoment.add(, period)
# 			timeLabels.push startTimeMoment.format('HH:mm')
# 			i += interval
#
# 		timeLabels
#
# 	this
