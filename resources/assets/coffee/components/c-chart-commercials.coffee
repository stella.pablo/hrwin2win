componentChartCommercialsCtrl = () ->
	ctrl = this

	ctrl.chart =
		chart:
			type: 'line'
		series: [],
		title:
			text: 'Comerciales'
		xAxis:
			categories: []
		yAxis:
			title:
				text: 'Comerciales',
		plotOptions:
			line:
				marker:
					enabled: true
				enableMouseTracking: true
			series:
				label:
					connectorAllowed: false

	ctrl.populate = () ->
		ctrl.chart.xAxis.categories = []

		contracts = []
		objective = []
		removed = []

		for stat in Object.entries(ctrl.stats.commercials)
			date = stat[0]
			statData = stat[1]

			ctrl.chart.xAxis.categories.push moment(date + '01', 'YYYYMMDD').format('MMM YYYY')

			contracts.push parseInt(statData.commercials)
			objective.push if ctrl.stats.commercialsNew[date]? then parseInt(ctrl.stats.commercialsNew[date]) else 0
			removed.push if ctrl.stats.commercialsRemoved[date]? then parseInt(ctrl.stats.commercialsRemoved[date]) else 0

		ctrl.chart.series.push {
			name: 'Total'
			data: contracts
		}
		ctrl.chart.series.push {
			name: 'Nuevos'
			data: objective
		}
		ctrl.chart.series.push {
			name: 'Bajas'
			data: removed
		}

		return

	ctrl.$onChanges = (changesObj) ->
		if changesObj.stats.currentValue
			ctrl.populate()
		return

	#	ctrl.stats.then (result) ->
	#		console.dir result
	#		return

	return


window.app.component 'chartCommercials', {
	bindings:
		stats: '<'
	controller: componentChartCommercialsCtrl
	templateUrl: '/views/_components/chart-commercials'
}
