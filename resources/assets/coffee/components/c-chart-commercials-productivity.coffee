componentChartCommercialsProductivityCtrl = () ->
	ctrl = this

	ctrl.chart =
		chart:
			type: 'line'
		series: [],
		title:
			text: 'Número de contratos'
		xAxis:
			categories: []
		yAxis:
			title:
				text: 'Contratos y objetivos',
		plotOptions:
			line:
				marker:
					enabled: true
				enableMouseTracking: true
			series:
				label:
					connectorAllowed: false

	ctrl.populate = () ->
		ctrl.chart.xAxis.categories = []

		contracts = []
		objective = []

		for stat in Object.entries(ctrl.stats.contracts)
			date = stat[0]
			statData = stat[1]

			ctrl.chart.xAxis.categories.push moment(date + '01', 'YYYYMMDD').format('MMM YYYY')

			contracts.push parseInt(statData.total)
			objective.push if ctrl.stats.commercials[date]? then parseInt(ctrl.stats.commercials[date].objective) else 0

		ctrl.chart.series.push {
			name: 'Contratos'
			data: contracts
		}
		ctrl.chart.series.push {
			name: 'Objetivo'
			data: objective
		}

		return

	ctrl.$onChanges = (changesObj) ->
		if changesObj.stats.currentValue
			ctrl.populate()
		return

	return


window.app.component 'chartCommercialsProductivity', {
	bindings:
		stats: '<'
	controller: componentChartCommercialsProductivityCtrl
	templateUrl: '/views/_components/chart-commercials-productivity'
}
