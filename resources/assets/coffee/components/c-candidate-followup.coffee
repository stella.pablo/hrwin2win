componentCandidateFollowupCtrl = (Candidates) ->
	ctrl = this

	return

window.app.component 'candidateFollowup', {
	bindings:
		candidate: '<'
	controller: componentCandidateFollowupCtrl
	templateUrl: '/views/_components/candidate-followup'
}
