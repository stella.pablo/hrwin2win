componentChartCandidatesStatsCtrl = ($rootScope, $filter, $timeout, CandidatesStats) ->
	ctrl = this

	ctrl.chartView = 'month'

	ctrl.chart =
		chart:
			type: 'column'
		title:
			text: ''
		xAxis:
			categories: []
			crosshair: true
		yAxis:
			min: 0
			title:
				text: 'Candidatos'
		tooltip:
			headerFormat: '<span style="font-size:10px">{point.key}</span><table>'
			pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' + '<td style="padding:0"><b>{point.y:.0f}</b></td></tr>'
			footerFormat: '</table>'
			shared: true
			useHTML: true
		plotOptions:
			column:
				pointPadding: 0
				borderWidth: 0
		series: []

	ctrl.populate = () ->
		ctrl.chart.title.text = ''
		ctrl.chart.xAxis.categories = []

		nuevos = []
		primeraEntrevista = []
		primeraEntrevistaLlego = []
		segundaEntrevista = []
		segundaEntrevistaLlego = []
		preaprobados = []
		aprobados = []
		contratados = []

		if ctrl.chartView is 'month'
			last = ctrl.stats.length - 1
			ctrl.chart.title.text = moment().format('MMMM YYYY')
			ctrl.chart.xAxis.categories.push moment().format('MMMM YYYY')

			nuevos = [ctrl.stats[last].new]
			primeraEntrevista = [ctrl.stats[last].a1]
			primeraEntrevistaLlego = [ctrl.stats[last].a1d]
			segundaEntrevista = [ctrl.stats[last].a2]
			segundaEntrevistaLlego = [ctrl.stats[last].a2d]
			preaprobados = [ctrl.stats[last].pre]
			aprobados = [ctrl.stats[last].apr]
			contratados = [ctrl.stats[last].hired]

		else if ctrl.chartView is 'year'
			monthFirst = moment(ctrl.stats[0].month + '01').format('MMMM YYYY')
			monthLast = moment(ctrl.stats[ctrl.stats.length - 1].month + '01').format('MMMM YYYY')
			ctrl.chart.title.text = monthFirst + ' - ' + monthLast

			for stat in ctrl.stats
				ctrl.chart.xAxis.categories.push moment(stat.month + '01').format('MMMM YYYY')

				nuevos.push stat.new
				primeraEntrevista.push stat.a1
				primeraEntrevistaLlego.push stat.a1d
				segundaEntrevista.push stat.a2
				segundaEntrevistaLlego.push stat.a2d
				preaprobados.push stat.pre
				aprobados.push stat.apr
				contratados.push stat.hired


		ctrl.chart.series = [
			{
				name: 'Nuevos'
				data: nuevos
			}
			{
				name: '1ª Entrevista'
				data: primeraEntrevista
			}
			{
				name: '1ª Entrevista Llegó'
				data: primeraEntrevistaLlego
			}
			{
				name: '2ª Entrevista'
				data: segundaEntrevista
			}
			{
				name: '2ª Entrevista Llegó'
				data: segundaEntrevistaLlego
			}
			{
				name: 'Preaprobados'
				data: preaprobados
			}
			{
				name: 'Aprobados'
				data: aprobados
			}
			{
				name: 'Contratados'
				data: contratados
			}
		]

		return

	ctrl.managements = _.filter($rootScope.user.managements_all, {company_id: $rootScope.user.company_id})

	if not ctrl.managements.length
		return

	if not ctrl.management_id
		ctrl.management_id = 0

	ctrl.loadData = () ->
		filters = {}

		if ctrl.management_id
			filters.management_id = ctrl.management_id

		CandidatesStats.query filters, (result) ->
			if result
				ctrl.stats = result
				ctrl.populate()
			return
		return

	$timeout (() ->
		ctrl.loadData()
		return
	), 1000

	ctrl.$onChanges = (changesObj) ->
		console.dir changesObj
		if changesObj.managementId.currentValue
			ctrl.management_id = ctrl.managementId
			$timeout () ->
				ctrl.loadData()
				return
		return

	return


window.app.component 'chartCandidatesStats', {
	bindings:
		managementId: '<'
	controller: componentChartCandidatesStatsCtrl
	templateUrl: '/views/_components/chart-candidates-stats'
}
