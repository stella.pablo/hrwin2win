# API Factories
window.app.factory 'AbsencesMotives', ($resource, BaseUrl) ->
	$resource(BaseUrl + '/commercials/absences/motives/:id', {id: '@id'}, {
		update:
			method: 'PUT'
	})

window.app.factory 'Activities', ($resource, BaseUrl) ->
	$resource(BaseUrl + '/activities/:id', {id: '@id'}, {
	# Change :id to :functionName
		update:
			method: 'PUT'
	})

window.app.factory 'Candidates', ($resource, BaseUrl) ->
	$resource(BaseUrl + '/candidates/:functionName/:id/:tipo/:tele_movil', {id: '@id', tipo: '@tipo', tele_movil: '@tele_movil'}, {
		external:
			method: 'GET'
			params:
				functionName: 'external'
		checkIsValid:
			method: 'GET'
			params:
				functionName: 'checkIsValid'
		checkHireAvailability:
			method: 'GET'
			params:
				functionName: 'checkHireAvailability'
		update:
			method: 'PUT'
	})

window.app.factory 'CandidatesAppointments', ($resource, BaseUrl) ->
	$resource(BaseUrl + '/candidates/appointments/:id', {id: '@id'}, {
		update:
			method: 'PUT'
	})

window.app.factory 'CandidatesFilesTypes', ($resource, BaseUrl) ->
	$resource(BaseUrl + '/candidates/files/types/:id', {id: '@id'}, {
		update:
			method: 'PUT'
	})

window.app.factory 'CandidatesOrigins', ($resource, BaseUrl) ->
	$resource(BaseUrl + '/candidates/origins/:id', {id: '@id'}, {
		update:
			method: 'PUT'
	})

window.app.factory 'CandidatesStatuses', ($resource, BaseUrl) ->
	$resource(BaseUrl + '/candidates/statuses/:id', {id: '@id'}, {
		update:
			method: 'PUT'
	})

window.app.factory 'CandidatesStats', ($resource, BaseUrl) ->
	$resource(BaseUrl + '/candidates/stats')

window.app.factory 'Commercials', ($resource, BaseUrl) ->
	$resource(BaseUrl + '/commercials/:functionName/:id_comercial/:tipo/:tele_movil/:subfunctionName/', {id_comercial: '@id_comercial', tipo: '@tipo', tele_movil: '@tele_movil'}, {
		campaigns:
			method: 'POST'
			params:
				subfunctionName: 'campaigns'
		internal:
			method: 'GET'
			params:
				functionName: 'internal'
		managers:
			isArray: true
			method: 'GET'
			params:
				functionName: 'managers'
		checkIsValid:
			method: 'GET'
			params:
				functionName: 'checkIsValid'
		update:
			method: 'PUT'
	})

window.app.factory 'CommercialsAbsences', ($resource, BaseUrl) ->
	$resource(BaseUrl + '/commercials/absences/:id', {id: '@id'}, {
		update:
			method: 'PUT'
	})

window.app.factory 'CommercialsCampaigns', ($resource, BaseUrl) ->
	$resource(BaseUrl + '/commercials/campaigns/:id', {id: '@id'}, {
		update:
			method: 'PUT'
	})

window.app.factory 'CommercialsManagements', ($resource, BaseUrl) ->
	$resource(BaseUrl + '/commercials/managements/:functionName/:id_gerencia', {id_gerencia: '@id_gerencia'}, {
		stats:
			isArray: true
			method: 'GET'
			params:
				functionName: 'stats'
		typesContracts:
			isArray: true
			method: 'GET'
			params:
				functionName: 'types-contracts'
		typesContractsSubtype:
			isArray: true
			method: 'GET'
			params:
				functionName: 'types-contracts-subtype'
		leaders:
			isArray: true
			method: 'GET'
			params:
				functionName: 'leaders'
		update:
			method: 'PUT'
	})

window.app.factory 'ContractsTypes', ($resource, BaseUrl) ->
	$resource(BaseUrl + '/administration/contractsTypes/:id_tipo_contrato_comercial', {id_tipo_contrato_comercial: '@id_tipo_contrato_comercial'}, {
		update:
			method: 'PUT'

	})
window.app.factory 'ContractsTypesStretches', ($resource, BaseUrl) ->
	$resource(BaseUrl + '/administration/contractsTypesStretches/:id_tramo', {id_tramo: '@id_tramo'}, {
		update:
			method: 'PUT'
	})


window.app.factory 'FeesGroups', ($resource, BaseUrl) ->
	$resource(BaseUrl + '/administration/feesGroup/:id/', {id: '@id'}, {
		update:
			method: 'PUT'
	})

window.app.factory 'FeesManagements', ($resource, BaseUrl) ->
	$resource(BaseUrl + '/administration/feesManagements/:id', {id: '@id'}, {
		update:
			method: 'PUT'
	})

window.app.factory 'FeesProducts', ($resource, BaseUrl) ->
	$resource(BaseUrl + '/administration/feesProducts/:id', {id: '@id'}, {
		update:
			method: 'PUT'
	})
window.app.factory 'FeesProductFees', ($resource, BaseUrl) ->
	$resource(BaseUrl + '/administration/feesProductFees/:id', {id: '@id'}, {
		update:
			method: 'PUT'
	})

window.app.factory 'FeesManagementFees', ($resource, BaseUrl) ->
	$resource(BaseUrl + '/administration/feesManagementFees/:id', {id: '@id'}, {
		update:
			method: 'PUT'
	})
window.app.factory 'FeesArticles', ($resource,BaseUrl) ->
	$resource(BaseUrl + '/administration/feesArticles/:id', { id: '@id'}, {
		update:
			method: 'PUT'
	})
window.app.factory 'FeesGroupsFees', ($resource, BaseUrl) ->
	$resource(BaseUrl + '/administration/feesGroupFees/:id', {id: '@id'}, {
		update:
			method: 'PUT'
	})
window.app.factory 'FeesGroupsEfectivityFees', ($resource, BaseUrl) ->
	$resource(BaseUrl + '/administration/feesGroupEfectivityFees/:id', {id: '@id'}, {
		update:
			method: 'PUT'
	})

window.app.factory 'Provinces', ($resource, BaseUrl) ->
	$resource(BaseUrl + '/provinces/:id_provincia', {id_provincia: '@id_provincia'}, {
		update:
			method: 'PUT'
	})

window.app.factory 'ManagementAddresses', ($resource, BaseUrl) ->
	$resource(BaseUrl + '/commercials/managementAddresses/:id_direccion/:id_gerencia', {id_direccion: '@id_direccion', id_gerencia: '@id_gerencia'}, {
		update:
			method: 'PUT'
	})

window.app.factory 'Products', ($resource, BaseUrl) ->
	$resource(BaseUrl + '/products/:id_producto', {id_producto: '@id_producto'}, {
		update:
			method: 'PUT'
	})

window.app.factory 'Articles', ($resource, BaseUrl) ->
	$resource(BaseUrl + '/articles/:id_articulo', {id_articulo: '@id_articulo'}, {
		update:
			method: 'PUT'
	})


window.app.factory 'ProductsStretches', ($resource, BaseUrl) ->
	$resource(BaseUrl + '/productsStretches/:id_tramo', {id_tramo: '@id_tramo'}, {
		update:
			method: 'PUT'
	})
###window.app.factory 'ProductsStretches', ['$resource', 'BaseUrl', ($resource, BaseUrl) ->
	$resource(BaseUrl + '/productsStretches/:id_tramo')
]###


window.app.factory 'Ivas', ($resource, BaseUrl) ->
	$resource(BaseUrl + '/administration/ivas/:id_iva', {id_iva: '@id_iva'}, {
		update:
			method: 'PUT'
	})
window.app.factory 'Itaus', ($resource,BaseUrl) ->
	$resource(BaseUrl + '/administration/itaus/:id_itau', { id_itau: '@id_itau' }, {
		update:
			method: 'PUT'
	})

window.app.factory 'Irpfs', ($resource, BaseUrl) ->
	$resource(BaseUrl + '/administration/irpfs/:id_irpf', {id_irpf: '@id_irpf'}, {
		update:
			method: 'PUT'
	})

window.app.factory 'ManagementStructures', ($resource, BaseUrl) ->
	$resource(BaseUrl + '/administration/managementStructures/:id_gerencia/:id_gerencia_estructura', {id_gerencia: '@id_gerencia', id_gerencia_estructura: '@id_gerencia_estructura'}, {
		update:
			method: 'PUT'
	})
window.app.factory 'Settlements', ($resource, BaseUrl) ->
	$resource(BaseUrl + '/administration/settlements/:functionName/:id/:id_liquidacion_comercial', {id: '@id', id_liquidacion_comercial : '@id_liquidacion_comercial'}, {
		update:
			method: 'PUT'
		getManagementSettlements:
			isArray: true
			method: 'GET'
			params:
				functionName: 'getManagementSettlements',
		pdfManagementSettlement:
			isArray: true
			method: 'GET'
			params:
				functionName: 'pdfManagementSettlement'
		pdfSettlement:
			isArray: true
			method: 'GET'
			params:
				functionName: 'pdfSettlement'
		pdfCommercialSettlement:
			isArray: true
			method: 'GET'
			params:
				functionName: 'pdfCommercialSettlement'
	})
window.app.factory 'SettlementConcepts', ($resource, BaseUrl) ->
	$resource(BaseUrl + '/administration/settlementconcepts/:id', {id: '@id'}, {
		update:
			method: 'PUT'
	})
window.app.factory 'Rates', ($resource,BaseUrl) ->
	$resource(BaseUrl + '/administration/rates/:id_tarifa', { id: '@id_tarifa' }, {
		update:
			method: 'PUT'
	})
window.app.factory 'PaymentMethods', ($resource,BaseUrl) ->
	$resource(BaseUrl + '/administration/paymentMethods/:id_forma_pago', { id: '@id_forma_pago' }, {
		update:
			method: 'PUT'
	})
window.app.factory 'Storages', ($resource,BaseUrl) ->
	$resource(BaseUrl + '/administration/storages/:id_almacen', { id: '@id_almacen' }, {
		update:
			method: 'PUT'
	})
window.app.factory 'CommercialsOrganizations', ($resource, BaseUrl) ->
	$resource(BaseUrl + '/commercials/organizations')

window.app.factory 'CommercialsReports', ($resource, BaseUrl) ->
	$resource(BaseUrl + '/commercials/reports/:id', {id: '@id'}, {
		update:
			method: 'PUT'
	})

window.app.factory 'CommercialsStrategies', ($resource, BaseUrl) ->
	$resource(BaseUrl + '/commercials/strategies/:id', {id: '@id'}, {
		update:
			method: 'PUT'
	})
window.app.factory 'CommercialsStructures', ($resource, BaseUrl) ->
	$resource(BaseUrl + '/commercials/:id_comercial/structures/:id_estructura', {id_estructura: '@id_estructura', id_comercial: '@id_comercial'}, {
		update:
			method: 'PUT'
	})
window.app.factory 'CommercialsCompanyContracts', ($resource, BaseUrl) ->
	$resource(BaseUrl + '/commercials/:id_comercial/company_contracts/:id_comercial_contrato', {id_comercial_contrato: '@id_comercial_contrato', id_comercial: '@id_comercial'}, {
		update:
			method: 'PUT'
	})
window.app.factory 'CompanyContracts', ($resource, BaseUrl) ->
	$resource(BaseUrl + '/companyContracts/:id_tipo_contrato', {id_tipo_contrato: '@id_tipo_contrato'}, {
		update:
			method: 'PUT'
	})
window.app.factory 'ReportsMotives', ($resource, BaseUrl) ->
	$resource(BaseUrl + '/commercials/reports/motives/:id', {id: '@id'}, {
		update:
			method: 'PUT'
	})

window.app.factory 'ReportsStatuses', ($resource, BaseUrl) ->
	$resource(BaseUrl + '/commercials/reports/statuses/:id', {id: '@id'}, {
		update:
			method: 'PUT'
	})

window.app.factory 'Companies', ($resource, BaseUrl) ->
	$resource(BaseUrl + '/companies/:id', {id: '@id'}, {
	# Change :id to :functionName
		update:
			method: 'PUT'
	})

window.app.factory 'Emails', ($resource, BaseUrl) ->
	$resource(BaseUrl + '/emails/:id', {id: '@id'}, {
		update:
			method: 'PUT'
	})

window.app.factory 'EmailsTypes', ($resource, BaseUrl) ->
	$resource(BaseUrl + '/emails/types/:id', {id: '@id'}, {
		update:
			method: 'PUT'
	})

window.app.factory 'FilesTypes', ($resource, BaseUrl) ->
	$resource(BaseUrl + '/files/types/:id_tipo_fichero', {id_tipo_fichero: '@id_tipo_fichero'}, {
		update:
			method: 'PUT'
	})

window.app.factory 'Managements', ($resource, BaseUrl) ->
	$resource(BaseUrl + '/managements/:id', {id: '@id'}, {
		update:
			method: 'PUT'
	})

window.app.factory 'Notifications', ($resource, BaseUrl) ->
	$resource(BaseUrl + '/notifications/:id', {id: '@id'}, {
		update:
			method: 'PUT'
	})

window.app.factory 'Productivity', ($resource, BaseUrl) ->
	$resource(BaseUrl + '/commercials/productivity/:functionName/:id', {id: '@id'}, {
		commercial:
			method: 'GET'
			params:
				functionName: 'commercial'
		commercials:
			isArray: true
			method: 'GET'
			params:
				functionName: 'commercials'
		managements:
			isArray: true
			method: 'GET'
			params:
				functionName: 'managements'
		low:
			isArray: true
			method: 'GET'
			params:
				functionName: 'low'
		stats:
			method: 'GET'
			params:
				functionName: 'stats'
	})

window.app.factory 'User', ($resource, BaseUrl) ->
	$resource(BaseUrl + '/user/:id', {id: '@id'}, {
		update:
			method: 'PUT'
	})

# Extra Factories
window.app.factory 'Toast', ($mdMedia, $mdToast) ->
	toast =
		show: (text) ->
#			position = if $mdMedia('xs') then 'bottom left' else 'top right'
			$mdToast.show($mdToast.simple().textContent(text).position('bottom right'))

			return
