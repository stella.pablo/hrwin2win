window.app.config ($cookiesProvider, $interpolateProvider, $locationProvider, $mdDateLocaleProvider, $mdThemingProvider, $compileProvider, $qProvider,
	cfpLoadingBarProvider,
	localStorageServiceProvider) ->
	$cookiesProvider.defaults.expires = moment().add(1, "months").toDate()
	$interpolateProvider.startSymbol("[[").endSymbol("]]")

	$locationProvider.html5Mode(true)

	$mdDateLocaleProvider.firstDayOfWeek = 1
	$mdDateLocaleProvider.formatDate = (date) ->
		return if date then moment(date).format("DD/MM/YYYY") else ""
	$mdDateLocaleProvider.parseDate = (date) ->
		dateObj = moment(date, "DD/MM/YYYY", true)
		return if dateObj.isValid() then dateObj.toDate() else new Date

	$mdThemingProvider.theme("default").primaryPalette("teal").accentPalette("pink")
	$mdThemingProvider.theme("default-dark").primaryPalette('orange').dark()
	# $mdThemingProvider.theme("default-dark").primaryPalette('pink').backgroundPalette("teal", {'default': '500'}).dark()
	# $mdThemingProvider.theme('dark-green').backgroundPalette('green').dark()
	# $mdThemingProvider.theme('dark-blue').backgroundPalette('blue').dark()
	# $mdThemingProvider.theme('dark-red').backgroundPalette('red').dark()

	$mdThemingProvider.enableBrowserColor({
		theme: 'default'
	})

	# $compileProvider.preAssignBindingsEnabled(true)
	$qProvider.errorOnUnhandledRejections(false)

	moment.locale('es')

	cfpLoadingBarProvider.includeSpinner = false

	localStorageServiceProvider.setPrefix 'hr'

	return

window.app.run ($mdDialog, $templateRequest, $transitions, $mdMedia, $mdSidenav, CommercialsReports, UserService, DTDefaultOptions, ViewsUrl) ->
	UserService.set()

	# $templateRequest ViewsUrl + 'callcenter/index', true
	# $templateRequest ViewsUrl + 'contracts/index', true
	# $templateRequest ViewsUrl + 'contracts/show', true
	# $templateRequest ViewsUrl + 'precontracts/store', true
	# $templateRequest ViewsUrl + 'precontracts/index', true
	# $templateRequest ViewsUrl + 'precontracts/store', true
	# $templateRequest ViewsUrl + 'statistics/index', true
	# $templateRequest ViewsUrl + 'settlements/index', true
	# $templateRequest ViewsUrl + 'settlements/show', true
	# $templateRequest ViewsUrl + 'user/profile', true

	# Dirty fix to remove overflow:hidden from a sidenav on transition change
	$transitions.onSuccess {}, (event, toState, toParams, fromState, fromParams) ->
		angular.element('#body > md-content').css('overflow', 'auto')

		if not $mdMedia('gt-sm') and $mdSidenav('left').isOpen()
			$mdSidenav('left').toggle()
		return

	DTDefaultOptions.setLanguage {
		"sProcessing": "Procesando...",
		"sLengthMenu": "Mostrar _MENU_ registros",
		"sZeroRecords": "No se encontraron resultados",
		"sEmptyTable": "Ningún dato disponible en esta tabla",
		"sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
		"sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
		"sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
		"sInfoPostFix": "",
		"sSearch": "Buscar:",
		"sUrl": "",
		"sInfoThousands": ",",
		"sLoadingRecords": "Cargando...",
		"oPaginate": {
			"sFirst": "Primero",
			"sLast": "Último",
			"sNext": "Siguiente",
			"sPrevious": "Anterior"
		},
		"oAria": {
			"sSortAscending": ": Activar para ordenar la columna de manera ascendente",
			"sSortDescending": ": Activar para ordenar la columna de manera descendente"
		}
	}

	DTDefaultOptions.loadingTemplate = """
	                                   	<h3 class="text-center margin-bottom-40" flex>Cargando...</h3>
	                                   """


	Highcharts.setOptions({
		lang: {
			months: [
				'Enero', 'Febrero', 'Marzo', 'Abril',
				'Mayo', 'Junio', 'Julio', 'Agosto',
				'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'
			],
			shortMonths: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
			weekdays: [
				'Domingo', 'Lunes', 'Martes', 'Miércoles',
				'Jueves', 'Viernes', 'Sábado'
			]
		}
	})

	return
