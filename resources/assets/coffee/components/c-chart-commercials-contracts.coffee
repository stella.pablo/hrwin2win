componentChartCommercialsContractsCtrl = () ->
	ctrl = this

	ctrl.chart =
		chart:
			type: 'line'
		series: [],
		title:
			text: 'Contratos el último año'
		xAxis:
			categories: []
		yAxis:
			title:
				text: 'Contratos',
		plotOptions:
			line:
				marker:
					enabled: true
				enableMouseTracking: true
			series:
				label:
					connectorAllowed: false

	ctrl.populate = () ->
		ctrl.chart.xAxis.categories = []

		contracts = []
		contracts_ok = []
		contracts_p = []
		contracts_ko = []

		for stat in Object.entries(ctrl.stats.contracts)
			date = stat[0]
			statData = stat[1]

			ctrl.chart.xAxis.categories.push moment(date + '01', 'YYYYMMDD').format('MMM YYYY')

			contracts.push parseInt(statData.total)
			contracts_ok.push parseInt(statData.ok)
			contracts_p.push parseInt(statData.p)
			contracts_ko.push parseInt(statData.ko)

		ctrl.chart.series.push {
			name: 'OK'
			data: contracts_ok
		}

		ctrl.chart.series.push {
			name: 'PENDIENTES'
			data: contracts_p
		}

		ctrl.chart.series.push {
			name: 'KO'
			data: contracts_ko
		}

		# ctrl.chart.series.push {
		# 	name: 'TOTAL'
		# 	data: contracts
		# }

		return

	ctrl.$onChanges = (changesObj) ->
		if changesObj.stats.currentValue
			ctrl.populate()
		return

	return


window.app.component 'chartCommercialsContracts', {
	bindings:
		stats: '<'
	controller: componentChartCommercialsContractsCtrl
	templateUrl: '/views/_components/chart-commercials-contracts'
}
