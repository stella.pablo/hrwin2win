window.app.config ($stateProvider, $urlRouterProvider, ViewsUrl) ->
	$stateProvider
		# Activities
		.state({
			name: 'activities'
			url: '/activities'
			controller: 'activitiesCtrl'
			templateUrl: ViewsUrl + 'activities/index'
		})
		# Candidates
		.state({
			name: 'candidates'
			url: '/candidates'
			resolve:
				managements: (Managements) ->
					Managements.query().$promise
			views:
				"@":
					controller: 'candidatesCtrl'
					templateUrl: ViewsUrl + 'candidates/index'
		})
		.state({
			name: 'candidates.create'
			url: '/create'
			views:
				"@":
					controller: 'candidateCreateCtrl'
					templateUrl: ViewsUrl + 'candidates/create'
		})
		.state({
			name: 'candidates.show'
			url: '/:id'
			resolve:
				candidate: (Candidates, $transition$) ->
					Candidates.get({ id: $transition$.params().id }).$promise
			views:
				"@":
					controller: 'candidateShowCtrl'
					templateUrl: ViewsUrl + 'candidates/show'
		})
		.state({
			name: 'candidates.show.edit'
			url: '/edit'
			resolve:
				candidatesOrigins: (CandidatesOrigins) ->
					CandidatesOrigins.query().$promise
				emailsTypes: (EmailsTypes) ->
					EmailsTypes.query().$promise
			views:
				"@":
					controller: 'candidateCreateCtrl'
					templateUrl: ViewsUrl + 'candidates/create'
		})
		# Commercials
		.state({
			name: 'commercials'
			url: '/commercials'
			views:
				"@":
					controller: 'commercialsCtrl'
					templateUrl: ViewsUrl + 'commercials/index'
		})
		.state({
			name: 'commercials.create'
			url: '/create?candidate_id'
			resolve:
				campaigns: (CommercialsCampaigns) ->
					CommercialsCampaigns.query().$promise
				managements: (CommercialsManagements) ->
					CommercialsManagements.query().$promise
				typesContracts: (CommercialsManagements) ->
					CommercialsManagements.typesContracts().$promise
			views:
				"@":
					controller: 'commercialCreateCtrl'
					templateUrl: ViewsUrl + 'commercials/create'
		})
		.state({
			name: 'commercials.show'
			url: '/:id_comercial'
			resolve:
				commercial: (Commercials, $transition$) ->
					Commercials.get({ id_comercial: $transition$.params().id_comercial }).$promise
				filesTypes: (CandidatesFilesTypes) ->
					CandidatesFilesTypes.query().$promise
			views:
				"@":
					controller: 'commercialShowCtrl'
					templateUrl: ViewsUrl + 'commercials/show'
		})
		.state({
			name: 'commercials.show.edit'
			url: '/edit'
			resolve:
				campaigns: (CommercialsCampaigns) ->
					CommercialsCampaigns.query().$promise
				managements: (CommercialsManagements) ->
					CommercialsManagements.query().$promise
				typesContracts: (CommercialsManagements) ->
					CommercialsManagements.typesContracts().$promise
			views:
				"@":
					controller: 'commercialCreateCtrl'
					templateUrl: ViewsUrl + 'commercials/create'
		})
		.state({
			name: 'commercialsabsences'
			url: '/commercials/absences'
			controller: 'commercialsAbsencesCtrl'
			templateUrl: ViewsUrl + 'commercials-absences/index'
			resolve:
				commercials: (Commercials) ->
					Commercials.query().$promise
				commercialsAbsences: (CommercialsAbsences, $transition$) ->
					CommercialsAbsences.query().$promise

		})
		.state({
			name: 'commercialsreports'
			url: '/commercials/reports'
			controller: 'commercialsReportsCtrl'
			templateUrl: ViewsUrl + 'commercials-reports/index'
		})
		# Dashboard
		.state({
			name: 'dashboard'
			url: '/dashboard'
			controller: 'dashboardCtrl'
			templateUrl: ViewsUrl + 'dashboard/index'
		})
		# Managements
		.state({
			name: 'managements'
			url: '/managements'
			views:
				"@":
					controller: 'managementsCtrl'
					templateUrl: ViewsUrl + 'managements/index'
		})
		.state({
			name: 'managements.create'
			url: '/create?commercial_id'
			resolve:
				campaigns: (CommercialsCampaigns) ->
					CommercialsCampaigns.query().$promise
			views:
				"@":
					controller: 'managementCreateCtrl'
					templateUrl: ViewsUrl + 'managements/create'
		})
		.state({
			name: 'managements.show'
			url: '/:id_gerencia'
			resolve:
				management: (CommercialsManagements, $transition$) ->
					CommercialsManagements.get({ id_gerencia: $transition$.params().id_gerencia }).$promise
			views:
				"@":
					controller: 'managementShowCtrl'
					templateUrl: ViewsUrl + 'managements/show'
		})
		.state({
			name: 'managements.show.edit'
			url: '/edit'
			resolve:
				campaigns: (CommercialsCampaigns) ->
					CommercialsCampaigns.query().$promise
				managements: (CommercialsManagements) ->
					CommercialsManagements.query().$promise
				typesContracts: (CommercialsManagements) ->
					CommercialsManagements.typesContracts().$promise
			views:
				"@":
					controller: 'managementCreateCtrl'
					templateUrl: ViewsUrl + 'managements/create'
		})
		# Productivity
		.state({
			name: 'productivity'
			url: '/productivity?organization_id&management_id'
			controller: 'productivityCtrl'
			templateUrl: ViewsUrl + 'productivity/index'
		})
		.state({
			name: 'productivity-low'
			url: '/productivity-low'
			controller: 'productivityLowCtrl'
			templateUrl: ViewsUrl + 'productivity-low/index'
		})
		# Profile
		.state({
			name: 'profile'
			url: '/profile'
			controller: 'profileCtrl'
			templateUrl: ViewsUrl + 'settings/profile'
		})

		# Settings
		.state({
			name: 'settings'
			url: '/settings'
			controller: 'settingsCtrl'
			templateUrl: ViewsUrl + 'settings/settings'
		})




	$urlRouterProvider.otherwise '/dashboard'

	return
