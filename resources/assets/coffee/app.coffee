new WOW().init()

window.app = angular.module 'app', [
	# 'angular-chartist'
	'angular-loading-bar'
	'angularMoment'
	'datatables'
	'datatables.buttons'
	'highcharts-ng'
	'LocalStorageModule'
	'ngAnimate'
	'ngCookies'
	'ngFileUpload'
	'ngMaterial'
	'ngMessages'
	'ngResource'
	# 'ng.deviceDetector'
	# 'rzModule'
	# 'ui-leaflet'
	'ui.router'
]

window.app.constant 'BaseUrl', '/api/v1'
window.app.constant 'CdnUrl', 'http://cdn.puntosoft.es/'
window.app.constant 'ViewsUrl', '/views/'

# window.app.constant 'Definitions', {
# 	'contractsTable': [
# 		{field: "id_estado", title: "Estado", show: true, class: ''}
# 		{field: "comercial", title: "Comercial", show: true, class: ''}
# 		{field: "email", title: "E-mail", show: true, class: ''}
# 		{field: "fecha_firma", title: "F.Firma", show: true, class: ''}
# 		{field: "tipoContrato", title: "Tipo Contrato", show: false, class: ''}
# 		{field: "gerencia", title: "Equipo", show: true, class: ''}
# 		{field: "id_contrato", title: "Identificador", show: true, class: ''}
# 		{field: "producto", title: "Producto", show: true, class: ''}
# 		{field: "tlf_fijo", title: "Telf. Fijo", show: false, class: ''}
# 		{field: "tlf_movil", title: "Telf. Móvil", show: false, class: ''}
# 	],
# }
