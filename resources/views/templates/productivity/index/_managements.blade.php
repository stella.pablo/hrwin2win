<div layout="column" layout-padding ng-show="(filters.organization_id || user.company.database != 'mexico') && !filters.management_id">
	<table
		class="row-border hover full-width small padding-0 table-mancamp dataTable"
	>
		<thead>
		<tr>
			<th>Gerencia</th>
			<th class="text-center" colspan="[[ managementsCampaigns.length * 4 ]]">Campaña(s)</th>
			<th class="text-right">Com. Activos</th>
		</tr>
		</thead>
		<tbody>
		<tr>
			<td></td>
			<td
				class="text-center font-weight-700 clickable"
				ng-class="{'mdc-bg-grey-100': $even, 'mdc-bg-grey-200': $odd}"
				ng-click="filterByCampaign(mCamp)"
				ng-repeat="mCamp in managementsCampaigns"
				colspan="4">
				[[ mCamp ]]
			</td>
		</tr>
		<tr>
			<td></td>
			<td class="text-center" ng-class="{'mdc-bg-grey-100': $even, 'mdc-bg-grey-200': $odd}" ng-repeat-start="mCamp in managementsCampaigns">
				<em>
					<small>C. OK</small>
				</em>
			</td>
			<td class="text-center" ng-class="{'mdc-bg-grey-100': $even, 'mdc-bg-grey-200': $odd}">
				<em>
					<small>C. P.</small>
				</em>
			</td>
			<td class="text-center" ng-class="{'mdc-bg-grey-100': $even, 'mdc-bg-grey-200': $odd}">
				<em>
					<small>C. KO</small>
				</em>
			</td>
			<td class="text-center" ng-class="{'mdc-bg-grey-100': $even, 'mdc-bg-grey-200': $odd}" ng-repeat-end>
				<em>
					<small>C. TOTAL</small>
				</em>
			</td>
		</tr>
		<tr ng-repeat="management in managements" class="clickable" ng-click="filters.management_id = management.id_gerencia">
			<td>[[ management.gerencia ]]</td>
			<td class="text-center" ng-class="{'mdc-bg-grey-100': $even, 'mdc-bg-grey-200': $odd}" ng-repeat-start="mCamp in managementsCampaigns">[[ management.campaigns[mCamp].c_ok ]]</td>
			<td class="text-center" ng-class="{'mdc-bg-grey-100': $even, 'mdc-bg-grey-200': $odd}">[[ management.campaigns[mCamp].c_p ]]</td>
			<td class="text-center" ng-class="{'mdc-bg-grey-100': $even, 'mdc-bg-grey-200': $odd}">[[ management.campaigns[mCamp].c_ko ]]</td>
			<td class="text-center" ng-class="{'mdc-bg-grey-100': $even, 'mdc-bg-grey-200': $odd}" ng-repeat-end>[[ management.campaigns[mCamp].c_total ]]</td>
			<td class="text-right">[[ management.commercials_count ]]</td>
		</tr>
		</tbody>
	</table>

	<div layout="column">
		<div flex>
			<highchart id="chart-contractsPerMng" config="chartsConfig.contractsPerMng"></highchart>
		</div>

		<div flex="33">
			<highchart id="chart-typesOfObjective" config="chartsConfig.campaigns"></highchart>
		</div>
	</div>
</div>
