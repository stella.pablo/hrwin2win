<div layout="column" layout-padding ng-hide="filters.organization_id || user.company.database != 'mexico'">
	<table
		datatable="ng"
		dt-instance="tableOrganizations.dtInstanceCallback"
		dt-options="tableOrganizations.dtOptions"
		dt-disable-deep-watchers="true"
		class="row-border hover full-width small padding-0 table-mancamp"
	>
		<thead>
		<tr>
			<th>Organización</th>
			<th class="text-center">C. OK</th>
			<th class="text-center">C. P</th>
			<th class="text-center">C. KO</th>
			<th class="text-center">C. TOTAL</th>
			<th class="text-right">GERENCIAS</th>
			<th class="text-right">COMERCIALES</th>
		</tr>
		</thead>
		<tbody>
		<tr ng-repeat="organization in organizations" class="clickable" ng-click="filters.organization_id = organization.id">
			<td>[[ organization.organizacion ]]</td>
			<td class="text-center">[[ organization.stats.c_ok ]]</td>
			<td class="text-center">[[ organization.stats.c_p ]]</td>
			<td class="text-center">[[ organization.stats.c_ko ]]</td>
			<td class="text-center">[[ organization.stats.c_total ]]</td>
			<td class="text-right">[[ organization.stats.managements ]]</td>
			<td class="text-right">[[ organization.stats.commercials ]]</td>
		</tr>
		</tbody>
		<tfoot>
		<tr ng-show="organizations.length">
			<th>TOTAL:</th>
			<th class="text-center">[[ sumBy(organizations, 'c_ok') ]]</th>
			<th class="text-center">[[ sumBy(organizations, 'c_p') ]]</th>
			<th class="text-center">[[ sumBy(organizations, 'c_ko') ]]</th>
			<th class="text-center">[[ sumBy(organizations, 'c_total') ]]</th>
			<th class="text-right">[[ sumBy(organizations, 'managements') ]]</th>
			<th class="text-right">[[ sumBy(organizations, 'commercials') ]]</th>
		</tr>
		</tfoot>
	</table>

	<div layout="column">
		<div flex>
			<highchart id="chart-contractsPerOrg" config="chartsConfig.contractsPerOrg"></highchart>
		</div>

		<div flex>
			<highchart id="chart-contractsAvgPerOrg" config="chartsConfig.contractsAvgPerOrg"></highchart>
		</div>
	</div>
</div>
