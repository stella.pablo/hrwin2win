<h2>Producción por gerencia</h2>

<table datatable="ng2" dt-options="tableManagements.dtOptions" dt-instance="tableManagements.dtInstanceCallback" class="row-border hover full-width small padding-0 table-mancamp">
	<thead>
	<tr>
		<th>Gerencia</th>
		<th class="text-center" colspan="[[ managementsCampaigns.length * 3 ]]">Campaña(s)</th>
	</tr>
	</thead>
	<tbody>
	<tr>
		<td></td>
		<td
			class="text-center font-weight-700 clickable"
			ng-class="{'mdc-bg-grey-100': $even, 'mdc-bg-grey-200': $odd}"
			ng-click="filterByCampaign(mCamp)"
			ng-repeat="mCamp in managementsCampaigns"
			colspan="3">
			[[ mCamp ]]
		</td>
	</tr>
	<tr>
		<td></td>
		<td class="text-center" ng-class="{'mdc-bg-grey-100': $even, 'mdc-bg-grey-200': $odd}" ng-repeat-start="mCamp in managementsCampaigns"><em>
				<small>C. OK</small>
			</em></td>
		<td class="text-center" ng-class="{'mdc-bg-grey-100': $even, 'mdc-bg-grey-200': $odd}"><em>
				<small>C. P.</small>
			</em></td>
		<td class="text-center" ng-class="{'mdc-bg-grey-100': $even, 'mdc-bg-grey-200': $odd}" ng-repeat-end><em>
				<small>C. KO</small>
			</em></td>
	</tr>
	<tr ng-repeat="management in managements" class="clickable" ng-click="filterByManagement(management.id_gerencia)">
		<td>[[ management.gerencia ]]</td>
		<td class="text-center" ng-class="{'mdc-bg-grey-100': $even, 'mdc-bg-grey-200': $odd}" ng-repeat-start="mCamp in managementsCampaigns">[[ management.campaigns[mCamp].c_ok ]]</td>
		<td class="text-center" ng-class="{'mdc-bg-grey-100': $even, 'mdc-bg-grey-200': $odd}">[[ management.campaigns[mCamp].c_p ]]</td>
		<td class="text-center" ng-class="{'mdc-bg-grey-100': $even, 'mdc-bg-grey-200': $odd}" ng-repeat-end>[[ management.campaigns[mCamp].c_ko ]]</td>
	</tr>
	</tbody>
</table>

<h2>Producción por comercial</h2>

<table datatable="ng" dt-options="tableCommercials.dtOptions" dt-instance="tableCommercials.dtInstanceCallback" class="row-border hover full-width small">
	<thead>
		<tr>
			<th></th>
			<th>ID Ext.</th>
			<th>Nombre</th>
			<th>Gerencia</th>
			<th>Campaña(s)</th>
			<th>Objetivo %</th>
			<th>C. OK</th>
			<th>C. PEND</th>
			<th>C. KO</th>
			<th>C. TOTAL</th>
			<th>Previsión</th>
			<th>Predicción</th>
		</tr>
	</thead>
	<tbody>
		<tr ng-repeat="commercial in commercials" class="clickable">
			<td ui-sref="commercials.show({ id_comercial: commercial.id_comercial })" class="text-center">
				<div class="productivity-circle" ng-class="{
					'mdc-bg-red-300': commercial.contratos_adiadehoy_porcentaje < 50,
					'mdc-bg-orange-300': commercial.contratos_adiadehoy_porcentaje >= 50 && commercial.contratos_adiadehoy_porcentaje < 80,
					'mdc-bg-green-300': commercial.contratos_adiadehoy_porcentaje >= 80
					}"
				></div>
			</td>
			<td ui-sref="commercials.show({ id_comercial: commercial.id_comercial })">[[ commercial.id_comercial_externo ]]</td>
			<td ui-sref="commercials.show({ id_comercial: commercial.id_comercial })">[[ commercial.nombre_completo ]]</td>
			<td ui-sref="commercials.show({ id_comercial: commercial.id_comercial })">[[ commercial.management.gerencia ]]</td>
			<td ui-sref="commercials.show({ id_comercial: commercial.id_comercial })">
				<span ng-repeat="campaign in commercial.campaigns">
					[[ campaign.abreviatura ]]<span ng-hide="$last">, </span>
				</span>
			</td>
			<td ui-sref="commercials.show({ id_comercial: commercial.id_comercial })" class="text-center">[[ commercial.contratos_adiadehoy_porcentaje ]]%</td>
			<td ui-sref="commercials.show({ id_comercial: commercial.id_comercial })" class="text-center">[[ commercial.numero_contratos_ok ]]</td>
			<td ui-sref="commercials.show({ id_comercial: commercial.id_comercial })" class="text-center">[[ commercial.numero_contratos_p ]]</td>
			<td ui-sref="commercials.show({ id_comercial: commercial.id_comercial })" class="text-center">[[ commercial.numero_contratos_ko ]]</td>
			<td ui-sref="commercials.show({ id_comercial: commercial.id_comercial })" class="text-center">[[ commercial.numero_contratos_total ]]</td>
			<td ui-sref="commercials.show({ id_comercial: commercial.id_comercial })" class="text-center">[[ commercial.contratos_prevision ]]</td>
			<td ui-sref="commercials.show({ id_comercial: commercial.id_comercial })" class="text-center">
				<div ng-show="commercial.contratos_prevision_porcentaje < 75">
					<md-icon md-font-icon="mdi mdi-arrow-bottom-right" class="mdc-text-red-300"></md-icon>
				</div>

				<div ng-show="commercial.contratos_prevision_porcentaje >= 75 && commercial.contratos_prevision_porcentaje < 95">
					<md-icon md-font-icon="mdi mdi-minus" class="mdc-text-orange-300"></md-icon>
				</div>

				<div ng-show="commercial.contratos_prevision_porcentaje >= 95">
					<md-icon md-font-icon="mdi mdi-arrow-top-right" class="mdc-text-green-300"></md-icon>
				</div>
			</td>
		</tr>
	</tbody>
</table>
