<table class="custom" ng-show="view == 'all'">
	<tbody>
	<tr>
		<td class="md-title" style="width: 1%; white-space: nowrap;">Gerencia</td>
		<td class="md-title" style="width: 1%; white-space: nowrap;">Gerente</td>
		<td colspan="15" class="md-title text-center">Campañas</td>
	</tr>
	<tr>
		<td></td>
		<td></td>
		<td colspan="3" ng-repeat="campaign in campaigns" class="font-weight-700 text-center" ng-class="{'mdc-bg-grey-50': $even, 'mdc-bg-grey-100': $odd}">
			[[ campaign.abreviatura ]]
		</td>
	</tr>
	<tr>
		<td></td>
		<td></td>
		<td class="text-center mdc-bg-grey-50">C.OK</td>
		<td class="text-center mdc-bg-grey-50">C.P</td>
		<td class="text-center mdc-bg-grey-50">C.KO</td>
		<td class="text-center mdc-bg-grey-100">C.OK</td>
		<td class="text-center mdc-bg-grey-100">C.P</td>
		<td class="text-center mdc-bg-grey-100">C.KO</td>
		<td class="text-center mdc-bg-grey-50">C.OK</td>
		<td class="text-center mdc-bg-grey-50">C.P</td>
		<td class="text-center mdc-bg-grey-50">C.KO</td>
		<td class="text-center mdc-bg-grey-100">C.OK</td>
		<td class="text-center mdc-bg-grey-100">C.P</td>
		<td class="text-center mdc-bg-grey-100">C.KO</td>
		<td class="text-center mdc-bg-grey-50">C.OK</td>
		<td class="text-center mdc-bg-grey-50">C.P</td>
		<td class="text-center mdc-bg-grey-50">C.KO</td>
	</tr>
	<tr ng-repeat="management in productivity">
		<td style="width: 1%; white-space: nowrap;">[[ management.gerencia ]]</td>
		<td style="width: 1%; white-space: nowrap;">[[ management.manager.nombre_completo ]]</td>
		<td class="text-center mdc-bg-grey-50">[[ management.campaigns[0].c_ok ]]</td>
		<td class="text-center mdc-bg-grey-50">[[ management.campaigns[0].c_p ]]</td>
		<td class="text-center mdc-bg-grey-50">[[ management.campaigns[0].c_ko ]]</td>
		<td class="text-center mdc-bg-grey-100">[[ management.campaigns[1].c_ok ]]</td>
		<td class="text-center mdc-bg-grey-100">[[ management.campaigns[1].c_p ]]</td>
		<td class="text-center mdc-bg-grey-100">[[ management.campaigns[1].c_ko ]]</td>
		<td class="text-center mdc-bg-grey-50">[[ management.campaigns[2].c_ok ]]</td>
		<td class="text-center mdc-bg-grey-50">[[ management.campaigns[2].c_p ]]</td>
		<td class="text-center mdc-bg-grey-50">[[ management.campaigns[2].c_ko ]]</td>
		<td class="text-center mdc-bg-grey-100">[[ management.campaigns[3].c_ok ]]</td>
		<td class="text-center mdc-bg-grey-100">[[ management.campaigns[3].c_p ]]</td>
		<td class="text-center mdc-bg-grey-100">[[ management.campaigns[3].c_ko ]]</td>
		<td class="text-center mdc-bg-grey-50">[[ management.campaigns[4].c_ok ]]</td>
		<td class="text-center mdc-bg-grey-50">[[ management.campaigns[4].c_p ]]</td>
		<td class="text-center mdc-bg-grey-50">[[ management.campaigns[4].c_ko ]]</td>
	</tr>
	</tbody>
</table>
