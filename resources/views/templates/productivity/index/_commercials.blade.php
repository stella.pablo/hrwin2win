<div layout="column" layout-padding ng-show="filters.management_id">
	<table
		datatable="ng"
		dt-instance="tableCommercials.dtInstanceCallback"
		dt-options="tableCommercials.dtOptions"
		dt-disable-deep-watchers="true"
		class="row-border hover full-width small padding-0 table-mancamp"
	>
		<thead>
		<tr>
			<th></th>
			<th>ID Ext.</th>
			<th>Nombre</th>
			<th>Gerencia</th>
			<th class="text-center">Campaña(s)</th>
			<th>Objetivo %</th>
			<th>C. OK</th>
			<th>C. PEND</th>
			<th>C. KO</th>
			<th>C. TOTAL</th>
			<th>Previsión</th>
			<th>Predicción</th>
		</tr>
		</thead>
		<tbody>
		<tr ng-repeat="commercial in commercials" class="clickable" ui-sref="commercials.show({ id_comercial: commercial.id_comercial })">
			<td class="text-center">
				<div class="productivity-circle" ng-class="{
					'mdc-bg-red-300': commercial.contratos_adiadehoy_porcentaje < 50,
					'mdc-bg-orange-300': commercial.contratos_adiadehoy_porcentaje >= 50 && commercial.contratos_adiadehoy_porcentaje < 80,
					'mdc-bg-green-300': commercial.contratos_adiadehoy_porcentaje >= 80
					}"
				></div>
			</td>
			<td>[[ commercial.id_comercial_externo ]]</td>
			<td>[[ commercial.nombre_completo ]]</td>
			<td>[[ commercial.management.gerencia ]]</td>
			<td class="text-center">
				<span ng-repeat="campaign in commercial.campaigns">
					[[ campaign.abreviatura ]]<span ng-hide="$last">, </span>
				</span>
			</td>
			<td class="text-center">[[ commercial.contratos_adiadehoy_porcentaje ]]%</td>
			<td class="text-center">[[ commercial.numero_contratos_ok ]]</td>
			<td class="text-center">[[ commercial.numero_contratos_p ]]</td>
			<td class="text-center">[[ commercial.numero_contratos_ko ]]</td>
			<td class="text-center">[[ commercial.numero_contratos_total ]]</td>
			<td class="text-center">[[ commercial.contratos_prevision ]]</td>
			<td class="text-center">
				<div ng-show="commercial.contratos_prevision_porcentaje < 75">
					<md-icon md-font-icon="mdi mdi-arrow-bottom-right" class="mdc-text-red-300"></md-icon>
				</div>

				<div ng-show="commercial.contratos_prevision_porcentaje >= 75 && commercial.contratos_prevision_porcentaje < 95">
					<md-icon md-font-icon="mdi mdi-minus" class="mdc-text-orange-300"></md-icon>
				</div>

				<div ng-show="commercial.contratos_prevision_porcentaje >= 95">
					<md-icon md-font-icon="mdi mdi-arrow-top-right" class="mdc-text-green-300"></md-icon>
				</div>
			</td>
		</tr>
		</tbody>
		<tfoot>
		<tr>
			<th></th>
			<th>TOTAL:</th>
			<th></th>
			<th></th>
			<th></th>
			<th></th>

			<th class="text-center">[[ sumByCom(commercials, 'numero_contratos_ok') ]]</th>
			<th class="text-center">[[ sumByCom(commercials, 'numero_contratos_p') ]]</th>
			<th class="text-center">[[ sumByCom(commercials, 'numero_contratos_ko') ]]</th>
			<th class="text-center">[[ sumByCom(commercials, 'numero_contratos_total') ]]</th>
			<th class="text-center">[[ sumByCom(commercials, 'contratos_prevision') ]]</th>

			<th></th>
		</tr>
		</tfoot>
	</table>


	<div layout="column" layout-gt-sm="row">
		<div flex-gt-sm="33">
			<highchart id="chart-typesOfObjective" config="chartsConfig.typesOfObjective"></highchart>
		</div>

		<div flex-gt-sm="66">
			<chart-commercials stats="stats"></chart-commercials>
		</div>
	</div>


</div>
