<md-toolbar id="top-toolbar" class="md-menu-toolbar">
	<div class="md-toolbar-tools">
		<h1>Productividad</h1>
	</div>
</md-toolbar>

<md-content layout="column" layout-margin>
	<div layout="column" md-whiteframe="2">
		<div layout="column" layout-align="center center" class="mdc-bg-grey-100 padding-15">
			<div id="filters" layout="row" layout-align="center center" layout-margin layout-wrap>
				<md-input-container class="input-no-errors" ng-show="productivityType == 'productivitylow'">
					<md-datepicker ng-model="filters.date" md-placeholder="Fecha"></md-datepicker>
				</md-input-container>

				<md-input-container class="input-no-errors" ng-hide="productivityType == 'productivitylow'">
					<md-datepicker ng-model="filters.date_from" md-placeholder="Fecha desde"></md-datepicker>
				</md-input-container>

				<md-input-container class="input-no-errors" ng-hide="productivityType == 'productivitylow'">
					<md-datepicker ng-model="filters.date_to" md-placeholder="Fecha hasta"></md-datepicker>
				</md-input-container>

				@if(auth()->user()->level < 3)
					<md-input-container class="input-no-errors">
						<label>Gerencia</label>
						<md-select ng-model="filters.manager">
							<md-option ng-value=""><em>Todos</em></md-option>
							<md-option ng-value="manager.id_gerencia" ng-repeat="manager in managers | orderBy:'gerencia'">[[ manager.gerencia ]]</md-option>
						</md-select>
					</md-input-container>
				@endif

				<md-input-container class="input-no-errors">
					<label>Campaña</label>
					<md-select ng-model="filters.campaign">
						<md-option ng-value=""><em>Todas</em></md-option>
						<md-option ng-value="campanya.abreviatura" ng-repeat="campanya in campanyas | orderBy:'nombre'">[[ campanya.nombre ]]</md-option>
					</md-select>
				</md-input-container>
			</div>

			{{-- <div layout="row" layout-align="center center"  ng-show="productivityType === 'productivity'">
				<div layout="row" layout-align="center center" class="margin-right-20" style="min-width: 300px;">
					<span style="white-space: nowrap;" class="margin-right-10">Objetivo %</span>
					<rzslider rz-slider-model="sliders.objective.value" rz-slider-options="sliders.objective.options"></rzslider>
				</div>
				<div layout="row" layout-align="center center" class="margin-right-20" style="min-width: 300px;">
					<span style="white-space: nowrap;" class="margin-right-10">Contratos OK</span>
					<rzslider rz-slider-model="sliders.contractsOK.minValue" rz-slider-high="sliders.contractsOK.maxValue" rz-slider-options="sliders.contractsOK.options"></rzslider>
				</div>
			</div>
			<div layout="row" layout-align="center center"  ng-show="productivityType === 'productivity'">
				<div layout="row" layout-align="center center" class="margin-right-20" style="min-width: 300px;">
					<span style="white-space: nowrap;" class="margin-right-10">Contratos P</span>
					<rzslider rz-slider-model="sliders.contractsP.minValue" rz-slider-high="sliders.contractsP.maxValue" rz-slider-options="sliders.contractsP.options"></rzslider>
				</div>
				<div layout="row" layout-align="center center" class="margin-right-20" style="min-width: 300px;">
					<span style="white-space: nowrap;" class="margin-right-10">Contratos KO</span>
					<rzslider rz-slider-model="sliders.contractsKO.minValue" rz-slider-high="sliders.contractsKO.maxValue" rz-slider-options="sliders.contractsKO.options"></rzslider>
				</div>
			</div> --}}

			<div layout="row" layout-align="center center">
				<md-button class="md-accent" ng-click="filter()">
					<md-icon md-font-icon="mdi mdi-filter"></md-icon>
					Filtrar
				</md-button>

				<md-button ng-click="clearFilters()">
					<md-icon md-font-icon="mdi mdi-filter-remove"></md-icon>
					Limpiar filtros
				</md-button>
			</div>
		</div>

		<md-content layout="column" layout-padding class="mdc-bg-white">
			@include('templates.productivity.index.table')
		</md-content>

		<div layout="row">
			<div flex>
				<highchart id="chart-typesOfObjective" config="chartsConfig.typesOfObjective"></highchart>
			</div>

			<div flex>
				<highchart id="chart-typesOfObjective" config="chartsConfig.campaigns"></highchart>
			</div>
		</div>

		{{--		<div layout="column" ng-if="false">--}}
		{{--			<highchart id="chart-productivity-range" config="chartsConfig.productivityRange"></highchart>--}}
		{{--		</div>--}}
	</div>

	<div layout="column" md-whiteframe="2" class="mdc-bg-white">
		<div layout="column" layout-padding>
			<h2>Acumulación anual</h2>
		</div>

		<div layout="row">
			<div flex>
				<chart-commercials-productivity stats="stats"></chart-commercials-productivity>
			</div>

			<div flex>
				<chart-commercials-contracts stats="stats"></chart-commercials-contracts>
			</div>
		</div>

		<chart-commercials stats="stats"></chart-commercials>
	</div>
</md-content>
