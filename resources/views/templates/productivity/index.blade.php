<md-toolbar id="top-toolbar" class="md-menu-toolbar">
	<div class="md-toolbar-tools">
		<md-button class="md-icon-button" ng-click="hiddenFilters = !hiddenFilters" hide-gt-xs>
			<md-icon md-font-icon="mdi mdi-filter"></md-icon>
		</md-button>

		<h2 flex md-truncate>
			Productividad<span ng-show="filters.organization_id">:</span>

			<strong ng-show="filters.organization_id">[[ organization.organizacion ]]</strong>
			<md-icon md-font-icon="mdi mdi-chevron-right" ng-show="filters.management_id"></md-icon>
			<strong ng-show="filters.management_id">
				[[ management.gerencia ]]
			</strong>
		</h2>
	</div>
</md-toolbar>

<md-content layout="column" layout-margin>
	<div layout="column" layout-align="center center" class="padding-15" ng-hide="$mdMedia('xs') && hiddenFilters">
		<div id="filters" layout="row" layout-align="center center" layout-margin layout-wrap>
			<md-input-container class="input-no-errors" flex-xs>
				<label>Fecha desde</label>
				<md-datepicker ng-model="filters.date_from" md-max-date="filters.date_to"></md-datepicker>
			</md-input-container>

			<md-input-container class="input-no-errors" flex-xs>
				<label>Fecha hasta</label>
				<md-datepicker ng-model="filters.date_to" md-min-date="filters.date_from"></md-datepicker>
			</md-input-container>

			@if(auth()->user()->level < 3)
				<md-input-container class="input-no-errors" flex-xs ng-show="user.company.database === 'mexico'">
					<label>Organización</label>
					<md-select ng-model="filters.organization_id">
						<md-option ng-value=""><em>Todas</em></md-option>
						<md-option ng-value="organization.id" ng-repeat="organization in organizations">[[ organization.organizacion ]]</md-option>
					</md-select>
				</md-input-container>

				<md-input-container class="input-no-errors" flex-xs ng-show="filters.organization_id || user.company.database != 'mexico'">
					<label>Gerencia</label>
					<md-select ng-model="filters.management_id">
						<md-option ng-value=""><em>Todas</em></md-option>
						<md-option ng-value="management.id_gerencia" ng-repeat="management in managements">[[ management.gerencia ]]</md-option>
					</md-select>
				</md-input-container>
			@endif

			<md-input-container class="input-no-errors" flex-xs ng-show="filters.organization_id">
				<label>Campaña</label>
				<md-select ng-model="filters.campaign_id">
					<md-option ng-value=""><em>Todas</em></md-option>
					<md-option ng-value="campaign.abreviatura" ng-repeat="campaign in campaigns | orderBy:'nombre'">[[ campaign.nombre ]]</md-option>
				</md-select>
			</md-input-container>

			<div>
				<md-button ng-click="clearFilters()">
					<md-icon md-font-icon="mdi mdi-filter-remove"></md-icon>
					Limpiar filtros
				</md-button>
			</div>
		</div>
	</div>

	<div layout="column" md-whiteframe="2">
		<md-content layout="column" layout-padding class="mdc-bg-white">
			@include('templates.productivity.index._organizations')
			@include('templates.productivity.index._managements')
			@include('templates.productivity.index._commercials')
		</md-content>
	</div>
</md-content>
