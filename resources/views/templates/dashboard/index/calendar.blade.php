<div layout="column" md-whiteframe="2" flex class="mdc-bg-green mdc-text-white-all">
	<md-toolbar class="mdc-bg-green">
		<div class="md-toolbar-tools">
			<h2 flex md-truncate>Tareas programadas</h2>

			<md-button class="md-icon-button" ng-click="">
				<md-icon md-font-icon="mdi mdi-plus"></md-icon>
			</md-button>
		</div>
	</md-toolbar>

	<md-content layout="column" layout-margin style="background-color: transparent;" flex>
		<div layout="column" flex>
			<div layout="row" layout-align="center center" ng-repeat="event in events" class="margin-5" ng-show="events.length">
				<md-icon md-font-icon="mdi mdi-calendar" class="margin-right-15"></md-icon>
				<div class="margin-0" flex>
					<strong class="margin-right-15">[[ todo.body ]]</strong>
					<span>[[ moment().day($index).hour($index).calendar() ]]</span>
				</div>
			</div>

			<div layout="column" layout-align="center center" ng-hide="events.length">
				<em>No tiene eventos programados</em>
			</div>
		</div>
	</md-content>
</div>
