<div layout="row" layout-margin class="margin-left-0 margin-right-0" style="border-bottom: 1px solid #e2e2e2;">
	<div layout="column" flex style="border-right: 3px solid #f5f5f5;">
		<md-content layout-padding>
			<div layout="row">
				<img src="/img/icons/shopper.png" class="margin-right-10">

				<div layout="column" layout-align="center start" flex>
					<h3 class="margin-0">Nuevos clientes</h3>

					<table class="full-width">
						<tbody>
							<tr>
								<td>Ayer:</td>
								<td layout="row">
									<span>7</span>
									<small class="mdc-text-light-green">(↑7%)</small>
								</td>
							</tr>
							<tr>
								<td>Esta semana:</td>
								<td layout="row">
									<span>23</span>
									<small class="mdc-text-red-300">(↓14%)</small>
								</td>
							</tr>
							<tr>
								<td>Este mes:</td>
								<td layout="row">
									<span>174</span>
									<small class="mdc-text-light-green">(↑32%)</small>
								</td>
							</tr>
							<tr>
								<td>Este año:</td>
								<td layout="row">
									<span>826</span>
									<small class="mdc-text-light-green">(↑89%)</small>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</md-content>
	</div>

	<div layout="column" flex style="border-right: 3px solid #f5f5f5;">
		<md-content layout-padding>
			<div layout="row">
				<img src="/img/icons/online-shop.png" class="margin-right-10">

				<div layout="column" layout-align="center start" flex>
					<h3 class="margin-0">Pedidos</h3>

					<table class="full-width">
						<tbody>
							<tr>
								<td>Ayer:</td>
								<td layout="row">
									<span>16</span>
									<small class="mdc-text-light-green">(↑17%)</small>
								</td>
							</tr>
							<tr>
								<td>Esta semana:</td>
								<td layout="row">
									<span>51</span>
									<small class="mdc-text-light-green">(↑6%)</small>
								</td>
							</tr>
							<tr>
								<td>Este mes:</td>
								<td layout="row">
									<span>113</span>
									<small class="mdc-text-red-300">(↓27%)</small>
								</td>
							</tr>
							<tr>
								<td>Este año:</td>
								<td layout="row">
									<span>542</span>
									<small class="mdc-text-light-green">(↑9%)</small>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</md-content>
	</div>

	<div layout="column" flex>
		<md-content layout-padding>
			<div layout="row">
				<img src="/img/icons/benefit.png" class="margin-right-10">

				<div layout="column" layout-align="center start" flex>
					<h3 class="margin-0">Ventas</h3>

					<table class="full-width">
						<tbody>
							<tr>
								<td>Ayer:</td>
								<td layout="row">
									<span>[[ 286 | currency ]]</span>
									<small class="mdc-text-red-300">(↓7%)</small>
								</td>
							</tr>
							<tr>
								<td>Esta semana:</td>
								<td layout="row">
									<span>[[ 1143 | currency ]]</span>
									<small class="mdc-text-light-green">(↑12%)</small>
								</td>
							</tr>
							<tr>
								<td>Este mes:</td>
								<td layout="row">
									<span>[[ 4962 | currency ]]</span>
									<small class="mdc-text-light-green">(↑30%)</small>
								</td>
							</tr>
							<tr>
								<td>Este año:</td>
								<td layout="row">
									<span>[[ 38516 | currency ]]</span>
									<small class="mdc-text-red-300">(↓11%)</small>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</md-content>
	</div>
</div>
