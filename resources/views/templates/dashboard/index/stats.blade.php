<div layout="row" layout-margin class="margin-left-0 margin-right-0" style="border-bottom: 1px solid #e2e2e2;">
	<div layout="column" flex style="border-right: 3px solid #f5f5f5;">
		<md-content layout-padding>
			<div layout="column" layout-align="center center">
				<h3 class="margin-top-0">Clientes</h3>
				<div class="c100 [[ 'p' + dashStats.clients_p ]]" ng-class="{'orange': dashStats.clients_p < 40, 'green': dashStats.clients_p > 75 }">
					<span style="top: 1.1em; line-height: 1em; width: 5em; font-size: 0.2em;">[[ dashStats.clients ]]<br><small>de</small><br>10</span>
					<div class="slice">
						<div class="bar"></div>
						<div class="fill"></div>
					</div>
				</div>
				<strong>Esta semana</strong>
			</div>
		</md-content>
	</div>

	<div layout="column" flex style="border-right: 3px solid #f5f5f5;">
		<md-content layout-padding>
			<div layout="column" layout-align="center center">
				<h3 class="margin-top-0">Visitas</h3>
				<div class="c100 [[ 'p' + dashStats.visits_p ]]" ng-class="{'orange': dashStats.visits_p < 40, 'green': dashStats.visits_p > 75 }">
					<span style="top: 1.1em; line-height: 1em; width: 5em; font-size: 0.2em;">[[ dashStats.visits ]]<br><small>de</small><br>10</span>
					<div class="slice">
						<div class="bar"></div>
						<div class="fill"></div>
					</div>
				</div>
				<strong>Esta semana</strong>
			</div>
		</md-content>
	</div>

	<div layout="column" flex style="border-right: 3px solid #f5f5f5;">
		<md-content layout-padding>
			<div layout="column" layout-align="center center">
				<h3 class="margin-top-0">Check Ins</h3>
				<div class="c100 [[ 'p' + dashStats.checkins_p ]]" ng-class="{'orange': dashStats.checkins_p < 40, 'green': dashStats.checkins_p > 75 }">
					<span style="top: 1.1em; line-height: 1em; width: 5em; font-size: 0.2em;">[[ dashStats.checkins ]]<br><small>de</small><br>10</span>
					<div class="slice">
						<div class="bar"></div>
						<div class="fill"></div>
					</div>
				</div>
				<strong>Esta semana</strong>
			</div>
		</md-content>
	</div>

	<div layout="column" flex style="border-right: 3px solid #f5f5f5;">
		<md-content layout-padding>
			<div layout="column" layout-align="center center">
				<h3 class="margin-top-0">Pedidos</h3>
				<div class="c100 [[ 'p' + dashStats.salesNum_p ]]" ng-class="{'orange': dashStats.salesNum_p < 40, 'green': dashStats.salesNum_p > 75 }">
					<span style="top: 1.1em; line-height: 1em; width: 5em; font-size: 0.2em;">[[ dashStats.salesNum ]]<br><small>de</small><br>30</span>
					<div class="slice">
						<div class="bar"></div>
						<div class="fill"></div>
					</div>
				</div>
				<strong>Esta semana</strong>
			</div>
		</md-content>
	</div>

	<div layout="column" flex>
		<md-content layout-padding>
			<div layout="column" layout-align="center center">
				<h3 class="margin-top-0">Ventas</h3>
				<div class="c100 [[ 'p' + dashStats.sales_p ]]" ng-class="{'orange': dashStats.sales_p < 40, 'green': dashStats.sales_p > 75 }">
					<span style="top: 2.3em; left: 1.3em; line-height: 1em; width: 5em; font-size: 0.13em;">[[ dashStats.sales | currency ]]<br><small>de</small><br>[[ '5000' | currency ]]</span>
					<div class="slice">
						<div class="bar"></div>
						<div class="fill"></div>
					</div>
				</div>
				<strong>Esta semana</strong>
			</div>
		</md-content>
	</div>
</div>
