<div layout="column" md-whiteframe="2" flex md-theme="default-dark" class="mdc-bg-orange mdc-text-white-all">
	<md-toolbar class="">
		<div class="md-toolbar-tools">
			<h2 flex md-truncate>Tareas rápidas</h2>
		</div>
	</md-toolbar>

	<md-content layout="column" layout-margin style="background-color: transparent;" flex>
		<div layout="column" flex>
			<div layout="row" ng-repeat="todo in todos" class="margin-5" ng-show="todos.length">
				<p class="margin-0" flex ng-class="{'line-through text-muted': todo.done}">[[ todo.title ]]</p>
				<md-icon md-font-icon="mdi mdi-close" ng-click="removeTodo(todo)" ng-hide="todo.done" class="clickable"></md-icon>
			</div>
		</div>

		<div layout="column" layout-align="center center" ng-hide="todos.length">
			<em>No tiene eventos programados</em>
		</div>

		<div layout="row" layout-align="center center">
			<md-input-container class="input-no-errors" flex>
				<md-icon md-font-icon="mdi mdi-comment"></md-icon>
				<input type="text" ng-model="todo" placeholder="Nueva tarea">
			</md-input-container>

			<div ng-show="todo">
				<md-button ng-click="saveTodo()">
					<md-icon md-font-icon="mdi mdi-content-save"></md-icon>
					Guardar
				</md-button>
			</div>
		</div>
	</md-content>
</div>
