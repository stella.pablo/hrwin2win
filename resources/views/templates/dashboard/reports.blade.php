<md-dialog style="width: 100%;">
	<md-dialog-content>
		<div class="md-dialog-content">
			<p>
				<strong>Ayúdanos a ser mas eficientes.</strong>
			</p>
			<p>Indicanos si tienes informacion sobre estas incidencias que te indicamos.</p>
		</div>

		<table datatable="ng" dt-instance="table.reports.dtInstanceCallback" dt-options="table.reports.dtOptions" dt-disable-deep-watchers="true" class="row-border hover full-width small">
			<thead>
				<tr>
					<th>Fecha</th>
					<th>ID</th>
					<th>Nombre</th>
					<th>Motivo</th>
					<th>Estado</th>
				</tr>
			</thead>
			<tbody>
				<tr ng-repeat="report in commercialsReports | filter:{show: true}" class="clickable">
					<td ui-sref="commercials.show({ id_comercial: report.commercial.id_comercial })">[[ moment(report.created_at).format('DD-MMM-YYYY') ]]</td>
					<td ui-sref="commercials.show({ id_comercial: report.commercial.id_comercial })">[[ report.commercial.id_comercial ]]</td>
					<td ui-sref="commercials.show({ id_comercial: report.commercial.id_comercial })">[[ report.commercial.nombre_completo ]]</td>
					<td>
						[[ report.motive.name ]]
					</td>
					<td>
						<select ng-model="report.status_id" ng-change="answerReport(report)">
							<option ng-value="status.id" ng-repeat="status in reports.statuses | orderBy:'name'">[[ status.name ]]</option>
						</select>
					</td>
				</tr>
			</tbody>
		</table>
	</md-dialog-content>

	<md-dialog-actions layout="row" layout-align="space-between center">
		<span></span>

		<md-button ng-click="hide()">
			<md-icon md-font-icon="mdi mdi-close"></md-icon>
			Cerrar
		</md-button>
	</md-dialog-actions>
</md-dialog>
