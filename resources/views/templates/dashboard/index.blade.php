<md-toolbar id="top-toolbar" class="md-menu-toolbar">
	<div class="md-toolbar-tools">
		<h1>Dashboard</h1>
	</div>
</md-toolbar>

<md-content>
	<div layout="column" layout-margin>
		<chart-candidates-stats></chart-candidates-stats>
	</div>

	{{--	@if(auth()->user()->level < 4)--}}
	{{--		<div layout="row" layout-margin>--}}
	{{--			<div layout="column" md-whiteframe="2" class="mdc-bg-white" flex>--}}
	{{--				<md-toolbar class="md-menu-toolbar">--}}
	{{--					<div class="md-toolbar-tools">--}}
	{{--						<h2 flex md-truncate>Últimos avisos</h2>--}}

	{{--						<md-button class="md-icon-button margin-0" ui-sref="commercialsreports">--}}
	{{--							<md-icon md-font-icon="mdi mdi-arrow-expand"></md-icon>--}}
	{{--						</md-button>--}}
	{{--					</div>--}}
	{{--				</md-toolbar>--}}

	{{--				<md-content class="mdc-bg-white">--}}
	{{--					<table datatable="ng" dt-instance="table.reports.dtInstanceCallback" dt-options="table.reports.dtOptions" dt-disable-deep-watchers="true" class="row-border hover full-width small">--}}
	{{--						<thead>--}}
	{{--							<tr>--}}
	{{--								<th>Fecha</th>--}}
	{{--								<th>ID</th>--}}
	{{--								<th>Nombre</th>--}}
	{{--								<th>Motivo</th>--}}
	{{--								<th>Estado</th>--}}
	{{--							</tr>--}}
	{{--						</thead>--}}
	{{--						<tbody>--}}
	{{--							<tr ng-repeat="report in commercialsReports" class="clickable">--}}
	{{--								<td ui-sref="commercials.show({ id_comercial: report.commercial.id_comercial })">[[ moment(report.created_at).format('DD-MMM-YYYY') ]]</td>--}}
	{{--								<td ui-sref="commercials.show({ id_comercial: report.commercial.id_comercial })">[[ report.commercial.id_comercial ]]</td>--}}
	{{--								<td ui-sref="commercials.show({ id_comercial: report.commercial.id_comercial })">[[ report.commercial.nombre_completo ]]</td>--}}
	{{--								<td>--}}
	{{--									[[ report.motive.name ]]--}}
	{{--								</td>--}}
	{{--								<td>--}}
	{{--									<select ng-model="report.status_id" ng-change="answerReport(report)">--}}
	{{--										<option ng-value="status.id" ng-repeat="status in reports.statuses | orderBy:'name'">[[ status.name ]]</option>--}}
	{{--									</select>--}}
	{{--								</td>--}}
	{{--							</tr>--}}
	{{--						</tbody>--}}
	{{--					</table>--}}
	{{--				</md-content>--}}
	{{--			</div>--}}
	{{--		</div>--}}
	{{--	@endif--}}

	@if(auth()->user()->level < 4)
		<div layout="column" layout-margin>
			<div layout="column" md-whiteframe="2" class="mdc-bg-white">
				<md-toolbar class="md-menu-toolbar">
					<div class="md-toolbar-tools">
						<h2 flex md-truncate>Acumulación este mes</h2>

						<div>
							<small class="mdc-bg-light-green-50 padding-5" style="border-radius: 5px;">
								OK: [[ statsThisMonthBadges.ok ]]
							</small>

							<small class="mdc-bg-amber-50 padding-5 margin-left-5" style="border-radius: 5px;">
								P: [[ statsThisMonthBadges.p ]]
							</small>

							<small class="mdc-bg-red-50 padding-5 margin-left-5" style="border-radius: 5px;">
								KO: [[ statsThisMonthBadges.ko ]]
							</small>

							<small class="mdc-bg-grey-50 padding-5 margin-left-5" style="border-radius: 5px;">
								TOTAL: [[ statsThisMonthBadges.total ]]
							</small>
						</div>
					</div>
				</md-toolbar>

				<chart-commercials-contracts-per-day stats="statsThisMonth"></chart-commercials-contracts-per-day>
			</div>

			<div layout="column" md-whiteframe="2" class="mdc-bg-white">
				<md-toolbar class="md-menu-toolbar">
					<div class="md-toolbar-tools">
						<h2 flex md-truncate>Acumulación anual</h2>
					</div>
				</md-toolbar>

				<div layout="column" layout-gt-sm="row">
					<div flex>
						<chart-commercials-productivity stats="stats"></chart-commercials-productivity>
					</div>

					<div flex>
						<chart-commercials-contracts stats="stats"></chart-commercials-contracts>
					</div>
				</div>

				<chart-commercials stats="stats"></chart-commercials>
			</div>
		</div>
	@endif

	<div layout="column" layout-gt-sm="row" layout-margin>
		<div layout="column" md-whiteframe="2" class="mdc-bg-white" flex>
			<md-toolbar class="md-menu-toolbar">
				<div class="md-toolbar-tools">
					<h2 flex md-truncate>Últimos candidatos</h2>

					<div>
						<md-button class="md-icon-button margin-0" aria-label="Add new candidate" ui-sref="candidates.create">
							<md-icon md-font-icon="mdi mdi-plus"></md-icon>
						</md-button>

						<md-button class="md-icon-button margin-0" ui-sref="candidates">
							<md-icon md-font-icon="mdi mdi-arrow-expand"></md-icon>
						</md-button>
					</div>
				</div>
			</md-toolbar>

			<md-content class="mdc-bg-white">
				<table datatable="ng" dt-instance="table.candidates.dtInstanceCallback" dt-options="table.candidates.dtOptions" dt-disable-deep-watchers="false" class="row-border hover full-width small">
					<thead>
						<tr>
							<th>Nombre</th>
							<th>F. Petición</th>
							<th>F. Inserción</th>
						</tr>
					</thead>
					<tbody>
						<tr ng-repeat="candidate in candidates" class="clickable" ui-sref="candidates.show({ id: candidate.id })">
							<td>[[ candidate.fullname ]]</td>
							<td>[[ candidate.petitioned_at ]]</td>
							<td>[[ candidate.created_at ]]</td>
						</tr>
					</tbody>
				</table>
			</md-content>
		</div>

		@if(auth()->user()->level < 5)
			<div layout="column" md-whiteframe="2" class="mdc-bg-white" flex>
				<md-toolbar class="md-menu-toolbar">
					<div class="md-toolbar-tools">
						<h2 flex md-truncate>Últimos comerciales</h2>

						<md-button class="md-icon-button margin-0" ui-sref="commercials">
							<md-icon md-font-icon="mdi mdi-arrow-expand"></md-icon>
						</md-button>
					</div>
				</md-toolbar>

				<md-content class="mdc-bg-white">
					<table datatable="ng" dt-instance="table.commercials.dtInstanceCallback" dt-options="table.commercials.dtOptions" dt-disable-deep-watchers="true" class="row-border hover full-width small">
						<thead>
							<tr>
								<th>ID Ext</th>
								<th>Nombre</th>
								<th>Gerencia</th>
								<th>Fecha Alta</th>
							</tr>
						</thead>
						<tbody>
						<tr ng-repeat="commercial in commercials" ui-sref="commercials.show({ id_comercial: commercial.id_comercial })" class="clickable">
							<td>[[ commercial.id_comercial_externo ]]</td>
							<td>[[ commercial.nombre_completo ]]</td>
							<td>[[ commercial.management.gerencia ]]</td>
							<td>[[ commercial.fecha_alta ]]</td>
							</tr>
						</tbody>
					</table>
				</md-content>
			</div>
		@endif
	</div>
</md-content>


{{-- <div layout="row" layout-margin>
	<div layout="column" md-whiteframe="2" flex class="mdc-bg-white">
		<md-toolbar class="md-menu-toolbar">
			<div class="md-toolbar-tools">
				<h2 flex md-truncate>Últimas actividades</h2>

				<md-button class="md-icon-button" aria-label="" ui-sref="activities">
					<md-icon md-font-icon="mdi mdi-arrow-expand"></md-icon>
				</md-button>
			</div>
		</md-toolbar>

		<md-content id="last-activities" class="mdc-bg-white">
			@include('templates.activities.index.timeline-dashboard')
		</md-content>
	</div>

	<div layout="column" md-whiteframe="2" flex class="mdc-bg-white">
		<md-toolbar class="md-menu-toolbar">
			<div class="md-toolbar-tools">
				<h2 flex md-truncate>Últimos pedidos</h2>

				<md-button class="md-icon-button" aria-label="" ui-sref="sales">
					<md-icon md-font-icon="mdi mdi-arrow-expand"></md-icon>
				</md-button>
			</div>
		</md-toolbar>

		<md-content id="last-sales" class="mdc-bg-white">
			<table datatable="" dt-options="table.dtOptions" dt-columns="table.dtColumns" dt-disable-deep-watchers="true" class="row-border hover full-width table-clickable"></table>
		</md-content>
	</div>
</div> --}}

{{-- <div layout="row" layout-margin>
	<div layout="column" md-whiteframe="2" flex class="mdc-bg-white" flex>
		<md-toolbar class="md-menu-toolbar">
			<div class="md-toolbar-tools">
				<h2 flex md-truncate>Análisis de pedidos</h2>
			</div>
		</md-toolbar>

		<md-content class="mdc-bg-white">
			<highchart id="chart-sales" config="chartsConfig.sales"></highchart>
		</md-content>
	</div>

	<div layout="column" md-whiteframe="2" class="mdc-bg-white" flex>
		<md-toolbar class="md-menu-toolbar">
			<div class="md-toolbar-tools">
				<h2 flex md-truncate>Productos más pedidos</h2>
			</div>
		</md-toolbar>

		<md-content class="mdc-bg-white">
			<highchart id="chart-products-popularity" config="chartsConfig.productsPopularity"></highchart>
		</md-content>
	</div>
</div> --}}
