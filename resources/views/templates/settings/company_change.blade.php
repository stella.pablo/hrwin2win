<md-dialog flex>
	<md-toolbar class="md-menu-toolbar">
		<div class="md-toolbar-tools">
			<h2>Cambiar de empresa</h2>

			<span flex></span>

			<md-button class="md-icon-button" ng-click="cancel()">
				<md-icon md-font-icon="mdi mdi-close" aria-label="Close dialog"></md-icon>
			</md-button>
		</div>
	</md-toolbar>

	<md-dialog-content>
		<md-list class="padding-0">
			<md-subheader class="md-no-sticky">[[ companies.length ]] empresas disponibles</md-subheader>
			<md-list-item ng-repeat="company in companies | orderBy:'name'" ng-click="setCompany(company)">
				<md-icon md-font-icon="mdi mdi-domain"></md-icon>
				<p>[[ company.name ]]</p>

				<md-icon md-font-icon="mdi mdi-check" ng-show="user.company_id === company.id"></md-icon>
			</md-list-item>
		</md-list>
	</md-dialog-content>
</md-dialog>
