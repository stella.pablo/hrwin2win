<div layout="row" layout-align="center center" layout-fill>
	<div>
		<md-icon md-font-icon="mdi mdi-account-circle" class="md-primary mdi-148px"></md-icon>
	</div>

	<div layout="column">
		<h1 class="margin-0 md-display-2">[[ user.name ]]</h1>
		<div class="alert alert-default padding-15">
			Código PIN: <strong style="background-color: #00c3b1; padding: 3px 3px 1px 3px; border-radius: 3px;">[[ user.pin ]]</strong>
		</div>
	</div>
</div>
