<md-content layout-padding class="mdc-bg-white">
	<form name="feesForm">
		<div layout="column" class="margin-10">
			<div layout="column" md-whiteframe="2" class="mdc-bg-white" flex>
				<md-toolbar class="md-menu-toolbar">
					<div class="md-toolbar-tools">
						<h2 flex md-truncate>Grupos de comisionamiento</h2>

						<md-button class="md-accent" ng-click="alterFeeGroup(null,null)">
							<md-icon md-font-icon="mdi mdi-plus"></md-icon>
							Añadir grupo de comisionamiento
						</md-button>
					</div>
				</md-toolbar>

				<md-content class="mdc-bg-white">
					<table datatable="ng" dt-instance="table.feeGroups.dtInstanceCallback" dt-options="table.feeGroups.dtOptions" dt-disable-deep-watchers="true"
					       class="row-border hover full-width small">
							<thead>
						<th>#</th>
						<th>Descripcion</th>
						<th>Tipo grupo</th>
						<th>Fecha inicio</th>
						<th>Fecha fin</th>

						<th>Editar</th>
						<th>Eliminar</th>
						</tr>
						</thead>
						<tbody>
						<tr ng-repeat="fee in feeGroups" class="clickable">
							<td>[[fee.id]]</td>
							<td>[[fee.nombre_grupo]]</td>
                            <td>[[fee.id_tipo == 1 ? 'Producto' : fee.id_tipo == 2 ? 'Artículo' : 'Ambos' ]]</td>
							<td>[[fee.fecha_desde]]</td>
							<td>[[fee.fecha_hasta]]</td>
							<td><a style="color:lightblue" ng-click="alterFeeGroup(fee.id,$index)"> Editar </a></td>
							<td><a style="color:lightcoral" ng-click="deleteFeeGroup(fee.id,fee.nombre_grupo)"> Eliminar </a></td>
						</tr>
						</tbody>
					</table>
				</md-content>
			</div>
		</div>
	</form>
</md-content>
