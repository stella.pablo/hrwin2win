<md-content layout-padding class="mdc-bg-white">
	<form name="productsForm">
		<div layout="column" class="margin-10">
			<div layout="column" md-whiteframe="2" class="mdc-bg-white" flex>
				<md-toolbar class="md-menu-toolbar">
					<div class="md-toolbar-tools">
						<h2 flex md-truncate>Productos</h2>

						<md-button class="md-accent" ng-click="alterProducts(null,null)">
							<md-icon md-font-icon="mdi mdi-plus"></md-icon>
							Añadir producto
						</md-button>
					</div>
				</md-toolbar>

				<md-content class="mdc-bg-white">
					<table datatable="ng" dt-instance="table.products.dtInstanceCallback" dt-options="table.products.dtOptions" dt-disable-deep-watchers="true"
					       class="row-border hover full-width small">
						<thead>
						<th>#</th>
						<th>Descripcion</th>
						<th>Tipo contrato</th>
						<th>Tipo producto</th>
						<th>Máximo líneas</th>
						<th>Activo</th>

						<th>Editar</th>
						<th>Eliminar</th>
						</tr>
						</thead>
						<tbody>
						<tr ng-repeat="product in products" class="clickable">
							<td>[[product.id_producto]]</td>
							<td>[[product.producto]]</td>

							<td>[[product.tipo_contrato == 'H' ? 'Hogar' : product.tipo_contrato == 'N' ? 'Negocio' : 'Ambos' ]]</td>
							<td>[[product.id_tipo_producto == 1 ? 'Línea' : product.id_tipo_producto==2 ? 'Solo internet' :producto.id_tipo_producto ==3 ? 'Entretenimiento'  : 'Gastos de instalación'	]]</td>
							<td>[[product.maximo_lineas]]</td>
							<td>[[product.activo == 'S' ? 'Si' : 'No']]</td>



							<td><a style="color:lightblue" ng-click="alterProducts(product.id_producto,$index)"> Editar </a></td>
							<td><a style="color:lightcoral" ng-click="deleteProduct(product.id_producto,product.id_tramo)"> Eliminar </a></td>
						</tr>
						</tbody>
					</table>
				</md-content>
			</div>
		</div>
	</form>
</md-content>
