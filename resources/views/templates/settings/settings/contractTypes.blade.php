<md-content layout-padding class="mdc-bg-white">
	<form name="contractsTypesForm">
		<div layout="column" class="margin-10">
			<div layout="column" md-whiteframe="2" class="mdc-bg-white" flex>
				<md-toolbar class="md-menu-toolbar">
					<div class="md-toolbar-tools">
						<h2 flex md-truncate>Tipos de contratacion</h2>

						<md-button class="md-accent" ng-click="alterContractsType(null,null)">
							<md-icon md-font-icon="mdi mdi-plus"></md-icon>
							Añadir tipo de contrato
						</md-button>
					</div>
				</md-toolbar>

				<md-content class="mdc-bg-white">
					<table datatable="ng" dt-instance="table.contracts.dtInstanceCallback" dt-options="table.contracts.dtOptions" dt-disable-deep-watchers="true"
					       class="row-border hover full-width small">
						<thead>
						<th>#</th>
						<th>Descripcion</th>
						<th>Tipo iva</th>
						<th>Tipo irpf</th>
						<th>Editar</th>
						<th>Eliminar</th>
						</tr>
						</thead>
						<tbody>
						<tr ng-repeat="contract in contractsTypes" class="clickable">
							<td>[[contract.id_tipo_contrato_comercial]]</td>
							<td>[[contract.tipo_contrato_comercial]]</td>
							<td>[[contract.iva ? contract.iva.descripcion   : 'No indicado']]</td>
							<td>[[contract.irpf ? contract.irpf.descripcion : 'No indicado' ]]</td>
							<td><a style="color:lightblue" ng-click="alterContractsType(contract.id_tipo_contrato_comercial,$index)"> Editar </a></td>
							<td><a style="color:lightcoral" ng-click="deleteContractsType(contract.id_tipo_contrato_comercial,contract.tipo_contrato_comercial)"> Eliminar </a></td>
						</tr>
						</tbody>
					</table>
				</md-content>
			</div>
		</div>
	</form>
</md-content>
