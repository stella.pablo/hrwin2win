<md-content class="md-padding mdc-bg-white">
	<table>
		<thead>
			<tr>
				<th>
					<h3>Tipos de documentos (para contratos)</h3>
				</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>
					<md-input-container class="input-no-errors margin-0 margin-right-15">
						<input type="text" ng-model="newFileContractType.tipo_fichero" placeholder="Añadir nuevo" aria-label="input">
					</md-input-container>

					<md-checkbox ng-model="newFileContractType.obligatorio" ng-true-value="1" ng-false-value="0">Obligatorio</md-checkbox>
				</td>
				<td style="width: 1%; white-space: nowrap;">
					<md-button class="md-icon-button margin-0" ng-click="createFileContractType(newFileContractType)">
						<md-icon md-font-icon="mdi mdi-content-save"></md-icon>
					</md-button>
				</td>
			</tr>
			<tr ng-repeat="fileContractType in fileContractTypes">
				<td>
					<md-input-container class="input-no-errors margin-0 margin-right-15">
						<input type="text" ng-model="fileContractType.tipo_fichero" aria-label="input">
					</md-input-container>

					<md-checkbox ng-model="fileContractType.obligatorio" ng-true-value="1" ng-false-value="0">Obligatorio</md-checkbox>
				</td>
				<td style="width: 1%; white-space: nowrap;">
					<md-button class="md-icon-button margin-0" ng-click="updateFileContractType(fileContractType)">
						<md-icon md-font-icon="mdi mdi-content-save"></md-icon>
					</md-button>

					<md-button class="md-icon-button margin-0" ng-click="deleteFileContractType(fileContractType, $index)">
						<md-icon md-font-icon="mdi mdi-delete"></md-icon>
					</md-button>
				</td>
			</tr>
		</tbody>
	</table>
</md-content>
