<md-content class="md-padding mdc-bg-white">
	<table>
		<thead>
		<tr>
			<th>
				<h3>Estructuras de gerencias</h3>
			</th>
			<th></th>
		</tr>
		</thead>
		<tbody>
		<tr>
			<td>
				<md-input-container class="input-no-errors margin-0 margin-right-15">
					<input type="text" ng-model="newManagementStructure.nombre" placeholder="Añadir nuevo" aria-label="input">
				</md-input-container>

				<md-select ng-model="newManagementStructure.id_gerencia" aria-label="Gerencia">
					<md-option ng-repeat="management in managements" ng-value="management.id_gerencia" ng-model="newManagementStructure.id_gerencia">[[management.gerencia]]
				</md-select>

				<md-select ng-model="newManagementStructure.id_tipo_estructura" aria-label="Tipo de estructura">
					<md-option ng-value="'G'">Gerencia</md-option>
					<md-option ng-value="'C'">Comercial</md-option>
					<md-option ng-value="'S'">Estructura</md-option>
				</md-select>
			</td>
			<td style="width: 1%; white-space: nowrap;">
				<md-button class="md-icon-button margin-0" ng-click="createStructure(newManagementStructure)">
					<md-icon md-font-icon="mdi mdi-content-save"></md-icon>
				</md-button>
			</td>
		</tr>
		</tbody>
	</table>
	<table ng-repeat="management in managements">
		<tbody>
		<tr>
			<td colspan="3">
				[[management.gerencia]]
			</td>
		</tr>
		<tr ng-repeat="structure in management.structures">

			<td># [[structure.id_gerencia_estructura]]</td>
			<td>

				<md-input-container class="input-no-errors margin-0 margin-right-15">
					<input type="text" ng-model="structure.nombre" aria-label="input">
				</md-input-container>

				<md-select ng-model="structure.id_tipo_estructura" aria-label="Tipo de estructura">
					<md-option ng-value="'G'">Gerencia</md-option>
					<md-option ng-value="'C'">Comercial</md-option>
					<md-option ng-value="'S'">Estructura</md-option>
				</md-select>
			</td>
			<td style="width: 1%; white-space: nowrap;">
				<md-button class="md-icon-button margin-0" ng-click="updateStructure(structure)">
					<md-icon md-font-icon="mdi mdi-content-save"></md-icon>
				</md-button>

				<md-button class="md-icon-button margin-0" ng-click="deleteStructure(structure, $index)">
					<md-icon md-font-icon="mdi mdi-delete"></md-icon>
				</md-button>
			</td>
		</tr>


		</tbody>
	</table>


</md-content>
