<md-content class="md-padding mdc-bg-white">

	<h3>Motivos de aviso</h3>

	<ul>
		<li ng-repeat="motive in reportsMotives | orderBy:'name'">[[ motive.name ]]</li>
	</ul>

	<table class="margin-top-10">
		<thead>
			<tr>
				<th>
					<h3>Estados de aviso</h3>
				</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>
					<md-input-container class="input-no-errors margin-0">
						<input type="text" ng-model="newReportStatus.name" placeholder="Añadir nuevo" aria-label="input">
					</md-input-container>
				</td>
				<td style="width: 1%; white-space: nowrap;">
					<md-button class="md-icon-button margin-0" ng-click="createReportStatus(newReportStatus)">
						<md-icon md-font-icon="mdi mdi-content-save"></md-icon>
					</md-button>
				</td>
			</tr>
			<tr ng-repeat="status in reportsStatuses | orderBy:'name'">
				<td>
					<md-input-container class="input-no-errors margin-0">
						<input type="text" ng-model="status.name" aria-label="input">
					</md-input-container>
				</td>
				<td style="width: 1%; white-space: nowrap;">
					<md-button class="md-icon-button margin-0" ng-click="updateReportStatus(status)">
						<md-icon md-font-icon="mdi mdi-content-save"></md-icon>
					</md-button>

					<md-button class="md-icon-button margin-0" ng-click="deleteReportStatus(status, $index)">
						<md-icon md-font-icon="mdi mdi-delete"></md-icon>
					</md-button>
				</td>
			</tr>
		</tbody>
	</table>
</md-content>
