<md-content class="md-padding mdc-bg-white">
	<table>
		<thead>
			<tr>
				<th>
					<h3>Tipos de documentos (interno RRHH)</h3>
				</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>
					<md-input-container class="input-no-errors margin-0">
						<input type="text" ng-model="newFileType.name" placeholder="Añadir nuevo" aria-label="input">
					</md-input-container>
				</td>
				<td>
					<md-checkbox ng-model="newFileType.required" ng-true-value="1" ng-false-value="0">Obligatorio</md-checkbox>
				</td>
				<td style="width: 1%; white-space: nowrap;">
					<md-button class="md-icon-button margin-0" ng-click="createFileType(newFileType)">
						<md-icon md-font-icon="mdi mdi-content-save"></md-icon>
					</md-button>
				</td>
			</tr>
			<tr ng-repeat="fileType in fileTypes | orderBy:'name'">
				<td>
					<md-input-container class="input-no-errors margin-0">
						<input type="text" ng-model="fileType.name" aria-label="input">
					</md-input-container>
				</td>
				<td>
				<md-checkbox ng-model="fileType.required" ng-true-value="1" ng-false-value="0">Obligatorio</md-checkbox>
				</td>
				<td style="width: 1%; white-space: nowrap;">
					<md-button class="md-icon-button margin-0" ng-click="updateFileType(fileType)">
						<md-icon md-font-icon="mdi mdi-content-save"></md-icon>
					</md-button>

					<md-button class="md-icon-button margin-0" ng-click="deleteFileType(fileType, $index)">
						<md-icon md-font-icon="mdi mdi-delete"></md-icon>
					</md-button>
				</td>
			</tr>
		</tbody>
	</table>
</md-content>
