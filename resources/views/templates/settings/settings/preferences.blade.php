<md-content class="md-padding mdc-bg-white">
	<form>
		<div layout="column">
			<h3>Productividad</h3>

			<md-input-container flex>
				<input type="number" min="1" max="100" ng-model="management.monthly_objective" placeholder="Objetivo mensual" aria-label="input">
			</md-input-container>
		</div>

		<div layout="row" layout-align="end center">
			<md-button class="md-accent md-raised" ng-click="submit()">
				<md-icon md-font-icon="mdi mdi-content-save"></md-icon>
				Guardar
			</md-button>
		</div>
	</form>
</md-content>
