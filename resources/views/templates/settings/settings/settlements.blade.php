<md-content layout-padding class="mdc-bg-white">
	<form name="settlementsForm">
		<div layout="column" class="margin-10">
			<div layout="column" md-whiteframe="2" class="mdc-bg-white" flex>
				<md-toolbar class="md-menu-toolbar">
					<div class="md-toolbar-tools">
						<h2 flex md-truncate>Liquidaciones</h2>

						<md-button class="md-accent" ng-click="alterSettlements(null,null)">
							<md-icon md-font-icon="mdi mdi-plus"></md-icon>
							Añadir liquidacion
						</md-button>
					</div>
				</md-toolbar>

				<md-content class="mdc-bg-white">
					<table datatable="ng" dt-instance="table.settlements.dtInstanceCallback" dt-options="table.settlements.dtOptions" dt-disable-deep-watchers="true"
					       class="row-border hover full-width small">
						<thead>
						<th>#</th>
						<th>Observaciones</th>
						<th>Gerencia</th>
						<th>Comercial</th>
						<th>Tipo de contratación</th>
						<th>Nivel comercial</th>
						<th>Fecha desde</th>
						<th>Fecha hasta</th>
						<th>Fecha Liquidacion</th>
						<th></th>

						<th>Editar</th>
						<th>Eliminar</th>
						</tr>
						</thead>
						<tbody>
						<tr ng-repeat="settlement in settlements" class="clickable">
							<td>[[settlement.id]]</td>
							<td>[[settlement.observaciones]]</td>
							<td>[[settlement.management ? settlement.management.id_gerencia : 'No indicada']]</td>
							<td>[[settlement.commercial ? settlement.commercial.id_comercial : 'No indicado']]</td>
							<td>[[settlement.contract ? settlement.contractType.descripcion: 'No indicado']]</td>
							<td>[[settlement.id_estructura_comercial? settlement.id_estructura_comercial : 'No indicada']]</td>
							<td>[[ moment(settlement.fecha_desde).format('DD-MM-YYYY') ]]</td>
							<td>[[ moment(settlement.fecha_hasta).format('DD-MM-YYYY') ]]</td>
							<td>[[ moment(settlement.fecha_liquidacion).format('DD-MM-YYYY') ]]</td>

							<td>
								<md-button class="md-accent" ng-click="settlementPdf(settlement.id)">
									<md-icon md-font-icon="mdi mdi-file-pdf mdi-50px"></md-icon>
								</md-button>
							</td>


							<td><a style="color:lightblue" ng-click="alterSettlements(settlement.id,$index)"> Editar </a></td>
							<td><a style="color:lightcoral" ng-click="deleteSettlement(settlement.id,settlement.observaciones)"> Eliminar </a></td>
						</tr>
						</tbody>
					</table>
				</md-content>
			</div>
		</div>
	</form>
</md-content>
