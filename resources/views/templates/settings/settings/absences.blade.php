<md-content class="md-padding mdc-bg-white">
	<table>
		<thead>
			<tr>
				<th>
					<h3>Motivos de ausencia</h3>
				</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>
					<md-input-container class="input-no-errors margin-0">
						<input type="text" ng-model="newAbsenceMotive.name" placeholder="Añadir nuevo" aria-label="input">
					</md-input-container>
				</td>
				<td style="width: 1%; white-space: nowrap;">
					<md-button class="md-icon-button margin-0" ng-click="createAbsenceMotive(newAbsenceMotive)">
						<md-icon md-font-icon="mdi mdi-content-save"></md-icon>
					</md-button>
				</td>
			</tr>
			<tr ng-repeat="motive in absencesMotives | orderBy:'name'">
				<td>
					<md-input-container class="input-no-errors margin-0">
						<input type="text" ng-model="motive.name" aria-label="input">
					</md-input-container>
				</td>
				<td style="width: 1%; white-space: nowrap;">
					<md-button class="md-icon-button margin-0" ng-click="updateAbsenceMotive(motive)">
						<md-icon md-font-icon="mdi mdi-content-save"></md-icon>
					</md-button>

					<md-button class="md-icon-button margin-0" ng-click="deleteAbsenceMotive(motive, $index)">
						<md-icon md-font-icon="mdi mdi-delete"></md-icon>
					</md-button>
				</td>
			</tr>
		</tbody>
	</table>
</md-content>
