<md-content class="md-padding mdc-bg-white">

	<form>
		<div layout="row" layout-margin>
			<md-input-container flex>
				<input type="text" ng-model="management.name" placeholder="Nombre" aria-label="input">
			</md-input-container>

			<md-input-container flex>
				<input type="text" ng-model="management.phone" placeholder="Teléfono" aria-label="input">
			</md-input-container>

			<md-input-container flex>
				<input type="text" ng-model="management.workhours" placeholder="Horario Laboral" aria-label="input">
			</md-input-container>
		</div>

		<div layout="row" layout-margin>
			<md-input-container flex>
				<input type="text" ng-model="management.address" placeholder="Dirección" aria-label="input">
			</md-input-container>

			<md-input-container flex>
				<input type="text" ng-model="management.address_link" placeholder="Dirección (URL Google Maps)" aria-label="input">
			</md-input-container>

			<md-input-container flex>
				<input type="text" ng-model="management.url" placeholder="Página web (URL)" aria-label="input">
			</md-input-container>
		</div>

		<div layout="row" layout-align="end center">
			<md-button class="md-accent md-raised" ng-click="submit()">
				<md-icon md-font-icon="mdi mdi-content-save"></md-icon>
				Guardar
			</md-button>
		</div>
	</form>

</md-content>
