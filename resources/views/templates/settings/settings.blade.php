<md-toolbar id="top-toolbar" class="md-menu-toolbar">
    <div class="md-toolbar-tools">
        <h1>Configuración [[user.company.database]]</h1>
    </div>
</md-toolbar>

<md-content layout="column" layout-margin>
    <div layout="row" layout-margin class="mdc-bg-white" md-whiteframe="2">
        <md-tabs md-dynamic-height md-border-bottom flex>
            <md-tab label="Tipos Doc. RRHH">
                @include('templates.settings.settings.files')
            </md-tab>

            <md-tab label="Tipos Doc. Contratos">
                @include('templates.settings.settings.filescontracts')
            </md-tab>

            <md-tab label="Ausencias">
                @include('templates.settings.settings.absences')
            </md-tab>

            <md-tab label="Avisos">
                @include('templates.settings.settings.reports')
            </md-tab>
            <md-tab label="Tipos de contratacion">
                @include('templates.settings.settings.contractTypes')
            </md-tab>

            <md-tab label="Productos"
                    ng-if="user.company.database == 'mexico' || user.company.database == 'mexico_test' || user.company.database == 'portugal'">
                @include('templates.settings.settings.products')
            </md-tab>
            <md-tab label="Estructuras de gerencias"
                    ng-if="user.company.database == 'mexico' || user.company.database == 'mexico_test'">
                @include('templates.settings.settings.managementStructures')
            </md-tab>
            @if(auth()->user()->level == 1)
                <md-tab label="Grupos de comisionamiento">
                    @include('templates.settings.settings.fees')
                </md-tab>


                <md-tab label="Liquidaciones">
                    @include('templates.settings.settings.settlements')
                </md-tab>
            @endif
            {{--

                        <div layout="column" layout-gt-sm="row" layout-margin class="margin-0">
                <md-toolbar class="md-menu-toolbar">
                    <div class="md-toolbar-tools">
                        <h2 flex md-truncate>Estructuras comerciales</h2>


                    </div>
                </md-toolbar>
                <md-content class="mdc-bg-white">
                    <table datatable="ng" dt-instance="table.tableStructures.dtInstanceCallback" dt-options="table.tableStructures.dtOptions" dt-disable-deep-watchers="true" class="row-border hover full-width small">
                        <thead>
                        <tr>
                            <th>Id estructura</th>
                            <th>Nombre</th>
                            <th>Tipo de estructura</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr ng-repeat="structure in management.structures"  class="clickable">
                            <td>[[ structure.id_gerencia_estructura ]]</td>
                            <td>[[ structure.nombre ]]</td>
                            <td>[[ structure.id_tipo_estructura == 'G' ? 'Gerencia' : structure.id_tipo_estructura == 'C' ? 'Comercial' : structure.id_tipo_estructura == 'E' ? 'Estructura' : 'Sin definir' ]]</td>
                        </tr>
                        </tbody>
                    </table>
                </md-content>--}}


        </md-tabs>
    </div>
</md-content>
