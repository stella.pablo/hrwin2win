<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Landing</title>

	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,600,700&display=swap" rel="stylesheet">
	<link href="https://unpkg.com/tailwindcss@^1.0/dist/tailwind.min.css" rel="stylesheet">

	<style>
		* {
			font-family: 'Montserrat', sans-serif;
			font-weight: 700;
		}

		h1, h2, h3 {
			font-family: 'Montserrat', sans-serif;
			font-weight: 600;
		}

		.bg-half-colour {
			background: linear-gradient(90deg, #fafafa 60%, #ffffff 40%);
		}

		.page-break img {
			width: 100%;
			height: 450px;
			object-fit: cover;
			object-position: center;
			position: absolute;
			z-index: 1;
		}

		.page-break .container {
			z-index: 2;
			position: relative;
		}

		.page-break .container > * {
			background-color: rgba(255, 255, 255, 0.45);
			width: fit-content;
		}

		.page-break .mask {
			background-color: rgba(0, 0, 0, 0.45);
			position: relative;
			z-index: 2;
		}

		.text-black {
			color: #2d2926;
		}

		.text-blue {
			color: #00b5d6;
		}

		.text-gray {
			color: #737373;
		}

		.text-uppercase {
			text-transform: uppercase;
		}

		.bg-blue {
			background-color: #00b5d6;
		}

		.bg-gray {
			background-color: #fafafa;
		}

		.bg-gray-dark {
			background-color: #737373;
		}

		.benefits img {
			width: 100px;
			height: 100px;
		}

		.benefits h4 {
			text-transform: uppercase;
		}


		@media (max-width: 768px) {
			.bg-half-colour {
				background: #fafafa;
			}
		}

		select:required:invalid {
			color: gray;
		}

		option[value=""][disabled] {
			display: none;
		}
	</style>
</head>
<body>

<div id="app" class="text-black pt-8">
	<div class="block container mx-auto mb-2">
		<img src="/images/landing/logo.jpeg" alt="" style="width: 200px;">
	</div>

	<div class="flex bg-half-colour">
		<div class="container mx-auto flex flex-col md:flex-row">
			<div class="flex flex-col justify-center md:w-1/2 p-10 md:p-0 md:pr-20">
				<div class="flex flex-col py-10">
					<h1 class="text-3xl font-bold mb-12">BUSCAMOS A LOS/AS MEJORES COMERCIALES</h1>
					<h2 class="text-2xl text-blue mb-4">Únete a nuestro gran equipo de trabajo</h2>
					<h3 class="text-2xl font-bold">TE ESTAMOS ESPERANDO</h3>
				</div>
			</div>

			<div class="md:w-1/2 bg-white">
				<div
					class="flex flex-col mx-auto pb-24 w-4/5"
					v-show="formSubmitted"
				>
					<p class="p-10 bg-blue text-white">
						Gracias! En breve nos pondremos en contacto contigo.
					</p>
				</div>

				<form
					class="flex flex-col mx-auto pt-4 pb-24 w-4/5"
					v-on:submit.prevent="submit"
					v-show="!formSubmitted"
				>
					<div class="my-4 text-center">
						<strong class="text-lg text-gray">POSTÚLATE AHORA</strong>
					</div>

					<div class="flex flex-col md:flex-row">
						<input
							class="md:mr-2 bg-gray-100 focus:bg-white mb-4 focus:outline-0 focus:shadow-outline border border-gray-300 rounded-lg py-2 px-4 block w-full appearance-none leading-normal"
							type="text"
							placeholder="Nombre *"
							required
							v-bind:class="{ 'border-gray-300': !formErrors.name, 'border-red-500': formErrors.name }"
							v-model="form.name"
						>

						<input
							class="md:mr-2 bg-gray-100 focus:bg-white mb-4 focus:outline-0 focus:shadow-outline border border-gray-300 rounded-lg py-2 px-4 block w-full appearance-none leading-normal"
							type="text"
							placeholder="1er Apellido"
							v-model="form.surname"
						>

						<input
							class="bg-gray-100 focus:bg-white mb-4 focus:outline-0 focus:shadow-outline border border-gray-300 rounded-lg py-2 px-4 block w-full appearance-none leading-normal"
							type="text"
							placeholder="2do Apellido"
							v-model="form.lastname"
						>
					</div>

					<div class="flex flex-col md:flex-row">
						<input
							class="md:mr-2 bg-gray-100 focus:bg-white mb-4 focus:outline-0 focus:shadow-outline border border-gray-300 rounded-lg py-2 px-4 block w-full appearance-none leading-normal"
							type="text"
							placeholder="Teléfono *"
							required
							v-bind:class="{ 'border-gray-300': !formErrors.phone, 'border-red-500': formErrors.phone }"
							v-model="form.phone"
						>

						<input
							class="bg-gray-100 focus:bg-white mb-4 focus:outline-0 focus:shadow-outline border border-gray-300 rounded-lg py-2 px-4 block w-full appearance-none leading-normal"
							type="email"
							placeholder="Email *"
							required
							v-bind:class="{ 'border-gray-300': !formErrors.email, 'border-red-500': formErrors.email }"
							v-model="form.email"
						>
					</div>

					<input
						class="bg-gray-100 focus:bg-white mb-4 focus:outline-0 focus:shadow-outline border border-gray-300 rounded-lg py-2 px-4 block w-full appearance-none leading-normal"
						type="text"
						placeholder="Ciudad"
						v-model="form.city"
					>

					<div class="inline-block relative w-full mb-4">
						<select
							class="block appearance-none w-full bg-gray-100 border border-gray-300 px-4 py-2 pr-8 rounded leading-tight focus:outline-none focus:shadow-outline"
							required
							v-model="form.location"
						>
							<option v-for="province in provinces" :value="province.provincia"> @{{province.provincia}}</option>
						</select>
						<div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
							<svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
								<path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/>
							</svg>
						</div>
					</div>

					<div class="inline-block relative w-full mb-4">
						<select
							class="block appearance-none w-full bg-gray-100 border border-gray-300 px-4 py-2 pr-8 rounded leading-tight focus:outline-none focus:shadow-outline"
							required
							v-model="form.offer"
						>
							<option value="" disabled selected>Vacante *</option>
							<option value="Comercial">Comercial</option>
							<option value="Gerente">Gerente</option>
						</select>
						<div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
							<svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
								<path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/>
							</svg>
						</div>
					</div>

					<div
						class="button text-gray bg-gray-100 hover:bg-gray-200 w-full py-2 px-4 relative focus:outline-0 rounded focus:shadow-outline border border-gray-300 mb-4"
						v-bind:class="{ 'border-gray-300': !formErrors.file, 'border-red-500': formErrors.file }"
					>
						<input class="opacity-0 absolute pin-x pin-y w-full" type="file" @change="fileSet" ref="file">
						<span v-show="!fileName">Adjuntar CV *</span>
						<span v-show="fileName" v-text="fileName"></span>
					</div>

					<label class="block font-semibold">
						<input class="mr-2 leading-tight" type="checkbox" v-model="form.cb1" required>
						<span class="text-sm" v-bind:class="{ 'text-gray': !formErrors.cb1, 'text-red-400': formErrors.cb1 }">
							Manifiesto que he leído, aceptado y entendido los términosy condiciones de Face App International Mexico
						</span>
					</label>

					<label class="block font-semibold">
						<input class="mr-2 leading-tight" type="checkbox" v-model="form.cb2" required>
						<span class="text-sm" v-bind:class="{ 'text-gray': !formErrors.cb2, 'text-red-400': formErrors.cb2 }">
							También acepto que Face App International Mexico o sus representantes puedan contactar conmigo por email,
							teléfono o SMS (así como a través de medios automatizados) a través del
							email o del teléfono que he indicado, incluso con fines promocionales.
						</span>
					</label>

					<button
						class="bg-blue-500 mt-4 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
						:disabled="!checkForm"
						type="submit"
					>
						Siguiente
					</button>
				</form>
			</div>
		</div>

	</div>

	<div class="block page-break">
		<img src="/images/landing/bg-1.jpg" alt="">

		<div class="container flex flex-col justify-center mx-auto py-3" style="height: 450px;">
			<h3 class="text-3xl font-bold mb-2 px-2 text-blue text-center md:text-left">Transforma tu futuro laboral</h3>
			<h3 class="text-3xl font-bold mb-4 px-2 text-center md:text-left">Gana dinero con tu <span class="text-blue">talento comercial</span></h3>
			<p class="px-2 text-center md:text-left">Saca rendimiento a tu dinamismo y don de gentes</p>
		</div>
	</div>

	<div class="block bg-gray">
		<div class="container mx-auto py-20">
			<div class="flex flex-col md:flex-row">
				<div class="md:w-3/5">
					<h2 class="text-center md:text-left text-2xl mb-4 md:mb-0">Empresa lider con más de 25 años de experiencia en el mercado</h2>
				</div>

				<div class="md:w-2/5">
					<p class="text-center md:text-right font-light">
						Somos una empresa que nace en 1991 en Canadá.
						Iniciando nuestra andadura con el firme propósito de
						convertirnos, en poco tiempo, en un referente del
						sector de servicios de Outsourcing.
					</p>
				</div>
			</div>
		</div>
	</div>

	<div class="container mx-auto benefits my-20">
		<div class="flex flex-col md:flex-row">
			<div class="flex flex-col flex-1 items-center mb-4 md:mb-0">
				<img src="/images/landing/icon-1.png" alt="" class="">
				<h4 class="text-blue mt-4">Pagos semanales</h4>
			</div>

			<div class="flex flex-col flex-1 items-center mb-4 md:mb-0">
				<img src="/images/landing/icon-2.png" alt="" class="">
				<h4 class="text-blue mt-4">Comisiones no topadas</h4>
			</div>

			<div class="flex flex-col flex-1 items-center mb-4 md:mb-0">
				<img src="/images/landing/icon-3.png" alt="" class="">
				<h4 class="text-blue mt-4">Capacitación constante</h4>
			</div>

			<div class="flex flex-col flex-1 items-center mb-4 md:mb-0">
				<img src="/images/landing/icon-4.png" alt="" class="">
				<h4 class="text-blue mt-4">Flexibilidad de horario</h4>
			</div>
		</div>
	</div>

	<div class="block page-break" style="height: 450px;">
		<img src="/images/landing/bg-2.png" alt="">

		<div class="pt-10 md:pt-40">
			<div class="mask mb-4">
				<div class="container mx-auto flex flex-col items-center py-4">
					<h2 class="text-3xl text-uppercase text-center" style="background-color: transparent; color: white; max-width: 1000px;">
						Expande tus habilidades y desarrolla un futuro profesional ¡Únete a nuestro equipo!
					</h2>
				</div>
			</div>
		</div>

		<div class="container mx-auto flex flex-col items-center">
			<h3 class="text-3xl text-uppercase text-center" style="background-color: transparent; color: white; max-width: 1000px;">
				Este trabajo es para ti
			</h3>
		</div>
	</div>

	<footer class="bg-gray-dark py-20">
		<div class="container mx-auto">
			<div class="flex flex-col items-center">
				<div class="flex flex-row mb-2">
					<a href="#" class="font-light text-white text-center" style="min-width: 200px;">Privacidad</a>
					<a href="#" class="font-light text-white text-center" style="min-width: 200px;">Condiciones</a>
				</div>

				<div class="block">
					<p class="font-light text-white text-center">2019 Face App International México ·Todos los derechos reservados</p>
				</div>
			</div>
		</div>
	</footer>
</div>


<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
{{--<script src="https://cdn.jsdelivr.net/npm/vue"></script>--}}
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>

<script>
	var app = new Vue({
		el: '#app',
		data: {
			file: null,
			fileName: null,
			formError: false,
			formErrors: {},
			formSubmitted: false,
			formSubmitting: false,
			provinces:{!! $provinces !!},
			form: {
				location: ""
				// offer: ""
			}
		},
		/* props: ['provinces'],
		    mounted () {
				provinces = {!! json_encode($provinces) !!}
		    },*/
		methods: {
			checkForm: function () {
				this.formErrors = {};

				if (!this.form.name) {
					this.formErrors.name = true
				}

				if (!this.form.email) {
					this.formErrors.email = true
				}

				if (!this.form.phone) {
					this.formErrors.phone = true
				}

				if (!this.form.location) {
					this.formErrors.location = true
				}

				if (!this.form.offer) {
					this.formErrors.offer = true
				}

				if (!this.form.cb1) {
					this.formErrors.cb1 = true
				}

				if (!this.form.cb2) {
					this.formErrors.cb2 = true
				}

				if (!this.file) {
					this.formErrors.file = true
				}

				return !Object.keys(this.formErrors).length;
			},
			fileSet: function (event, file) {
				this.file = null;
				this.fileName = null;

				if (this.$refs.file.files.length) {
					this.file = this.$refs.file.files[0];
					this.fileName = this.file.name;
				}
			},
			submitForm: function () {
				var that = this;

				this.formSubmitting = true;

				axios.post('/landing/store', this.form)
					.then(function (response) {
						if (response.data.id) {
							if (that.file) {
								var formData = new FormData();
								formData.append('candidate_id', response.data.id);
								formData.append('file', that.file);

								axios.post('/landing/store-cv', formData, {
									headers: {
										'Content-Type': 'multipart/form-data'
									}
								}).then(function (responseCV) {
									if (responseCV.data.id) {
										that.formSubmitting = false;
										that.formSubmitted = true;
									}
								}).catch(function (error) {
									that.formSubmitting = false;
									alert('Ha ocurrido un error interno. Inténtelo de nuevo más tarde.');
								});
							}
							else {
								that.formSubmitting = false;
								that.formSubmitted = true;
							}
						}
					}).catch(function (error) {
						that.formSubmitting = false;
						alert('Ha ocurrido un error interno. Inténtelo de nuevo más tarde.');
					});
			},
			submit: function (e) {
				if (this.checkForm()) {
					this.submitForm()
				} else {
					console.log('error');
				}
			}
		}
	});
</script>
</body>
</html>
