<md-toolbar id="top-toolbar" class="md-menu-toolbar">
	<div class="md-toolbar-tools">
		<h1>Avisos</h1>
	</div>
</md-toolbar>

<md-content layout="column" layout-margin>
	<div layout="column" md-whiteframe="2">
		<div layout="column" layout-align="center center" class="mdc-bg-grey-100 padding-15">
			<div id="filters" layout="row" layout-align="center center" layout-margin layout-wrap>
				<md-input-container class="input-no-errors">
					<label>Fecha desde</label>
					<md-datepicker ng-model="filtersDates.date_from"></md-datepicker>
				</md-input-container>

				<md-input-container class="input-no-errors">
					<label>Fecha hasta</label>
					<md-datepicker ng-model="filtersDates.date_to"></md-datepicker>
				</md-input-container>

				<md-input-container class="input-no-errors">
					<label>Comercial</label>
					<md-select ng-model="filters.external_id">
						<md-option ng-value=""><em>Todos</em></md-option>
						<md-option ng-value="commercial.comercial" ng-repeat="commercial in commercials | orderBy:'nombre_comercial'">[[ commercial.nombre_comercial + ' ' + commercial.apellido_comercial ]]</md-option>
					</md-select>
				</md-input-container>

				<md-input-container class="input-no-errors">
					<label>Motivo</label>
					<md-select ng-model="filters.motive_id">
						<md-option ng-value=""><em>Todos</em></md-option>
						<md-option ng-value="motive.id" ng-repeat="motive in reports.motives | orderBy:'name'">[[ motive.name ]]</md-option>
					</md-select>
				</md-input-container>
			</div>

			<div layout="row" layout-align="center center">
				<md-button class="md-accent" ng-click="filter()">
					<md-icon md-font-icon="mdi mdi-filter"></md-icon>
					Filtrar
				</md-button>

				<md-button ng-click="clearFilters()">
					<md-icon md-font-icon="mdi mdi-filter-remove"></md-icon>
					Limpiar filtros
				</md-button>
			</div>
		</div>

		<md-content class="mdc-bg-white">
			<table datatable="ng" dt-instance="table.reports.dtInstanceCallback" dt-options="table.reports.dtOptions" dt-disable-deep-watchers="true" class="row-border hover full-width small">
				<thead>
					<tr>
						<th>Fecha</th>
						<th>ID</th>
						<th>Nombre</th>
						<th>Motivo</th>
						<th>Estado</th>
					</tr>
				</thead>
				<tbody>
					<tr ng-repeat="report in commercialsReports" class="clickable">
						<td ui-sref="commercials.show({ id_comercial: report.commercial.id_comercial })">[[ moment(report.created_at).format('DD-MMM-YYYY') ]]</td>
						<td ui-sref="commercials.show({ id_comercial: report.commercial.id_comercial })">[[ report.commercial.id_comercial ]]</td>
						<td ui-sref="commercials.show({ id_comercial: report.commercial.id_comercial })">[[ report.commercial.nombre_completo ]]</td>
						<td>
							[[ report.motive.name ]]
						</td>
						<td>
							<select ng-model="report.status_id" ng-change="answerReport(report)">
								<option ng-value="status.id" ng-repeat="status in reports.statuses | orderBy:'name'">[[ status.name ]]</option>
							</select>
						</td>
					</tr>
				</tbody>
			</table>

			<div layout="row">
				<div flex>
					<highchart id="chart-typesOfMotives" config="chartsConfig.typesOfMotives"></highchart>
				</div>

				<div flex>
					<highchart id="chart-reportsDaily" config="chartsConfig.reportsDaily"></highchart>
				</div>
			</div>
		</md-content>
	</div>
</md-content>
