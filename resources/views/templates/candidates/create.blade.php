<md-toolbar id="top-toolbar" class="md-menu-toolbar">
	<div class="md-toolbar-tools">
		<h1>[[ candidate.id ? candidate.fullname : 'Nuevo candidato' ]]</h1>
	</div>
</md-toolbar>

<md-content layout="column" layout-margin>
	<div layout="column" md-whiteframe="2" flex class="mdc-bg-white">
		<md-content layout-padding class="mdc-bg-white">
			<form name="candidateForm">
				<h3>Información básica</h3>

				<div layout="column" layout-gt-sm="row" layout-margin>
					<md-input-container flex class="margin-bottom-25">
						<md-icon md-font-icon="mdi mdi-account-multiple"></md-icon>
						<label>Gerencia</label>
						<md-select ng-model="candidate.management_id" required>
							<md-option ng-value=""><em>Todas</em></md-option>
							<md-option ng-value="management.internal.id" ng-repeat="management in managements" ng-show="management.internal">[[ management.gerencia ]]</md-option>
						</md-select>
					</md-input-container>
				</div>

				<div layout="column" layout-gt-sm="row" layout-margin>
					<md-input-container flex>
						<md-icon md-font-icon="mdi mdi-account-box"></md-icon>
						<input type="text" ng-model="candidate.name" placeholder="Nombre" required>
					</md-input-container>

					<md-input-container flex>
						<md-icon md-font-icon="mdi mdi-account-box"></md-icon>
						<input type="text" ng-model="candidate.surname" placeholder="Primer Apellido">
					</md-input-container>

					<md-input-container flex>
						<md-icon md-font-icon="mdi mdi-account-box"></md-icon>
						<input type="text" ng-model="candidate.lastname" placeholder="Segundo Apellido">
					</md-input-container>
				</div>

				<div layout="column" layout-gt-sm="row" layout-margin>
					<md-input-container flex>
						<md-icon md-font-icon="mdi mdi-phone"></md-icon>
						<input type="text" ng-model="candidate.phone" placeholder="Teléfono">
					</md-input-container>

					<md-input-container flex>
						<md-icon md-font-icon="mdi mdi-at"></md-icon>
						<input type="email" ng-model="candidate.email" placeholder="Email">
					</md-input-container>
				</div>

				<div layout="column" layout-gt-sm="row" layout-margin>
					<md-input-container flex>
						<label>Origen</label>
						<md-icon md-font-icon="mdi mdi-web"></md-icon>
						<md-select ng-model="candidate.origin_id" required>
							<md-option ng-repeat="origin in origins | orderBy:'name'" ng-value="origin.id">
								[[ origin.name ]]
							</md-option>
						</md-select>
					</md-input-container>

					<md-input-container flex>
						<label>Fecha petición</label>
						<md-datepicker ng-model="candidate.petitioned_at" required></md-datepicker>
					</md-input-container>
				</div>

				<div layout="column" layout-gt-sm="row" layout-margin>
					<md-input-container flex>
						<md-icon md-font-icon="mdi mdi-comment"></md-icon>
						<label>Comentario</label>
						<textarea ng-model="candidate.comment" rows="1"></textarea>
					</md-input-container>
				</div>

				<div layout="column" layout-margin ng-hide="candidate.id">
					<md-input-container>
						<md-checkbox ng-model="sendEmail" aria-label="" ng-disabled="!candidate.email">
							Enviar email
						</md-checkbox>
					</md-input-container>

					<md-input-container flex ng-show="sendEmail">
						<label>Seleccionar email</label>
						<md-icon md-font-icon="mdi mdi-email"></md-icon>
						<md-select ng-model="candidate.email_type_id">
							<md-option ng-value="'0'"><em>Ninguno</em></md-option>
							<md-option ng-repeat="emailsType in emailsTypes | orderBy:'name'" ng-value="emailsType.id">
								[[ emailsType.name ]]
							</md-option>
						</md-select>
					</md-input-container>
				</div>

				<div layout="row" layout-align="space-between center" class="margin-top-40">
					<md-button ng-click="cancel()">
						<md-icon md-font-icon="mdi mdi-cancel"></md-icon>
						Cancelar
					</md-button>

					<md-button ng-click="checkIsValid($event)" class="md-primary md-raised" ng-disabled="candidateForm.$invalid || candidateForm.$pristine">
						<md-icon md-font-icon="mdi mdi-content-save"></md-icon>
						Guardar
					</md-button>
				</div>
			</form>
		</md-content>
	</div>
</md-content>
