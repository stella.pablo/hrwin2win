<div layout="column" md-whiteframe="2" flex class="mdc-bg-white">
	<md-toolbar class="md-menu-toolbar">
		<div class="md-toolbar-tools">
			<h2 flex md-truncate>Información básica</h2>
		</div>
	</md-toolbar>

	<md-content layout-padding class="mdc-bg-white">
		<table class="full-width">
			<tbody>
				<tr>
					<td>
						<md-icon md-font-icon="mdi mdi-account-multiple"></md-icon> Gerencia
					</td>
					<td>
						[[ candidate.management.external.gerencia ]]
					</td>
				</tr>
				<tr>
					<td>
						<md-icon md-font-icon="mdi mdi-phone-classic"></md-icon> Teléfono
					</td>
					<td>
						<a ng-href="tel:[[ candidate.phone ]]">[[ candidate.phone ]]</a>
					</td>
				</tr>
				<tr>
					<td>
						<md-icon md-font-icon="mdi mdi-at"></md-icon> Email
					</td>
					<td>
						<a ng-href="mailto:[[ candidate.email ]]">[[ candidate.email ]]</a>
					</td>
				</tr>
				<tr>
					<td>
						<md-icon md-font-icon="mdi mdi-web"></md-icon> Origen
					</td>
					<td>
						[[ candidate.origin.name ]]
					</td>
				</tr>
				<tr>
					<td>
						<md-icon md-font-icon="mdi mdi-calendar"></md-icon> Fecha Petición
					</td>
					<td>
						[[ moment(candidate.petitioned_at).format('DD-MMM-YYYY') ]]
					</td>
				</tr>
				<tr>
					<td>
						<md-icon md-font-icon="mdi mdi-clock"></md-icon> Última actualización
					</td>
					<td>
						[[ moment.utc(candidate.updated_at).local().format('DD-MMM-YYYY HH:mm:ss') ]]
					</td>
				</tr>
			</tbody>
		</table>
	</md-content>
</div>
