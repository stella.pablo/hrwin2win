<table class="table-first-nowrap table-second-full">
	<tbody>
		<tr>
			<td>
				<md-icon md-font-icon="mdi mdi-pound"></md-icon> Estado
			</td>
			<td class="text-right">
				[[ candidate.status.name ]]
			</td>
		</tr>
		<tr>
			<td>
				<md-icon md-font-icon="mdi mdi-calendar"></md-icon> 1ª Entrevista
			</td>
			<td class="text-right">
				<div ng-show="candidate.appointmentfirst" ng-mouseenter="candidate.appointmentfirst.hide = true" ng-mouseleave="candidate.appointmentfirst.hide = false">
					<span ng-class="{'mdc-text-red': candidate.appointmentfirst.status === 0, 'mdc-text-light-green': candidate.appointmentfirst.status === 2}" ng-hide="candidate.appointmentfirst.hide">
						[[ moment(candidate.appointmentfirst.date).format('DD-MMM-YYYY HH:mm') ]]
					</span>

					<span ng-show="candidate.appointmentfirst.hide">
						<md-icon md-font-icon="mdi mdi-calendar-check" class="clickable" ng-click="appointments.updateFast(candidate.appointmentfirst, candidate, { status: 2 })"></md-icon>
						<md-icon md-font-icon="mdi mdi-calendar-remove" class="clickable" ng-click="appointments.updateFast(candidate.appointmentfirst, candidate, { status: 0 })"></md-icon>
						<md-icon md-font-icon="mdi mdi-calendar" class="clickable" ng-click="appointments.update(candidate.appointmentfirst, candidate, $event)"></md-icon>
						<md-icon md-font-icon="mdi mdi-calendar-plus" class="clickable" ng-click="appointments.store(1, candidate, $event)"></md-icon>
					</span>
				</div>

				<div ng-hide="candidate.appointmentfirst">
					No citado
					<md-icon md-font-icon="mdi mdi-calendar-plus" class="clickable" ng-click="appointments.store(1, candidate, $event)"></md-icon>
				</div>

				<p class="margin-0" ng-show="candidate.appointmentfirst.status === 0 && candidate.appointmentfirst.reason">
					"<em class="mdc-text-red">[[ candidate.appointmentfirst.reason ]]</em>"
				</p>
			</td>
		</tr>
		<tr>
			<td>
				<md-icon md-font-icon="mdi mdi-calendar"></md-icon> 2ª Entrevista
			</td>
			<td class="text-right">
				<div ng-show="candidate.appointmentsecond" ng-mouseenter="candidate.appointmentsecond.hide = true" ng-mouseleave="candidate.appointmentsecond.hide = false">
					<span ng-class="{'mdc-text-red': candidate.appointmentsecond.status === 0, 'mdc-text-light-green': candidate.appointmentsecond.status === 2}" ng-hide="candidate.appointmentsecond.hide">
						[[ moment(candidate.appointmentsecond.date).format('DD-MMM-YYYY HH:mm') ]]
					</span>

					<span ng-show="candidate.appointmentsecond.hide">
						<md-icon md-font-icon="mdi mdi-calendar-check" class="clickable" ng-click="appointments.updateFast(candidate.appointmentsecond, candidate, { status: 2 })"></md-icon>
						<md-icon md-font-icon="mdi mdi-calendar-remove" class="clickable" ng-click="appointments.updateFast(candidate.appointmentsecond, candidate, { status: 0 })"></md-icon>
						<md-icon md-font-icon="mdi mdi-calendar" class="clickable" ng-click="appointments.update(candidate.appointmentsecond, candidate, $event)"></md-icon>
						<md-icon md-font-icon="mdi mdi-calendar-plus" class="clickable" ng-click="appointments.store(2, candidate, $event)"></md-icon>
					</span>
				</div>

				<div ng-hide="candidate.appointmentsecond">
					No citado
					<md-icon md-font-icon="mdi mdi-calendar-plus" class="clickable" ng-click="appointments.store(2, candidate, $event)" ng-show="candidate.appointmentfirst && candidate.appointmentfirst.status === 2"></md-icon>
				</div>

				<p class="margin-0" ng-show="candidate.appointmentsecond.status === 0">
					<span class="mdc-text-red" ng-show="candidate.appointmentsecond.reason">"[[ candidate.appointmentsecond.reason ]]"</span>
					<em ng-hide="candidate.appointmentsecond.reason">Motivo no especificado</em>
				</p>
			</td>
		</tr>
		<tr>
			<td>
				<md-icon md-font-icon="mdi mdi-check"></md-icon> Preaprobado
			</td>
			<td class="text-right">
				<div ng-show="candidate.preapproved_user_id">
					<strong>[[ candidate.preapproved ? 'Sí' : 'No' ]]</strong>, por [[ candidate.preapproved_user.name ]] ([[ moment(candidate.preapproved_at).format('DD-MMM-YYYY HH:mm') ]])
				</div>

				<div ng-hide="candidate.preapproved_user_id">
					<em>Sin información</em>
				</div>
			</td>
		</tr>
		<tr>
			<td>
				<md-icon md-font-icon="mdi mdi-check-all"></md-icon> Aprobado
			</td>
			<td class="text-right">
				<div ng-show="candidate.approved_user_id">
					<strong>[[ candidate.approved ? 'Sí' : 'No' ]]</strong>, por [[ candidate.approved_user.name ]] ([[ moment(candidate.approved_at).format('DD-MMM-YYYY HH:mm') ]])
				</div>

				<div ng-hide="candidate.approved_user_id">
					<em>Sin información</em>
				</div>
			</td>
		</tr>
		<tr>
			<td>
				<md-icon md-font-icon="mdi mdi-account-check"></md-icon> Contratado
			</td>
			<td class="text-right">
				<div ng-show="candidate.hired_user_id">
					<strong>[[ candidate.hired ? 'Sí' : 'No' ]]</strong>, por [[ candidate.hired_user.name ]] ([[ moment(candidate.hired_at).format('DD-MMM-YYYY HH:mm') ]])
				</div>

				<div ng-hide="candidate.hired_user_id">
					<em>Sin información</em>
				</div>
			</td>
		</tr>
	</tbody>
</table>
