<div layout="column" md-whiteframe="2" flex class="mdc-bg-white">
	<md-toolbar class="md-menu-toolbar">
		<div class="md-toolbar-tools">
			<h2 flex md-truncate>Seguimiento</h2>

			<div class="padding-0">
				<md-button class="md-icon-button margin-0" ng-click="pause()">
					<md-icon md-font-icon="mdi mdi-pause"></md-icon>
					<md-tooltip>En retención</md-tooltip>
				</md-button>

				<md-button class="md-icon-button margin-0" ng-click="doesNotAnswer()">
					<md-icon md-font-icon="mdi mdi-phone-missed"></md-icon>
					<md-tooltip>No contesta</md-tooltip>
				</md-button>
			</div>
		</div>
	</md-toolbar>

	<md-content layout-padding class="mdc-bg-white">
		@include('templates.candidates.show.followup.table')
	</md-content>
</div>
