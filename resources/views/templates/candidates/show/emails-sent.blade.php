<div layout="column" md-whiteframe="2" flex class="mdc-bg-white">
	<md-toolbar class="md-menu-toolbar">
		<div class="md-toolbar-tools">
			<h2 flex md-truncate>Emails enviados</h2>
		</div>
	</md-toolbar>

	<md-content layout-padding class="mdc-bg-white">
		<div ng-show="candidate.emails.length">
			<table datatable="ng" dt-options="{ dom: 't'}" class="full-width">
				<thead>
					<tr>
						<th>Fecha</th>
						<th>Email</th>
						<th>Enviado por</th>
					</tr>
				</thead>
				<tbody>
					<tr ng-repeat="email in candidate.emails">
						<td>
							[[ moment(email.created_at).format('DD-MMM-YYYY HH:mm') ]]
						</td>

						<td>
							[[ email.type.name ]]
						</td>

						<td>[[ email.user.name ]]</td>
					</tr>
				</tbody>
			</table>
		</div>

		<div layout="row" layout-align="center center" ng-hide="candidate.emails.length">
			<em>No se han enviado emails todavía.</em>
		</div>
	</md-content>
</div>
