<md-input-container class="full-width input-no-errors margin-top-20 margin-bottom-20">
	<label>Comentario</label>
	<textarea rows="4" class="full-width" ng-model="candidate.comment" ng-model-options="{'updateOn': 'blur'}" ng-change="setComment()"></textarea>
	<div class="hint">Haz click fuera del campo para guardar.</div>
</md-input-container>
