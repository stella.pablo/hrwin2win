<md-toolbar id="top-toolbar" class="md-menu-toolbar">
	<div class="md-toolbar-tools">
		<md-button class="md-icon-button" ng-click="hiddenFilters = !hiddenFilters" hide-gt-xs>
			<md-icon md-font-icon="mdi mdi-filter"></md-icon>
		</md-button>

		<h1 flex md-truncate>Candidatos</h1>

		<div class="padding-0">
			<md-button class="margin-0 md-primary md-raised" aria-label="" ui-sref="candidates.create">
				<md-icon md-font-icon="mdi mdi-plus"></md-icon>
				<span hide-xs>Nuevo candidato</span>
				<span hide-gt-xs>Nuevo</span>
			</md-button>
		</div>
	</div>
</md-toolbar>

<md-content layout="column" layout-margin>
	@include('templates.candidates.index.filters')

	<div layout="column" md-whiteframe="2">
		<div layout="column" class="mdc-bg-white padding-bottom-15 padding-top-15">
			@include('templates.candidates.index.table')
		</div>
	</div>
</md-content>

@include('templates.candidates.index.sidenav')
