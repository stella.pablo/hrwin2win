<div layout="column" layout-align="center center" class="padding-15" ng-hide="$mdMedia('xs') && hiddenFilters">
	<div id="filters" layout="row" layout-align="center center" layout-margin layout-wrap>
		<md-input-container class="input-no-errors" flex-xs>
			<md-datepicker ng-model="filters.date_from" md-max-date="filters.date_to" md-placeholder="Fecha desde"></md-datepicker>
		</md-input-container>

		<md-input-container class="input-no-errors" flex-xs>
			<md-datepicker ng-model="filters.date_to" md-min-date="filters.date_from" md-placeholder="Fecha hasta"></md-datepicker>
		</md-input-container>

		<md-input-container class="input-no-errors" flex-xs>
			<label>Origen</label>
			<md-icon md-font-icon="mdi mdi-web"></md-icon>
			<md-select ng-model="filters.origin_id">
				<md-option ng-value=""><em>Todos</em></md-option>
				<md-option ng-repeat="origin in origins | orderBy:'name'" ng-value="origin.id">
					[[ origin.name ]]
				</md-option>
			</md-select>
		</md-input-container>

		<md-input-container class="input-no-errors" flex-xs>
			<label>Gerencia</label>
			<md-icon md-font-icon="mdi mdi-account-outline"></md-icon>
			<md-select ng-model="filters.management_id">
				<md-option ng-value=""><em>Todos</em></md-option>
				<md-option ng-repeat="management in managements | orderBy:'name'" ng-value="management.id">
					[[ management.name ]]
				</md-option>
			</md-select>
		</md-input-container>

		<md-input-container class="input-no-errors" flex-xs>
			<label>Estado</label>
			<md-select ng-model="filters.status_id">
				<md-option ng-value=""><em>Todos</em></md-option>
				<md-option ng-repeat="status in candidatesStatuses | orderBy:'name'" ng-value="status.id">
					[[ status.name ]]
				</md-option>
			</md-select>
		</md-input-container>

		<md-input-container class="input-no-errors" flex-xs>
			<label>1ª Entrevista</label>
			<md-select ng-model="filters.appointmentfirst">
				<md-option ng-value="" selected><em>Todos</em></md-option>
				<md-option ng-value="1">Sin citar</md-option>
				<md-option ng-value="2">Citado</md-option>
				<md-option ng-value="3">Ha acudido</md-option>
				<md-option ng-value="4">No ha acudido</md-option>
			</md-select>
		</md-input-container>

		<md-input-container class="input-no-errors" flex-xs>
			<label>2ª Entrevista</label>
			<md-select ng-model="filters.appointmentsecond">
				<md-option ng-value="" selected><em>Todos</em></md-option>
				<md-option ng-value="1">Sin citar</md-option>
				<md-option ng-value="2">Citado</md-option>
				<md-option ng-value="3">Ha acudido</md-option>
				<md-option ng-value="4">No ha acudido</md-option>
			</md-select>
		</md-input-container>

		<md-input-container class="input-no-errors" flex-xs>
			<label>Preaprobado</label>
			<md-select ng-model="filters.preapproved">
				<md-option ng-value="" selected><em>Todos</em></md-option>
				<md-option ng-value="false">No</md-option>
				<md-option ng-value="true">Sí</md-option>
			</md-select>
		</md-input-container>

		<md-input-container class="input-no-errors" flex-xs>
			<label>Aprobado</label>
			<md-select ng-model="filters.approved">
				<md-option ng-value="" selected><em>Todos</em></md-option>
				<md-option ng-value="false">No</md-option>
				<md-option ng-value="true">Sí</md-option>
			</md-select>
		</md-input-container>

		<md-input-container class="input-no-errors" flex-xs>
			<label>Contratado</label>
			<md-select ng-model="filters.hired">
				<md-option ng-value="" selected><em>Todos</em></md-option>
				<md-option ng-value="false">No</md-option>
				<md-option ng-value="true">Sí</md-option>
			</md-select>
		</md-input-container>

		<md-input-container class="input-no-errors" flex-xs>
			<md-checkbox ng-model="filters.trashed" ng-false-value="" aria-label="">Descartados</md-checkbox>
		</md-input-container>
	</div>

	<div layout="row" layout-align="center center">
		<md-button ng-click="clearFilters()">
			<md-icon md-font-icon="mdi mdi-filter-remove"></md-icon>
			Limpiar filtros
		</md-button>

		<md-button class="md-accent" ng-click="filter()">
			<md-icon md-font-icon="mdi mdi-filter"></md-icon>
			Filtrar
		</md-button>
	</div>
</div>
