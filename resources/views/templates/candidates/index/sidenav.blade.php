<md-sidenav class="md-sidenav-right sidenav-candidate-followup" md-component-id="right" md-disable-scroll-target="#body > md-content">
	<md-toolbar layout="row" layout-align="space-between center">
		<h1 class="md-toolbar-tools" md-truncate>[[ candidate.name ]] [[ candidate.surname ]] [[ candidate.lastname ]]</h1>

		<md-button class="md-icon-button" ng-click="closeFollowup()">
			<md-icon md-font-icon="mdi mdi-close" aria-label="Close dialog"></md-icon>
		</md-button>
	</md-toolbar>

	<md-content layout="column" layout-margin>
		@include('templates.candidates.show.followup.table')

		<div layout="row" layout-align="center center" layout-margin>
			<md-button class="md-primary md-raised" ui-sref="candidates.show({ id: candidate.id })" flex>
				<md-icon md-font-icon="mdi mdi-eye"></md-icon>
				Ver
			</md-button>

			<md-button class="md-raised" ui-sref="candidates.show.edit({ id: candidate.id })" flex>
				<md-icon md-font-icon="mdi mdi-pencil"></md-icon>
				Editar
			</md-button>
		</div>
	</md-content>
</md-sidenav>
