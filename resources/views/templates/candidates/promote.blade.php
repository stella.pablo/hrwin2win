<md-dialog>
	<md-toolbar class="md-menu-toolbar">
		<div class="md-toolbar-tools">
			<h2>Promocionar</h2>

			<span flex></span>

			<md-button class="md-icon-button" ng-click="cancel()">
				<md-icon md-font-icon="mdi mdi-close" aria-label="Close dialog"></md-icon>
			</md-button>
		</div>
	</md-toolbar>

	<md-dialog-content>
		<div class="md-dialog-content">
			<p class="margin-top-0">
				El candidato <strong>[[ candidate.name + ' ' + candidate.surname ]]</strong> pasará a la siguiente fase de la contratación: <strong>[[ getCandidateNextState() ]]</strong>.
			</p>

			<div ng-hide="candidate.approved_user_id && candidate.approved">
				<p>Por favor, confirme esta operación con su <strong>Código PIN</strong> personal.</p>

				<form name="pinCodeForm">
					<md-input-container class="md-block">
						<label>Código PIN</label>
						<input ng-model="pinCode" type="password" ng-required="!(candidate.approved_user_id && candidate.approved)">
					</md-input-container>
				</form>
			</div>
		</div>
	</md-dialog-content>

	<md-dialog-actions layout="row" layout-align="space-between center">
		<md-button ng-click="cancel()">
			<md-icon md-font-icon="mdi mdi-close"></md-icon>
			Cancelar
		</md-button>

		<md-button class="md-primary md-raised" ng-click="submit()" ng-disabled="pinCodeForm.$invalid">
			<md-icon md-font-icon="mdi mdi-account-star"></md-icon>
			Promocionar
		</md-button>
	</md-dialog-actions>
</md-dialog>
