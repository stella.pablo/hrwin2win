<md-toolbar id="top-toolbar" class="md-menu-toolbar">
	<div class="md-toolbar-tools">
		<h1 md-truncate>[[ candidate.name ]] [[ candidate.surname ]] [[ candidate.lastname ]]</h1>

		<span flex></span>

		<md-menu class="padding-0" ng-hide="candidate.hired" hide-gt-xs>
			<md-button aria-label="Open phone interactions menu" class="md-icon-button" ng-click="$mdMenu.open($event)">
				<md-icon md-font-icon="mdi mdi-dots-vertical"></md-icon>
			</md-button>
			<md-menu-content width="4">
				<md-menu-item ng-hide="candidate.deleted_at">
					<md-button class="margin-0 md-warn" ng-click="delete()">
						<md-icon md-font-icon="mdi mdi-delete"></md-icon>
						Retirar
					</md-button>
				</md-menu-item>

				<md-menu-item ng-show="candidate.deleted_at">
					<md-button class="margin-0 md-primary md-raised" ng-click="unDelete()">
						<md-icon md-font-icon="mdi mdi-play"></md-icon>
						Habilitar
					</md-button>
				</md-menu-item>

				<md-menu-item>
					<md-button class="margin-0 margin-right-10" aria-label="" ui-sref="candidates.show.edit({ id: candidate.id })">
						<md-icon md-font-icon="mdi mdi-pencil"></md-icon>
						Editar
					</md-button>
				</md-menu-item>

				<md-menu-item>
					<md-button class="md-primary margin-0" aria-label="" ng-click="promote()">
						<md-icon md-font-icon="mdi mdi-account-star"></md-icon>
						Promocionar
					</md-button>
				</md-menu-item>

				<md-menu-item ng-show="candidate.hired">
					<md-button ui-sref="commercials.show({ id_comercial: candidate.external_id })">
						<md-icon md-font-icon="mdi mdi-account" style="color: inherit;"></md-icon>
						Ficha de Comercial
					</md-button>
				</md-menu-item>
			</md-menu-content>
		</md-menu>

		<div class="padding-0" ng-hide="candidate.hired" hide-xs>
			<md-button class="margin-0 md-warn" ng-click="delete()" ng-hide="candidate.deleted_at">
				<md-icon md-font-icon="mdi mdi-delete"></md-icon>
				Retirar
			</md-button>

			<md-button class="margin-0 md-primary md-raised" ng-click="unDelete()" ng-show="candidate.deleted_at">
				<md-icon md-font-icon="mdi mdi-play"></md-icon>
				Habilitar
			</md-button>

			<md-button class="margin-0 margin-right-10" aria-label="" ui-sref="candidates.show.edit({ id: candidate.id })">
				<md-icon md-font-icon="mdi mdi-pencil"></md-icon>
				Editar
			</md-button>

			<md-button class="md-primary margin-0" aria-label="" ng-click="promote()">
				<md-icon md-font-icon="mdi mdi-account-star"></md-icon>
				Promocionar
			</md-button>
		</div>

		<div class="padding-0" ng-show="candidate.hired">
			<md-button ui-sref="commercials.show({ id_comercial: candidate.external_id })">
				<md-icon md-font-icon="mdi mdi-account" style="color: inherit;"></md-icon>
				Ficha de Comercial
			</md-button>
		</div>
	</div>
</md-toolbar>

<md-content layout="column">
	<div layout="column" layout-margin ng-show="candidate.hired">
		<div layout="row" layout-align="space-between center" class="mdc-bg-light-green mdc-text-white" style="border-radius: 5px;">
			<p class="margin-10">Este candidato ha sido contratado!</p>
		</div>
	</div>

	<div layout="column" layout-gt-sm="row" layout-margin>
		@include('templates.candidates.show.info-basic')
		@include('templates.candidates.show.followup')
	</div>

	<div layout="row" layout-margin>
		@include('templates.candidates.show.comments')
	</div>

	<div layout="row" layout-margin>
		@include('templates.candidates.show.emails-sent')
	</div>

	<div layout="row" layout-margin ng-hide="candidate.hired">
		@include('templates.candidates.show.documents')
	</div>
</md-content>

