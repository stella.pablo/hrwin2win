<md-dialog>
	<md-toolbar class="md-menu-toolbar">
		<div class="md-toolbar-tools">
			<h2>[[ appointment.id ? 'Modificar cita' : 'Añadir cita' ]]</h2>

			<span flex></span>

			<md-button class="md-icon-button" ng-click="cancel()">
				<md-icon md-font-icon="mdi mdi-close" aria-label="Close dialog"></md-icon>
			</md-button>
		</div>
	</md-toolbar>

	<md-dialog-content>
		<div class="md-dialog-content">
			<div layout="row">
				<md-datepicker ng-model="appointment.date_only" md-min-date="today" md-placeholder="Fecha"></md-datepicker>

				<md-input-container class="margin-0 margin-left-20">
					<label>Hora</label>
					<input ng-model="appointment.time_only" type="text">
				</md-input-container>
			</div>

			<md-radio-group ng-model="appointment.status">
				<md-radio-button value="1">Citar</md-radio-button>
				<md-radio-button value="2">Ha acudido</md-radio-button>
				<md-radio-button value="0">No ha acudido</md-radio-button>
			</md-radio-group>

			<md-checkbox ng-model="appointment.sendMail" ng-true-value="1" ng-false-value="0">Enviar Email</md-checkbox>

			<md-input-container class="md-block">
				<label>Incluir en el email</label>
				<textarea ng-model="appointment.extrabody" rows="3"></textarea>
			</md-input-container>


			<md-input-container ng-show="appointment.status == 0" class="md-block">
				<label>Motivo</label>
				<textarea ng-model="appointment.reason" rows="1" placeholder="Motivo por el que no ha acudido a la cita"></textarea>
			</md-input-container>
		</div>
	</md-dialog-content>

	<md-dialog-actions layout="row" layout-align="space-between center">
		<md-button ng-click="cancel()">
			<md-icon md-font-icon="mdi mdi-close"></md-icon>
			Cancelar
		</md-button>

		<md-button class="md-accent md-raised" ng-click="submit()">
			<md-icon md-font-icon="mdi mdi-content-save"></md-icon>
			Guardar
		</md-button>
	</md-dialog-actions>
</md-dialog>
