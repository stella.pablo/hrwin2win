<table datatable="ng" dt-options="table.dtOptions" dt-instance="table.dtInstanceCallback" class="row-border hover full-width small">
	<thead>
	<tr>
		<th>ID Ext.</th>
		<th>Nombre</th>
		<th>Gerencia</th>
		<th>Campaña(s)</th>
		<th>Último contrato</th>
		<th>Fecha Alta</th>
	</tr>
	</thead>
	<tbody>
	<tr ui-sref="commercials.show({ id_comercial: commercial.id_comercial })" ng-repeat="commercial in commercials" class="clickable">
		<td>[[ commercial.id_comercial_externo ]]</td>
		<td>[[ commercial.nombre_completo ]]</td>
		<td>[[ commercial.management.gerencia ]]</td>
		<td>
			<span ng-repeat="campaign in commercial.campaigns">
				[[ campaign.abreviatura ]]<span ng-hide="$last">, </span>
			</span>
		</td>
		<td>[[ commercial.contract_last.hora_registro ]]</td>
		<td>[[ commercial.fecha_alta ]]</td>
	</tr>
	</tbody>
</table>
