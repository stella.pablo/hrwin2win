<md-toolbar id="top-toolbar" class="md-menu-toolbar">
	<div class="md-toolbar-tools">
		<md-button class="md-icon-button" ng-click="hiddenFilters = !hiddenFilters" hide-gt-xs>
			<md-icon md-font-icon="mdi mdi-filter"></md-icon>
		</md-button>

		<h2 flex md-truncate>
			Baja Productividad
		</h2>
	</div>
</md-toolbar>

<md-content layout="column" layout-margin>
	<div layout="column" layout-gt-sm="row" layout-align="center center" class="padding-15" ng-hide="$mdMedia('xs') && hiddenFilters">
		<div id="filters" layout="row" layout-align="center center" layout-margin layout-wrap>
			<md-input-container class="input-no-errors" flex-xs>
				<label>Fecha</label>
				<md-datepicker ng-model="filters.date_to" required ng-change="filter()"></md-datepicker>
			</md-input-container>

			<md-input-container class="input-no-errors" flex-xs>
				<label>Dias</label>
				<md-select ng-model="filters.days" required ng-change="filter()">
					<md-option ng-repeat="day in days" ng-value="day + 1">
						[[ day + 1 ]]
					</md-option>
				</md-select>
			</md-input-container>

			@if(auth()->user()->level < 3)
				<md-input-container class="input-no-errors" flex-xs>
					<label>Gerencia</label>
					<md-select ng-model="filters.management_id" ng-change="filter()">
						<md-option ng-value=""><em>Todos</em></md-option>
						<md-option ng-value="management.id_gerencia" ng-repeat="management in managements">[[ management.gerencia ]]</md-option>
					</md-select>
				</md-input-container>
			@endif

			<md-input-container class="input-no-errors" flex-xs>
				<label>Campaña</label>
				<md-select ng-model="filters.campaign_id" ng-change="filter()">
					<md-option ng-value=""><em>Todas</em></md-option>
					<md-option ng-value="campaign.abreviatura" ng-repeat="campaign in campaigns">[[ campaign.nombre ]]</md-option>
				</md-select>
			</md-input-container>
		</div>

		{{-- <div layout="column" layout-gt-sm="row" layout-align="center center"  ng-show="productivityType === 'productivity'">
			<div layout="column" layout-gt-sm="row" layout-align="center center" class="margin-right-20" style="min-width: 300px;">
				<span style="white-space: nowrap;" class="margin-right-10">Objetivo %</span>
				<rzslider rz-slider-model="sliders.objective.value" rz-slider-options="sliders.objective.options"></rzslider>
			</div>
			<div layout="column" layout-gt-sm="row" layout-align="center center" class="margin-right-20" style="min-width: 300px;">
				<span style="white-space: nowrap;" class="margin-right-10">Contratos OK</span>
				<rzslider rz-slider-model="sliders.contractsOK.minValue" rz-slider-high="sliders.contractsOK.maxValue" rz-slider-options="sliders.contractsOK.options"></rzslider>
			</div>
		</div>
		<div layout="column" layout-gt-sm="row" layout-align="center center"  ng-show="productivityType === 'productivity'">
			<div layout="column" layout-gt-sm="row" layout-align="center center" class="margin-right-20" style="min-width: 300px;">
				<span style="white-space: nowrap;" class="margin-right-10">Contratos P</span>
				<rzslider rz-slider-model="sliders.contractsP.minValue" rz-slider-high="sliders.contractsP.maxValue" rz-slider-options="sliders.contractsP.options"></rzslider>
			</div>
			<div layout="column" layout-gt-sm="row" layout-align="center center" class="margin-right-20" style="min-width: 300px;">
				<span style="white-space: nowrap;" class="margin-right-10">Contratos KO</span>
				<rzslider rz-slider-model="sliders.contractsKO.minValue" rz-slider-high="sliders.contractsKO.maxValue" rz-slider-options="sliders.contractsKO.options"></rzslider>
			</div>
		</div> --}}

		<div layout="column" layout-gt-sm="row" layout-align="center center">
			<md-button ng-click="clearFilters()">
				<md-icon md-font-icon="mdi mdi-filter-remove"></md-icon>
				Limpiar filtros
			</md-button>
		</div>
	</div>

	<div layout="column" md-whiteframe="2">
		<md-content layout="column" layout-padding class="mdc-bg-white">
			@include('templates.productivity-low.index.table')
		</md-content>
	</div>
</md-content>
