<md-dialog>
	<md-toolbar class="md-menu-toolbar">
		<div class="md-toolbar-tools">
			<h2>Enviar aviso sobre comercial</h2>

			<span flex></span>

			<md-button class="md-icon-button" ng-click="cancel()">
				<md-icon md-font-icon="mdi mdi-close" aria-label="Close dialog"></md-icon>
			</md-button>
		</div>
	</md-toolbar>

	<md-dialog-content>
		<div class="md-dialog-content">
			<md-select ng-model="report.motive_id">
				<md-option ng-value="motive.id" ng-repeat="motive in motives">[[ motive.name ]]</md-option>
			</md-select>
		</div>
	</md-dialog-content>

	<md-dialog-actions layout="row" layout-align="space-between center">
		<md-button ng-click="cancel()">
			<md-icon md-font-icon="mdi mdi-close"></md-icon>
			Cancelar
		</md-button>

		<md-button class="md-accent md-raised" ng-click="submit()" ng-disabled="!report.motive_id">
			<md-icon md-font-icon="mdi mdi-content-save"></md-icon>
			Guardar
		</md-button>
	</md-dialog-actions>
</md-dialog>
