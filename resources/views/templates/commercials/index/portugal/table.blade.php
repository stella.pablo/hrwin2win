<table datatable="ng" dt-instance="table.dtInstanceCallback" dt-options="table.dtOptions" dt-disable-deep-watchers="true" class="row-border hover full-width small">
	<thead>
		<tr>
			<th>Nombre</th>
			<th>ID Ext.</th>
			<th>Gerencia</th>
			<th>Gerente</th>
			<th>Campaña</th>
			<th>Fecha Alta</th>
		</tr>
	</thead>
	<tbody>
		<tr
			ng-repeat="commercial in commercials"
			ui-sref="commercials.show({ id_comercial: commercial.id_comercial })"
			class="clickable"
		>
			<td ng-class="{'mdc-text-red': commercial.fecha_baja}">
				<div layout="row" layout-align="start center">
					<img ng-src="[[ commercial.url_imagen_perfil ]]" class="mini-avatar" alt="">
					[[ commercial.nombre_completo ]]
				</div>
			</td>
			<td ng-class="{'mdc-text-red': commercial.fecha_baja}">[[ commercial.id_comercial_externo ]]</td>
			<td>[[ commercial.management.gerencia ]]</td>
			<td>[[ commercial.management.manager.nombre_completo ]]</td>
			<td>
				<span ng-repeat="campaign in commercial.campaigns">
					[[ campaign.abreviatura ]]<span ng-hide="$last">, </span>
				</span>
			</td>
			<td>[[ commercial.fecha_alta ]]</td>
		</tr>
	</tbody>
</table>
