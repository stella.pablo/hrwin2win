<table
	datatable="ng"
	dt-instance="table.dtInstanceCallback"
	dt-options="table.dtOptions"
	dt-disable-deep-watchers="true"
	class="row-border hover full-width small table-with-avatars"
>
	<thead>
		<tr>
			<th>Nombre</th>
			<th>ID Ext.</th>
			<th>Estrategia</th>
			<th>Gerente</th>
			<th>Gerencia</th>
			<th>Organización</th>
			<th>Campaña</th>
			<th>Fecha Alta</th>
			<th>Pago Sem 14</th>
			<th>Pago Sem 15</th>
			<th>Pago Sem 16</th>
			<th>Pago Sem 17</th>
		</tr>
	</thead>
	<tbody>
		<tr
			ng-repeat="commercial in commercials"
		>
			<td ui-sref="commercials.show({ id_comercial: commercial.id_comercial })" class="clickable" ng-class="{'mdc-text-red': commercial.fecha_baja}">
				<div layout="row" layout-align="start center">
					<img ng-src="[[ commercial.url_imagen_perfil ]]" class="mini-avatar" alt="">
					[[ commercial.nombre_completo ]]
				</div>
			</td>
			<td ui-sref="commercials.show({ id_comercial: commercial.id_comercial })" class="clickable" ng-class="{'mdc-text-red': commercial.fecha_baja}">[[ commercial.id_comercial_externo ]]</td>
			<td ui-sref="commercials.show({ id_comercial: commercial.id_comercial })" class="clickable">[[ commercial.strategy.id_estrategia ]]</td>
			<td ui-sref="commercials.show({ id_comercial: commercial.id_comercial })" class="clickable">[[ commercial.management.manager.nombre_completo ]]</td>
			<td ui-sref="commercials.show({ id_comercial: commercial.id_comercial })" class="clickable">[[ commercial.management.gerencia ]]</td>
			<td ui-sref="commercials.show({ id_comercial: commercial.id_comercial })" class="clickable">[[ commercial.management.organization.organizacion ]]</td>
			<td ui-sref="commercials.show({ id_comercial: commercial.id_comercial })" class="clickable">
				<span ng-repeat="campaign in commercial.campaigns">
					[[ campaign.abreviatura ]]<span ng-hide="$last">, </span>
				</span>
			</td>
			<td ui-sref="commercials.show({ id_comercial: commercial.id_comercial })" class="clickable">[[ commercial.fecha_alta ]]</td>
			<td class="text-center">
				<a
					ng-href="[[ getPaymentProofByWeek(commercial, 14) ]]"
					ng-if="getPaymentProofByWeek(commercial, 14)"
					target="_blank"
				>
					<md-icon md-font-icon="mdi mdi-thumb-up"></md-icon>
				</a>
			</td>
			<td class="text-center">
				<a
					ng-href="[[ getPaymentProofByWeek(commercial, 15) ]]"
					ng-if="getPaymentProofByWeek(commercial, 15)"
					target="_blank"
				>
					<md-icon md-font-icon="mdi mdi-thumb-up"></md-icon>
				</a>
			</td>
			<td class="text-center">
				<a
					ng-href="[[ getPaymentProofByWeek(commercial, 16) ]]"
					ng-if="getPaymentProofByWeek(commercial, 16)"
					target="_blank"
				>
					<md-icon md-font-icon="mdi mdi-thumb-up"></md-icon>
				</a>
			</td>
			<td class="text-center">
				<a
					ng-href="[[ getPaymentProofByWeek(commercial, 17) ]]"
					ng-if="getPaymentProofByWeek(commercial, 17)"
					target="_blank"
				>
					<md-icon md-font-icon="mdi mdi-thumb-up"></md-icon>
				</a>
			</td>
		</tr>
	</tbody>
</table>

{{--<div class="avatar-hover" ng-show="avatarUrl">--}}
{{--	<img ng-src="[[ avatarUrl ]]" alt="" md-whiteframe="4">--}}
{{--</div>--}}
