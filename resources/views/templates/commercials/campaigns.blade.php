<md-dialog>
	<md-toolbar class="md-menu-toolbar">
		<div class="md-toolbar-tools">
			<h2>Gestión de campañas</h2>

			<span flex></span>

			<md-button class="md-icon-button" ng-click="cancel()">
				<md-icon md-font-icon="mdi mdi-close" aria-label="Close dialog"></md-icon>
			</md-button>
		</div>
	</md-toolbar>

	<md-dialog-content>
		<div layout="row" layout-align="center center" class="md-dialog-content">
			<md-input-container flex>
				<label>Campaña</label>
				<md-select ng-model="commercialCampaigns" multiple required>
					<md-option ng-value="campaign.id" ng-repeat="campaign in campaigns | orderBy:'nombre'">[[ campaign.nombre ]] ([[ campaign.abreviatura ]])</md-option>
				</md-select>
			</md-input-container>

			<div>
				<md-button class="md-primary" ng-click="submit()">
					Guardar
				</md-button>
			</div>
		</div>
	</md-dialog-content>
</md-dialog>
