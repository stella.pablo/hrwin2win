<form name="commercialForm">
	<div layout="column" class="margin-10">
		<div layout="column" ng-show="candidate.id">
			<h3>Datos del candidato</h3>

			<md-input-container>
				<md-icon md-font-icon="mdi mdi-account-box"></md-icon>
				<input ng-model="candidate.fullname" placeholder="Nombre completo" disabled>
			</md-input-container>
		</div>

		<div layout="column">
			<h3>Datos de estructura</h3>

			<div layout="row" layout-margin>
				<md-input-container flex>
					<label>Gerencia (Gerente)</label>
					<md-select ng-model="commercial.id_gerencia">
						<md-option ng-value="management.id_gerencia" ng-repeat="management in managements | orderBy:'gerencia'">[[ management.gerencia ]] ([[ management.manager.nombre_completo ]])</md-option>
					</md-select>
				</md-input-container>

				<md-input-container flex ng-hide="commercial.id_comercial">
					<label>Campaña(s)</label>
					<md-select ng-model="commercial.campaigns" ng-model-options="{trackBy: '$value.id'}" multiple required>
						<md-option ng-value="campaign" ng-repeat="campaign in campaigns | orderBy:'nombre'">[[ campaign.nombre ]] ([[ campaign.abreviatura ]])</md-option>
					</md-select>
				</md-input-container>

				<md-input-container flex>
					<md-icon md-font-icon="mdi mdi-account-box"></md-icon>
					<input ng-model="commercial.id_comercial_externo" placeholder="ID Externo" required>
				</md-input-container>
			</div>
		</div>

		<div layout="column">
			<h3>Datos personales</h3>

			<div layout="row" layout-margin layout-wrap>
				<md-input-container>
					<md-icon md-font-icon="mdi mdi-account-box"></md-icon>
					<input ng-model="commercial.nombre" placeholder="Nombre" required>
				</md-input-container>

				<md-input-container>
					<md-icon md-font-icon="mdi mdi-account-box"></md-icon>
					<input ng-model="commercial.apellido1" placeholder="Primer Apellido" required>
				</md-input-container>

				<md-input-container>
					<md-icon md-font-icon="mdi mdi-account-box"></md-icon>
					<input ng-model="commercial.apellido2" placeholder="Segundo Apellido" required>
				</md-input-container>

				<md-input-container>
					<md-icon md-font-icon="mdi mdi-account-box"></md-icon>
					<input ng-model="commercial.nif" placeholder="DNI/NIF/Nº Bi" required>
				</md-input-container>

				<md-input-container>
					<md-icon md-font-icon="mdi mdi-email"></md-icon>
					<input ng-model="commercial.email" placeholder="E-mail" required>
				</md-input-container>

				<md-input-container>
					<md-icon md-font-icon="mdi mdi-phone-classic"></md-icon>
					<input ng-model="commercial.telefono_fijo" placeholder="Telefono fijo">
				</md-input-container>

				<md-input-container>
					<md-icon md-font-icon="mdi mdi-cellphone"></md-icon>
					<input ng-model="commercial.telefono_movil" placeholder="Telefono movil" required>
				</md-input-container>

				<md-input-container>
					<md-icon md-font-icon="mdi mdi-map-marker"></md-icon>
					<input ng-model="commercial.direccion" placeholder="Dirección">
				</md-input-container>

				<md-input-container>
					<md-icon md-font-icon="mdi mdi-map-marker"></md-icon>
					<input ng-model="commercial.poblacion" placeholder="Población">
				</md-input-container>

				<md-input-container>
					<md-icon md-font-icon="mdi mdi-map-marker"></md-icon>
					<input ng-model="commercial.cod_postal" placeholder="Código Postal">
				</md-input-container>
			</div>
		</div>

		<div layout="column">
			<h3>Datos del contrato</h3>

			<div layout="row" layout-margin layout-wrap>
				<md-input-container flex>
					<label>Tipo de contrato</label>
					<md-select ng-model="commercial.company_contract.id_tipo_contrato_comercial" ng-change="loadContractsSubtypes()" required>
						<md-option ng-value="type.id_tipo_contrato_comercial" ng-repeat="type in typesContracts | orderBy:'tipo_contrato_comercial'">[[ type.tipo_contrato_comercial ]]</md-option>
					</md-select>
				</md-input-container>

				<md-input-container flex>
					<label>Tramo</label>
					<md-select ng-model="commercial.company_contract.id_tramo" required>
						<md-option ng-value="subtype.id_tramo" ng-repeat="subtype in typesContractsSubtype | orderBy:'descripcion'">[[ subtype.descripcion ]]</md-option>
					</md-select>
				</md-input-container>
			</div>

			<div layout="column" ng-show="commercial.company_contract.id_tipo_contrato_comercial">
				<table class="full-width margin-top-10 margin-bottom-20">
					<thead>
					<tr>
						<th>Tramo</th>
						<th>Horas</th>
						<th>Mínimo contratos</th>
						<th>Mínimo plus</th>
						<th>Plus</th>
						<th>Sueldo base</th>
					</tr>
					</thead>

					<tbody>
					<tr
						ng-repeat="subtype in typesContractsSubtype | orderBy:'descripcion'"
						ng-class="{'mdc-bg-grey-100': commercial.company_contract.id_tramo == subtype.id_tramo}"
					>
						<td>[[ subtype.descripcion ]]</td>
						<td>[[ subtype.horas ]]h</td>
						<td>[[ subtype.minimo_contratos ]]</td>
						<td>[[ subtype.minimo_plus ]]</td>
						<td>[[ subtype.plus | currency ]]</td>
						<td>[[ subtype.sueldo_base | currency ]]</td>
					</tr>
					</tbody>
				</table>
			</div>
		</div>

		<div layout="column">
			<h3>Acceso sistema</h3>

			<div layout="column" layout-margin>
				<md-checkbox
					ng-model="commercial.create_access_mobile"
					class="md-align-top-left"
					ng-disabled="!commercial.email || !commercial.id_comercial_externo"
				>
					Crear o re-crear acceso a la plataforma: Mobile<br>
					<small class="mdc-text-grey">
						Es obligatorio que el comercial tenga Email e ID Ext. establecido en su perfil
					</small>
				</md-checkbox>
			</div>
		</div>

		<div layout="row" layout-align="space-between center" class="margin-top-40">
			<md-button ng-click="cancel()">
				<md-icon md-font-icon="mdi mdi-cancel"></md-icon>
				Cancelar
			</md-button>

			<md-button ng-click="submit($event)" class="md-primary md-raised" ng-disabled="commercialForm.$invalid || commercialForm.$pristine">
				<md-icon md-font-icon="mdi mdi-content-save"></md-icon>
				Guardar
			</md-button>
		</div>
	</div>
</form>
