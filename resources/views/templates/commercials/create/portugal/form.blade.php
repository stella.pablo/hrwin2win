<form name="commercialForm">
	<div layout="column" class="margin-10">
		<div layout="column" ng-show="candidate.id">
			<h3>Datos del candidato</h3>

			<md-input-container>
				<md-icon md-font-icon="mdi mdi-account-box"></md-icon>
				<input ng-model="candidate.fullname" placeholder="Nombre completo" disabled>
			</md-input-container>
		</div>

		<div layout="column">
			<h3>Datos de estructura</h3>

			<div layout="row" layout-margin>
				<md-input-container flex>
					<label>Gerencia (Gerente)</label>
					<md-select ng-model="commercial.id_gerencia">
						<md-option ng-value="management.id_gerencia" ng-repeat="management in managements | orderBy:'gerencia'">[[ management.gerencia ]] ([[ management.manager.nombre_completo ]])
						</md-option>
					</md-select>
				</md-input-container>

				<md-input-container flex ng-hide="commercial.id_comercial">
					<label>Campaña(s)</label>
					<md-select ng-model="commercial.campaigns" ng-model-options="{trackBy: '$value.id'}" multiple required>
						<md-option ng-value="campaign" ng-repeat="campaign in campaigns | orderBy:'nombre'">[[ campaign.nombre ]] ([[ campaign.abreviatura ]])</md-option>
					</md-select>
				</md-input-container>

				<md-input-container flex>
					<md-icon md-font-icon="mdi mdi-account-box"></md-icon>
					<input ng-model="commercial.id_comercial_externo" placeholder="ID Externo" required>
				</md-input-container>
			</div>
		</div>

		<div layout="column">
			<h3>Datos personales</h3>

			<div layout="row" layout-margin layout-wrap>
				<md-input-container>
					<md-icon md-font-icon="mdi mdi-account-box"></md-icon>
					<input ng-model="commercial.nombre" placeholder="Nombre" required>
				</md-input-container>

				<md-input-container>
					<md-icon md-font-icon="mdi mdi-account-box"></md-icon>
					<input ng-model="commercial.apellido1" placeholder="Primer Apellido" required>
				</md-input-container>

				<md-input-container>
					<md-icon md-font-icon="mdi mdi-account-box"></md-icon>
					<input ng-model="commercial.apellido2" placeholder="Segundo Apellido" required>
				</md-input-container>

				<md-input-container>
					<md-icon md-font-icon="mdi mdi-account-box"></md-icon>
					<input ng-model="commercial.nif" placeholder="DNI/NIF/Nº Bi" required>
				</md-input-container>

				<md-input-container>
					<md-icon md-font-icon="mdi mdi-email"></md-icon>
					<input ng-model="commercial.email" placeholder="E-mail" required>
				</md-input-container>

				<md-input-container>
					<md-icon md-font-icon="mdi mdi-phone-classic"></md-icon>
					<input ng-model="commercial.telefono_fijo" placeholder="Telefono fijo">
				</md-input-container>

				<md-input-container>
					<md-icon md-font-icon="mdi mdi-cellphone"></md-icon>
					<input ng-model="commercial.telefono_movil" placeholder="Telefono movil" required>
				</md-input-container>

				<md-input-container>
					<md-icon md-font-icon="mdi mdi-map-marker"></md-icon>
					<input ng-model="commercial.direccion" placeholder="Dirección">
				</md-input-container>

				<md-input-container>
					<md-icon md-font-icon="mdi mdi-map-marker"></md-icon>
					<input ng-model="commercial.poblacion" placeholder="Población">
				</md-input-container>

				<md-input-container>
					<md-icon md-font-icon="mdi mdi-map-marker"></md-icon>
					<input ng-model="commercial.cod_postal" placeholder="Código Postal">
				</md-input-container>
			</div>
		</div>

		<div layout="column" md-whiteframe="2" class="mdc-bg-white" flex>
			<md-toolbar class="md-menu-toolbar">
				<div class="md-toolbar-tools">
					<h2 flex md-truncate>Historial de contratos</h2>

					<md-button class="md-accent" ng-click="alterContracts(null,null)">
						<md-icon md-font-icon="mdi mdi-plus"></md-icon>
						Añadir contrato
					</md-button>
				</div>
			</md-toolbar>

			<md-content class="mdc-bg-white">
				<table datatable="ng" dt-instance="table.contracts.dtInstanceCallback" dt-options="table.contracts.dtOptions" dt-disable-deep-watchers="true"
				       class="row-border hover full-width small">
					<thead>
					<tr>
						<th>#</th>
						<th>Tipo contrato</th>
						<th>Tipo tramo</th>
						<th>Fecha desde</th>
						<th>Fecha fin</th>
						<th>Editar</th>
						<th>Eliminar</th>
					</tr>
					</thead>
					<tbody>
					<tr ng-repeat="contract in commercial.company_contracts" class="clickable">
						<td>[[contract.id_comercial_contrato ? contract.id_comercial_contrato : 'Pendiente']]</td>
						<td>[[contract.contract_type.tipo_contrato_comercial]]</td>
						<td>[[contract.contract_stretch.descripcion]]</td>
						<td>[[moment(contract.fecha_desde).format('D-MMM-YYYY')]]</td>
						<td>[[contract.fecha_hasta ? moment(contract.fecha_hasta).format('D-MMM-YYYY') : 'Sin limite' ]]</td>
						<td><a style="color:lightblue" ng-click="alterContracts(contract.id_comercial_contrato,$index)"> Editar </a></td>
						<td><a style="color:lightcoral" ng-click="removeContract(contract.id_comercial_contrato,$index)"> Eliminar </a></td>

					</tr>
					</tbody>
				</table>
			</md-content>
		</div>


		<div layout="column">
			<h3>Acceso sistema</h3>

			<div layout="column" layout-margin>
				<md-checkbox
					ng-model="commercial.create_access_mobile"
					class="md-align-top-left"
					ng-disabled="!commercial.email || !commercial.id_comercial_externo"
				>
					Crear o re-crear acceso a la plataforma: Mobile<br>
					<small class="mdc-text-grey">
						Es obligatorio que el comercial tenga Email e ID Ext. establecido en su perfil
					</small>
				</md-checkbox>
			</div>
		</div>

		<div layout="row" layout-align="space-between center" class="margin-top-40">
			<md-button ng-click="cancel()">
				<md-icon md-font-icon="mdi mdi-cancel"></md-icon>
				Cancelar
			</md-button>

			<md-button ng-click="checkIsValid($event)" class="md-primary md-raised" ng-disabled="commercialForm.$invalid || commercialForm.$pristine">
				<md-icon md-font-icon="mdi mdi-content-save"></md-icon>
				Guardar
			</md-button>
		</div>
	</div>
</form>
