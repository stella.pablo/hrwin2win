<md-content>
	<div layout="column" layout-margin>
		<div layout="column" md-whiteframe="2" flex class="mdc-bg-white">
			<md-progress-linear md-mode="determinate" value="[[ avatarUpProgress ]]" ng-style="avatarUpBarStyle" style="margin-bottom: -5px !important; z-index: 9;"></md-progress-linear>

			<div layout="column" layout-gt-sm="row" class="commercial-show">
				<div layout="row" layout-align="center center">
					<div
						class="avatar"
						ng-mouseenter="showAvatarOptions = true"
						ng-mouseleave="showAvatarOptions = false"
						ng-style="{'background-image': 'url(' + commercial.url_imagen_perfil + ')'}"
					>
						<div layout="row" class="avatar-options" ng-show="showAvatarOptions">
							<div class="avatar-option" flex ngf-select="uploadAvatar($file)">
								<md-icon md-font-icon="mdi mdi-upload"></md-icon>
							</div>

							<div class="avatar-option" flex ng-click="deleteAvatar()">
								<md-icon md-font-icon="mdi mdi-delete"></md-icon>
							</div>
						</div>
					</div>

					<div hide-gt-xs flex-xs>
						<div layout="column">
							<h2 class="margin-0" style="font-size: 21px;">[[ commercial.nombre_completo ]]</h2>

							<div style="font-size: 12px;" ng-class="{'mdc-text-green': commercial.id_estado == 0, 'mdc-text-red': commercial.id_estado != 0}">
								<span ng-show="commercial.id_estado == 0">
									Estado: <strong>de Alta</strong>
									<span ng-show="commercial.fecha_alta">(desde: [[ moment(commercial.fecha_alta).format('DD-MMM-YYYY') ]])</span>
								</span>

								<span ng-show="commercial.id_estado != 0">
									Estado: <strong>de Baja</strong>
									<span ng-show="commercial.fecha_baja">(desde: [[ moment(commercial.fecha_baja).format('DD-MMM-YYYY') ]])</span>
								</span>
							</div>
						</div>
					</div>
				</div>

				<div layout="column" layout-margin flex class="content">
					<div
						layout="column"
						layout-gt-sm="row"
						layout-align="center center"
						layout-align-gt-sm="space-between start"
					>
						<div layout="column" hide-xs>
							<h2 class="margin-0" style="font-size: 21px;">[[ commercial.nombre_completo ]]</h2>

							<div style="font-size: 12px;" ng-class="{'mdc-text-green': commercial.id_estado == 0, 'mdc-text-red': commercial.id_estado != 0}">
								<span ng-show="commercial.id_estado == 0">
									Estado: <strong>de Alta</strong>
									<span ng-show="commercial.fecha_alta">(desde: [[ moment(commercial.fecha_alta).format('DD-MMM-YYYY') ]])</span>
								</span>

								<span ng-show="commercial.id_estado != 0">
									Estado: <strong>de Baja</strong>
									<span ng-show="commercial.fecha_baja">(desde: [[ moment(commercial.fecha_baja).format('DD-MMM-YYYY') ]])</span>
								</span>
							</div>
						</div>

						<div class="padding-0" layout="row" layout-wrap layout-align="center center">
							<md-button ui-sref="candidates.show({ id: candidate.id })" ng-show="candidate.id" class="margin-top-0">
								<md-icon md-font-icon="mdi mdi-account-convert" style="color: inherit;"></md-icon>
								Ficha de Candidato
							</md-button>
							@if(auth()->user()->level < 3)
								<md-button ui-sref="commercials.show.edit({ id: commercial.id_comercial })" class="margin-top-0">
									<md-icon md-font-icon="mdi mdi-pencil"></md-icon>
									Editar
								</md-button>

								<md-button ng-click="enableCommercial()" class="margin-top-0 md-primary" ng-show="commercial.fecha_baja">
									<md-icon md-font-icon="mdi mdi-account-check"></md-icon>
									Habilitar
								</md-button>

								<md-button
									ng-click="disableCommercial()"
									ng-disabled="commercial.id_comercial == commercial.management.id_comercial_liquidacion"
									ng-hide="commercial.fecha_baja"
									class="margin-top-0 md-warn"
								>
									<md-icon md-font-icon="mdi mdi-account-off"></md-icon>
									Deshabilitar
								</md-button>
							@endif
						</div>
					</div>

					<div layout="column" layout-gt-sm="row" ng-show="$mdMedia('gt-sm')">
						<div flex>
							<table class="full-width table-first-nowrap small">
								<tbody>
								<tr>
									<td>
										<md-icon md-font-icon="mdi mdi-calendar-check"></md-icon>
										Fecha de alta
									</td>
									<td>
										<span ng-show="commercial.fecha_alta">[[ moment(commercial.fecha_alta).format('DD-MMM-YYYY') ]]</span>
									</td>
									<td></td>
								</tr>
								<tr ng-show="commercial.id_estado != 0 && commercial.fecha_baja">
									<td>
										<md-icon md-font-icon="mdi mdi-calendar-minus"></md-icon>
										Fecha de baja
									</td>
									<td>
										<span>[[ moment(commercial.fecha_baja).format('DD-MMM-YYYY') ]]</span>
									</td>
									<td></td>
								</tr>
								<tr>
									<td>
										<md-icon md-font-icon="mdi mdi-pound"></md-icon>
										ID
									</td>
									<td>
										[[ commercial.id_comercial ]]
									</td>
									<td></td>
								</tr>
								<tr>
									<td>
										<md-icon md-font-icon="mdi mdi-pound"></md-icon>
										ID Externo
									</td>
									<td>
										[[ commercial.id_comercial_externo ]]
									</td>
									<td></td>
								</tr>
								<tr ng-show="commercial.nif">
									<td>
										<md-icon md-font-icon="mdi mdi-pound"></md-icon>
										DNI/NIF
									</td>
									<td>
										[[ commercial.nif ]]
									</td>
									<td></td>
								</tr>
								<tr ng-show="commercial.telefono_fijo">
									<td>
										<md-icon md-font-icon="mdi mdi-phone-classic"></md-icon>
										Teléfono fijo
									</td>
									<td>
										<a ng-href="tel:[[ commercial.telefono_fijo ]]">[[ commercial.telefono_fijo ? commercial.telefono_fijo : 'Sin información' ]]</a>
									</td>
									<td></td>
								</tr>
								<tr ng-show="commercial.telefono_movil">
									<td>
										<md-icon md-font-icon="mdi mdi-phone"></md-icon>
										Teléfono móvil
									</td>
									<td>
										<a ng-href="tel:[[ commercial.telefono_movil ]]">[[ commercial.telefono_movil ? commercial.telefono_movil : 'Sin información' ]]</a>
									</td>
									<td></td>
								</tr>
								<tr ng-show="commercial.email">
									<td>
										<md-icon md-font-icon="mdi mdi-at"></md-icon>
										Email
									</td>
									<td>
										<a ng-href="mailto:[[ commercial.email ]]">[[ commercial.email ? commercial.email : 'Sin información' ]]</a>
									</td>
									<td></td>
								</tr>
								</tbody>
							</table>
						</div>

						<div flex>
							<table class="full-width table-first-nowrap small">
								<tbody>
								<tr ng-show="commercial.direccion">
									<td>
										<md-icon md-font-icon="mdi mdi-map-marker"></md-icon>
										Dirección
									</td>
									<td>
										[[ commercial.direccion ]]
									</td>
									<td></td>
								</tr>
								<tr ng-show="commercial.poblacion">
									<td>
										<md-icon md-font-icon="mdi mdi-map-marker"></md-icon>
										Población (Cód. Postal)
									</td>
									<td>
										[[ commercial.poblacion ]] <span ng-show="commercial.cod_postal">([[ commercial.cod_postal ]])</span>
									</td>
									<td></td>
								</tr>

								<tr>
									<td>
										<md-icon md-font-icon="mdi mdi-account-multiple"></md-icon>
										Gerencia
									</td>
									<td>
										<a ui-sref="managements.show({ id_gerencia: commercial.management.id_gerencia })">
											[[ commercial.management.gerencia ]]
										</a>
									</td>
									<td></td>
								</tr>
								<tr>
									<td>
										<md-icon md-font-icon="mdi mdi-account"></md-icon>
										Gerente
									</td>
									<td>
										<a ui-sref="commercials.show({ id_comercial: commercial.management.manager.id_comercial })">
											[[ commercial.management.manager.nombre_completo ]]
										</a>
									</td>
									<td></td>
								</tr>
								<tr>
									<td>
										<md-icon md-font-icon="mdi mdi-account-badge-horizontal-outline"></md-icon>
										Campaña(s)
									</td>
									<td>
										<div ng-show="commercial.campaigns.length">
											<span ng-repeat="campaign in commercial.campaigns">
												[[ campaign.nombre ]] ([[ campaign.abreviatura ]])<span ng-hide="$last">, </span>
											</span>
										</div>
										<em ng-hide="commercial.campaigns.length">No tiene</em>
									</td>
									<td>
										<md-icon md-font-icon="mdi mdi-square-edit-outline" ng-click="campaignsSet()" class="margin-left-10 mdc-text-teal clickable"></md-icon>
									</td>
								</tr>
								</tbody>
							</table>
						</div>
					</div>

					<div layout="column" ng-hide="$mdMedia('gt-sm')">
						<table class="full-width table-first-nowrap small">
							<tbody>
							<tr>
								<td>
									<md-icon md-font-icon="mdi mdi-calendar-check"></md-icon>
									Fecha de alta
								</td>
								<td></td>
							</tr>

							<tr>
								<td class="text-center" colspan="2">
									<span ng-show="commercial.fecha_alta">[[ moment(commercial.fecha_alta).format('DD-MMM-YYYY') ]]</span>
								</td>
							</tr>
							<tr ng-show="commercial.id_estado != 0 && commercial.fecha_baja">
								<td>
									<md-icon md-font-icon="mdi mdi-calendar-minus"></md-icon>
									Fecha de baja
								</td>
								<td></td>
							</tr>

							<tr ng-show="commercial.id_estado != 0 && commercial.fecha_baja">
								<td class="text-center" colspan="2">
									<span>[[ moment(commercial.fecha_baja).format('DD-MMM-YYYY') ]]</span>
								</td>
							</tr>
							<tr>
								<td>
									<md-icon md-font-icon="mdi mdi-pound"></md-icon>
									ID
								</td>
								<td></td>
							</tr>

							<tr>
								<td class="text-center" colspan="2">
									[[ commercial.id_comercial ]]
								</td>
							</tr>
							<tr>
								<td>
									<md-icon md-font-icon="mdi mdi-pound"></md-icon>
									ID Externo
								</td>
								<td></td>
							</tr>

							<tr>
								<td class="text-center" colspan="2">
									[[ commercial.id_comercial_externo ]]
								</td>
							</tr>
							<tr ng-show="commercial.nif">
								<td>
									<md-icon md-font-icon="mdi mdi-pound"></md-icon>
									DNI/NIF
								</td>
								<td></td>
							</tr>

							<tr ng-show="commercial.nif">
								<td class="text-center" colspan="2">
									[[ commercial.nif ]]
								</td>
							</tr>
							<tr ng-show="commercial.telefono_fijo">
								<td>
									<md-icon md-font-icon="mdi mdi-phone-classic"></md-icon>
									Teléfono fijo
								</td>
								<td></td>
							</tr>

							<tr ng-show="commercial.telefono_fijo">
								<td class="text-center" colspan="2">
									<a ng-href="tel:[[ commercial.telefono_fijo ]]">[[ commercial.telefono_fijo ? commercial.telefono_fijo : 'Sin información' ]]</a>
								</td>
							</tr>
							<tr ng-show="commercial.telefono_movil">
								<td>
									<md-icon md-font-icon="mdi mdi-phone"></md-icon>
									Teléfono móvil
								</td>
								<td></td>
							</tr>

							<tr ng-show="commercial.telefono_movil">
								<td class="text-center" colspan="2">
									<a ng-href="tel:[[ commercial.telefono_movil ]]">[[ commercial.telefono_movil ? commercial.telefono_movil : 'Sin información' ]]</a>
								</td>
							</tr>
							<tr ng-show="commercial.email">
								<td>
									<md-icon md-font-icon="mdi mdi-at"></md-icon>
									Email
								</td>
								<td></td>
							</tr>

							<tr ng-show="commercial.email">
								<td class="text-center" colspan="2">
									<a ng-href="mailto:[[ commercial.email ]]">[[ commercial.email ? commercial.email : 'Sin información' ]]</a>
								</td>
							</tr>
							<tr ng-show="commercial.direccion">
								<td>
									<md-icon md-font-icon="mdi mdi-map-marker"></md-icon>
									Dirección
								</td>
								<td></td>
							</tr>

							<tr ng-show="commercial.direccion">
								<td class="text-center" colspan="2">
									[[ commercial.direccion ]]
								</td>
							</tr>
							<tr ng-show="commercial.poblacion">
								<td>
									<md-icon md-font-icon="mdi mdi-map-marker"></md-icon>
									Población (Cód. Postal)
								</td>
								<td></td>
							</tr>

							<tr ng-show="commercial.poblacion">
								<td class="text-center" colspan="2">
									[[ commercial.poblacion ]] <span ng-show="commercial.cod_postal">([[ commercial.cod_postal ]])</span>
								</td>
							</tr>

							<tr>
								<td>
									<md-icon md-font-icon="mdi mdi-account-multiple"></md-icon>
									Gerencia
								</td>
								<td></td>
							</tr>

							<tr>
								<td class="text-center" colspan="2">
									<a ui-sref="managements.show({ id_gerencia: commercial.management.id_gerencia })">
										[[ commercial.management.gerencia ]]
									</a>
								</td>
							</tr>
							<tr>
								<td>
									<md-icon md-font-icon="mdi mdi-account"></md-icon>
									Gerente
								</td>
								<td></td>
							</tr>

							<tr>
								<td class="text-center" colspan="2">
									<a ui-sref="commercials.show({ id_comercial: commercial.management.manager.id_comercial })">
										[[ commercial.management.manager.nombre_completo ]]
									</a>
								</td>
							</tr>
							<tr>
								<td>
									<md-icon md-font-icon="mdi mdi-account-badge-horizontal-outline"></md-icon>
									Campaña(s)
								</td>
								<td></td>
							</tr>

							<tr>
								<td class="text-center" colspan="2">
									<div ng-show="commercial.campaigns.length" style="display: inline;">
										<span ng-repeat="campaign in commercial.campaigns">
											[[ campaign.nombre ]] ([[ campaign.abreviatura ]])<span ng-hide="$last">, </span>
										</span>
									</div>
									<em ng-hide="commercial.campaigns.length">No tiene</em>
									<md-icon md-font-icon="mdi mdi-square-edit-outline" ng-click="campaignsSet()" class="margin-left-10 mdc-text-teal clickable"></md-icon>
								</td>
							</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>

			<div
				class="mdc-bg-grey-50 padding-top-5 padding-bottom-5 padding-left-10 padding-right-10"
			>
				<div
					layout="row"
					layout-align="space-between center"
					ng-show="$mdMedia('gt-xs')"
				>
					<small>Objetivo este mes: [[ commercialStats.contratos_adiadehoy_porcentaje ]]%</small>
					<small>OK: [[ commercialStats.contracts_ok_count ]]</small>
					<small>Pend: [[ commercialStats.contracts_p_count ]]</small>
					<small>KO: [[ commercialStats.contracts_ko_count ]]</small>
					<small>Total: [[ commercialStats.numero_contratos_total ]]</small>
					<small>
						Previsión: [[ commercialStats.contratos_prevision ]]

						<md-icon md-font-icon="mdi mdi-arrow-bottom-right" class="mdc-text-red-300" ng-show="commercialStats.contratos_prevision_porcentaje < 75"></md-icon>
						<md-icon md-font-icon="mdi mdi-minus" class="mdc-text-orange-300"
						         ng-show="commercialStats.contratos_prevision_porcentaje >= 75 && commercialStats.contratos_prevision_porcentaje < 95"></md-icon>
						<md-icon md-font-icon="mdi mdi-arrow-top-right" class="mdc-text-green-300" ng-show="commercialStats.contratos_prevision_porcentaje >= 95"></md-icon>
					</small>
				</div>

				<table ng-hide="$mdMedia('gt-xs')" class="full-width">
					<thead>
					<tr>
						<th style="font-size: 12px;" class="text-center">Objetivo:</th>
						<th style="font-size: 12px;" class="text-center">OK:</th>
						<th style="font-size: 12px;" class="text-center">Pend:</th>
						<th style="font-size: 12px;" class="text-center">KO:</th>
						<th style="font-size: 12px;" class="text-center">Total:</th>
						<th style="font-size: 12px;" class="text-center">Previsión:</th>
					</tr>
					</thead>
					<tbody>
					<tr>
						<td style="font-size: 12px" class="text-center">[[ commercialStats.contratos_adiadehoy_porcentaje ]]%</td>
						<td style="font-size: 12px" class="text-center">[[ commercialStats.contracts_ok_count ]]</td>
						<td style="font-size: 12px" class="text-center">[[ commercialStats.contracts_p_count ]]</td>
						<td style="font-size: 12px" class="text-center">[[ commercialStats.contracts_ko_count ]]</td>
						<td style="font-size: 12px" class="text-center">[[ commercialStats.numero_contratos_total ]]</td>
						<td style="font-size: 12px" class="text-center">
							[[ commercialStats.contratos_prevision ]]

							<md-icon md-font-icon="mdi mdi-18px mdi-arrow-bottom-right" class="mdc-text-red-300" ng-show="commercialStats.contratos_prevision_porcentaje < 75"></md-icon>
							<md-icon md-font-icon="mdi mdi-18px mdi-minus" class="mdc-text-orange-300"
							         ng-show="commercialStats.contratos_prevision_porcentaje >= 75 && commercialStats.contratos_prevision_porcentaje < 95"></md-icon>
							<md-icon md-font-icon="mdi mdi-18px mdi-arrow-top-right" class="mdc-text-green-300" ng-show="commercialStats.contratos_prevision_porcentaje >= 95"></md-icon>
						</td>
					</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>

	@include('templates.commercials.show.stats')
	@include('templates.commercials.show.absences')
	@include('templates.commercials.show.files')
	@include('templates.commercials.show.structures')

</md-content>
