<table class="full-width table-first-nowrap">
	<tbody>
		<tr>
			<td>
				<md-icon md-font-icon="mdi mdi-calendar-check"></md-icon> Fecha de alta
			</td>
			<td>
				<span ng-show="commercial.fecha_alta">[[ moment(commercial.fecha_alta).format('DD-MMM-YYYY') ]]</span>
			</td>
			<td></td>
		</tr>
		<tr ng-show="commercial.id_estado != 0 && commercial.fecha_baja">
			<td>
				<md-icon md-font-icon="mdi mdi-calendar-minus"></md-icon> Fecha de baja
			</td>
			<td>
				<span>[[ moment(commercial.fecha_baja).format('DD-MMM-YYYY') ]]</span>
			</td>
			<td></td>
		</tr>
		<tr>
			<td>
				<md-icon md-font-icon="mdi mdi-pound"></md-icon> ID
			</td>
			<td>
				[[ commercial.id_comercial ]]
			</td>
			<td></td>
		</tr>
		<tr>
			<td>
				<md-icon md-font-icon="mdi mdi-pound"></md-icon> ID Externo
			</td>
			<td>
				[[ commercial.id_comercial_externo ]]
			</td>
			<td></td>
		</tr>
		<tr>
			<td>
				<md-icon md-font-icon="mdi mdi-pound"></md-icon> DNI/NIF
			</td>
			<td>
				[[ commercial.nif ]]
			</td>
			<td></td>
		</tr>
		<tr ng-show="commercial.telefono_fijo">
			<td>
				<md-icon md-font-icon="mdi mdi-phone-classic"></md-icon> Teléfono fijo
			</td>
			<td>
				<a ng-href="tel:[[ commercial.telefono_fijo ]]">[[ commercial.telefono_fijo ? commercial.telefono_fijo : 'Sin información' ]]</a>
			</td>
			<td></td>
		</tr>
		<tr ng-show="commercial.telefono_movil">
			<td>
				<md-icon md-font-icon="mdi mdi-phone"></md-icon> Teléfono móvil
			</td>
			<td>
				<a ng-href="tel:[[ commercial.telefono_movil ]]">[[ commercial.telefono_movil ? commercial.telefono_movil : 'Sin información' ]]</a>
			</td>
			<td></td>
		</tr>
		<tr ng-show="commercial.email">
			<td>
				<md-icon md-font-icon="mdi mdi-at"></md-icon> Email
			</td>
			<td>
				<a ng-href="mailto:[[ commercial.email ]]">[[ commercial.email ? commercial.email : 'Sin información' ]]</a>
			</td>
			<td></td>
		</tr>
		<tr>
			<td>
				<md-icon md-font-icon="mdi mdi-map-marker"></md-icon> Dirección
			</td>
			<td>
				[[ commercial.direccion ]]
			</td>
			<td></td>
		</tr>
		<tr>
			<td>
				<md-icon md-font-icon="mdi mdi-map-marker"></md-icon> Población (Cód. Postal)
			</td>
			<td>
				[[ commercial.poblacion ]] <span ng-show="commercial.cod_postal">([[ commercial.cod_postal ]])</span>
			</td>
			<td></td>
		</tr>

		<tr>
			<td>
				<md-icon md-font-icon="mdi mdi-account-multiple"></md-icon> Gerencia
			</td>
			<td>
				[[ commercial.management.gerencia ]]
			</td>
			<td></td>
		</tr>
		<tr>
			<td>
				<md-icon md-font-icon="mdi mdi-account"></md-icon> Gerente
			</td>
			<td>
				<a ui-sref="commercials.show({ id_comercial: commercial.management.manager.id_comercial })">
					[[ commercial.management.manager.nombre_completo ]]
				</a>
			</td>
			<td></td>
		</tr>
		<tr>
			<td>
				<md-icon md-font-icon="mdi mdi-account-badge-horizontal-outline"></md-icon> Campaña(s)
			</td>
			<td>
				<div ng-show="commercial.campaigns.length">
					<span ng-repeat="campaign in commercial.campaigns">
						[[ campaign.nombre ]] ([[ campaign.abreviatura ]])<span ng-hide="$last">, </span>
					</span>
				</div>
				<em ng-hide="commercial.campaigns.length">No tiene</em>
			</td>
			<td>
				<md-icon md-font-icon="mdi mdi-square-edit-outline" ng-click="campaignsSet()" class="margin-left-10 mdc-text-teal clickable"></md-icon>
			</td>
		</tr>
	</tbody>
</table>
