<div layout="row" layout-margin>
    <div layout="column" md-whiteframe="2" flex class="mdc-bg-white">
        <md-toolbar class="md-menu-toolbar">
            <div class="md-toolbar-tools">
                <h2 flex md-truncate>Estructuras</h2>
				@if(auth()->user()->level < 3)
	                <md-button class="md-accent" ng-click="alterStructures(null)" ng-disabled="commercial.id_nivel_funcional!='001' && commercial.id_nivel_funcional!='002'">
	                    <md-icon md-font-icon="mdi mdi-plus"></md-icon> Añadir estructura
	                </md-button>
	            @endif

            </div>
        </md-toolbar>


        <md-content layout="column" layout-padding class="mdc-bg-white">



            <md-content id="commercial-structures" class="mdc-bg-white">
                <table datatable="" dt-options="table.dtOptions" dt-columns="table.dtColumns" dt-instance="table.instance" dt-disable-deep-watchers="true"
                       class=" hover full-width table-clickable"></table>
            </md-content>

            <div layout="row" layout-align="center center" ng-hide="commercial.structures.length">
                <em>No se ha indicado estructura adicional todavía.</em>
            </div>
        </md-content>
    </div>
</div>
