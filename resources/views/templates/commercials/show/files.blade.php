<div layout="row" layout-margin>
	<div layout="column" md-whiteframe="2" flex class="mdc-bg-white">
		<md-toolbar class="md-menu-toolbar">
			<div class="md-toolbar-tools">
				<h2 flex md-truncate>Documentación</h2>

				<div class="md-body-1" ng-show="$mdMedia('gt-sm')">
					<md-input-container>
						<label>Tipo de archivo</label>
						<md-select ng-model="fileType">
							<md-option ng-value="type.id" ng-repeat="type in filesTypes">[[ type.name ]]</md-option>
						</md-select>
					</md-input-container>

					<md-button class="md-accent" ngf-select="upload($file)" ng-disabled="!fileType">
						<md-icon md-font-icon="mdi mdi-upload"></md-icon> Subir archivo
					</md-button>
				</div>
			</div>
		</md-toolbar>

		<md-progress-linear md-mode="determinate" value="[[ fileUpProgress ]]" ng-style="fileUpBarStyle"></md-progress-linear>

		<md-content layout="column" layout-padding class="mdc-bg-white">

			<div layout="row" ng-hide="$mdMedia('gt-sm')">
				<md-input-container class="input-no-errors">
					<label>Tipo de archivo</label>
					<md-select ng-model="fileType">
						<md-option ng-value="type.id" ng-repeat="type in filesTypes">[[ type.name ]]</md-option>
					</md-select>
				</md-input-container>

				<md-button class="md-accent" ngf-select="upload($file)" ng-disabled="!fileType">
					<md-icon md-font-icon="mdi mdi-upload"></md-icon> Subir archivo
				</md-button>
			</div>

			<table datatable="ng" dt-options="{ dom: 't' }" class="full-width" ng-show="commercial.files.length">
				<thead>
				<tr>
					<th>Fecha</th>
					<th>Tipo</th>
					<th>Subido por</th>
					<th>Archivo</th>
					<th></th>
				</tr>
				</thead>
				<tbody>
				<tr ng-repeat="file in commercial.files">
					<td>
						[[ moment(file.created_at).format('DD-MMM-YYYY HH:mm') ]]
					</td>

					<td>
						[[ file.type.name ]]
					</td>

					<td>[[ file.user.name ]]</td>

					<td>
						<a target="_blank" ng-href="/api/v1/candidates/files/[[ file.id ]]">
							<md-icon md-font-icon="mdi mdi-file"></md-icon>
							[[ file.filename ]]
						</a>
					</td>
					<td class="text-right">
						<md-button class="md-icon-button" ng-click="deleteFile(file, $index, $event)">
							<md-icon md-font-icon="mdi mdi-delete"></md-icon>
						</md-button>
					</td>
				</tr>
				</tbody>
			</table>

			<div layout="row" layout-align="center center" ng-hide="commercial.files.length">
				<em>No se ha añadido documentación todavía.</em>
			</div>
		</md-content>
	</div>
</div>
