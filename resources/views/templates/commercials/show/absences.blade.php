<div layout="row" layout-margin>
	<div layout="column" md-whiteframe="2" flex class="mdc-bg-white">
		<md-toolbar class="md-menu-toolbar">
			<div class="md-toolbar-tools">
				<h2 flex md-truncate>Ausencias</h2>
			</div>
		</md-toolbar>

		<md-content layout-padding class="mdc-bg-white">
			<md-list class="padding-0">
				<md-subheader class="md-no-sticky">[[ commercial.absences.length ]] ausencias registradas</md-subheader>
				<md-list-item class="md-2-line" ng-repeat="absence in commercial.absences">
					<md-icon md-font-icon="mdi mdi-calendar-remove md-avatar-icon"></md-icon>

					<div class="md-list-item-text">
						<h3>[[ absence.motive.name ]]</h3>
						<p>[[ moment(absence.date).format('DD-MMMM-YYYY') ]]</p>
					</div>
				</md-list-item>
			</md-list>
		</md-content>
	</div>
</div>
