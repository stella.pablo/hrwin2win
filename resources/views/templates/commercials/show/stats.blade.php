<div layout="column" layout-margin>
	<div layout="column" layout-gt-sm="row" md-whiteframe="2" flex class="mdc-bg-white padding-top-20">
		<md-content class="mdc-bg-white" flex>
			<chart-commercials-contracts-per-day stats="stats"></chart-commercials-contracts-per-day>
		</md-content>

		<md-content class="mdc-bg-white" flex>
			<chart-commercials-contracts-per-month stats="stats"></chart-commercials-contracts-per-month>
		</md-content>
	</div>
</div>
