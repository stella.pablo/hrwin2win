<md-toolbar id="top-toolbar" class="md-menu-toolbar">
	<div class="md-toolbar-tools">
		<md-button class="md-icon-button" ng-click="hiddenFilters = !hiddenFilters" hide-gt-xs>
			<md-icon md-font-icon="mdi mdi-filter"></md-icon>
		</md-button>

		<h1 flex md-truncate>Comerciales</h1>
	{{--	@if(auth()->user()->level < 3)--}}
			<div class="padding-0">
				<md-button class="margin-0 md-primary md-raised" aria-label="" ui-sref="commercials.create">
					<md-icon md-font-icon="mdi mdi-plus"></md-icon>
					<span hide-xs>Nuevo comercial</span>
					<span hide-gt-xs>Nuevo</span>
				</md-button>
			</div>
{{--		@endif--}}
	</div>
</md-toolbar>

<md-content layout="column" layout-margin>
	<div layout="column" layout-gt-sm="row" layout-align="center center" class="padding-15" ng-hide="$mdMedia('xs') && hiddenFilters">
		<div id="filters" layout="row" layout-align="center center" layout-margin layout-wrap>
			<md-input-container class="input-no-errors" flex-xs>
				<label>Fecha desde</label>
				<md-datepicker ng-model="filters.date_from" md-max-date="filters.date_to" ng-change="filter()"></md-datepicker>
			</md-input-container>

			<md-input-container class="input-no-errors" flex-xs>
				<label>Fecha hasta</label>
				<md-datepicker ng-model="filters.date_to" md-min-date="filters.date_from" ng-change="filter()"></md-datepicker>
			</md-input-container>

			@if(auth()->user()->level < 3)
				<md-input-container class="input-no-errors" flex-xs ng-show="user.company.database === 'mexico'">
					<label>Organización</label>
					<md-select ng-model="filters.organization_id" ng-change="filter()">
						<md-option ng-value=""><em>Todas</em></md-option>
						<md-option ng-value="organization.id" ng-repeat="organization in organizations">[[ organization.organizacion ]]</md-option>
					</md-select>
				</md-input-container>

				<md-input-container class="input-no-errors" flex-xs>
					<label>Gerencia</label>
					<md-select ng-model="filters.management_id" ng-change="filter()">
						<md-option ng-value=""><em>Todas</em></md-option>
						<md-option ng-value="management.id_gerencia" ng-repeat="management in managements">[[ management.gerencia ]]</md-option>
					</md-select>
				</md-input-container>
			@endif

			<md-input-container class="input-no-errors" flex-xs ng-show="strategies.length">
				<label>Estrategia</label>
				<md-select ng-model="filters.strategy_id" ng-change="filter()">
					<md-option ng-value=""><em>Todas</em></md-option>
					<md-option ng-value="strategy.id" ng-repeat="strategy in strategies">[[ strategy.id_estrategia ]]</md-option>
				</md-select>
			</md-input-container>

			<md-input-container class="input-no-errors" flex-xs ng-show="filters.management_id">
				<label>Campaña</label>
				<md-select ng-model="filters.campaign_id" ng-change="filter()">
					<md-option ng-value=""><em>Todas</em></md-option>
					<md-option ng-value="campaign.abreviatura" ng-repeat="campaign in campaigns | orderBy:'nombre'">[[ campaign.nombre ]]</md-option>
				</md-select>
			</md-input-container>

			<md-checkbox
				ng-model="filters.with_trashed"
				ng-change="filter()"
				aria-label="Incluir eliminaods"
			>
				Incluir eliminados
			</md-checkbox>
		</div>

		<div layout="row" layout-align="center center">
			<md-button ng-click="clearFilters(true)">
				<md-icon md-font-icon="mdi mdi-filter-remove"></md-icon>
				Limpiar filtros
			</md-button>
		</div>
	</div>

	<div layout="column" md-whiteframe="2">
		<div layout="column" class="mdc-bg-white padding-bottom-15 padding-top-15">
			@include('templates.commercials.index.' . session('database') . '.table')
		</div>
	</div>
</md-content>
