<div layout="column" layout-gt-sm="row" layout-margin class="margin-0">
	<div layout="column" md-whiteframe="2" class="mdc-bg-white" flex>
		<md-toolbar class="md-menu-toolbar">
			<div class="md-toolbar-tools">
				<h2 flex md-truncate>Direcciones</h2>
				<md-button class="md-accent" ng-click="alterAddress(null,null)">
					<md-icon md-font-icon="mdi mdi-plus"></md-icon>
					Añadir direccion
				</md-button>
			</div>
		</md-toolbar>
		<md-content class="mdc-bg-white">
			<table datatable="ng" dt-instance="addressTable.addresses.dtInstanceCallback" dt-options="addressTable.addresses.dtOptions" dt-disable-deep-watchers="true"
			       class="row-border hover full-width small">
				<thead>
				<tr>
					<th>#</th>
					<th>Tipo de direccion</th>
					<th>Descripcion</th>
					<th>Contacto</th>
					<th>Provincia</th>
					<th>Poblacion</th>
					<th>Direccion</th>
					<th>Codigo postal</th>
					<th></th>
					<th></th>
				</tr>
				</thead>
				<tbody>
				<tr ng-repeat="address in management.addresses" class="clickable">
					<td>[[address.id_direccion]]</td>
					<td>[[address.id_tipo_direccion = 0 ? 'Entrega' : address.id_tipo_direccion = 1 ? 'Postal' : address.id_tipo_direccion = 2 ? 'Base' : '']]</td>
					<td>[[address.nombre_direccion]]</td>
					<td>[[address.contacto_direccion]]</td>
					<td>[[address.province.provincia]]</td>
					<td>[[address.poblacion]]</td>
					<td>[[address.direccion]]</td>
					<td>[[address.codigo_postal]]</td>
					<td><a style="color:lightblue" ng-click="alterAddress(address.id_direccion,address.id_gerencia, $index)"> Editar </a></td>
					<td><a style="color:lightcoral" ng-click="removeAddress(address.id_direccion,address.id_gerencia, $index)"> Eliminar </a></td>
				</tr>
				</tbody>
			</table>
		</md-content>

	</div>
</div>
