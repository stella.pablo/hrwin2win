<div layout="row" flex ng-show="$mdMedia('gt-sm')">
	<div flex>
		<table class="full-width table-first-nowrap small">
			<tbody>
			<tr ng-show="management.fecha_baja">
				<td>
					<md-icon md-font-icon="mdi mdi-calendar-minus"></md-icon>
					Fecha de baja
				</td>
				<td>
					<span>[[ moment(management.fecha_baja).format('DD-MMM-YYYY') ]]</span>
				</td>
				<td></td>
			</tr>
			<tr>
				<td>
					<md-icon md-font-icon="mdi mdi-pound"></md-icon>
					ID
				</td>
				<td>
					[[ management.id_gerencia ]]
				</td>
				<td></td>
			</tr>
			<tr>
				<td>
					<md-icon md-font-icon="mdi mdi-account"></md-icon>
					Gerente
				</td>
				<td>
					<a ui-sref="commercials.show({ id_comercial: management.manager.id_comercial })">
						[[ management.manager.nombre_completo ]]
					</a>
				</td>
				<td></td>
			</tr>
			<tr>
				<td>
					<md-icon md-font-icon="mdi mdi-account-badge-horizontal-outline"></md-icon>
					Campaña(s)
				</td>
				<td>
					<div ng-show="management.campaigns.length">
					<span ng-repeat="campaign in management.campaigns">
						[[ campaign.nombre ]] ([[ campaign.abreviatura ]])<span ng-hide="$last">, </span>
					</span>
					</div>
					<em ng-hide="management.campaigns.length">No tiene</em>
				</td>
				<td></td>
			</tr>
			<tr ng-show="management.strategies">
				<td>
					<md-icon md-font-icon="mdi mdi-account-network-outline"></md-icon>
					Estrategia(s)
				</td>
				<td>
					<span ng-show="management.strategies.length">
						<span ng-repeat="strategy in management.strategies">
							[[ strategy.id_estrategia ]]<span ng-if="!$last">, </span>
						</span>
					</span>

					<em ng-hide="management.strategies.length">No tiene</em>
				</td>
				<td>
					@if(auth()->user()->level < 3)
						<md-icon md-font-icon="mdi mdi-square-edit-outline" ng-click="strategiesSet($event)" class="margin-left-10 mdc-text-teal clickable"></md-icon>
					@endif
				</td>
			</tr>
			<tr ng-show="management.organization">
				<td>
					<md-icon md-font-icon="mdi mdi-account-network"></md-icon>
					Organización
				</td>
				<td>[[ management.organization.organizacion ]]</td>
				<td></td>
			</tr>
			</tbody>
		</table>
	</div>

	<div flex>
		<table class="full-width table-first-nowrap small">
			<tbody>
			<tr>
				<td>
					<md-icon md-font-icon="mdi mdi-phone"></md-icon>
					Teléfono
				</td>
				<td>[[ management.internal.phone ]]</td>
			</tr>
			<tr>
				<td>
					<md-icon md-font-icon="mdi mdi-timer"></md-icon>
					Horario Laboral
				</td>
				<td>[[ management.internal.workhours ]]</td>
			</tr>
			<tr>
				<td>
					<md-icon md-font-icon="mdi mdi-map-marker"></md-icon>
					Dirección
				</td>
				<td>
					[[ management.internal.address ]] <a ng-href="[[ management.internal.address_link ]]" target="_blank" ng-show="management.internal.address_link">(Google Maps)</a>
				</td>
			</tr>
			<tr>
				<td>
					<md-icon md-font-icon="mdi mdi-web"></md-icon>
					Página web
				</td>
				<td>[[ management.internal.url ]]</td>
			</tr>
			</tbody>
		</table>
	</div>
</div>

<div layout="column" ng-hide="$mdMedia('gt-sm')">
	<table class="full-width table-first-nowrap small">
		<tbody>
		<tr ng-show="management.fecha_baja">
			<td>
				<md-icon md-font-icon="mdi mdi-calendar-minus"></md-icon>
				Fecha de baja
			</td>
			<td>
				<span>[[ moment(management.fecha_baja).format('DD-MMM-YYYY') ]]</span>
			</td>
			<td></td>
		</tr>
		<tr>
			<td>
				<md-icon md-font-icon="mdi mdi-pound"></md-icon>
				ID
			</td>
			<td>
				[[ management.id_gerencia ]]
			</td>
			<td></td>
		</tr>
		<tr>
			<td>
				<md-icon md-font-icon="mdi mdi-account"></md-icon>
				Gerente
			</td>
			<td>
				<a ui-sref="commercials.show({ id_comercial: management.manager.id_comercial })">
					[[ management.manager.nombre_completo ]]
				</a>
			</td>
			<td></td>
		</tr>
		<tr>
			<td>
				<md-icon md-font-icon="mdi mdi-account-badge-horizontal-outline"></md-icon>
				Campaña(s)
			</td>
			<td>
				<div ng-show="management.campaigns.length">
					<span ng-repeat="campaign in management.campaigns">
						[[ campaign.nombre ]] ([[ campaign.abreviatura ]])<span ng-hide="$last">, </span>
					</span>
				</div>
				<em ng-hide="management.campaigns.length">No tiene</em>
			</td>
			<td></td>
		</tr>
		<tr ng-show="management.strategies">
			<td>
				<md-icon md-font-icon="mdi mdi-account-network-outline"></md-icon>
				Estrategia(s)
			</td>
			<td>
				<span ng-show="management.strategies.length">
					<span ng-repeat="strategy in management.strategies">
						[[ strategy.id_estrategia ]]<span ng-if="!$last">, </span>
					</span>
				</span>

				<em ng-hide="management.strategies.length">No tiene</em>
			</td>
			<td>
				<md-icon md-font-icon="mdi mdi-square-edit-outline" ng-click="strategiesSet($event)" class="margin-left-10 mdc-text-teal clickable"></md-icon>
			</td>
		</tr>
		<tr ng-show="management.organization">
			<td>
				<md-icon md-font-icon="mdi mdi-account-network"></md-icon>
				Organización
			</td>
			<td>[[ management.organization.organizacion ]]</td>
			<td></td>
		</tr>
		<tr>
			<td>
				<md-icon md-font-icon="mdi mdi-phone"></md-icon>
				Teléfono
			</td>
			<td>[[ management.internal.phone ]]</td>
		</tr>
		<tr>
			<td>
				<md-icon md-font-icon="mdi mdi-timer"></md-icon>
				Horario Laboral
			</td>
			<td>[[ management.internal.workhours ]]</td>
		</tr>
		<tr>
			<td>
				<md-icon md-font-icon="mdi mdi-map-marker"></md-icon>
				Dirección
			</td>
			<td>
				[[ management.internal.address ]] <a ng-href="[[ management.internal.address_link ]]" target="_blank" ng-show="management.internal.address_link">(Google Maps)</a>
			</td>
		</tr>
		<tr>
			<td>
				<md-icon md-font-icon="mdi mdi-web"></md-icon>
				Página web
			</td>
			<td>[[ management.internal.url ]]</td>
		</tr>
		</tbody>
	</table>
</div>
