<div ng-controller="productivityCtrl">
	<div layout="column" layout-align="center center" class="padding-15">
		<div id="filters" layout="row" layout-align="center center" layout-margin layout-wrap>
			<md-input-container class="input-no-errors">
				<label>Fecha desde</label>
				<md-datepicker ng-model="filters.date_from" md-max-date="filters.date_to"></md-datepicker>
			</md-input-container>

			<md-input-container class="input-no-errors">
				<label>Fecha hasta</label>
				<md-datepicker ng-model="filters.date_to" md-min-date="filters.date_from"></md-datepicker>
			</md-input-container>

			<md-input-container class="input-no-errors">
				<label>Campaña</label>
				<md-select ng-model="filters.campaign_id">
					<md-option ng-value=""><em>Todas</em></md-option>
					<md-option ng-value="campaign.abreviatura" ng-repeat="campaign in campaigns | orderBy:'nombre'">[[ campaign.nombre ]]</md-option>
				</md-select>
			</md-input-container>

			<div>
				<md-button ng-click="clearFilters()">
					<md-icon md-font-icon="mdi mdi-filter-remove"></md-icon>
					Limpiar filtros
				</md-button>
			</div>
		</div>
	</div>

	<md-content layout="column" layout-padding class="mdc-bg-white" md-whiteframe="2">
		@include('templates.productivity.index._commercials')
	</md-content>
</div>

<div layout="column" layout-margin class="margin-0">
	<div layout="column" md-whiteframe="2" class="mdc-bg-white">
		<div layout="column" layout-gt-sm="row" layout-align="space-between center" layout-padding>
			<h2>Acumulación este mes</h2>

			<div>
				<small class="mdc-bg-light-green-50 padding-5" style="border-radius: 5px;">
					OK: [[ statsThisMonthBadges.ok ]]
				</small>

				<small class="mdc-bg-amber-50 padding-5 margin-left-5" style="border-radius: 5px;">
					P: [[ statsThisMonthBadges.p ]]
				</small>

				<small class="mdc-bg-red-50 padding-5 margin-left-5" style="border-radius: 5px;">
					KO: [[ statsThisMonthBadges.ko ]]
				</small>
			</div>
		</div>

		<chart-commercials-contracts-per-day stats="statsThisMonth"></chart-commercials-contracts-per-day>
	</div>

	<div layout="column" md-whiteframe="2" class="mdc-bg-white">
		<div layout="column" layout-padding>
			<h2>Acumulación anual</h2>
		</div>

		<div layout="column" layout-gt-sm="row">
			<div flex>
				<chart-commercials-productivity stats="statsProd"></chart-commercials-productivity>
			</div>

			<div flex>
				<chart-commercials-contracts stats="statsProd"></chart-commercials-contracts>
			</div>
		</div>
	</div>
</div>

<chart-candidates-stats management-id="management.internal.id"></chart-candidates-stats>

<div layout="column" layout-gt-sm="row" layout-margin class="margin-0">
	<div layout="column" md-whiteframe="2" class="mdc-bg-white" flex>
		<md-toolbar class="md-menu-toolbar">
			<div class="md-toolbar-tools">
				<h2 flex md-truncate>Últimos candidatos</h2>

				<div>
					<md-button class="md-icon-button margin-0" aria-label="Add new candidate" ui-sref="candidates.create">
						<md-icon md-font-icon="mdi mdi-plus"></md-icon>
					</md-button>

					<md-button class="md-icon-button margin-0" ui-sref="candidates">
						<md-icon md-font-icon="mdi mdi-arrow-expand"></md-icon>
					</md-button>
				</div>
			</div>
		</md-toolbar>

		<md-content class="mdc-bg-white">
			<table datatable="ng" dt-instance="table.candidates.dtInstanceCallback" dt-options="table.candidates.dtOptions" dt-disable-deep-watchers="false" class="row-border hover full-width small">
				<thead>
				<tr>
					<th>Nombre</th>
					<th>F. Petición</th>
					<th>F. Inserción</th>
				</tr>
				</thead>
				<tbody>
				<tr ng-repeat="candidate in candidates" class="clickable" ui-sref="candidates.show({ id: candidate.id })">
					<td>[[ candidate.fullname ]]</td>
					<td>[[ candidate.petitioned_at ]]</td>
					<td>[[ candidate.created_at ]]</td>
				</tr>
				</tbody>
			</table>
		</md-content>
	</div>

	<div layout="column" md-whiteframe="2" class="mdc-bg-white" flex>
		<md-toolbar class="md-menu-toolbar">
			<div class="md-toolbar-tools">
				<h2 flex md-truncate>Últimos comerciales</h2>

				<md-button class="md-icon-button margin-0" ui-sref="commercials">
					<md-icon md-font-icon="mdi mdi-arrow-expand"></md-icon>
				</md-button>
			</div>
		</md-toolbar>

		<md-content class="mdc-bg-white">
			<table datatable="ng" dt-instance="table.commercials.dtInstanceCallback" dt-options="table.commercials.dtOptions" dt-disable-deep-watchers="true" class="row-border hover full-width small">
				<thead>
				<tr>
					<th>ID Ext</th>
					<th>Nombre</th>
					<th>Gerencia</th>
					<th>Fecha Alta</th>
				</tr>
				</thead>
				<tbody>
				<tr ng-repeat="commercial in commercials" ui-sref="commercials.show({ id_comercial: commercial.id_comercial })" class="clickable">
					<td>[[ commercial.id_comercial_externo ]]</td>
					<td>[[ commercial.nombre_completo ]]</td>
					<td>[[ commercial.management.gerencia ]]</td>
					<td>[[ commercial.fecha_alta ]]</td>
				</tr>
				</tbody>
			</table>
		</md-content>
	</div>
</div>
<div layout="column" layout-gt-sm="row" layout-margin class="margin-0">
	<div layout="column" md-whiteframe="2" class="mdc-bg-white" flex>
		<md-toolbar class="md-menu-toolbar">
			<div class="md-toolbar-tools">
				<h2 flex md-truncate>Últimas liquidaciones</h2>


			</div>
		</md-toolbar>


		<md-content class="mdc-bg-white">
			<table datatable="ng" dt-instance="table.settlements.dtInstanceCallback" dt-options="table.settlements.dtOptions" dt-disable-deep-watchers="true"
			       class="row-border hover full-width small">
				<thead>
				<th>#</th>
				<th>Observaciones</th>
				<th>Gerencia</th>
				<th>Comercial</th>
				<th>Tipo de contratación</th>
				<th>Nivel comercial</th>
				<th>Fecha desde</th>
				<th>Fecha hasta</th>
				<th>Fecha Liquidacion</th>

				<th></th>
				<th></th>

				</tr>
				</thead>
				<tbody>
				<tr ng-repeat="settlement in settlements" class="clickable">
					<td>[[settlement.id]]</td>
					<td>[[settlement.observaciones]]</td>
					<td>[[settlement.management ? settlement.management.id_gerencia : 'No indicada']]</td>
					<td>[[settlement.commercial ? settlement.commercial.id_comercial : 'No indicado']]</td>
					<td>[[settlement.contract ? settlement.contractType.descripcion: 'No indicado']]</td>
					<td>[[settlement.id_estructura_comercial? settlement.id_estructura_comercial : 'No indicada']]</td>
					<td>[[ moment(settlement.fecha_desde).format('DD-MM-YYYY') ]]</td>
					<td>[[ moment(settlement.fecha_hasta).format('DD-MM-YYYY') ]]</td>
					<td>[[ moment(settlement.fecha_liquidacion).format('DD-MM-YYYY') ]]</td>


					<td><a style="color:lightblue" ng-click="showSettlement(settlement.id)"> Ver </a></td>
					<td><md-button class="md-accent" ng-click="managementPdf(settlement.id)"><md-icon md-font-icon="mdi mdi-file-pdf mdi-50px" ></md-icon>

						</md-button></td>

				</tr>
				</tbody>
			</table>
		</md-content>

	</div>
</div>
