<md-toolbar id="top-toolbar" class="md-menu-toolbar">
	<div class="md-toolbar-tools">
		<h1>[[ management.id_gerencia ? 'Editar' : 'Crear' ]] gerencia</h1>
	</div>
</md-toolbar>

<md-content layout="column" layout-margin>
	<div layout="column" md-whiteframe="2" flex class="mdc-bg-white">
		<md-content layout-padding class="mdc-bg-white">
			@include('templates.managements.create.' . session('database') . '.form')
		</md-content>
	</div>
</md-content>
