<table
	datatable="ng"
	dt-instance="table.dtInstanceCallback"
	dt-options="table.dtOptions"
	dt-disable-deep-watchers="true"
	class="row-border hover full-width small table-with-avatars"
>
	<thead>
	<tr>
		<th>Gerencia</th>
		<th>Gerente</th>
		<th class="text-right">Com. Activos</th>
	</tr>
	</thead>
	<tbody>
	<tr ng-repeat="management in managements" ui-sref="managements.show({ id_gerencia: management.id_gerencia })" class="clickable">
		<td ng-class="{'mdc-text-red-300': management.fecha_baja}">[[ management.gerencia ]]</td>
		<td ng-class="{'mdc-text-red-300': management.manager.id_estado}">[[ management.manager.nombre_completo ]]</td>
		<td class="text-right">[[ management.commercials_count ]]</td>
	</tr>
	</tbody>
</table>
