<form name="managementForm">
	<div layout="column" class="margin-10">
		<div layout="column">
			<h3>Datos de la gerencia</h3>

			<div layout="column" layout-margin>
				<md-input-container class="input-no-errors" flex>
					<md-icon md-font-icon="mdi mdi-account-multiple"></md-icon>
					<label>Nombre</label>
					<input ng-model="management.gerencia" required>
				</md-input-container>

				<md-input-container flex>
					<md-icon md-font-icon="mdi mdi-access-point-network"></md-icon>
					<label>Gerente</label>
					<md-select ng-model="management.id_comercial_liquidacion" required>
						<md-option ng-value="commercial.id_comercial" ng-repeat="commercial in commercials">
							[[ commercial.nombre_completo ]]
							<span ng-show="commercial.id_comercial_externo">([[ commercial.id_comercial_externo ]])</span>
						</md-option>
					</md-select>
				</md-input-container>

				<md-input-container flex>
					<md-icon md-font-icon="mdi mdi-account-network"></md-icon>
					<label>Organización</label>
					<md-select ng-model="management.id_organizacion" required>
						<md-option ng-value="organization.id" ng-repeat="organization in organizations">[[ organization.organizacion ]]</md-option>
					</md-select>
				</md-input-container>
			</div>
		</div>

		<div layout="column">
			<h3>Datos de la oficina</h3>

			<div layout="column" layout-margin>
				<md-input-container class="input-no-errors" flex>
					<input type="text" ng-model="management.internal.phone" placeholder="Teléfono" aria-label="input">
				</md-input-container>

				<md-input-container class="input-no-errors" flex>
					<input type="text" ng-model="management.internal.workhours" placeholder="Horario Laboral" aria-label="input">
				</md-input-container>
			</div>

			<div layout="column" layout-margin>
				<md-input-container class="input-no-errors" flex>
					<input type="text" ng-model="management.internal.address" placeholder="Dirección" aria-label="input">
				</md-input-container>

				<md-input-container class="input-no-errors" flex>
					<input type="text" ng-model="management.internal.address_link" placeholder="Dirección (URL Google Maps)" aria-label="input">
				</md-input-container>

				<md-input-container class="input-no-errors" flex>
					<input type="text" ng-model="management.internal.url" placeholder="Página web (URL)" aria-label="input">
				</md-input-container>
				<md-input-container flex>
					<label>Almacén asociado</label>
					<md-select ng-model="management.id_almacen">
						<md-option ng-value="storage.id_almacen" ng-repeat="storage in storages">
							[[ storage.descripcion ]]
						</md-option>
					</md-select>
				</md-input-container>


			</div>
		</div>


		<div layout="column">
			<h3>Datos de facturacion</h3>

			<div layout="column" layout-margin>
				<md-input-container flex>
					<md-icon md-font-icon="mdi mdi-access-point-network"></md-icon>
					<label>Tarifa principal</label>
					<md-select ng-model="management.id_tarifa" >
						<md-option ng-value="rate.id_tarifa" ng-repeat="rate in rates">
							[[ rate.descripcion ]]
						</md-option>
					</md-select>
				</md-input-container>
				<md-input-container flex>
					<md-icon md-font-icon="mdi mdi-access-point-network"></md-icon>
					<label>Forma de pago</label>
					<md-select ng-model="management.id_forma_pago" >
						<md-option ng-value="paymentMethod.id_forma_pago" ng-repeat="paymentMethod in paymentMethods">
							[[ paymentMethod.forma_pago ]]
						</md-option>
					</md-select>
				</md-input-container>

				<md-input-container flex>
				<md-icon md-font-icon="mdi mdi-access-point-network"></md-icon>
					<label>Tipo de iva</label>
					<md-select ng-model="management.id_tipo_iva" >
						<md-option ng-value="'E'">Exento</md-option>
						<md-option ng-value="'N'">Normal</md-option>
					</md-select>
				</md-input-container>
				<md-input-container flex ng-show="management.id_tipo_iva == 'N'">
					<md-icon md-font-icon="mdi mdi-access-point-network"></md-icon>
					<label>Iva</label>
					<md-select ng-model="management.id_iva" >
						<md-option ng-value="iva.id_iva" ng-repeat="iva in ivas">
							[[ iva.descripcion  ]]  ([[iva.iva]] %)
						</md-option>
					</md-select>
				</md-input-container>

				<md-input-container flex>
					<md-icon md-font-icon="mdi mdi-access-point-network"></md-icon>
					<label>Itau</label>
					<md-select ng-model="management.id_itau" >
						<md-option ng-value="itau.id_itau" ng-repeat="itau in itaus">
							[[ itau.descripcion ]] ([[ itau.itau ]] %)
						</md-option>
					</md-select>
				</md-input-container>
			</div>


		</div>

		<div layout="column">
			<h3>Productividad</h3>

			<div layout="column" layout-margin>
				<md-input-container class="input-no-errors" flex>
					<input type="number" min="1" max="100" ng-model="management.internal.monthly_objective" placeholder="Objetivo mensual" aria-label="input" required>
				</md-input-container>
			</div>
		</div>

		<div layout="column">
			<h3>Acceso sistema</h3>

			<div layout="column" layout-margin>
				<md-checkbox
					ng-model="management.create_access_hr"
					class="md-align-top-left"
					ng-disabled="!management.manager.email || !management.manager.id_comercial_externo"
				>
					Crear o re-crear acceso para el gerente a la plataforma: Recursos Humanos<br>
					<small class="mdc-text-grey">Es obligatorio que el gerente tenga Email e ID Ext. establecido en su perfil</small>
				</md-checkbox>
			</div>
		</div>

		<div layout="row" layout-align="space-between center" class="margin-top-40">
			<md-button ng-click="cancel()">
				<md-icon md-font-icon="mdi mdi-cancel"></md-icon>
				Cancelar
			</md-button>

			<md-button ng-click="submit($event)" class="md-primary md-raised" ng-disabled="managementForm.$invalid || managementForm.$pristine">
				<md-icon md-font-icon="mdi mdi-content-save"></md-icon>
				Guardar
			</md-button>
		</div>
	</div>
</form>
