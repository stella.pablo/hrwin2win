<md-content>
	<div layout="column" layout-margin>
		<div layout="column" md-whiteframe="2" flex class="mdc-bg-white">
			<div layout="column" layout-gt-sm="row" class="management-show">
				<div
					class="avatar"
					ng-mouseenter="showAvatarOptions = true"
					ng-mouseleave="showAvatarOptions = false"
					ng-class="{'full-width': $mdMedia('xs') || $mdMedia('sm')}"
				>
				</div>

				<div layout="column" layout-margin flex class="content">
					<div
						layout="column"
						layout-gt-sm="row"
						layout-align="center center"
						layout-align-gt-sm="space-between start"
					>
						<div layout="column">
							<h2 class="margin-0" style="font-size: 21px;">[[ management.gerencia ]]</h2>

							<div style="font-size: 12px;" ng-class="{'mdc-text-green': !management.fecha_baja, 'mdc-text-red': management.fecha_baja}">
								<span ng-show="management.fecha_baja">
									Estado: <strong>de Baja</strong>
									<span ng-show="management.fecha_baja">(desde: [[ moment(management.fecha_baja).format('DD-MMM-YYYY') ]])</span>
								</span>
							</div>
						</div>

						<div class="padding-0" layout="column" layout-gt-sm="row" layout-align="center center">
							<md-button ui-sref="candidates.show({ id: candidate.id })" ng-show="candidate.id" class="margin-top-0">
								<md-icon md-font-icon="mdi mdi-account-convert" style="color: inherit;"></md-icon>
								Ficha de Candidato
							</md-button>

							<md-button ui-sref="managements.show.edit({ id: management.id_comercial })" class="margin-top-0">
								<md-icon md-font-icon="mdi mdi-pencil"></md-icon>
								Editar
							</md-button>

							<md-button ng-click="enableManagement()" class="margin-top-0 md-primary" ng-show="management.fecha_baja">
								<md-icon md-font-icon="mdi mdi-account-check"></md-icon>
								Habilitar
							</md-button>

							<md-button
								ng-click="disableManagement()"
								ng-hide="management.fecha_baja"
								class="margin-top-0 md-warn"
							>
								<md-icon md-font-icon="mdi mdi-account-off"></md-icon>
								Deshabilitar
							</md-button>
						</div>
					</div>

					@include('templates.managements.show.info-basic')
				</div>
			</div>

			<div layout="row" layout-align="space-between center" class="mdc-bg-grey-50 padding-top-5 padding-bottom-5 padding-left-10 padding-right-10">
				<small>Objetivo este mes: [[ managementStats.contratos_adiadehoy_porcentaje ]]%</small>
				<small>OK: [[ managementStats.numero_contratos_ok ]]</small>
				<small>Pend: [[ managementStats.numero_contratos_p ]]</small>
				<small>KO: [[ managementStats.numero_contratos_ko ]]</small>
				<small>Total: [[ managementStats.numero_contratos_total ]]</small>
				<small>
					Previsión: [[ managementStats.contratos_prevision ]]

					<md-icon
						md-font-icon="mdi mdi-arrow-bottom-right"
					    class="mdc-text-red-300"
						ng-show="managementStats.contratos_prevision_porcentaje < 75"
					></md-icon>
					<md-icon
						md-font-icon="mdi mdi-minus"
					    class="mdc-text-orange-300"
						ng-show="managementStats.contratos_prevision_porcentaje >= 75 && managementStats.contratos_prevision_porcentaje <95"
					></md-icon>
					<md-icon
						md-font-icon="mdi mdi-arrow-top-right"
					    class="mdc-text-green-300"
						ng-show="managementStats.contratos_prevision_porcentaje >= 95"
					></md-icon>
				</small>
			</div>
		</div>

		@include('templates.managements.show.stats')
		@include('templates.managements.show.addresses')
	</div>
</md-content>
