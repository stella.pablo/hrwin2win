<md-toolbar id="top-toolbar" class="md-menu-toolbar">
	<div class="md-toolbar-tools">
		<h2>Gerencias</h2>

		<span flex></span>

		@if(auth()->user()->level <= 2)
			<div class="padding-0">
				<md-button class="margin-0 md-primary md-raised" aria-label="" ui-sref="managements.create">
					<md-icon md-font-icon="mdi mdi-plus"></md-icon>
					<span hide-xs>Nueva gerencia</span>
					<span hide-gt-xs>Nueva</span>
				</md-button>
			</div>
		@endif
	</div>
</md-toolbar>

<md-content layout="column" layout-margin>
	<div layout="column" layout-gt-sm="row" layout-align="center center" class="padding-15" ng-show="user.company.database === 'mexico'">
		<div id="filters" layout="column" layout-gt-sm="row" layout-align="center center" layout-margin layout-wrap>
			@if(auth()->user()->level < 3)
				<md-input-container class="input-no-errors">
					<label>Organización</label>
					<md-select ng-model="filters.organization_id">
						<md-option ng-value=""><em>Todas</em></md-option>
						<md-option ng-value="organization.id" ng-repeat="organization in organizations">[[ organization.organizacion ]]</md-option>
					</md-select>
				</md-input-container>
			@endif

			<md-input-container class="input-no-errors" ng-show="strategies.length">
				<label>Estrategia</label>
				<md-select ng-model="filters.strategy_id">
					<md-option ng-value=""><em>Todas</em></md-option>
					<md-option ng-value="strategy.id" ng-repeat="strategy in strategies">[[ strategy.id_estrategia ]]</md-option>
				</md-select>
			</md-input-container>

			<md-input-container class="input-no-errors" ng-show="filters.management_id">
				<label>Campaña</label>
				<md-select ng-model="filters.campaign_id">
					<md-option ng-value=""><em>Todas</em></md-option>
					<md-option ng-value="campaign.abreviatura" ng-repeat="campaign in campaigns | orderBy:'nombre'">[[ campaign.nombre ]]</md-option>
				</md-select>
			</md-input-container>
		</div>

		<div layout="row" layout-align="center center">
			<md-button ng-click="clearFilters(true)">
				<md-icon md-font-icon="mdi mdi-filter-remove"></md-icon>
				Limpiar filtros
			</md-button>
		</div>
	</div>

	<div layout="column" md-whiteframe="2">
		<div layout="column" class="mdc-bg-white padding-bottom-15 padding-top-15">
			@include('templates.managements.index.' . session('database') . '.table')
		</div>
	</div>
</md-content>
