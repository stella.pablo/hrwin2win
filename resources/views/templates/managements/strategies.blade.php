<md-dialog flex>
	<md-toolbar class="md-menu-toolbar">
		<div class="md-toolbar-tools">
			<h2>Gestión de estrategias</h2>

			<span flex></span>

			<md-button class="md-icon-button" ng-click="close()">
				<md-icon md-font-icon="mdi mdi-close" aria-label="Close dialog"></md-icon>
			</md-button>
		</div>
	</md-toolbar>

	<md-dialog-content>
		<div class="md-dialog-content">
			<form name="strategiesForm" layout="row" layout-align="space-between center">
				<div layout="row">
					<md-input-container class="input-no-errors" flex>
						<label>ID</label>
						<input ng-model="strategy.id_estrategia" required>
					</md-input-container>

					<md-input-container class="input-no-errors" flex>
						<label>Nombre</label>
						<input ng-model="strategy.estrategia" required>
					</md-input-container>
				</div>

				<div layout="row">
					<md-button class="md-primary" ng-click="submit()" ng-disabled="strategiesForm.$invalid || strategiesForm.$pristine">
						<md-icon md-font-icon="mdi mdi-content-save"></md-icon>
						Guardar
					</md-button>

					<md-button ng-click="clearForm()" ng-disabled="strategiesForm.$pristine">
						<md-icon md-font-icon="mdi mdi-cancel"></md-icon>
						Cancelar
					</md-button>
				</div>
			</form>

			<md-list>
				<md-subheader class="md-no-sticky">[[ management.strategies.length ]] estrategias</md-subheader>
				<md-list-item class="md-2-line" ng-repeat="strategy in management.strategies" ng-click="setForm(strategy)">
					<md-icon class="md-avatar-icon" md-font-icon="mdi mdi-account-multiple-outline"></md-icon>
					<div class="md-list-item-text">
						<h3>([[ strategy.id_estrategia ]]) [[ strategy.estrategia ]]</h3>
						<p>Creada: [[ strategy.created_at ]]</p>
					</div>

					<md-button class="md-secondary md-icon-button" ng-click="delete($index, strategy)">
						<md-icon md-font-icon="mdi mdi-delete"></md-icon>
					</md-button>
				</md-list-item>
			</md-list>
		</div>
	</md-dialog-content>
</md-dialog>
