<md-toolbar id="top-toolbar" class="md-menu-toolbar">
	<div class="md-toolbar-tools">
		<h1>Ausencias</h1>
	</div>
</md-toolbar>

<md-content layout="column" layout-margin>
	<div layout="column" md-whiteframe="2">
		<md-content>
			<md-tabs md-dynamic-height md-stretch-tabs="always" md-selected="selectedView">
				<md-tab label="Añadir">
					<md-content class="mdc-bg-white">
						@include('templates.commercials-absences.index.create')
					</md-content>
				</md-tab>
				<md-tab label="Listado">
					<md-content class="mdc-bg-white" ng-controller="commercialsAbsencesListCtrl">
						@include('templates.commercials-absences.index.list')
					</md-content>
				</md-tab>
			</md-tabs>
		</md-content>
	</div>
</md-content>
