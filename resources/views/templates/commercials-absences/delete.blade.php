<md-dialog>
	<md-toolbar class="md-menu-toolbar">
		<div class="md-toolbar-tools">
			<h2>Eliminar ausencia</h2>

			<span flex></span>

			<div class="padding-0">
				<md-button class="md-icon-button margin-0" ng-click="cancel()">
					<md-icon md-font-icon="mdi mdi-close" aria-label="Close dialog"></md-icon>
				</md-button>
			</div>
		</div>
	</md-toolbar>

	<md-dialog-content>
		<div class="md-dialog-content">
			<p>¿Marcar a <strong>[[ commercial.nombre_comercial + ' ' + commercial.apellido_comercial ]]</strong> como presente el día <strong>[[ moment(date).format('DD-MMM-YYYY') ]]</strong>?</p>
		</div>
	</md-dialog-content>

	<md-dialog-actions layout="row" layout-align="space-between center">
		<md-button ng-click="cancel()">
			<md-icon md-font-icon="mdi mdi-close"></md-icon>
			Cancelar
		</md-button>

		<md-button class="md-accent md-raised" ng-click="submit()">
			<md-icon md-font-icon="mdi mdi-content-save"></md-icon>
			Guardar
		</md-button>
	</md-dialog-actions>
</md-dialog>
