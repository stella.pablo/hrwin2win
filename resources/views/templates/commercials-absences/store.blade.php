<md-dialog flex>
	<md-toolbar class="md-menu-toolbar">
		<div class="md-toolbar-tools">
			<h2>Añadir ausencia</h2>

			<span flex></span>

			<div class="padding-0">
				<md-button class="md-icon-button margin-0" ng-click="cancel()">
					<md-icon md-font-icon="mdi mdi-close" aria-label="Close dialog"></md-icon>
				</md-button>
			</div>
		</div>
	</md-toolbar>

	<md-dialog-content>
		<div class="md-dialog-content">
			<p>Marcar a <strong>[[ commercial.nombre_comercial + ' ' + commercial.apellido_comercial ]]</strong> como ausente el día <strong>[[ moment(date).format('DD-MMM-YYYY') ]]</strong> con el siguiente motivo:</p>

			<div class="margin-top-10">
				<md-input-container class="input-no-errors md-block">
					<label>Motivo</label>
					<md-select ng-model="absence.motive_id">
						<md-option ng-value="motive.id" ng-repeat="motive in motives">[[ motive.name ]]</md-option>
					</md-select>
				</md-input-container>
			</div>

			<div layout="row" layout-align="center center" class="margin-top-10">
				<div flex>
					<md-input-container class="input-no-errors md-block">
						<label>Tipo de archivo</label>
						<md-select ng-model="fileType">
							<md-option ng-value="type.id" ng-repeat="type in filesTypes">[[ type.name ]]</md-option>
						</md-select>
					</md-input-container>
				</div>

				<div>
					<md-button class="md-accent" ngf-select ng-model="file" ng-disabled="!fileType">
						<md-icon md-font-icon="mdi mdi-upload"></md-icon> Seleccionar archivo
					</md-button>
				</div>
			</div>
		</div>
	</md-dialog-content>

	<md-dialog-actions layout="row" layout-align="space-between center">
		<md-button ng-click="cancel()">
			<md-icon md-font-icon="mdi mdi-close"></md-icon>
			Cancelar
		</md-button>

		<md-button class="md-accent md-raised" ng-click="submit()" ng-disabled="!absence.motive_id || disableSave">
			<md-icon md-font-icon="mdi mdi-content-save"></md-icon>
			Guardar
		</md-button>
	</md-dialog-actions>
</md-dialog>
