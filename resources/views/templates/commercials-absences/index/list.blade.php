<div layout="row" layout-align="center center" layout-padding class="mdc-bg-grey-100">
	<md-input-container class="input-no-errors">
		<label>Elegir mes</label>
		<md-datepicker id="month-select" ng-model="filters.month" md-mode="month" ng-change="setDatepickerText()"></md-datepicker>
	</md-input-container>

	<div>
		<md-button class="md-accent" ng-click="filter()">
			<md-icon md-font-icon="mdi mdi-filter"></md-icon>
			Filtrar
		</md-button>
	</div>
</div>

<div layout="column" layout-margin>
	<table datatable="ng" dt-instance="table.dtInstanceCallback" dt-options="table.dtOptions" dt-disable-deep-watchers="false" class="row-border hover full-width clickable" ng-show="commercialsAbsences.length">
		<thead>
			<tr>
				<th>Fecha</th>
				<th>Comercial</th>
				<th>Gerencia</th>
				<th>Gerente</th>
				<th>Motivo</th>
			</tr>
		</thead>
		<tbody>
			<tr ng-repeat="absence in commercialsAbsences" ui-sref="commercials.show({ id_comercial: absence.commercial_id })">
				<td>[[ moment(absence.date).format('DD-MMMM-YYYY') ]]</td>
				<td>[[ absence.commercial.nombre_completo ]]</td>
				<td>[[ absence.commercial.management.gerencia ]]</td>
				<td>[[ absence.commercial.management.manager.nombre_completo ]]</td>
				<td>[[ absence.motive.name ]]</td>
			</tr>
		</tbody>
	</table>

	<div class="margin-10 text-center" ng-hide="commercialsAbsences.length">
		<em>No hay ausencias este mes.</em>
	</div>
</div>
