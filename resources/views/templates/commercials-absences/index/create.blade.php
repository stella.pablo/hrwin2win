<div layout="row" layout-align="space-between center" class="mdc-bg-grey-100 padding-10">
	<div>
		<md-button ng-click="setDateYesterday()">
			<md-icon md-font-icon="mdi mdi-chevron-left"></md-icon>
			Anterior
		</md-button>
	</div>

	<div>
		<md-datepicker ng-model="dateObj" ng-change="calcAbsences()" md-max-date="dateToday"></md-datepicker>
	</div>

	<div ng-style="tomorrowBtnStyle">
		<md-button ng-click="setDateTomorrow()">
			Siguiente
			<md-icon md-font-icon="mdi mdi-chevron-right"></md-icon>
		</md-button>
	</div>
</div>

<md-list class="padding-0">
	<md-subheader class="md-no-sticky">Listado de comerciales</md-subheader>
	<md-list-item ng-repeat="commercial in commercials">
		<p>[[ commercial.nombre_completo ]]</p>
		<span class="margin-right-10">[[ commercial.absence_reason ]]</span>
		<md-checkbox class="md-secondary md-warn" ng-model="commercial.absence" ng-change="absence(commercial)"></md-checkbox>
	</md-list-item>
</md-list>
