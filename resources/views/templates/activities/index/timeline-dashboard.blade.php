<div layout="column" layout-margin>
	<div layout="column" ng-repeat="date in dates | filter:dateFilter ? moment(dateFilter).format('YYYY-MM-DD') : undefined" ng-show="activities.length">
		<div layout="row" class="margin-bottom-10" style="border-bottom: 5px solid #03A9F4;">
			<h3 class="margin-top-0" style="text-transform: capitalize;">[[ moment(date).format('dddd[,] D [de] MMMM [de] YYYY') ]]</h3>
		</div>

		<div layout="column" flex>
			<div layout="row" ng-repeat="activity in activities | filter:{'date': date, 'type': typeFilter, 'client_id': clientFilter}:true | limitTo:5" class="mdc-bg-white margin-bottom-5 padding-10" style="border-bottom: 1px solid whitesmoke;">
				<div layout="column" layout-align="center center" flex="20">
					<md-icon md-font-icon="mdi mdi-48px" ng-class="{'mdi-crosshairs-gps': activity.type === 1, 'mdi-account-location': activity.type === 2, 'mdi-cart': activity.type === 3}" class="margin-0"></md-icon>
					<span>[[ moment(activity.created_at).format('HH:mm') ]]</span>
				</div>

				<div layout="column" layout-align="center start" flex>
					<h3 class="margin-0" style="font-family: 'Mada';">
						<a ui-sref="clients.show({ id: activity.client.id })">[[ activity.client.name ]]</a>
					</h3>
					<div>
						<md-icon md-font-icon="mdi mdi-account"></md-icon>
						<em class="margin-0" style="font-family: 'Mada';">[[ activity.user.name ]]</em>
					</div>
					<div>
						<md-icon md-font-icon="mdi mdi-map-marker"></md-icon>
						<em class="margin-0" style="font-family: 'Mada';">[[ activity.client.address ]]</em>
					</div>
					<div>
						<md-icon md-font-icon="mdi mdi-comment"></md-icon>
						<em class="margin-0" style="font-family: 'Mada';">[[ activity.comment ]]</em>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div ng-hide="activities.length">
		<h3 class="text-center">No hay actividades disponibles</h3>
	</div>
</div>
