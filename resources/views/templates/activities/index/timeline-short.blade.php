<div layout="column" layout-margin layout-align="center center" ng-hide="activitiesFiltersDisabled">
	<div layout="row" layout-margin>
		<md-input-container class="input-no-errors">
			<md-datepicker ng-model="dateFilter" md-placeholder="Fecha"></md-datepicker>
		</md-input-container>

		<md-input-container class="input-no-errors">
			<label>Tipo</label>
			<md-select ng-model="typeFilter">
				<!--md-on-close: expression; multiple:boolean; placeholder: string;-->
				<md-option value=""><em>Todos</em></md-option>
				<md-option ng-value="1">Check In</md-option>
				<md-option ng-value="2">Visita</md-option>
				<md-option ng-value="3">Pedido</md-option>
			</md-select>
		</md-input-container>

		<md-button ng-click="resetFilters()">
			<md-icon md-font-icon="mdi mdi-filter-remove"></md-icon>
			Limpiar
		</md-button>
	</div>
</div>

<div layout="column" layout-margin>
	<div layout="column" ng-repeat="date in dates | filter:dateFilter ? moment(dateFilter).format('YYYY-MM-DD') : undefined" ng-show="activities.length">
		<div layout="row" class="margin-bottom-10" style="border-bottom: 5px solid #03A9F4;">
			<h3 style="text-transform: capitalize;">[[ moment(date).format('dddd[,] D [de] MMMM [de] YYYY') ]]</h3>
		</div>

		<div layout="column" flex>
			<div layout="row" ng-repeat="activity in activities | filter:{'date': date, 'type': typeFilter, 'client_id': clientFilter}:true" class="mdc-bg-white margin-bottom-5 padding-10" md-whiteframe="2">
				<div layout="column" layout-align="center center" flex="20">
					<md-icon md-font-icon="mdi mdi-48px" ng-class="{'mdi-crosshairs-gps': activity.type === 1, 'mdi-account-location': activity.type === 2, 'mdi-cart': activity.type === 3}" class="margin-0"></md-icon>
					<span>[[ moment(activity.created_at).format('HH:mm') ]]</span>
				</div>

				<div layout="column" layout-align="center start" flex>
					<h3 class="margin-0" style="font-family: 'Mada';">
						[[ activity.client.name ]]
					</h3>
					<div>
						<md-icon md-font-icon="mdi mdi-account"></md-icon>
						<em class="margin-0" style="font-family: 'Mada';">[[ activity.user.name ]]</em>
					</div>
					<div>
						<md-icon md-font-icon="mdi mdi-map-marker"></md-icon>
						<em class="margin-0" style="font-family: 'Mada';">[[ activity.client.address ]]</em>
					</div>
					<div>
						<md-icon md-font-icon="mdi mdi-comment"></md-icon>
						<em class="margin-0" style="font-family: 'Mada';">[[ activity.comment ]]</em>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div ng-hide="activities.length">
		<h3 class="text-center">No hay actividades disponibles</h3>
	</div>
</div>
