<div layout="column" layout-margin layout-align="center center" ng-hide="activitiesFiltersDisabled">
	<div layout="row" layout-margin>
		<md-input-container ng-hide="hideClientFilter">
			<label>Cliente</label>
			<md-select ng-model="clientFilter"> <!--md-on-close: expression; multiple:boolean; placeholder: string;-->
			    <md-option value=""><em>Todos</em></md-option>
			    <md-option ng-value="client.id" ng-repeat="client in clients | orderBy:'name'">[[ client.name ]]</md-option>
			</md-select>
		</md-input-container>

		<md-input-container class="input-no-errors">
			<md-datepicker ng-model="dateFilter" md-placeholder="Fecha"></md-datepicker>
		</md-input-container>

		<md-input-container class="input-no-errors">
			<label>Tipo</label>
			<md-select ng-model="typeFilter"> <!--md-on-close: expression; multiple:boolean; placeholder: string;-->
			    <md-option value=""><em>Todos</em></md-option>
			    <md-option ng-value="1">Check In</md-option>
				<md-option ng-value="2">Visita</md-option>
				<md-option ng-value="3">Pedido</md-option>
			</md-select>
		</md-input-container>

		<md-button ng-click="resetFilters()">
			<md-icon md-font-icon="mdi mdi-filter-remove"></md-icon>
			Limpiar
		</md-button>
	</div>
</div>

<div layout="column" layout-margin>
	<div layout="row" ng-repeat="date in dates | filter:dateFilter ? moment(dateFilter).format('YYYY-MM-DD') : undefined" ng-show="activities.length">
		<div layout="row" layout-align="end center" flex="25" class="padding-right-10">
			<h1 style="text-transform: capitalize;">[[ moment(date).format('dddd[,] D [de] MMMM [de] YYYY') ]]</h1>
		</div>

		<div layout="column" flex class="padding-left-10" style="border-left: 5px solid #03A9F4;">
			<div layout="row" ng-repeat="activity in activities | filter:{'date': date, 'type': typeFilter, 'client_id': clientFilter}:true" class="mdc-bg-white margin-bottom-5 padding-10" md-whiteframe="2">
				<div layout="column" layout-align="center center" flex="10">
					<md-icon md-font-icon="mdi mdi-48px" ng-class="{'mdi-crosshairs-gps': activity.type === 1, 'mdi-account-location': activity.type === 2, 'mdi-cart': activity.type === 3}" class="margin-0"></md-icon>
					<span>[[ moment(activity.created_at).format('HH:mm') ]]</span>
				</div>

				<div layout="column" layout-align="center center" flex="20">
					<img src="/img/icons/shopper.png" style="width: 50px; height: 50px; border-radius: 50%;">
					<strong class="margin-top-10 text-center">[[ activity.user.name ]]</strong>
				</div>

				<div layout="column" layout-align="center start" flex>
					<h3 class="margin-0" style="font-family: 'Mada';">
						<a ui-sref="clients.show({ id: activity.client.id })">[[ activity.client.name ]]</a>
					</h3>
					<div>
						<md-icon md-font-icon="mdi mdi-map-marker"></md-icon>
						<em class="margin-0" style="font-family: 'Mada';">[[ activity.client.address ]]</em>
					</div>
					<div>
						<md-icon md-font-icon="mdi mdi-comment"></md-icon>
						<em class="margin-0" style="font-family: 'Mada';">[[ activity.comment ]]</em>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div ng-hide="activities.length">
		<h3 class="text-center">No hay actividades disponibles</h3>
	</div>
</div>
