<md-dialog>

	<md-toolbar class="md-menu-toolbar">
		<div class="md-toolbar-tools">

			<h2>[[ action == 'save' ? 'Añadir grupo de comisionamiento' : 'Editar grupo de comisionamiento' ]]</h2>

			<span flex></span>

			<md-button class="md-icon-button" ng-click="cancel()">
				<md-icon md-font-icon="mdi mdi-close" aria-label="Close dialog"></md-icon>
			</md-button>
		</div>
	</md-toolbar>

	<md-dialog-content>
		<form name="feeGroupForm">
			<div layout="row" layout-align="center center" class="md-dialog-content">
				<md-input-container flex>
					{{--<md-icon md-font-icon="mdi file-document-outline"></md-icon>--}}
					<input ng-model="fee.nombre_grupo" placeholder="Tipo de Contrato">
				</md-input-container>
			</div>

			<div layout="row" layout-align="center center" class="md-dialog-content">
				<md-input-container flex>
					<label>Tipo de grupo</label>
					<md-select ng-model="fee.id_tipo" ng-required>
						<md-option ng-value=""> ---------</md-option>
						<md-option ng-value="1">Producto</md-option>
						<md-option ng-value="2"> Artículo</md-option>
						<md-option ng-value="3"> Ambos</md-option>
					</md-select>
				</md-input-container>
			</div>
			<div layout="row" layout-align="center center" class="md-dialog-content">
				<md-input-container class="input-no-errors" flex-xs>
					<md-datepicker ng-model="fee.fecha_desde" md-placeholder="Fecha inicio"></md-datepicker>
				</md-input-container>
			</div>

			<div layout="row" layout-align="center center" class="md-dialog-content">
				<md-input-container class="input-no-errors" flex-xs>
					<md-datepicker ng-model="fee.fecha_hasta" md-placeholder="Fecha hasta"></md-datepicker>
				</md-input-container>
			</div>

			<div layout="column" md-whiteframe="2" class="md-dialog-content" flex ng-show="fee.id_tipo==1 ||fee.id_tipo == 3">
				<md-toolbar class="md-menu-toolbar">
					<div class="md-toolbar-tools">
						<h2 flex md-truncate>Productos del grupo de comisionamiento</h2>

						<md-button class="md-accent" ng-click="addFeeProduct()">
							<md-icon md-font-icon="mdi mdi-plus"></md-icon>
							Añadir producto
						</md-button>

						<md-button class="md-accent" ng-click="addFeeProductsAll()">
							<md-icon md-font-icon="mdi mdi-plus"></md-icon>
							Añadir todos
						</md-button>


					</div>
				</md-toolbar>

				<md-content class="mdc-bg-white">
					<table datatable="ng" dt-instance="feeProducstTable.feeProducts.dtInstanceCallback" dt-options="feeProducstTable.feeProducts.dtOptions" dt-disable-deep-watchers="true"
					       class="row-border hover full-width small">
						<thead>
						<tr>
							<th>#</th>
							<th>Producto</th>
							<th>Tramo</th>
							<th></th>
							<th></th>
						</tr>
						</thead>
						<tbody>

						<tr ng-repeat="product in fee.fee_products" class="clickable">
							<td>


								[[product.id]]
							</td>
							<td>
								<div ng-repeat="controlproduct in productsControls">
									<md-input-container flex>
										<md-select ng-model="product.id_producto" required>
                                            <label>Producto</label>
											<md-option ng-repeat="availableProduct in availableProducts" ng-value="availableProduct.id_producto">
												[[availableProduct.producto]] ([[availableProduct.tipo_contrato == 'H' ? 'Hogar' : availableProduct.tipo_contrato =='N' ? 'Negocio' : 'Hogar y negocio']])
											</md-option>
										</md-select>
									</md-input-container>
								</div>
							</td>
							<td>
								<div ng-repeat="controlproduct in productsControls">
									<div ng-repeat="availableProduct in availableProducts" ng-if="availableProduct.id_producto == product.id_producto">
										<md-input-container flex>

											<md-select ng-model="product.id_tramo" required>
                                                <label>Tramo</label>
												<md-option ng-repeat="stretch in availableProduct.stretches" ng-value="stretch.id">[[stretch.descripcion]]</md-option>
											</md-select>

										</md-input-container>
									</div>
								</div>
							</td>
							<td>
								<a style="color:lightblue" ng-click="selectSettlementProduct(product.id_producto,product.id_tramo,$index)" ng-disabled="!product.id_tramo || !product.id_producto">
									Seleccionar </a>

							</td>
							<td>
								<a ng-show="!removeQProduct[$index]" style="color:lightcoral" ng-click="removeQProduct[$index]=true"> Eliminar </a>
								<div layout-align="center" flex layout="column" ng-show="removeQProduct[$index]">¿Eliminar?</div>
								<div layout-align="space-around center" layout="row"><a ng-show="removeQProduct[$index]" style="color:lightcoral" ng-click="removeFeeProduct(product.id,$index)">
										Si</a></div>
								<div layout-align="space-around center" layout="row"><a ng-show="removeQProduct[$index]" style="color:lightblue" ng-click="removeQProduct[$index]=false">No</a>
								</div>
								{{--<a ng-show="!removeQ[$index]" style="color:lightcoral" ng-click="removeStretch(stretch.id_tramo,$index)"> Eliminar </a>--}}
							</td>
						</tr>
						</tbody>
					</table>
				</md-content>

			</div>


			<div layout="column" md-whiteframe="2" class="md-dialog-content" flex ng-show="selectedProductName">
				<md-toolbar class="md-menu-toolbar">
					<div class="md-toolbar-tools">
						<h2 flex md-truncate>Comisiones por posteo del producto [[selectedProductName + ' (' + selectedStretchName + ')']]</h2>

						<md-button class="md-accent" ng-click="addProductFee(selectedProductIndex)">
							<md-icon md-font-icon="mdi mdi-plus"></md-icon>
							Añadir comisión de producto
						</md-button>
					</div>
				</md-toolbar>

				<md-content class="mdc-bg-white">
					<table datatable="ng" dt-instance="feeProductFeesTable.feeProductsFees.dtInstanceCallback" dt-options="feeProductFeesTable.feeProductsFees.dtOptions"
					       dt-disable-deep-watchers="true"
					       class="row-border hover full-width small">
						<thead>
						<tr>
							<th>#</th>
							<th>Desde promotores</th>
							<th>Hasta promotores</th>
							<th>Importe comision promotor</th>
							<th>Importe comision Estructura</th>
							<th></th>
						</tr>
						</thead>
						<tbody>

						<tr ng-repeat="fee in fee.fee_products[selectedProductIndex].fees" class="clickable">
							<td>
								[[fee.id]]
							</td>
							<td>
								<md-input-container flex>
									<input type="number" ng-model="fee.desde_promotores" required>
								</md-input-container>
							</td>
							<td>
								<md-input-container flex>
									<input type="number" ng-model="fee.hasta_promotores" required>
								</md-input-container>
							</td>
							<td>
								<md-input-container flex>
									<input type="number" ng-model="fee.importe_comision" required>
								</md-input-container>
							</td>
							<td>
								<md-input-container flex>
									<input type="number" ng-model="fee.importe_comision_estructura" required>
								</md-input-container>
							</td>
							<td>
								<a ng-show="!removeQProductFee[$index]" style="color:lightcoral" ng-click="removeQProductFee[$index]=true"> Eliminar </a>
								<div layout-align="center" flex layout="column" ng-show="removeQProductFee[$index]">¿Eliminar?</div>
								<div layout-align="space-around center" layout="row"><a ng-show="removeQProductFee[$index]" style="color:lightcoral" ng-click="removeProductFee(fee.id,$index)">
										Si</a></div>
								<div layout-align="space-around center" layout="row"><a ng-show="removeQProductFee[$index]" style="color:lightblue" ng-click="removeQProductFee[$index]=false">No</a>
								</div>
								{{--<a ng-show="!removeQ[$index]" style="color:lightcoral" ng-click="removeStretch(stretch.id_tramo,$index)"> Eliminar </a>--}}
							</td>
						</tr>
						</tbody>
					</table>
				</md-content>

			</div>


			<div layout="column" md-whiteframe="2" class="md-dialog-content" flex ng-show="fee.id_tipo==2 ||fee.id_tipo == 3">
				<md-toolbar class="md-menu-toolbar">
					<div class="md-toolbar-tools">
						<h2 flex md-truncate>Artículos del grupo de comisionamiento</h2>

						<md-button class="md-accent" ng-click="addFeeArticle(null,null)" ng-disabled="fee.id_tipo==1 || fee.id_tipo==null">
							<md-icon md-font-icon="mdi mdi-plus"></md-icon>
							Añadir artículo
						</md-button>
					</div>
				</md-toolbar>

				<md-content class="mdc-bg-white">
					<table datatable="ng" dt-instance="feeArticlesTable.feeArticles.dtInstanceCallback" dt-options="feeArticlesTable.feeArticles.dtOptions" dt-disable-deep-watchers="true"
					       class="row-border hover full-width small">
						<thead>
						<tr>
							<th>#</th>
							<th>Artículo</th>
							<th>Unidades lote</th>
							<th></th>
						</tr>
						</thead>
						<tbody>

						<tr ng-repeat="article in fee.fee_articles" class="clickable">
							<td>


								[[article.id]]
							</td>
							<td>
								<div ng-repeat="controlarticle in articlesControls">
									<md-input-container flex>
										<md-select ng-model="article.id_articulo" required>
                                            <label>Articulo</label>
											<md-option ng-repeat="availableArticle in availableArticles" ng-value="availableArticle.id_articulo">[[availableArticle.descripcion_corta]]</md-option>
										</md-select>
									</md-input-container>
								</div>
							</td>
							<td>
								<div ng-repeat="controlarticle in articlesControls">
									<div ng-repeat="availableArticle in availableArticles" ng-if="availableArticle.id_articulo == article.id_articulo">
										<md-input-container flex>

											<md-select ng-model="article.unidades_lote" required>
                                                <label>Uds. lote</label>
												<md-option ng-repeat="package in availableArticle.packages" ng-value="package.unidades_lote">[[package.definicion]]</md-option>
											</md-select>

										</md-input-container>
									</div>
								</div>
							</td>
							<td>
								<a ng-show="!removeQArticle[$index]" style="color:lightcoral" ng-click="removeQArticle[$index]=true"> Eliminar </a>
								<div layout-align="center" flex layout="column" ng-show="removeQArticle[$index]">¿Eliminar?</div>
								<div layout-align="space-around center" layout="row"><a
                                            ng-show="removeQArticle[$index]" style="color:lightcoral"
                                            ng-click="removeFeeArticle(article.id,$index)">
										Si</a></div>
								<div layout-align="space-around center" layout="row"><a ng-show="removeQArticle[$index]" style="color:lightblue" ng-click="removeQArticle[$index]=false">No</a>
								</div>
								{{--<a ng-show="!removeQ[$index]" style="color:lightcoral" ng-click="removeStretch(stretch.id_tramo,$index)"> Eliminar </a>--}}
							</td>
						</tr>
						</tbody>
					</table>
				</md-content>

			</div>


			<div layout="column" md-whiteframe="2" class="md-dialog-content" flex>
				<md-toolbar class="md-menu-toolbar">
					<div class="md-toolbar-tools">
						<h2 flex md-truncate>Gerencias del grupo de comisionamiento</h2>

						<md-button class="md-accent" ng-click="addFeeManagement(null,null)">
							<md-icon md-font-icon="mdi mdi-plus"></md-icon>
							Añadir gerencia
						</md-button>
					</div>
				</md-toolbar>


				<md-content class="mdc-bg-white">
					<table datatable="ng" dt-instance="feeManagementsTable.feeManagements.dtInstanceCallback" dt-options="feeManagementsTable.feeManagements.dtOptions"
					       dt-disable-deep-watchers="true"
					       class="row-border hover full-width small">


						<thead>
						<tr>
							<th>#</th>
							<th>Gerencia</th>

							<th></th>
							<th></th>
						</tr>
						</thead>
						<tbody>
						<tr ng-repeat="fManagement in fee.fee_managements" class="clickable">


							<td>


								[[fManagement.id]]
							</td>
							<td>
								{{--Seems that due to redraws, it crashes unless i place the select inside a ng-repeat that initially is an empty array.
								 Then an empty object is inserted after the	management object is created--}}
								<div ng-repeat="control in controls">
									<md-input-container flex>
										<label>Gerencia</label>
										<md-select ng-model="fManagement.id_gerencia" ng-required ng-change="refreshStructures(fManagement.id_gerencia)">
											<md-option ng-value="availableManagement.id_gerencia" ng-repeat="availableManagement in managements">[[availableManagement.gerencia]]</md-option>
										</md-select>
									</md-input-container>
								</div>

							</td>
							<td>
								<a style="color:lightblue" ng-click="selectManagement(fManagement.id_gerencia,$index)" ng-disabled="!fManagement.id_gerencia">
									Seleccionar </a>
								{{--							<td>
																<a style="color:lightblue" ng-click="selectManagement(fManagement.id_gerencia)" ng-show="selectedManagement != fManagement.id_gerencia"> Seleccionar </a>
																<span style="color:lightgreen" ng-show="selectedManagement == fManagement.id_gerencia">Seleccionada</span>
															</td>--}}
							</td>
							<td>

								<a ng-show="!removeQManagement[$index]" style="color:lightcoral" ng-click="removeQManagement[$index]=true"> Eliminar </a>
								<div layout-align="center" flex layout="column" ng-show="removeQManagement[$index]">¿Eliminar?</div>
								<div layout-align="space-around center" layout="row">
									<a ng-show="removeQManagement[$index]" style="color:lightcoral" ng-click="removeManagement(fManagement.id,$index)">
										Si</a></div>
								<div layout-align="space-around center" layout="row">
									<a ng-show="removeQManagement[$index]" style="color:lightblue" ng-click="removeQManagement[$index]=false">
										No</a>
								</div>


							</td>
						</tr>
						</tbody>
					</table>
				</md-content>

			</div>









			<div layout="column" md-whiteframe="2" class="md-dialog-content" flex ng-show="selectedManagementName">
				<md-toolbar class="md-menu-toolbar">
					<div class="md-toolbar-tools">
						<h2 flex md-truncate>Comisiones y bonos de calidad o comercio de la gerencia [[selectedManagementName]]</h2>

						<md-button class="md-accent" ng-click="addManagementFee(selectedManagementIndex)">
							<md-icon md-font-icon="mdi mdi-plus"></md-icon>
							Añadir comisión de gerencia
						</md-button>
					</div>
				</md-toolbar>

				<md-content class="mdc-bg-white">
					<table datatable="ng" dt-instance="feeManagementFeesTable.feeManagementFees.dtInstanceCallback" dt-options="feeManagementFeesTable.feeManagementFees.dtOptions"
					       dt-disable-deep-watchers="true"
					       class="row-border hover full-width small">
						<thead>
						<tr>
							<th>#</th>
							<th>Desde zonas</th>
							<th>Hasta zonas</th>
							<th>Cantidad desde</th>
							<th>Cantidad hasta</th>
							<th>Importe comision</th>
							<th></th>
						</tr>
						</thead>
						<tbody>

						<tr ng-repeat="fee in fee.fee_managements[selectedManagementIndex].fees" class="clickable">
							<td>
								[[fee.id]]
							</td>
							<td>


								<md-input-container flex>
									<input type="number" ng-model="fee.zonas_desde" required>
								</md-input-container>

							</td>
							<td>
								<md-input-container flex>
									<input type="number" ng-model="fee.zonas_hasta" required>
								</md-input-container>
							</td>
							<td>
								<md-input-container flex>

									<input type="number" ng-model="fee.cantidad_desde" required>
								</md-input-container>
							</td>
							<td>
								<md-input-container flex>
									<input type="number" ng-model="fee.cantidad_hasta" required>
								</md-input-container>
							</td>
							<td>
								<md-input-container flex>
									<input type="number" ng-model="fee.importe_comision" required>
								</md-input-container>
							</td>
							<td>
								<a ng-show="!removeQManagementFee[$index]" style="color:lightcoral" ng-click="removeQManagementFee[$index]=true"> Eliminar </a>
								<div layout-align="center" flex layout="column" ng-show="removeQManagementFee[$index]">¿Eliminar?</div>
								<div layout-align="space-around center" layout="row"><a ng-show="removeQManagementFee[$index]" style="color:lightcoral" ng-click="removeManagementFee(fee.id,$index)">
										Si</a></div>
								<div layout-align="space-around center" layout="row"><a ng-show="removeQManagementFee[$index]" style="color:lightblue"
								                                                        ng-click="removeQManagementFee[$index]=false">No</a>
								</div>
								{{--<a ng-show="!removeQ[$index]" style="color:lightcoral" ng-click="removeStretch(stretch.id_tramo,$index)"> Eliminar </a>--}}
							</td>
						</tr>
						</tbody>
					</table>
				</md-content>

			</div>


			<div layout="column" md-whiteframe="2" class="md-dialog-content" flex>
				<md-toolbar class="md-menu-toolbar">
					<div class="md-toolbar-tools">
						<h2 flex md-truncate>Comisiones Generales</h2>

						<md-button class="md-accent" ng-click="addFee(null,null)">
							<md-icon md-font-icon="mdi mdi-plus"></md-icon>
							Añadir comision
						</md-button>
					</div>
				</md-toolbar>

				<md-content class="mdc-bg-white">
					<table datatable="ng" dt-instance="feeManagementsTable.feeManagements.dtInstanceCallback" dt-options="feeManagementsTable.feeManagements.dtOptions" dt-disable-deep-watchers="true"
					       class="row-border hover full-width small">

						<thead>
						<th>#</th>
						<th>Cantidad desde</th>
						<th>Cantidad hasta</th>
						<th>Tipo de contrato comercial</th>
						<th>Estructura comercial</th>
						<th>Tipo importe comercial</th>
						<th>Importe comercial</th>
						<th>Tipo importe estructura</th>
						<th>Importe estructura</th>
						<th>Tipo importe gerencia</th>
						<th>Importe gerencia</th>

						<th></th>
						</tr>
						</thead>
						<tbody>
						<tr ng-repeat="fee in fee.fees" class="clickable">
							<td>


								[[fee.id]]
							</td>
							<td>
								<md-input-container flex>
									<input type="number" ng-model="fee.cantidad_desde" required>
								</md-input-container>
							</td>
							<td>
								<md-input-container flex>
									<input type="number" ng-model="fee.cantidad_hasta" required>
								</md-input-container>
							</td>
							<td>
								<div ng-repeat="feecontrol in feesControls">
									<md-input-container flex>
										<label>Tipo de contrato</label>
										<md-select ng-model="fee.id_tipo_contrato_comercial" required>

											<md-option ng-repeat="contractType in contractsTypes" ng-value="contractType.id_tipo_contrato_comercial">[[contractType
												.tipo_contrato_comercial]]
											</md-option>
										</md-select>
									</md-input-container>
								</div>
							</td>
							<td>
								<div ng-repeat="feecontrol in feesControls">
									<md-input-container flex>
                                        <label>Estructura comerc.</label>
										<md-select ng-model="fee.id_estructura" required>
											<md-option ng-repeat="structure in structures" ng-value="structure.id_gerencia_estructura">[[structure.id_gerencia_estructura]]</md-option>
										</md-select>
									</md-input-container>
								</div>
							</td>


							<td>

								<md-input-container flex>
									<label>Tipo de importe comercial</label>
									<md-select ng-model="fee.id_tipo_importe_comercial" required>
										<md-option ng-value=""> ---------</md-option>
										<md-option ng-value="1">Importe</md-option>
										<md-option ng-value="2">Porcentaje</md-option>
									</md-select>
								</md-input-container>


							</td>
							<td>
								<md-input-container flex>
									<input type="number" ng-model="fee.importe_comercial" required>
								</md-input-container>
							</td>
							<td>

								<md-input-container flex>
									<label>Tipo de importe estructura</label>
									<md-select ng-model="fee.id_tipo_importe_estructura" required>
										<md-option ng-value=""> ---------</md-option>
										<md-option ng-value="1">Importe</md-option>
										<md-option ng-value="2">Porcentaje</md-option>
									</md-select>
								</md-input-container>


							</td>
							<td>
								<md-input-container flex>
									<input type="number" ng-model="fee.importe_estructura" required>
								</md-input-container>

							</td>
							<td>

								<md-input-container flex>
									<label>Tipo de importe gerencia</label>
									<md-select ng-model="fee.id_tipo_importe_gerencia" required>
										<md-option ng-value=""> ---------</md-option>
										<md-option ng-value="1">Importe</md-option>
										<md-option ng-value="2">Porcentaje</md-option>
									</md-select>
								</md-input-container>


							</td>
							<td>
								<md-input-container flex>
									<input type="number" ng-model="fee.importe_gerencia" required>
								</md-input-container>

							</td>
							<td>
								<a ng-show="!RemoveQManagementFee[$index]" style="color:lightcoral" ng-click="RemoveQManagementFee[$index]=true"> Eliminar </a>
								<div layout-align="center" flex layout="column" ng-show="RemoveQManagementFee[$index]">¿Eliminar?</div>
								<div layout-align="space-around center" layout="row"><a ng-show="RemoveQManagementFee[$index]" style="color:lightcoral" ng-click="removeFee(fee.id,$index)">
										Si</a></div>
								<div layout-align="space-around center" layout="row"><a ng-show="RemoveQManagementFee[$index]" style="color:lightblue"
								                                                        ng-click="RemoveQManagementFee[$index]=false">No</a></div>
								{{--<a ng-show="!removeQ[$index]" style="color:lightcoral" ng-click="removeStretch(stretch.id_tramo,$index)"> Eliminar </a>--}}
							</td>
						</tr>
						</tbody>
					</table>
				</md-content>

			</div>


			<div layout="column" md-whiteframe="2" class="md-dialog-content" flex ng-show="fee.id_tipo==1 ||fee
			.id_tipo == 3">
				<md-toolbar class="md-menu-toolbar">
					<div class="md-toolbar-tools">
						<h2 flex md-truncate>Comisiones por efectividad</h2>

						<md-button class="md-accent" ng-click="addEfectivityFee(null,null)">
							<md-icon md-font-icon="mdi mdi-plus"></md-icon>
							Añadir comision
						</md-button>
					</div>
				</md-toolbar>

				<md-content class="mdc-bg-white">
					<table datatable="ng" dt-instance="feeProductEfectivityFeesTable.efectivityFees.dtInstanceCallback" dt-options="feeProductEfectivityFeesTable.efectivityFees.dtOptions" dt-disable-deep-watchers="true"
					       class="row-border hover full-width small">

						<thead>
						<th>#</th>
						<th>% efectividad desde</th>
						<th>% efectividad hasta</th>
						<th>Importe promotor</th>
						<th>Importe supervisor</th>
						<th>Importe lider</th>


						<th></th>
						</tr>
						</thead>
						<tbody>
						<tr ng-repeat="fee in fee.efectivity_fees" class="clickable">
							<td>


								[[fee.id]]
							</td>
							<td>
								<md-input-container flex>
									<input type="number" ng-model="fee.efectividad_desde" required>
								</md-input-container>
							</td>
							<td>
								<md-input-container flex>
									<input type="number" ng-model="fee.efectividad_hasta" required>
								</md-input-container>
							</td>
							<td>
								<md-input-container flex>
									<input type="number" ng-model="fee.importe_promotor" required>
								</md-input-container>
							</td>
							<td>
								<md-input-container flex>
									<input type="number" ng-model="fee.importe_estructura" required>
								</md-input-container>
							</td>
							<td>
								<md-input-container flex>
									<input type="number" ng-model="fee.importe_gerencia" required>
								</md-input-container>
							</td>


							<td>
								<a ng-show="!RemoveQEfectivityFee[$index]" style="color:lightcoral" ng-click="RemoveQEfectivityFee[$index]=true"> Eliminar </a>
								<div layout-align="center" flex layout="column" ng-show="RemoveQEfectivityFee[$index]">¿Eliminar?</div>
								<div layout-align="space-around center" layout="row"><a ng-show="RemoveQEfectivityFee[$index]" style="color:lightcoral" ng-click="removeEfectivityFee(fee.id,$index)">
										Si</a></div>
								<div layout-align="space-around center" layout="row"><a ng-show="RemoveQEfectivityFee[$index]" style="color:lightblue"
								                                                        ng-click="RemoveQEfectivityFee[$index]=false">No</a></div>

							</td>
						</tr>
						</tbody>
					</table>
				</md-content>

			</div>



			<div layout="row" layout-align="center center" class="md-dialog-content">
				<div>

					<md-button class="md-primary" ng-click="submit()" ng-disabled="contractTypeForm.$invalid || contractTypeForm.$pristine">
						[[ action == 'save' ? 'CREAR' : 'EDITAR' ]]
					</md-button>
					<md-button class="md-primary" ng-click="cancel()">
						Cancelar
					</md-button>

				</div>
			</div>
		</form>
	</md-dialog-content>

</md-dialog>

