<md-dialog>

	<md-toolbar class="md-menu-toolbar">
		<div class="md-toolbar-tools">

			<h2>[[ action == 'save' ? 'Crear estructura' : 'Editar estructura'   ]]</h2>

			<span flex></span>

			<md-button class="md-icon-button" ng-click="cancel()">
				<md-icon md-font-icon="mdi mdi-close" aria-label="Close dialog"></md-icon>
			</md-button>
		</div>
	</md-toolbar>

	<md-dialog-content>
		<form name="structureform">
			<div layout="row" layout-align="center center" class="md-dialog-content" ng-disabled="action!='save'">
				<md-input-container flex>
					<label>Tipo de estructura</label>
					<md-select ng-model="structure.id_gerencia_estructura" ng-change="structure.id_comercial_estructura = null" ng-disabled="action!='save'">
						<md-option ng-value="'001'">Gerente</md-option>
						<md-option ng-value="'002'">Estructura</md-option>
						<md-option ng-value="'003'" ng-show="enableCommercials">Comercial</md-option>

					</md-select>
				</md-input-container>
			</div>
			<div layout="row" layout-align="center center" class="md-dialog-content" ng-show="structure.id_gerencia_estructura == '001'">
				<md-input-container flex>
					<label>Comercial estructura</label>
					<md-select ng-model="structure.id_comercial_estructura"  ng-required="structure.id_gerencia_estructura=='001'"
					           ng-disabled="action!='save'">
						<md-option ng-repeat="manager in managers | filter:{enabled:true}:true" ng-value="manager.id_comercial"> [[manager.nombre_completo]]</md-option>
					</md-select>
				</md-input-container>
			</div>
			<div layout="row" layout-align="center center" class="md-dialog-content" ng-show="structure.id_gerencia_estructura == '002'">
				<md-input-container flex>
					<label>Lider estructura</label>
					<md-select ng-model="structure.id_comercial_estructura"  ng-required="structure.id_gerencia_estructura=='002'"
					           ng-disabled="action!='save'">
						<md-option ng-repeat="leader in leaders | filter:{enabled:true}:true"  ng-value="leader.id_comercial"> [[leader.nombre_completo]]</md-option>
					</md-select>
				</md-input-container>
			</div>
			<div layout="row" layout-align="center center" class="md-dialog-content">

				<md-input-container class="input-no-errors" flex-xs required>
					<input type="number" placeholder="Porcentaje de cobro" value="0" max="100" ng-model="structure.porcentaje_cobro">
				</md-input-container>
			</div>
			<div layout="row" layout-align="center center" class="md-dialog-content">
				<md-input-container class="input-no-errors" flex-xs>
					<md-datepicker ng-model="structure.fecha_desde" md-placeholder="Fecha desde" required></md-datepicker>
				</md-input-container>
			</div>
			<div layout="row" layout-align="center center" class="md-dialog-content">

				<md-input-container class="input-no-errors" flex-xs>
					<md-datepicker ng-model="structure.fecha_hasta" md-placeholder="Fecha fin"></md-datepicker>
				</md-input-container>

			</div>
			<div layout="row" layout-align="center center" class="md-dialog-content">
				<div>


					<md-button class="md-primary" ng-click="submit()" ng-disabled="structureform.$invalid || structureform.$pristine">
						[[ action == 'save' ? 'CREAR' : 'EDITAR'    ]]
					</md-button>
					<md-button class="md-primary" ng-click="cancel()">
						Cancelar
					</md-button>

				</div>
			</div>
		</form>
	</md-dialog-content>

</md-dialog>

