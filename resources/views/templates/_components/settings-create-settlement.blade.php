<md-dialog>

    <md-toolbar class="md-menu-toolbar">
        <div class="md-toolbar-tools">

            <h2>[[ action == 'save' ? 'Generar liquidacion' : 'Editar liquidacion' ]]</h2>

            <span flex></span>

            <md-button class="md-icon-button" ng-click="cancel()">
                <md-icon md-font-icon="mdi mdi-close" aria-label="Close dialog"></md-icon>
            </md-button>
        </div>
    </md-toolbar>

    <md-dialog-content>
        <form name="settlementForm">
            <div layout="row" layout-xs="column" layout-align="center center" class="md-dialog-content">
                <div flex layout-margin="10">
                    <md-input-container flex>
                        {{--<md-icon md-font-icon="mdi file-document-outline"></md-icon>--}}
                        <input ng-model="settlement.observaciones" placeholder="Observaciones">
                    </md-input-container>
                </div>
            </div>
            <div ng-repeat="settlementcontrol in settlementControls">
                <div layout="row" layout-xs="column" layout-align="center center" class="md-dialog-content">
                    <div flex layout-margin="10">
                        <label>Gerencia</label>
                        <md-select ng-model="settlement.id_gerencia" ng-change="switchManagement(settlement.id_gerencia)" ng-disabled="settlement.id">
                            <md-option ng-value="">---------</md-option>
                            <md-option ng-repeat="management in managements" ng-value="management.id_gerencia"> [[management.gerencia]]</md-option>
                        </md-select>
                    </div>
                    <div flex layout-margin="10">
                        <label>Comercial</label>
                        <md-select ng-model="settlement.id_comercial" ng-change="switchCommercial(settlement.id_comercial)" ng-disabled="settlement.id">
                            <md-option ng-value="">---------</md-option>
                            <md-option ng-repeat="commercial in commercials" ng-value="commercial.id_comercial"> [[commercial.nombre_completo]]</md-option>
                        </md-select>
                    </div>
                    <div flex layout-margin="10">
                        <label>Estructura comercial</label>
                        <md-select ng-model="settlement.id_estructura_comercial" ng-disabled="settlement.id_comercial || settlement.id">
                            <md-option ng-value="">---------</md-option>
                            <md-option ng-repeat="structure in structures" ng-value="structure.id_gerencia_estructura"> [[structure.nombre]]</md-option>
                        </md-select>
                    </div>
                    <div flex layout-margin="10">
                        <label>Tipo contrato comercial</label>
                        <md-select ng-model="settlement.id_tipo_contrato_comercial" ng-disabled="settlement.id_comercial || settlement.id ">
                            <md-option ng-value="">---------</md-option>
                            <md-option ng-repeat="contractType in contractsTypes" ng-value="contractType.id_tipo_contrato_comercial"> [[contractType.tipo_contrato_comercial]]</md-option>
                        </md-select>
                    </div>
                </div>
                <div flex layout-margin="10">
                    <md-input-container class="input-no-errors" flex-xs required>
                        <md-datepicker md-placeholder="Fecha de inicio" ng-model="settlement.fecha_desde" required ng-change="fixDate('fecha_desde')" ng-disabled="settlement.id">
                    </md-input-container>
                </div>
                <div flex layout-margin="10">
                    <md-input-container class="input-no-errors" flex-xs required>
                        <md-datepicker md-placeholder="Fecha final" ng-model="settlement.fecha_hasta" ng-change="fixDate('fecha_hasta')"required ng-disabled="settlement.id">
                    </md-input-container>
                </div>
                <div flex layout-margin="10">
                    <md-input-container class="input-no-errors" flex-xs required>
                        <md-datepicker md-placeholder="Fecha de liquidacion" ng-model="settlement.fecha_liquidacion" ng-change="fixDate('fecha_liquidacion')"required ng-disabled="settlement.id">
                    </md-input-container>
                </div>
            </div>
            <div layout="column" md-whiteframe="2" class="md-dialog-content" flex>
                <md-toolbar class="md-menu-toolbar">
                    <div class="md-toolbar-tools">
                        <h2 flex md-truncate>Comerciales en la liquidacion</h2>

                    </div>
                </md-toolbar>
                <md-content class="mdc-bg-white">
                    <table datatable="ng" dt-instance="settlementCommercialsTable.settlementCommercials.dtInstanceCallback" dt-options="settlementCommercialsTable.settlementCommercials.dtOptions"
                           dt-disable-deep-watchers="true"
                           class="row-border hover full-width small">
                        <thead>
                        <th>#</th>
                        <th>Comercial</th>
                        <th>Importe directo</th>
                        <th>Importe referido</th>
                        <th>Numero de ventas</th>
                        <th>Numero de artículos</th>
                        <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr ng-repeat="fCommercial in settlement.commercial_settlements" class="clickable">
                            <td>
                                [[fCommercial.id_comercial]]
                            </td>
                            <td>
                                [[fCommercial.commercial.nombre_completo]]
                            </td>
                            <td>
                                [[fCommercial.importe_inm.toFixed(2)]]
                            </td>
                            <td>
                                [[fCommercial.importe_car.toFixed(2)]]
                            </td>
                            <td>
                                [[fCommercial.numero_ventas]]
                            </td>
                            <td>
                                [[fCommercial.numero_articulos]]
                            </td>
                            <td>
                                <div layout-align="space-around center" layout="row">
                                    <a style="color:lightgreen" ng-click="selectCommercialSettlement(fCommercial.id)">Seleccionar</a>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </md-content>
            </div>


            <div layout="column" md-whiteframe="2" class="md-dialog-content" flex>
                <md-toolbar class="md-menu-toolbar" ng-hide="!selectedCommercialSettlement">
                    <div class="md-toolbar-tools">
                        <h2 flex md-truncate>Conceptos de liquidacion de [[selectedCommercialName]]</h2>
                        <md-button class="md-accent" ng-click="addSettlementConcept()" ng-disabled="!settlement.id">
                            <md-icon md-font-icon="mdi mdi-plus"></md-icon>
                            Añadir linea de concepto
                        </md-button>
                    </div>
                </md-toolbar>
                <md-content class="mdc-bg-white" ng-hide="!selectedCommercialSettlement">
                    <table datatable="ng" dt-instance="settlementCommercialConceptsTable.settlementCommercialConcepts.dtInstanceCallback" dt-options="settlementCommercialConceptsTable.settlementCommercialConcepts.dtOptions"
                           dt-disable-deep-watchers="true"

                           class="row-border hover full-width small">
                        <thead>
                        <th>#</th>
                        <th>Concepto</th>
                        <th>Venta/Contrato asoc.</th>
                        <th>Linea venta/ Producto asoc.</th>
                        <th>Importe</th>
                        <th></th>

                        </tr>
                        </thead>
                        <tbody>
                        <tr ng-repeat="sConcept in settlement.commercial_settlements[commercialSettlementIndex].concepts" class="clickable">

                            <td>

                                [[sConcept.id]]
                            </td>
                            <td>
                                <md-input-container flex>
                                    <md-select ng-model="sConcept.concepto" ng-disabled="sConcept.tipo_linea==0" required>
                                        <md-option ng-value="1">Comisión</md-option>
                                        <md-option ng-value="2">Descuento pago metalico</md-option>
                                        <md-option ng-value="3">Comisión referenciada</md-option>
                                        <md-option ng-value="4">Otros</md-option>
                                        <md-option ng-value="5">Devolucion coste material</md-option>
                                        <md-option ng-value="6">Cobro entrega en mano</md-option>
                                        <md-option ng-value="7">Cobro albaran</md-option>
                                        <md-option ng-value="8">Cobro alquiler</md-option>
                                        <md-option ng-value="9">Pago directo de producto</md-option>
                                        <md-option ng-value="10">Pago referenciado de producto</md-option>
                                        <md-option ng-value="11">Pago por contrato posteado</md-option>
                                        <md-option ng-value="12">Pago por contrato posteado ref.</md-option>
                                        <md-option ng-value="13">Pago por produccion (%posteo)</md-option>
                                        <md-option ng-value="14">Pago por productividad (zonas)</md-option>
                                    </md-select>
                                </md-input-container>
                            </td>
                            <td>

                                <div ng-repeat="settlementcontrol in settlementControls" ng-if="sConcept.concepto < 9">
                                    <md-input-container flex>
                                        <md-select ng-model="sConcept.id_venta" ng-disabled="sConcept.tipo_linea==0">
                                            <md-option ng-repeat="sale in availableSales" ng-value="sale.id_venta">[[sale.id_venta + '(' + sale.fecha + ')'   ]]</md-option>
                                        </md-select>
                                    </md-input-container>
                                </div>

                                <div ng-repeat="settlementcontrol in settlementControls" ng-if="sConcept.concepto >= 9">
                                    <md-input-container flex>
                                        <md-select ng-model="sConcept.id_contrato" ng-disabled="sConcept.tipo_linea==0">
                                            <md-option ng-repeat="contract in availableContracts" ng-value="contract.id_contrato">
                                                [[contract.id_contrato + '(' + contract.fecha_firma + ')']]
                                            </md-option>
                                        </md-select>
                                    </md-input-container>
                                </div>
                            </td>
                            <td>
                                <div ng-repeat="settlementcontrol in settlementControls">
                                    <div ng-repeat="sale in availableSales" ng-if="sale.id_venta == sConcept.id_venta" ng-if="sConcept.concepto < 9">
                                        <md-input-container flex>
                                            <md-select ng-model="sConcept.id_linea_venta" ng-disabled="sConcept.tipo_linea==0">
                                                <md-option ng-repeat="line in sale.articles" ng-value="line.id_linea">[[line.id_linea + '(' + line.total.toFixed(2) + ')'   ]]</md-option>
                                            </md-select>
                                        </md-input-container>
                                    </div>

                                </div>


                                <div ng-repeat="settlementcontrol in settlementControls">
                                    <div ng-repeat="contract in availableContracts" ng-if="contract.id_contrato == sConcept.id_contrato" ng-if="sConcept.concepto >= 9">
                                        <md-input-container flex>
                                            <md-select ng-model="sConcept.id_contrato_producto" ng-disabled="sConcept.tipo_linea==0">
                                                <md-option ng-repeat="product in contract.products" ng-value="product.id">[[product.id + '(' + product.id_producto + ')']]</md-option>
                                            </md-select>
                                        </md-input-container>
                                    </div>

                                </div>
                            </td>
                            <td>
                                <md-input-container flex>
                                    <input type="number" ng-disabled="sConcept.tipo_linea==0" ng-model="sConcept.importe" required>
                                </md-input-container>
                            </td>

                            <td>
                                <a ng-show="!removeQSettlementConcept[$index]" ng-style="sConcept.tipo_linea == 0	&& {'color':'lightgrey'} || sConcept.tipo_linea == 1 && {'color':'lightcoral'}"
                                   ng-click="removeQSettlementConcept[$index]=true" ng-disabled="sConcept.tipo_linea == 0"> Eliminar </a>
                                <div layout-align="center" flex layout="column" ng-show="removeQSettlementConcept[$index]">¿Eliminar?</div>
                                <div layout-align="space-around center" layout="row">
                                    <a ng-show="removeQSettlementConcept[$index]" style="color:lightcoral"
                                       ng-click="removeSettlementConcept(sConcept.id,$index)">
                                        Si</a></div>
                                <div layout-align="space-around center" layout="row">
                                    <a ng-show="removeQSettlementConcept[$index]" style="color:lightblue" ng-click="removeQSettlementConcept[$index]=false">
                                        No</a>
                                </div>
                            </td>

                        </tr>
                        </tbody>
                    </table>
                </md-content>
            </div>


            <div layout="row" layout-align="center center" class="md-dialog-content">
                <div>

                    <md-button class="md-primary" ng-click="submit()" ng-disabled="settlementForm.$invalid || settlementForm.$pristine">
                        [[ action == 'save' ? 'CREAR' : 'EDITAR' ]]
                    </md-button>
                    <md-button class="md-primary" ng-click="cancel()">
                        Cancelar
                    </md-button>

                </div>
            </div>
        </form>
    </md-dialog-content>

</md-dialog>

