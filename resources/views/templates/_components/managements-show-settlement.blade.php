<md-dialog>

	<md-toolbar class="md-menu-toolbar">
		<div class="md-toolbar-tools">

			<h2>Liquidación [[settlement.observaciones]] </h2>

			<span flex></span>

			<md-button class="md-icon-button" ng-click="cancel()">
				<md-icon md-font-icon="mdi mdi-close" aria-label="Close dialog"></md-icon>
			</md-button>
		</div>
	</md-toolbar>

	<md-dialog-content>
		<form name="settlementForm">
			<div flex layout-margin="10">
				<p>Fecha inicio de liquidación: [[ moment(settlement.fecha_desde).format('DD-MM-YYYY')]]</p>
			</div>
			<div flex layout-margin="10">
				<p>Fecha final de liquidación: [[moment(settlement.fecha_hasta).format('DD-MM-YYYY')]]</p>
			</div>
			<div flex layout-margin="10">
				<p>Fecha de liquidación: [[moment(settlement.fecha_liquidacion).format('DD-MM-YYYY')]]</p>
			</div>

			<div layout="column" md-whiteframe="2" class="md-dialog-content" flex>
				<md-toolbar class="md-menu-toolbar">
					<div class="md-toolbar-tools">
						<h2 flex md-truncate>Comerciales en la liquidacion</h2>

					</div>
				</md-toolbar>
				<md-content class="mdc-bg-white">
					<table datatable="ng" dt-instance="settlementCommercialsTable.settlementCommercials.dtInstanceCallback" dt-options="settlementCommercialsTable.settlementCommercials.dtOptions"
					       dt-disable-deep-watchers="true"
					       class="row-border hover full-width small">
						<thead>
						<th>#</th>
						<th>Comercial</th>
						<th>Importe directo</th>
						<th>Importe referido</th>
						<th>Numero de ventas</th>
						<th>Numero de artículos</th>
						<th></th>

						</tr>
						</thead>
						<tbody>
						<tr ng-repeat="fCommercial in settlement.commercial_settlements" class="clickable">
							<td>
								[[fCommercial.id_comercial]]
							</td>
							<td>
								[[fCommercial.commercial.nombre_completo]]
							</td>
							<td>
								[[fCommercial.importe_inm.toFixed(2)]]
							</td>
							<td>
								[[fCommercial.importe_car.toFixed(2)]]
							</td>
							<td>
								[[fCommercial.numero_ventas]]
							</td>
							<td>
								[[fCommercial.numero_articulos]]
							</td>
							<td>
								<div layout-align="space-around center" layout="row">
									<a style="color:lightgreen" ng-click="selectCommercialSettlement(fCommercial.id)">Seleccionar</a>
								</div>
							</td>


						</tr>
						</tbody>
					</table>
				</md-content>
			</div>


			<div layout="column" md-whiteframe="2" class="md-dialog-content" flex>
				<md-toolbar class="md-menu-toolbar" ng-hide="!selectedCommercialSettlement">
					<div class="md-toolbar-tools">
						<h2 flex md-truncate>Conceptos de liquidacion de [[selectedCommercialName]]</h2>


						<md-button class="md-accent" ng-click="commercialPdf()" ng-disabled="!selectedCommercialSettlement">
							<md-icon md-font-icon="mdi mdi-file-pdf"></md-icon>
							Obtener PDF
						</md-button>
					</div>

				</md-toolbar>
				<md-content class="mdc-bg-white" ng-hide="!selectedCommercialSettlement">
					<table datatable="ng" dt-instance="settlementCommercialConceptsTable.settlementCommercialConcepts.dtInstanceCallback"
					       dt-options="settlementCommercialConceptsTable.settlementCommercialConcepts.dtOptions"
					       dt-disable-deep-watchers="true"

					       class="row-border hover full-width small">
						<thead>
						<th>#</th>
						<th>Concepto</th>
						<th>Id. Fuente asociada</th>
						<th>Linea de fuente asociada</th>
						<th>Importe</th>


						</tr>
						</thead>
						<tbody>
						<tr ng-repeat="sConcept in settlement.commercial_settlements[commercialSettlementIndex].concepts" class="clickable">

							<td>

								[[sConcept.id]]
							</td>
							<td>
								[[sConcept.concepto ==1 ? 'Comisión directa' :
								sConcept.concepto == 2 ? 'Descuento pago en metálico' :
								sConcept.concepto ==3 ? 'Comisión referenciada' :
								sConcept.concepto ==4 ? 'Otros' :
								sConcept.concepto == 5 ? 'Devolucion coste material' :
								sConcept.concepto == 6 ? 'Cobro de entrega en mano' :
								sConcept.concepto == 7 ? 'Cobro de albaran' :
								sConcept.concepto == 8 ? 'Cobro de alquiler' :
                                sConcept.concepto == 9 ? 'Comisión directa producto' :
                                sConcept.concepto == 10 ? 'Pago referenciado de producto' :
                                sConcept.concepto == 11 ? 'Pago por contrato posteado' :
                                sConcept.concepto == 12 ? 'Pago por contrato posteado ref.' :
                                sConcept.concepto == 13 ? 'Pago por produccion (%posteo)' :
                                sConcept.concepto == 14 ? 'Pago por productividad (zona)' :
								'No definido' ]]

							</td>
							<td>
								[[ sConcept.sale ? 'Venta ' + sConcept.sale.id_venta + '(' + sConcept.sale.fecha + ')' :
								sConcept.delivery_line ? 'Entrega' + sConcept.delivery_line.delivery.id_entrega + '(' + sConcept.delivery_line.delivery.fecha + ')' :
								sConcept.delivery_note_line ? 'Albaran' + sConcept.delivery_note_line.id_albaran + '(' + sConcept.delivery_note_line.delivery_note.fecha + ')' :
								sConcept.contract ? 'Contrato ' + sConcept.contract.id_contrato + '(' + sConcept.contract.fecha_firma + ')' : '']]
							</td>
							<td>

								[[ sConcept.sale ? sConcept.line.id_linea :
								sConcept.delivery_line ? sConcept.delivery_line.lin_entrega :
								sConcept.delivery_note_line ? sConcept.delivery_note_line.lin_albaran :
								sConcept.contract ? sConcept.contract.id_producto : '']]

							</td>
							<td>
								[[sConcept.importe]]
							</td>


						</tr>
						</tbody>
					</table>
				</md-content>
			</div>


			<div layout="row" layout-align="center center" class="md-dialog-content">
				<div>


					<md-button class="md-primary" ng-click="cancel()">
						Cerrar
					</md-button>

				</div>
			</div>
		</form>
	</md-dialog-content>

</md-dialog>

