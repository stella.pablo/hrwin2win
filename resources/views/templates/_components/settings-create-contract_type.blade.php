<md-dialog>

	<md-toolbar class="md-menu-toolbar">
		<div class="md-toolbar-tools">

			<h2>[[ action == 'save' ? 'Añadir tipo de contratacion' : 'Editar tipo de contratacion' ]]</h2>

			<span flex></span>

			<md-button class="md-icon-button" ng-click="cancel()">
				<md-icon md-font-icon="mdi mdi-close" aria-label="Close dialog"></md-icon>
			</md-button>
		</div>
	</md-toolbar>

	<md-dialog-content>
		<form name="contractTypeForm">
			<div layout="row" layout-align="center center" class="md-dialog-content">
				<md-input-container flex>
					{{--<md-icon md-font-icon="mdi file-document-outline"></md-icon>--}}
					<input ng-model="contractType.tipo_contrato_comercial" placeholder="Tipo de Contrato">
				</md-input-container>
			</div>

			<div layout="row" layout-align="center center" class="md-dialog-content">
				<md-input-container flex>
					<label>Tipo de iva</label>
					<md-select ng-model="contractType.id_iva" ng-required>
						<md-option ng-repeat="iva in ivas" ng-value="iva.id_iva"> [[iva.descripcion]]</md-option>
					</md-select>
				</md-input-container>
			</div>
			<div layout="row" layout-align="center center" class="md-dialog-content">
				<md-input-container flex>
					<label>Tipo de irpf</label>
					<md-select ng-model="contractType.id_irpf" ng-required>
						<md-option ng-repeat="irpf in irpfs" ng-value="irpf.id_irpf"> [[irpf.descripcion]]</md-option>
					</md-select>
				</md-input-container>
			</div>


			<div layout="column" md-whiteframe="2" class="md-dialog-content" flex>
				<md-toolbar class="md-menu-toolbar">
					<div class="md-toolbar-tools">
						<h2 flex md-truncate>Tramos del contrato</h2>

						<md-button class="md-accent" ng-click="addStretch(null,null)">
							<md-icon md-font-icon="mdi mdi-plus"></md-icon>
							Añadir tramo
						</md-button>
					</div>
				</md-toolbar>

				<md-content class="mdc-bg-white">
					<table datatable="ng" dt-instance="stretchTable.stretches.dtInstanceCallback" dt-options="stretchTable.stretches.dtOptions" dt-disable-deep-watchers="true"
					       class="row-border hover full-width small">

						<thead>
						<th>#</th>
						<th>Descripcion</th>
						<th>Sueldo base</th>
						<th>Horas</th>
						{{--						<th>Minimo importe ventas</th>
												<th>Minimo plus</th>
												<th>Plus</th>--}}
						<th></th>
						</tr>
						</thead>
						<tbody>
						<tr ng-repeat="stretch in contractType.stretches" class="clickable">
							<td>
								[[stretch.id_tramo]]
							</td>
							<td>
								<md-input-container flex>
									<input ng-model="stretch.descripcion" required>
								</md-input-container>
							</td>
							<td>
								<md-input-container flex>
									<input type="number" ng-model="stretch.sueldo_base" required>
								</md-input-container>
							</td>
							<td>
								<md-input-container flex>
									<input type="number" ng-model="stretch.horas" required>
								</md-input-container>
							</td>
							{{--						<td>
														<md-input-container flex>
															<input type="number" ng-model="stretch.minimo_importe_ventas">
														</md-input-container>
													</td>
													<td>
														<md-input-container flex>
															<input type="number" ng-model="stretch.minimo_plus">
														</md-input-container>
													</td>

													<td>
														<md-input-container flex>
															<input type="number" ng-model="stretch.minimo_plus">
														</md-input-container>
													</td>		--}}
							<td>
								<a  ng-show="!removeQ[$index]" style="color:lightcoral" ng-click="removeQ[$index]=true"> Eliminar </a>

								<div layout-align="center" flex layout="column" ng-show="removeQ[$index]" >¿Eliminar? </div>
								<div layout-align="space-around center"  layout="row"> <a ng-show="removeQ[$index]" style="color:lightcoral" ng-click="removeStretch(stretch.id_tramo,$index)">
										Si</a></div>
								<div layout-align="space-around center"  layout="row"><a  ng-show="removeQ[$index]" style="color:lightblue"  ng-click="removeQ[$index]=false">No</a></div>
								{{--<a ng-show="!removeQ[$index]" style="color:lightcoral" ng-click="removeStretch(stretch.id_tramo,$index)"> Eliminar </a>--}}
							</td>
						</tr>
						</tbody>
					</table>
				</md-content>

			</div>
			<div layout="row" layout-align="center center" class="md-dialog-content">
				<div>

					<md-button class="md-primary" ng-click="submit()" ng-disabled="contractTypeForm.$invalid || contractTypeForm.$pristine">
						[[ action == 'save' ? 'CREAR' : 'EDITAR' ]]
					</md-button>
					<md-button class="md-primary" ng-click="cancel()">
						Cancelar
					</md-button>

				</div>
			</div>
		</form>
	</md-dialog-content>

</md-dialog>

