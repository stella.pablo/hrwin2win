<md-dialog>

	<md-toolbar class="md-menu-toolbar">
		<div class="md-toolbar-tools">

			<h2>[[ action == 'save' ? 'Añadir producto' : 'Editar producto' ]]</h2>

			<span flex></span>

			<md-button class="md-icon-button" ng-click="cancel()">
				<md-icon md-font-icon="mdi mdi-close" aria-label="Close dialog"></md-icon>
			</md-button>
		</div>
	</md-toolbar>

	<md-dialog-content>
		<form name="productForm">
			<div layout="row" layout-xs="column" layout-align="center center" class="md-dialog-content">
				<div flex layout-margin="10">
					<md-input-container flex>
						{{--<md-icon md-font-icon="mdi file-document-outline"></md-icon>--}}
						<input ng-model="product.producto" placeholder="Descripcion" required>
					</md-input-container>
				</div>

				<div flex layout-margin="10">

					<md-input-container flex>
						{{--<md-icon md-font-icon="mdi file-document-outline"></md-icon>--}}
						<input type="number" ng-model="product.maximo_lineas" required placeholder="Máximo de líneas">
					</md-input-container>
				</div>
			</div>

			<div layout="row" layout-xs="column" layout-align="center center" class="md-dialog-content">
				<div flex layout-margin="10">
					<label>Tipo de contrato</label>
					<md-select ng-model="product.tipo_contrato" required>
						<md-option ng-value="">---------</md-option>
						<md-option ng-value="'H'">Particular</md-option>
						<md-option ng-value="'N'">Comercial</md-option>
						<md-option ng-value="'A'">Ambos</md-option>
					</md-select>
				</div>

				<div flex layout-margin="10">
					<label>Tipo de producto</label>
					<md-select ng-model="product.id_tipo_producto" required>
						<md-option ng-value="">---------</md-option>
						<md-option ng-value="1">Línea</md-option>
						<md-option ng-value="2">Paquete</md-option>
						<md-option ng-value="3">Entretenimiento</md-option>
						<md-option ng-value="4">Gastos de instalación</md-option>
					</md-select>
				</div>
				<div flex layout-margin="10">
					<label>Activo</label>
					<md-select ng-model="product.activo" required>
						<md-option ng-value="">---------</md-option>
						<md-option ng-value="'S'">Si</md-option>
						<md-option ng-value="'N'">No</md-option>
					</md-select>
				</div>

			</div>


			<div layout="column" md-whiteframe="2" class="md-dialog-content" flex>
				<md-toolbar class="md-menu-toolbar">
					<div class="md-toolbar-tools">
						<h2 flex md-truncate>Tramos del producto</h2>

						<md-button class="md-accent" ng-click="addStretch(null,null)">
							<md-icon md-font-icon="mdi mdi-plus"></md-icon>
							Añadir tramo
						</md-button>
					</div>
				</md-toolbar>

				<md-content class="mdc-bg-white">
					<table datatable="ng" dt-instance="ProductStretchTable.stretches.dtInstanceCallback" dt-options="ProductStretchTable.stretches.dtOptions" dt-disable-deep-watchers="true"
					       class="row-border hover full-width small">

						<thead>
						<th>#</th>
						<th>Descripcion</th>
						<th>Tipo de tramo</th>
						<th>Precio</th>

						<th></th>
						</tr>
						</thead>
						<tbody>
						<tr ng-repeat="stretch in product.stretches" class="clickable">
							<td>
								[[stretch.id]]
							</td>
							<td>
								<md-input-container flex>
									<input ng-model="stretch.descripcion" required>
								</md-input-container>
							</td>
							<td>
								<md-select ng-model="stretch.tipo_tramo" required flex>
									<md-option ng-value="">---------</md-option>
									<md-option ng-value="'C'">Cantidad</md-option>
									<md-option ng-value="'I'">Importe</md-option>
								</md-select>
							</td>
							<td>
								<md-input-container flex>
									<input type="number" ng-model="stretch.precio">
								</md-input-container>
							</td>
							<td>
								<a ng-show="!removeProductQ[$index]" style="color:lightcoral" ng-click="removeProductQ[$index]=true"> Eliminar </a>

								<div layout-align="center" flex layout="column" ng-show="removeProductQ[$index]">¿Eliminar?</div>
								<div layout-align="space-around center" layout="row"><a ng-show="removeProductQ[$index]" style="color:lightcoral" ng-click="removeStretch(stretch.id,$index)">
										Si</a></div>
								<div layout-align="space-around center" layout="row"><a ng-show="removeProductQ[$index]" style="color:lightblue" ng-click="removeProductQ[$index]=false">No</a></div>
								{{--<a ng-show="!removeQ[$index]" style="color:lightcoral" ng-click="removeStretch(stretch.id_tramo,$index)"> Eliminar </a>--}}
							</td>
						</tr>
						</tbody>
					</table>
				</md-content>

			</div>
			<div layout="row" layout-align="center center" class="md-dialog-content">
				<div>

					<md-button class="md-primary" ng-click="submit()" ng-disabled="productForm.$invalid || productForm.$pristine">
						[[ action == 'save' ? 'CREAR' : 'EDITAR' ]]
					</md-button>
					<md-button class="md-primary" ng-click="cancel()">
						Cancelar
					</md-button>

				</div>
			</div>
		</form>
	</md-dialog-content>

</md-dialog>

