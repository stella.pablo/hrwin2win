<div layout="column" md-whiteframe="2" class="mdc-bg-white" flex>
	<md-toolbar class="md-menu-toolbar">
		<div class="md-toolbar-tools">
			<h2 flex md-truncate>Candidatos</h2>

			<div layout="row" layout-align="center center" style="font-size: 16px;">
				{{--					<md-input-container class="no-input-errors margin-top-0 margin-bottom-0 margin-right-20" ng-show="$ctrl.managements.length > 1">--}}
				{{--						<md-icon md-font-icon="mdi mdi-account-multiple"></md-icon>--}}
				{{--						<label>Gerencia</label>--}}
				{{--						<md-select ng-model="$ctrl.management_id" ng-change="$ctrl.loadData()" aria-label="Select management">--}}
				{{--							<md-option ng-value="0">Todas</md-option>--}}
				{{--							<md-option ng-value="management.id" ng-repeat="management in $ctrl.managements">[[ management.external.gerencia ]]</md-option>--}}
				{{--						</md-select>--}}
				{{--					</md-input-container>--}}

				<md-radio-group layout="row" layout-align="center center" ng-model="$ctrl.chartView" ng-change="$ctrl.populate()">
					<md-radio-button value="month">Este mes</md-radio-button>
					<md-radio-button value="year">Este año</md-radio-button>
				</md-radio-group>
			</div>
		</div>
	</md-toolbar>

	<md-content class="mdc-bg-white">
		<highchart id="chart-candidates" config="$ctrl.chart"></highchart>
	</md-content>
</div>
