<md-dialog>

	<md-toolbar class="md-menu-toolbar">
		<div class="md-toolbar-tools">

			<h2>[[ action == 'save' ? 'Crear direccion' : 'Editar direccion' ]]</h2>

			<span flex></span>

			<md-button class="md-icon-button" ng-click="cancel()">
				<md-icon md-font-icon="mdi mdi-close" aria-label="Close dialog"></md-icon>
			</md-button>
		</div>
	</md-toolbar>

	<md-dialog-content>
		<form name="addressForm">
			<div layout="row">
				<div layout="column" layout-align="center center" class="md-dialog-content">
					<md-input-container flex>
						<label>Tipo de direccion</label>
						<md-select ng-model="address.id_tipo_direccion" required>
							<md-option ng-repeat="addressType in addressTypes" ng-value="addressType.id_tipo_direccion">[[addressType.tipo_direccion]]</md-option>
						</md-select>
					</md-input-container>
				</div>
				<div layout="column" layout-align="center center" class="md-dialog-content">
					<md-input-container flex>
						<input type="text" placeholder="Descripcion" ng-model="address.nombre_direccion" required>
					</md-input-container>
				</div>

			</div>
			<div layout="row">
				<div layout="column" layout-align="center center" class="md-dialog-content">
					<md-input-container flex>
						<input type="text" placeholder="Contacto" ng-model="address.contacto_direccion" required>
					</md-input-container>
				</div>


				<div layout="column" layout-align="center center" class="md-dialog-content">
					<label>Provincia</label>
					<md-select ng-model="address.id_provincia" required>
						<md-option ng-repeat="province in provinces" ng-value="province.id_provincia"> [[province.provincia]]</md-option>
					</md-select>
				</div>
			</div>
			<div layout="row">
				<div layout="column" layout-align="center center" class="md-dialog-content">
					<md-input-container flex>
						<input type="text" placeholder="Poblacion" ng-model="address.poblacion" required>
					</md-input-container>
				</div>

				<div layout="column" layout-align="center center" class="md-dialog-content">
					<md-input-container flex>
						<input type="text" placeholder="Codigo postal" ng-model="address.codigo_postal" required>
					</md-input-container>
				</div>
			</div>
			<div layout="row" layout-align="center center" class="md-dialog-content">

					<md-input-container flex>
						<input type="text" placeholder="Direccion" ng-model="address.direccion" required>
					</md-input-container>

			</div>
			<div layout="row" layout-align="center center" class="md-dialog-content">
				<div>

					<md-button class="md-primary" ng-click="submit()" ng-disabled="addressForm.$invalid || addressForm.$pristine">
						[[ action == 'save' ? 'CREAR' : 'EDITAR' ]]
					</md-button>
					<md-button class="md-primary" ng-click="cancel()">
						Cancelar
					</md-button>

				</div>
			</div>
		</form>
	</md-dialog-content>

</md-dialog>
