<md-dialog>
	<md-toolbar class="md-menu-toolbar">
		<div class="md-toolbar-tools">
			<h2>Repetir telefono movil</h2>

			<span flex></span>

			<md-button class="md-icon-button" ng-click="cancel()">
				<md-icon md-font-icon="mdi mdi-close" aria-label="Close dialog"></md-icon>
			</md-button>
		</div>
	</md-toolbar>

	<md-dialog-content>
		<div layout="row" layout-align="center center" class="md-dialog-content">
			<md-input-container flex>
				<span>¿Desea repetir el teléfono [[candidate.phone]] ?

				</span>
			</md-input-container>
		</div>
		<div layout="row" layout-align="center center" class="md-dialog-content">
			<div>
				<md-button class="md-primary" ng-click="submit()">
					Repetir
				</md-button>
				<md-button class="md-primary" ng-click="cancel()">
					Cancelar
				</md-button>

			</div>
		</div>
	</md-dialog-content>
</md-dialog>
