<!doctype html>
<html lang="es">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">


	<title>{{ strtoupper($data->company->empresa ) }} | Liquidacion #{{ $data->id }} de {{ $data->pdfManagement->gerencia }} a fecha {{ $data->fecha_liquidacion }}</title>

</head>
<style>
    .text {
        font-size: 12px;
        font-family: Roboto, "Helvetica Neue", sans-serif;

    }

    .textbold {
        font-size: 12px;
        font-weight: bold;
        text-align: center;
        font-family: Roboto, "Helvetica Neue", sans-serif;

    }


    .thead {
        text-align: center;
        font-size: 12px;
        font-family: Roboto, "Helvetica Neue", sans-serif;

    }

	.thead2 {
		font-size: 10px;
		font-family: Roboto, "Helvetica Neue", sans-serif;
	}
    .theadB {
        font-weight: bold;
        font-size: 18px;
        text-align: center;
        font-family: Roboto, "Helvetica Neue", sans-serif;

    }

    table {
        width: 80%;
        margin-top: 15px;
    }

</style>
<body>






<div class="container">
    <div class="row">

        <table align="center">
            <tbody>


            <tr>
                <td width="25%" class="textbold">

                    <img src="{{base_path().'/public/images/'.$data->company->imagen_logo}}" width="150px" >
                    <br>
                    {{ strtoupper($data->company->empresa ) }}
                    {{--	<img src="{{'/images/'.$data->company->imagen_logo}}" style="max-width: 100%">--}}
                </td>
                <td width="50%" align="center">

                    <table>
                        <tr class="theadB">
                            <td colspan="5"> Liquidación #{{$data->id}} ({{ date('d-m-Y',strtotime($data->fecha_liquidacion))}}) </td>
                        </tr>
                        <tr>
                            <td colspan="5" style="line-height: 20px"><br></td>
                        </tr>
                        <tr>
                            <td class="thead" colspan="5">CIF: {{ $data->company->nif }} </td>
                        </tr>
                        <tr>
                            <td colspan="5" class="thead">{{ $data->company->direccion1 }}</td>
                        </tr>
                        <tr>
                            <td colspan="5" class="thead">{{$data->company->direccion2}}</td>
                        </tr>
                        <tr>
                            <td colspan="5" class="thead">{{$data->company->cp}}  {{ $data->company->ciudad }}</td>
                        </tr>
                        <tr>
                            <td colspan="5" class="thead">TEL: {{$data->company->telefono1}}
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5" class="thead">@if (session('database')!='mexico') FAX: {{$data->company->fax}} @endif</td>
                        </tr>

                    </table>

                </td>
                <td width="25%">

                    <table>
                        <tr>
                            <td class="thead2">Liquidacion</td>
                            <td class="thead2">#{{ $data->id }}</td>
                        </tr>
                        <tr>
                            <td class="thead2">
                                Intervalo desde:
                            </td>
                            <td class="thead2">
                                {{ date('d-m-Y',strtotime($data->fecha_desde))}}
                            </td>
                        </tr>
                        <tr>
                            <td class="thead2">
                                Intervalo hasta:
                            </td>
                            <td class="thead2">
                                {{ date('d-m-Y',strtotime($data->fecha_hasta))}}
                            </td>
                        </tr>


                        <tr>
                            <td class="thead2">
                                Fecha liquidacion
                            </td>
                            <td class="thead2"> {{ date('d-m-Y',strtotime($data->fecha_liquidacion))}}</td>
                        </tr>
                        <tr>
                            <td class="thead2">
                                Tipo de contratación
                            </td>
                            <td class="thead2"> {{ $data->contractType->tipo_contrato_comercial }}</td>
                        </tr>
                    </table>


                </td>
            </tr>


            </tbody>
        </table>
        <table layout-margin="top" align="center">


            <tr>
                <td align="left" class="textbold">Comercial</td>
                <td align="center" class="textbold">Nº ventas asociadas</td>
                <td align="center" class="textbold">Nº productos asociados</td>
                <td align="center" class="textbold">Nº articulos vendidos directamente</td>
                <td align="center" class="textbold">Nº productos vendidos directamente</td>
                <td align="center" class="textbold">Importe comercial ({{$data->company->divisa_base}})</td>
            </tr>
            <tr>
                <td colspan="6">
                    <br>
                </td>
            </tr>
            @foreach($data->commercialSettlements as $commercialSettlement)
                <tr>
                    <td align="left" class="text">
                        {{$commercialSettlement->commercial->nombre_completo}}
                    </td>
                    <td align="center" class="text">
                        {{$commercialSettlement->distinct_sales}}
                    </td>
                    <td align="center" class="text">
                        {{$commercialSettlement->distinct_products}}
                    </td>
                    <td align="center" class="text">
                        {{$commercialSettlement->total_articles}}
                    </td>
                    <td align="center" class="text">
                        {{$commercialSettlement->total_products}}
                    </td>
                    <td align="center" class="text">
                        {{ number_format((float)$commercialSettlement->total, 2, '.', '')}}
                    </td>
                </tr>
            @endforeach

            <tr>
                <td colspan="6"><br></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td align="center" class="textbold"> TOTAL</td>
                <td align="center" class="textbold">{{number_format((float)$data->total, 2, '.', '')}} </td>
            </tr>


        </table>


    </div>


</div>










{{--
<div class="container">

	<div class="row">

		<div>


			<table width="80%" align="center">
				<tbody>
				<tr class="theadB">
					<td colspan="4"> {{ strtoupper($data->company->empresa ) }}</td>
				</tr>
				<tr>
					<td colspan="4" style="line-height: 20px"><br></td>
				</tr>
				<tr>
					<td class="thead" colspan="4">CIF: {{ $data->company->nif }} </td>
				</tr>
				<tr>
					<td colspan="4" class="thead">{{ $data->company->nombre_abreviado }}</td>
				</tr>
				<tr>
					<td colspan="4" class="thead">{{ $data->company->direccion1 }}</td>
				</tr>
				<tr>
					<td colspan="4" class="thead">{{$data->company->direccion2}}</td>
				</tr>
				<tr>
					<td colspan="4" class="thead">{{$data->company->cp}}  {{ $data->company->ciudad }}</td>
				</tr>
				<tr>
					<td colspan="4" class="thead">TEL: {{$data->company->telefono1}}  @if (session('database')!='mexico') FAX: {{$data->company->fax}} @endif</td>
				</tr>
				<tr>
					<td colspan="4">
						<hr style="border:1px dotted #000000;"/>
					</td>
				</tr>

				<tr>
					<td colspan="4" class="thead">{{ strtoupper($data->company->empresa ) }} | Liquidacion #{{ $data->id }} de {{ $data->pdfManagement->gerencia }} a fecha {{ date('d-m-Y',strtotime($data->fecha_liquidacion))
					}}</td>
				</tr>

				<tr>
					<td align="left" class="text">Comercial</td>
					<td align="center" class="text">Nº ventas asociadas</td>
					<td align="center" class="text">Nº articulos vendidos directamente</td>

					<td align="right" class="text">Importe comercial ({{$data->company->divisa_base}})</td>
				</tr>

				@foreach($data->commercialSettlements as $commercialSettlement)
					<tr class="article">
						<td align="left" class="text">
							{{$commercialSettlement->commercial->nombre_completo}}
						</td>
						<td align="center" class="text">
							{{$commercialSettlement->distinct_sales}}
						</td>
						<td align="center" class="text">
							{{$commercialSettlement->total_articles}}
						</td>
						<td align="right" class="text">{{ number_format((float)$commercialSettlement->total, 2, '.', '')}} </td>
					</tr>
				@endforeach
				<tr>
					<td colspan="4">
						<hr style="border:1px dotted #000000;"/>
					</td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td align="center" class="text"> TOTAL</td>
					<td align="right" class="text">{{number_format((float)$data->pdfTotal, 2, '.', '')}} </td>
				</tr>
				<tr>
					<td colspan="4" class="text">
						<hr style="border:1px dotted #000000;"/>
					</td>
				</tr>

				</tbody>
			</table>
			<hr>
		</div>
	</div>
</div>--}}
</body>
</html>
