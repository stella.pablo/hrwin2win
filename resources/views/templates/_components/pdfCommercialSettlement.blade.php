<!doctype html>
<html lang="es">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">


	<title>Imperat | Liquidacion #{{ $data->id }} de {{ $data->commercial->nombre_completo }} a fecha {{ $data->fecha_liquidacion }}</title>

</head>
<style>
	.text {
		font-size: 12px;
		font-family: Roboto, "Helvetica Neue", sans-serif;

	}

	.textbold {
		font-size: 12px;
		font-weight: bold;
		text-align: center;
		font-family: Roboto, "Helvetica Neue", sans-serif;

	}


	.thead {
		text-align: center;
		font-size: 12px;
		font-family: Roboto, "Helvetica Neue", sans-serif;

	}

	.thead2 {
		font-size: 10px;
		font-family: Roboto, "Helvetica Neue", sans-serif;
	}

	.theadB {
		font-weight: bold;
		font-size: 18px;
		text-align: center;
		font-family: Roboto, "Helvetica Neue", sans-serif;

	}

	table {
		width: 80%;
		margin-top: 15px;
	}

</style>
<body>


<div class="container">
	<div class="row">

		<table align="center">
			<tbody>


			<tr>
				<td width="25%" class="textbold">

					<img src="{{base_path().'/public/images/'.$data->settlement->company->imagen_logo}}" width="150px">
					<br>
					{{ strtoupper($data->settlement->company->empresa ) }}
					{{--	<img src="{{'/images/'.$data->company->imagen_logo}}" style="max-width: 100%">--}}
				</td>
				<td width="50%" align="center">

					<table>
						<tr class="theadB">
							<td colspan="5"> Liquidación #{{$data->settlement->id}} ({{ date('d-m-Y',strtotime($data->settlement->fecha_liquidacion))}})</td>
						</tr>
						<tr>
							<td colspan="5" style="line-height: 20px"><br></td>
						</tr>
						<tr>
							<td class="thead" colspan="5">CIF: {{ $data->settlement->company->nif }} </td>
						</tr>
						<tr>
							<td colspan="5" class="thead">{{ $data->settlement->company->direccion1 }}</td>
						</tr>
						<tr>
							<td colspan="5" class="thead">{{$data->settlement->company->direccion2}}</td>
						</tr>
						<tr>
							<td colspan="5" class="thead">{{$data->settlement->company->cp}}  {{ $data->settlement->company->ciudad }}</td>
						</tr>
						<tr>
							<td colspan="5" class="thead">TEL: {{$data->settlement->company->telefono1}}
							</td>
						</tr>
						<tr>
							<td colspan="5" class="thead">@if (session('database')!='mexico') FAX: {{$data->settlement->company->fax}} @endif</td>
						</tr>

					</table>

				</td>
				<td width="25%">


					<table>
						<tr>
							<td class="thead2">Liquidacion</td>
							<td class="thead2">#{{ $data->settlement->id }}</td>
						</tr>
						<tr>
							<td class="thead2">
								Intervalo desde:
							</td>
							<td class="thead2">
								{{ date('d-m-Y',strtotime($data->settlement->fecha_desde))}}
							</td>
						</tr>
						<tr>
							<td class="thead2">
								Intervalo hasta:
							</td>
							<td class="thead2">
								{{ date('d-m-Y',strtotime($data->settlement->fecha_hasta))}}
							</td>
						</tr>


						<tr>
							<td class="thead2">
								Fecha liquidacion
							</td>
							<td class="thead2"> {{ date('d-m-Y',strtotime($data->settlement->fecha_liquidacion))}}</td>
						</tr>
						<tr>
							<td class="thead2">
								Tipo de contratación
							</td>
							<td class="thead2"> {{ $data->settlement->contractType->tipo_contrato_comercial }}</td>
						</tr>
					</table>


				</td>
			</tr>


			</tbody>
		</table>


		<table layout-margin="top" align="center">


			<tr>
				<td align="left" class="text">Concepto</td>
				<td align="center" class="text">Fuente</td>
				<td align="center" class="text">Importe ({{$data->settlement->company->divisa_base}})</td>
			</tr>
			@foreach($data->concepts as $concept)
				<tr class="article">
					<td align="left" class="text">
						@if($concept->concepto==1)
							Comisión directa
						@elseif($concept->concepto==2)
							Descuento pago metálico
						@elseif($concept->concepto==3)
							Comisión referenciada
						@elseif($concept->concepto == 4)
							Otros
						@elseif($concept->concepto==5)
							Devolución coste material
						@elseif($concept->concepto==6)
							Cobro de entrega en mano
						@elseif($concept->concepto==7)
							Cobro de albarán
						@elseif($concept->concepto==8)
							Cobro de alquiler
						@elseif($concept->concepto==9)
							Pago directo de producto
						@elseif($concept->concepto==10)
							Pago referenciado de producto
						@elseif($concept->concepto==11)
							Pago por contrato posteado
						@elseif($concept->concepto==12)
							Pago por contrato posteado ref.
						@elseif($concept->concepto==13)
							Pago por produccion (%posteo)
						@elseif($concept->concepto==14)
							Pago por productividad (zona)
						@else
							No definido
						@endif
					</td>


					<td align="center" class="text">

						@if(isset($concept->sale->id_venta) and ($concept->sale->id_venta != null))

							{{'Venta ' . $concept->sale->id_venta . '(' . $concept->sale->fecha . ')'}}
						@elseif(isset($concept->deliveryLine->lin_entrega) and ($concept->deliveryLine->id_entrega != null))
							{{'Entrega ' . $concept->deliveryLine->delivery->id_entrega . '(' . $concept->deliveryLine->delivery->fecha . ')' }}
						@elseif(isset($concept->deliveryNoteLine->lin_albaran) and ($concept->deliveryNoteLine->deliveryNote->id_albaran != null))
							{{'Albaran ' . $concept->deliveryNoteLine->id_albaran . '(' . $concept->deliveryNoteLine->deliveryNote->fecha . ')'}}
						@elseif(isset($concept->product) and ($concept->product->contract->id_contrato != null))
							{{'Contrato ' . $concept->product->id_contrato . ' '. $concept->product->product->producto . '(' . $concept->product->fecha_alta . ')'}}

						@else
							Otros

						@endif

					</td>

					<td align="center" class="text">{{$concept->importe}} </td>
				</tr>
			@endforeach


			<tr>
				<td></td>
				<td align="center" class="text"> TOTAL</td>
				<td align="center" class="textbold">{{number_format((float)$data->total, 2, '.', '')}} </td>
			</tr>


		</table>


	</div>


</div>
{{--
<div class="container">

	<div class="row">

		<div>


			<table width="80%" align="center">
				<tbody>
				<tr class="theadB">
					<td colspan="3"> {{ strtoupper($data->settlement->company->empresa ) }}</td>
				</tr>
				<tr>
					<td colspan="3" style="line-height: 20px"><br></td>
				</tr>
				<tr>
					<td class="thead" colspan="3">CIF: {{ $data->settlement->company->nif }} </td>
				</tr>
				<tr>
					<td colspan="3" class="thead">{{ $data->settlement->company->nombre_abreviado }}</td>
				</tr>
				<tr>
					<td colspan="3" class="thead">{{ $data->settlement->company->direccion1 }}</td>
				</tr>
				<tr>
					<td colspan="3" class="thead">{{$data->settlement->company->direccion2}}</td>
				</tr>
				<tr>
					<td colspan="3" class="thead">{{$data->settlement->company->cp}}{{ $data->settlement->company->ciudad }}</td>
				</tr>
				<tr>
					<td colspan="3" class="thead">TEL: {{$data->settlement->company->telefono1}} @if (session('database')!='mexico') FAX: {{$data->settlement->company->fax}} @endif</td>
				</tr>
				<tr>
					<td colspan="3">
						<hr style="border:1px dotted #000000;"/>
					</td>
				</tr>

				<tr>
					<td colspan="3" class="thead">{{ strtoupper($data->settlement->company->empresa ) }} Liquidacion #{{ $data->id }} de {{ $data->commercial->nombre_completo }} a fecha {{ date('d-m-Y',strtotime($data->settlement->fecha_liquidacion))}}</td>
				</tr>

				<tr>
					<td align="left" class="text">Concepto</td>


					<td align="center" class="text">Fuente</td>
					<td align="right" class="text">Importe ({{$data->settlement->company->divisa_base}})</td>
				</tr>

				@foreach($data->concepts as $concept)
					<tr class="article">
						<td align="left" class="text">
							@if($concept->concepto==1)
								Comisión directa
							@elseif($concept->concepto==2)
								Descuento pago metálico
							@elseif($concept->concepto==3)
								Comisión referenciada
							@elseif($concept->concepto == 4)
								Otros
							@elseif($concept->concepto==5)
								Devolución coste material
							@elseif($concept->concepto==6)
								Cobro de entrega en mano
							@elseif($concept->concepto==7)
								Cobro de albarán
                            @elseif($concept->concepto==8)
                                Cobro de alquiler
                            @elseif($concept->concepto==9)
                                Pago directo de producto
                            @elseif($concept->concepto==10)
                                Pago referenciado de producto
                            @elseif($concept->concepto==11)
                                Pago por contrato posteado
                            @elseif($concept->concepto==12)
                                Pago por contrato posteado ref.
                            @elseif($concept->concepto==13)
                                Pago por produccion (%posteo)
                            @elseif($concept->concepto==14)
                                Pago por productividad (zona)
                            @else
								No definido
							@endif
						</td>


						<td align="center" class="text">

							@if(isset($concept->sale->id_venta) and ($concept->sale->id_venta != null))

								{{'Venta' . $concept->sale->id_venta . '(' . $concept->sale->fecha . ')'}}
							@elseif(isset($concept->deliveryLine->lin_entrega) and ($concept->deliveryLine->id_entrega != null))
								{{'Entrega' . $concept->deliveryLine->delivery->id_entrega . '(' . $concept->deliveryLine->delivery->fecha . ')' }}
							@elseif(isset($concept->deliveryNoteLine->lin_albaran) and ($concept->deliveryNoteLine->deliveryNote->id_albaran != null))
								{{'Albaran' . $concept->deliveryNoteLine->id_albaran . '(' . $concept->deliveryNoteLine->deliveryNote->fecha . ')'}}
							@else
								Otros

							@endif

						</td>

						<td align="right" class="text">{{$concept->importe}} </td>
					</tr>
				@endforeach
				<tr>
					<td colspan="3">
						<hr style="border:1px dotted #000000;"/>
					</td>
				</tr>
				--}}{{--alñskdjf--}}{{--
--}}{{--
				<tr>
					<td></td>
					<td align="center" class="text"> TOTAL</td>
					<td align="right" class="text">{{$data->total}} </td>
				</tr>--}}{{--
				<tr>
					<td colspan="3" class="text">
						<hr style="border:1px dotted #000000;"/>
					</td>
				</tr> --}}{{--fdd--}}{{--
--}}{{--				<tr>
					<td align="left" class="text">@if(session('database')=='mexico')  IVA % @else  VAT %  @endif</td>
					<td align="center" class="text"> Base imponible</td>
					<td align="right" class="text"> Cuota</td>
				</tr>--}}{{--
				<tr>
					<td colspan="3">
						<hr style="border:1px dotted #000000;"/>
					</td>
				</tr>
--}}{{--
				<tr>
					<td></td>
					<td align="center" class="text">@if ($data->id_metodo_pago ===2) TARJETA @else EFECTIVO @endif </td>
					<td align="right" class="text">{{$data->totals}} </td>
				</tr>--}}{{--
				<tr>
					<td></td>
					<td align="center" class="text">TOTAL--}}{{-- SIN @if(session('database')=='mexico') IVA  @else VAT  @endif--}}{{--</td>
					<td align="right" class="text">{{ $data->total/* - $data->tax */}}</td>
				</tr>


				</tbody>
			</table>
			<hr>
		</div>
	</div>
</div>--}}
</body>
</html>
