<md-dialog>

	<md-toolbar class="md-menu-toolbar">
		<div class="md-toolbar-tools">

			<h2>[[ action == 'save' ? 'Añadir contrato' : 'Editar contrato'   ]]</h2>

			<span flex></span>

			<md-button class="md-icon-button" ng-click="cancel()">
				<md-icon md-font-icon="mdi mdi-close" aria-label="Close dialog"></md-icon>
			</md-button>
		</div>
	</md-toolbar>

	<md-dialog-content>
		<form name="contractForm">
			<div layout="row" layout-align="center center" class="md-dialog-content" ng-disabled="action!='save'">
				<md-input-container flex>
					<label>Tipo de Contrato</label>
					<md-select ng-model="contract.id_tipo_contrato_comercial" ng-change="switchStretches()" ng-required>
						<md-option ng-repeat="type in contractTypes" ng-value="type.id_tipo_contrato_comercial">[[type.tipo_contrato_comercial]]</md-option>
					</md-select>
				</md-input-container>
			</div>

			<div layout="row" layout-align="center center" class="md-dialog-content" ng-hide="!contract.id_tipo_contrato_comercial">
				<md-input-container flex>
					<label>Tramo del contrato</label>
					<md-select ng-model="contract.id_tramo"  ng-required>
						<md-option ng-repeat="stretch in stretches" ng-value="stretch.id_tramo"> [[stretch.descripcion]]</md-option>
					</md-select>
				</md-input-container>
			</div>
			<div layout="row" layout-align="center center" class="md-dialog-content">
				<md-input-container class="input-no-errors" flex-xs required>
					<md-datepicker md-placeholder="Fecha  de inicio" ng-model="contract.fecha_desde" required>
				</md-input-container>
			</div>
			<div layout="row" layout-align="center center" class="md-dialog-content">

				<md-input-container class="input-no-errors" flex-xs>
					<md-datepicker ng-model="contract.fecha_hasta" md-placeholder="Fecha fin"></md-datepicker>
				</md-input-container>

			</div>
			<div layout="row" layout-align="center center" class="md-dialog-content">
				<div>

					<md-button class="md-primary" ng-click="submit()" ng-disabled="structureform.$invalid || structureform.$pristine">
						[[ action == 'save' ? 'CREAR' : 'EDITAR'    ]]
					</md-button>
					<md-button class="md-primary" ng-click="cancel()">
						Cancelar
					</md-button>

				</div>
			</div>
		</form>
	</md-dialog-content>

</md-dialog>

