<md-sidenav class="md-sidenav-left" md-component-id="left" md-is-locked-open="$mdMedia('gt-sm') && isLockedOpen">
	<md-content layout="column" class="padding-10" flex>
		<div layout="column" layout-align="start start">
			<md-button ui-sref="dashboard" ui-sref-active="md-primary">
				<md-icon md-font-icon="mdi mdi-18px mdi-view-dashboard"></md-icon>
				Dashboard
			</md-button>

			<md-button ui-sref="candidates" ui-sref-active="md-primary">
				<md-icon md-font-icon="mdi mdi-18px mdi-account-convert"></md-icon>
				Candidatos
			</md-button>

			@if(auth()->user()->level < 5)
				<md-button ui-sref="commercials" ui-sref-active="md-primary">
					<md-icon md-font-icon="mdi mdi-18px mdi-account"></md-icon>
					Comerciales
				</md-button>
			@endif

			@if(auth()->user()->level <= 3)
				<md-button
					ui-sref="managements"
					ui-sref-active="md-primary"
					class=""
					ng-show="user.managements.length > 1"
				>
					<md-icon md-font-icon="mdi mdi-18px mdi-account-supervisor"></md-icon>
					Gerencias
				</md-button>

				<md-button
					ui-sref="managements.show({ id_gerencia: user.management.external_id })"
					ui-sref-active="md-primary"
					class=""
					ng-show="user.managements.length === 1"
				>
					<md-icon md-font-icon="mdi mdi-18px mdi-account-supervisor"></md-icon>
					Mi Gerencia
				</md-button>

				<md-button
					ui-sref="productivity"
					ui-sref-active="md-primary"
					class=""
					ng-show="user.managements.length > 1"
				>
					<md-icon md-font-icon="mdi mdi-18px mdi-chart-areaspline"></md-icon>
					Productividad
				</md-button>
			@endif

			@if(auth()->user()->level <= 3)
				<md-button ui-sref="productivity-low" ui-sref-active="md-primary" class="">
					<md-icon md-font-icon="mdi mdi-18px mdi-poll"></md-icon>
					Baja productividad
				</md-button>
			@endif

			@if(auth()->user()->level < 5)
				<md-button ui-sref="commercialsabsences" ui-sref-active="md-primary" class="">
					<md-icon md-font-icon="mdi mdi-18px mdi-timetable"></md-icon>
					Ausencias
				</md-button>
			@endif
		</div>
	</md-content>
</md-sidenav>
