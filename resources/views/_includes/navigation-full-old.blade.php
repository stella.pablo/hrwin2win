<md-sidenav class="md-sidenav-left" md-component-id="left" md-is-locked-open="sidenavLockedOpen">
	<md-toolbar class="mdc-text-white-all title" layout="row" layout-align="start center">
		<md-button class="md-icon-button" ng-click="toggle()" aria-label="open menu">
			<md-icon md-font-icon="mdi mdi-menu"></md-icon>
		</md-button>

		<h1 class="md-toolbar-tools">CRM v0.1</h1>
	</md-toolbar>

	<md-content class="margin-bottom-40 margin-top-40">
		<md-subheader class="md-no-sticky">Control del sistema</md-subheader>

		<md-list-item ui-sref="dashboard" ui-sref-active="active">
			<md-icon md-font-icon="mdi mdi-view-dashboard"></md-icon>
			<p>Dashboard</p>
		</md-list-item>

		<md-list-item ui-sref="activities" ui-sref-active="active">
			<md-icon md-font-icon="mdi mdi-calendar"></md-icon>
			<p>Actividades</p>
		</md-list-item>

		<md-list-item>
			<md-icon md-font-icon="mdi mdi-settings"></md-icon>
			<p>Opciones</p>
		</md-list-item>


		<md-subheader class="md-no-sticky">Control de stock</md-subheader>

		<md-list-item ui-sref="products.catalogue" ui-sref-active="active">
			<md-icon md-font-icon="mdi mdi-sitemap"></md-icon>
			<p>Catálogo</p>
		</md-list-item>

		<md-list-item ui-sref="products" ui-sref-active="active">
			<md-icon md-font-icon="mdi mdi-format-list-bulleted-type"></md-icon>
			<p>Productos</p>
		</md-list-item>

		<md-list-item ui-sref="sales" ui-sref-active="active">
			<md-icon md-font-icon="mdi mdi-sale"></md-icon>
			<p>Ventas</p>
		</md-list-item>


		<md-subheader class="md-no-sticky">Control de clientes</md-subheader>

		<md-list-item ui-sref="clients" ui-sref-active="active">
			<md-icon md-font-icon="mdi mdi-account"></md-icon>
			<p>Clientes</p>
		</md-list-item>

		<md-list-item ui-sref="contacts" ui-sref-active="active">
			<md-icon md-font-icon="mdi mdi-account-convert"></md-icon>
			<p>Contactos</p>
		</md-list-item>


		<md-subheader class="md-no-sticky">Análisis</md-subheader>

		<md-list-item ui-sref="analytics.products" ui-sref-active="active">
			<md-icon md-font-icon="mdi mdi-format-list-bulleted-type"></md-icon>
			<p>Productos</p>
		</md-list-item>

		<md-list-item ui-sref="analytics.sales" ui-sref-active="active">
			<md-icon md-font-icon="mdi mdi-signal"></md-icon>
			<p>Ventas</p>
		</md-list-item>
	</md-content>
</md-sidenav>
