<md-sidenav class="md-sidenav-right" md-component-id="notifications">
	<md-toolbar class="md-menu-toolbar">
		<div class="md-toolbar-tools">
			<h2>Notificaciones</h2>
		</div>
	</md-toolbar>

	<md-content layout-padding ng-controller="notificationsCtrl">
		<div layout="row" class="mdc-bg-teal mdc-text-white-all margin-bottom-10" md-whiteframe="2">
			<div layout="column" layout-align="center center" class="padding-right-10" style="border-right: 3px solid white;">
				<md-icon md-font-icon="mdi mdi-account-location mdi-48px"></md-icon>
				<span class="margin-top-10 text-center">Check In</span>
			</div>

			<div layout="column" layout-align="center start" class="padding-left-10">
				<h3 layout="row" layout-align="space-between center" class="margin-top-0 margin-bottom-10 full-width">
					<a ui-sref="clients.show({ id: 1 })">Carnicería Norte</a>
					<md-icon md-font-icon="mdi mdi-close" class="margin-0"></md-icon>
				</h3>
				<p class="margin-0">Francisco Pérez ha realizado un Check In a las 12:48 el 14-Dic-2017</p>
			</div>
		</div>

		<div layout="row" class="mdc-bg-teal mdc-text-white-all margin-bottom-10" md-whiteframe="2">
			<div layout="column" layout-align="center center" class="padding-right-10" style="border-right: 3px solid white;">
				<md-icon md-font-icon="mdi mdi-crosshairs-gps mdi-48px"></md-icon>
				<span class="margin-top-10 text-center">Visita</span>
			</div>

			<div layout="column" layout-align="center start" class="padding-left-10">
				<h3 layout="row" layout-align="space-between center" class="margin-top-0 margin-bottom-10 full-width">
					<a ui-sref="clients.show({ id: 1 })">Carnicería Norte</a>
					<md-icon md-font-icon="mdi mdi-close" class="margin-0"></md-icon>
				</h3>
				<p class="margin-0">Francisco Pérez ha realizado una Visita a las 15:13 el 12-Dic-2017</p>
			</div>
		</div>

		<div layout="row" class="mdc-bg-grey-100 margin-bottom-10" md-whiteframe="2">
			<div layout="column" layout-align="center center" class="padding-right-10" style="border-right: 3px solid #03a9f4;">
				<md-icon md-font-icon="mdi mdi-cart mdi-48px"></md-icon>
				<span class="margin-top-10">Pedido</span>
			</div>

			<div layout="column" layout-align="center start" class="padding-left-10">
				<h3 layout="row" layout-align="space-between center" class="margin-top-0 margin-bottom-10 full-width">
					<span>Pedido #31</span>
					<md-icon md-font-icon="mdi mdi-close" class="margin-0"></md-icon>
				</h3>
				<p class="margin-0">Ha entrado un nuevo pedido para <a ui-sref="clients.show({ id: 1 })">Carnicería Norte</a> por valor de 308€</p>
			</div>
		</div>

		<div layout="row" class="mdc-bg-light-green margin-bottom-10 mdc-text-white-all" md-whiteframe="2">
			<div layout="column" layout-align="center center" class="padding-right-10" style="border-right: 3px solid white;">
				<md-icon md-font-icon="mdi mdi-cart mdi-48px"></md-icon>
				<span class="margin-top-10">Pedido</span>
			</div>

			<div layout="column" layout-align="center start" class="padding-left-10">
				<h3 layout="row" layout-align="space-between center" class="margin-top-0 margin-bottom-10 full-width">
					<span>Pedido #31 - Pagado</span>
					<md-icon md-font-icon="mdi mdi-close" class="margin-0"></md-icon>
				</h3>
				<p class="margin-0">Se ha confirmado el pago del pedido</p>
			</div>
		</div>

		<div layout="row" class="mdc-bg-light-blue margin-bottom-10 mdc-text-white-all" md-whiteframe="2">
			<div layout="column" layout-align="center center" class="padding-right-10" style="border-right: 3px solid white;">
				<md-icon md-font-icon="mdi mdi-cart mdi-48px"></md-icon>
				<span class="margin-top-10">Pedido</span>
			</div>

			<div layout="column" layout-align="center start" class="padding-left-10">
				<h3 layout="row" layout-align="space-between center" class="margin-top-0 margin-bottom-10 full-width">
					<span>Pedido #31 - Enviado</span>
					<md-icon md-font-icon="mdi mdi-close" class="margin-0"></md-icon>
				</h3>
				<p class="margin-0">El pedido ha sido enviado y el cliente ha sido notificado</p>
			</div>
		</div>

		<div layout="row" class="mdc-bg-red margin-bottom-10 mdc-text-white-all" md-whiteframe="2">
			<div layout="column" layout-align="center center" class="padding-right-10" style="border-right: 3px solid white;">
				<md-icon md-font-icon="mdi mdi-cart mdi-48px"></md-icon>
				<span class="margin-top-10">Pedido</span>
			</div>

			<div layout="column" layout-align="center start" class="padding-left-10">
				<h3 layout="row" layout-align="space-between center" class="margin-top-0 margin-bottom-10 full-width">
					<span>Pedido #31 - Descartado</span>
					<md-icon md-font-icon="mdi mdi-close" class="margin-0"></md-icon>
				</h3>
				<p class="margin-0">Se ha descartado el pedido por: Falta de stock</p>
			</div>
		</div>
	</md-content>
</md-sidenav>
