<md-toolbar id="top-toolbar" ng-controller="headerCtrl" class="md-menu-toolbar">
	<div class="md-toolbar-tools">
		<span flex></span>

		{{-- <md-button ng-click="notifications()" class="md-icon-button">
			<md-icon md-font-icon="mdi mdi-alert-circle-outline"></md-icon>
			<md-tooltip md-direction="bottom">Notificaciones</md-tooltip>
		</md-button> --}}

		{{-- <md-button ng-click="" class="md-icon-button">
			<md-icon md-font-icon="mdi mdi-email-outline"></md-icon>
			<md-tooltip md-direction="bottom">Email</md-tooltip>
		</md-button> --}}

		<md-button ui-sref="profile" class="md-primary">
			<md-icon md-font-icon="mdi mdi-face"></md-icon>
			[[ user.name ]]
			<md-tooltip md-direction="bottom">Perfil de usuario</md-tooltip>
		</md-button>

		<md-button href="{{ url('logout') }}" class="md-icon-button" target="_self">
			<md-icon md-font-icon="mdi mdi-logout"></md-icon>
			<md-tooltip md-direction="bottom">Cerrar sesión</md-tooltip>
		</md-button>
	</div>
</md-toolbar>
