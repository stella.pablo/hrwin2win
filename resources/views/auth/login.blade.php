<!DOCTYPE html>
<html lang="en" ng-app="app">

<head>
	@include('_includes.head')

	<style>
		html, body, md-content {
			background-color: white;
		}

		md-content {
			border: 1px solid rgba(0, 0, 0, 0.12);
			-webkit-border-radius: 4px;
			-moz-border-radius: 4px;
			border-radius: 4px;
			max-width: 350px;
		}

		md-content .logo {
			padding: 48px;
			text-align: center;
		}


		.separator {
			width: 90%;
			margin: 0 5%;
			height: 1px;
			background-color: rgba(0, 0, 0, 0.12);
		}

		@media(min-width: 600px) {
			form {
				min-width: 360px;
			}
		}
	</style>
</head>

<body ng-cloak>
<div layout="column" layout-fill layout-align="center center" layout-align-xs="space-between center" ng-controller="loginCtrl">
	<div id="login-box" layout="column" flex-xs ng-hide="hideLogin">
		<form ng-submit="submit()" name="loginForm" ng-class="{'margin-20': $mdMedia('xs')}">
			<input type="hidden" name="rememberme" value="[[ rememberme ]]">

			<md-content class="mdc-bg-white">
				<div class="logo">
					<img src="https://m.win2wintech.com/statics/icons/icon-128x128.png" alt="">
				</div>

				<div class="separator"></div>

				<h1 class="text-center font-weight-500 margin-top-20" style="font-size: 20px;">Recursos Humanos</h1>
				<h2 class="text-center font-weight-500" style="font-size: 14px;">win2wintech</h2>

				<div layout="column" class="margin-20">
					<div class="margin-bottom-10 mdc-bg-red-300 mdc-text-white" ng-show="error">
						Los datos proporcionados no son correctos.
					</div>

					<md-input-container class="md-icon-float md-block">
						<label>Usuario</label>
						<md-icon md-font-icon="mdi mdi-account-outline"></md-icon>
						<input type="text" name="username" ng-model="user.username" required>
					</md-input-container>

					<md-input-container class="md-icon-float md-block">
						<label>Contraseña</label>
						<md-icon md-font-icon="mdi mdi-lock-outline"></md-icon>
						<input type="password" name="password" ng-model="user.password" required>
					</md-input-container>

					<div layout="row" layout-align="end center">
						<md-button class="md-primary md-raised margin-left-20" type="submit" ng-disabled="!user.username || !user.password || loading">
							<md-icon md-font-icon="mdi" ng-class="{'mdi-loading mdi-spin': loading, 'mdi-login': !loading}"></md-icon>
							<span ng-hide="loading">Entrar</span>
						</md-button>
					</div>
				</div>
			</md-content>
		</form>
	</div>
</div>

<script src="{{ elixir('js/vendor.js') }}" charset="utf-8"></script>
<script>
	angular.module('app', ['ngMaterial']).config(
		function($interpolateProvider, $locationProvider, $mdThemingProvider, $qProvider) {
			$interpolateProvider.startSymbol("[[").endSymbol("]]");
			$locationProvider.html5Mode(true);
			$mdThemingProvider.theme("default").primaryPalette("teal").accentPalette("pink");
			$mdThemingProvider.enableBrowserColor({ theme: 'default' });
			$qProvider.errorOnUnhandledRejections(false);
		}
	).controller('loginCtrl', function($scope, $http, $timeout, $mdMedia) {
		$scope.$mdMedia = $mdMedia;
		$scope.error = false;
		$scope.hideLogin = true;
		$scope.loading = false;
		$scope.submit = function() {
			$scope.login()
		};

		$scope.login = function() {
			$scope.loading = true;

			$http.post('{{ url('login') }}', $scope.user).then(function(response) {
				if(response.data.user) {
					$timeout(function() {
						window.location.href = '/dashboard';
					}, 500);
				}
				else {
					$scope.loginCancel();
					$scope.loading = false;
				}
			}, function(response) {
				$scope.loginCancel();

				$scope.loading = false;
			});
		};

		$scope.loginCancel = function() {
			$scope.error = true;
		};

		// In case we are already logged in
		$http.get('/me').then(function(response) {
			if (response.data.id) {
				window.location.href = '/dashboard';
			}
			else {
				$scope.hideLogin = false;
			}
		});

	});
</script>
</body>

</html>
