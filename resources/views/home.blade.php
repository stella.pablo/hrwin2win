<!DOCTYPE html>
<html lang="en" ng-app="app">

<head>
	@include('_includes.head')
</head>

<body ng-cloak>
	@if(config('app.env') === 'production')
	<div
		id="presentation"
		layout="column"
		layout-fill
		layout-align="center center"
		class="wow slideOutLeft"
		data-wow-delay="3.5s"
		data-wow-duration="0.5s"
	>
		<h1 class="mdc-text-white wow fadeIn margin-bottom-0">
			Bienvenid@ {{ \Illuminate\Support\Str::title(auth()->user()->name) }}
		</h1>
		<h3 class="mdc-text-white wow fadeIn" data-wow-delay="1s">
				Cargando aplicación
				<span class="mdc-text-white wow fadeIn" data-wow-delay="1.4s">.</span>
				<span class="mdc-text-white wow fadeIn" data-wow-delay="1.8s">.</span>
				<span class="mdc-text-white wow fadeIn" data-wow-delay="2.2s">.</span>
			</h3>
	</div>
	@endif

	<section id="body" layout="row" layout-fill ng-controller="rootCtrl">
		@include('_includes.navigation')

		<md-content flex>
			<md-toolbar id="main-toolbar" class="md-menu-toolbar">
				<div class="md-toolbar-tools">
					<md-button class="md-icon-button margin-right-0" ng-click="toggleMenu()">
						<md-icon md-font-icon="mdi mdi-menu"></md-icon>
					</md-button>

					<span class="separator"></span>

					<img
						src="https://m.win2wintech.com/statics/app-logo-128x128.png"
						alt="win2win human resources"
						style="max-width: 24px;"
						class="margin-left-5 margin-right-5"
					>

					<span class="separator"></span>

					<span flex></span>

					<div layout="row">
						@if(auth()->user()->level === 1)
							<md-button
								ng-click="changeCompany()"
								class="md-icon-button"
								ng-show="user.managements_all.length > 1"
							>
								<md-icon md-font-icon="mdi mdi-key-change"></md-icon>
								<md-tooltip md-direction="bottom">Empresa activa: [[ user.company.name ]]</md-tooltip>
							</md-button>
						@endif

						<md-button ui-sref="profile" class="md-icon-button">
							<md-icon md-font-icon="mdi mdi-face"></md-icon>
							<md-tooltip md-direction="bottom">Perfil de [[ user.name ]]</md-tooltip>
						</md-button>

						@if(auth()->user()->level < 3)
							<md-button ui-sref="settings" class="md-icon-button">
								<md-icon md-font-icon="mdi mdi-settings"></md-icon>
								<md-tooltip md-direction="bottom">Configuración del sistema</md-tooltip>
							</md-button>
						@endif

						<md-button href="{{ url('logout') }}" class="md-icon-button" target="_self">
							<md-icon md-font-icon="mdi mdi-logout" style="margin-left: 3px;"></md-icon>
							<md-tooltip md-direction="bottom">Cerrar sesión</md-tooltip>
						</md-button>
					</div>
				</div>
			</md-toolbar>

			<ui-view></ui-view>
		</md-content>

		@include('_includes.notifications')
	</section>


{{--	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCMjVI7uBndm2GovjJwSjvgMEp727Ugkww&libraries=places&language=es"></script>--}}
	<script src="{{ mix('js/vendor.js') }}" charset="utf-8"></script>
	<script src="{{ mix('js/app.js') }}" charset="utf-8"></script>
	<script src="{{ asset('js/locales/angular-locale_' . auth()->user()->management->company->locale . '.js') }}" charset="utf-8"></script>
	<script>
		window.UserData = {!! auth()->user()->load(['management.external', 'managements.external', 'managementsAll.external']) !!};
	</script>
</body>

</html>
