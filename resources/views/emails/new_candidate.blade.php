<h3>Apreciado/a {{ $candidate->name . ' ' . $candidate->surname . ' ' . $candidate->lastname }},</h3>
<p>Agradecemos la confianza depositada en nuestra empresa y te comunicamos que durante el proceso de selección consideraremos atentamente tu CV, y si tu perfil encaja en el puesto ofertado, volveremos a contactar contigo vía telefónica.</p>
<p>Visita nuestra Web:</p>
<p>{{ $user->management->url }}</p>
<p>Atentamente, un cordial saludo</p>
<p>RRHH {{ $user->company->name }}</p>
<p>{{ $user->management->phone }}</p>

