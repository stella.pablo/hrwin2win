<h3>{{$greetText}} {{ $candidate->name . ' ' . $candidate->surname . ' ' . $candidate->lastname }},</h3>

<p>{{$secondAppointmentText}}</p>

<p><strong>{{$dateTimeText}}</strong>:  {{ $appointment->date->format('d-m-Y H:i') }} </p>
@if($candidate->management AND $candidate->management->address AND $candidate->management->address_link)
<p><strong>{{$locationText}}</strong>: {{ $candidate->management->address }}</p>
<p><strong>{{$howToReachText}}</strong>:  <a href="{{ $candidate->management->address_link }}">Google maps</a></p>
@endif

@if($extrabody)
<p>
	{{ $extrabody }}
</p>
@endif

<p>{{$thanksText}}</p>
<p>{{ $signHeadText }} </p>
<p>{{ $signBodyText }} {{ $candidate->management->company->name }}.</p>

@if($candidate->management AND $candidate->management->phone AND $candidate->management->url)
<p>{{ $candidate->management->phone }}</p>
<p>{{ $candidate->management->url }}</p>
@endif

<em>
	<small>
		<p>{{$legalInfoText}}
		</p>
	</small>
</em>
