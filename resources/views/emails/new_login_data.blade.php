@component('mail::message')
# Hola {{ $name }},

Te adjuntamos tus datos de acceso para que puedas acceder a la plataforma de {{ mb_strtolower($platformName) }}.

@component('mail::panel')
- Dirección: {{ $url }}
- Usuario: {{ $username }}
- Contraseña: {{ $password }}
@endcomponent

@component('mail::button', ['url' => $url])
Acceder a {{ $platformName  }}
@endcomponent

Gracias,<br>
{{ config('app.name') }}
@endcomponent
