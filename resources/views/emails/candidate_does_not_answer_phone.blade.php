<h3>Buenos días {{ $candidate->name . ' ' . $candidate->surname . ' ' . $candidate->lastname }}</h3>

<p>Te escribimos para informate que nuestro departamento de RRHH ha intentado contactar contigo para invitarte a una entrevista de trabajo.</p>
<p>Estamos en el proceso de ampliación de nuestra plantilla, por lo que intentaremos llamarte de nuevo en los próximos días.  Así mismo, si lo deseas, puedes ponerte en contacto con nosotros respondiendo a este correo electrónico o vía telefónica al 91 000 9391 en horario de 09:00 a 14:00.</p>
<p>Gracias por tu interés en nuestra empresa y esperamos tener noticias tuyas pronto.</p>

<p>Un saludo,</p>
<p>RRHH {{ $user->management->company->name }}</p>
<p>{{ $user->management->url }}</p>
