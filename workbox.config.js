module.exports = {
	"globDirectory": "public/",
	"globPatterns": [
		"{css,js,statics,statics/icons}/*.{css,js,png}"
	],
	"maximumFileSizeToCacheInBytes": 10 * 1024 * 1024,
	"swDest": "public/service-worker.js",
	"swSrc": "resources/assets/js/service-worker.js"
};
