const elixir = require('laravel-elixir');
// require('laravel-elixir-vue-2');
elixir.config.sourcemaps = false;

var coffee = require('gulp-coffee');
var concat = require('gulp-concat');

gulp.task('coffeeBare', function() {
	gulp.src([
	  'resources/assets/coffee/app.coffee',
	  'resources/assets/coffee/components/**',
	  'resources/assets/coffee/controllers/**/*',
	])
  	.pipe(concat('app.js'))
    .pipe(coffee({bare: true}))
    .pipe(gulp.dest('public/js/'));
});

elixir((mix) => {
    mix
		// VENDOR
		.copy('resources/assets/less/vendor/mdc-get-colors-mixin.less', 'resources/assets/bower/material-design-color-palette/less/mixins/get-colors.less')
		.less('../bower/material-design-color-palette/less/material-design-color-palette.less', 'resources/assets/bower/material-design-color-palette/css')
		.styles([
			'normalize-css/normalize.css',

			'leaflet/dist/leaflet.css',

			// 'chartist/dist/chartist.min.css',

			// 'percircle/dist/css/percircle.css',
			// 'angularjs-slider/dist/rzslider.css',

			'angular-loading-bar/build/loading-bar.min.css',
			'angular-material/angular-material.css',
			//'material-design-lite/material.min.css',
			'../js/vendor/DataTables/datatables.css',

			'highcharts/css/highcharts.css',
			'highcharts-ng/dist/highcharts-ng.css',

			'fullcalendar/dist/fullcalendar.min.css',

			'angular-datatables/dist/css/angular-datatables.min.css',
			'material-design-color-palette/css/material-design-color-palette.css',
			'mdi/css/materialdesignicons.min.css',
			'animate.css/animate.min.css',



		], 'public/css/vendor.css', 'resources/assets/bower')

		.scripts([
			'wow/dist/wow.min.js',
			'animejs/anime.min.js',
			'moment/min/moment-with-locales.min.js',

			// 'leaflet/dist/leaflet.js',

			'jquery/dist/jquery.min.js',
			// '../js/vendor/jquery.sparkline.min.js',

			// 'percircle/dist/js/percircle.js',

			'chartist/dist/chartist.min.js',

			'../js/vendor/DataTables/datatables.min.js',
			'../js/vendor/DataTables/datatables-sum.js',
			'../js/vendor/DataTables/accent-neutralise.js',
			'../js/vendor/DataTables/datetime-moment.js',

			'jszip/dist/jszip.min.js',
			'pdfmake/build/pdfmake.min.js',
			'pdfmake/build/vfs_fonts.js',

			// '../js/vendor/DataTables/Buttons-1.2.4/js/buttons.flash.js',
			'../js/vendor/DataTables/Buttons-1.2.4/js/buttons.html5.js',
			// '../js/vendor/DataTables/Buttons-1.2.4/js/buttons.print.js',

			// 'highcharts/js/highcharts.js',
			// 'highcharts/js/highmaps.js',
			'highcharts/js/highstock.js',
			'highcharts/js/modules/series-label.js',
			// 'highcharts/js/modules/exporting.js',
			'highcharts/js/themes/grid-light.src.js',

			'angular/angular.min.js',
			'angular-loading-bar/build/loading-bar.min.js',
			'angular-animate/angular-animate.min.js',
			'angular-aria/angular-aria.min.js',
			'angular-cookies/angular-cookies.min.js',
			'angular-messages/angular-messages.min.js',
			'angular-moment/angular-moment.min.js',
			'angular-resource/angular-resource.min.js',
			'angular-local-storage/dist/angular-local-storage.min.js',
			// 'angular-route/angular-route.min.js',
			'angular-ui-router/release/angular-ui-router.min.js',
			'highcharts-ng/dist/highcharts-ng.js',

			'angular-material/angular-material.min.js',

			// 'angular-chartist.js/dist/angular-chartist.min.js',
			// 'chartist-plugin-legend-latest/chartist-plugin-legend.js',
			// 'chartist-plugin-tooltip/dist/chartist-plugin-tooltip.min.js',
			// '../js/vendor/chartist-plugin-pointlabels.js',

			'angular-datatables/dist/angular-datatables.min.js',
			'angular-datatables/dist/plugins/buttons/angular-datatables.buttons.min.js',

			// 'angular-simple-logger/dist/angular-simple-logger.min.js',
			'ui-leaflet/dist/ui-leaflet.min.js',

			'angularjs-slider/dist/rzslider.min.js',

			// 're-tree/re-tree.min.js',
			// 'ng-device-detector/ng-device-detector.min.js',
			'ng-file-upload/ng-file-upload.min.js',

			'fullcalendar/dist/fullcalendar.min.js',
			'fullcalendar/dist/locale/en-gb.js',
			'fullcalendar/dist/locale/es.js',

		], 'public/js/vendor.js', 'resources/assets/bower')

		.copy('resources/assets/fonts', 'public/build/fonts')
		.copy('resources/assets/bower/angular-i18n', 'public/build/js/locales')
		.copy('resources/assets/bower/mdi/fonts', 'public/build/fonts')
		.copy('resources/assets/bower/leaflet/dist/images', 'public/build/css/images')
		.copy('resources/assets/js/vendor/DataTables/DataTables-1.10.13/images', 'public/build/css/images')
		.copy('resources/assets/js/vendor/DataTables/Buttons-1.2.4/swf', 'public/build/js/swf')


		// APPLICATION
		.sass('app.scss')

		.task('coffeeBare', [
	  	  'resources/assets/coffee/app.coffee',
	  	  'resources/assets/coffee/components/**',
	  	  'resources/assets/coffee/controllers/**',
	    ])

		.version([
			'css/vendor.css',
			'css/app.css',
			'js/vendor.js',
			'js/app.js'
		])
		;
});
