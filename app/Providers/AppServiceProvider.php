<?php

namespace App\Providers;

use App\Models\External\TComerciales;
use App\Models\External\TComercialesEstructuras;
use App\Models\External\TGerencias;
use App\Models\External\TGruposComisionamiento;
use App\Models\External\TLiquidaciones;
use App\Models\External\TProductos;
use App\Models\External\TTiposContratosComercial;
use App\Observers\CommercialObserver;
use App\Observers\CommercialStructureObserver;
use App\Observers\ContractsTypesObserver;
use App\Observers\ManagementObserver;
use App\Observers\ProductsObserver;
use Illuminate\Support\ServiceProvider;
use App\Models\Candidate;
use App\Models\CandidatesAppointment;
use App\Observers\CandidateObserver;
use App\Observers\CandidatesAppointmentObserver;
use App\Observers\FeeGroupsObserver;
use App\Observers\SettlementsObserver;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Candidate::observe(CandidateObserver::class);
		CandidatesAppointment::observe(CandidatesAppointmentObserver::class);
		TComerciales::observe(CommercialObserver::class);
		TGerencias::observe(ManagementObserver::class);
		TTiposContratosComercial::observe(ContractsTypesObserver::class);
		TProductos::observe(ProductsObserver::class);
		TGruposComisionamiento::observe(FeeGroupsObserver::class);
		TLiquidaciones::observe(SettlementsObserver::class);
		TComercialesEstructuras::observe(CommercialStructureObserver::class);

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
