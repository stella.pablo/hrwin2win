<?php

namespace App\Providers;

use App\Models\External\TComerciales;
use App\Models\External\TGerencias;
use App\Policies\CommercialPolicy;
use App\Policies\ManagementPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{

	/**
	 * The policy mappings for the application.
	 *
	 * @var array
	 */
	protected $policies = [
		TComerciales::class => CommercialPolicy::class,
		TGerencias::class   => ManagementPolicy::class,
	];

	/**
	 * Register any authentication / authorization services.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->registerPolicies();

		//
	}
}
