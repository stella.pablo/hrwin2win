<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;

class NewLoginData extends Mailable implements ShouldQueue
{

	use Queueable;

	public $url;
	public $username;
	public $password;
	public $platformName;
	public $name;

	/**
	 * NewLoginData constructor.
	 *
	 * @param $url
	 * @param $username
	 * @param $password
	 */
	public function __construct($name, $username, $password, $platform)
	{
		$this->name = $name;


		//$this->username = $platform != 'hr' ? $username : $username . '.com';
		$this->username = $username;
		$this->password = $password;

		$this->platformName = $platform === 'hr' ? 'Recursos Humanos' : 'Mobile';
		$this->url = $platform === 'hr' ? 'https://hr.win2wintech.com/' : 'https://m.win2wintech.com/';
	}


	/**
	 * Build the message.
	 *
	 * @return $this
	 */
	public function build()
	{
		$from = config('application.app.mail_settings.' . session('database') . '.from');

		return $this
			->subject('[' . $this->platformName . '] Tus datos de acceso')
			->from($from['address'], $from['name'])
			->markdown('emails.new_login_data');
	}
}
