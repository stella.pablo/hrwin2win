<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CandidateAppointment extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

	public $appointment;
	public $candidate;
	public $extrabody;
	public $greetText;
	public $subjectText;
	public $firstLineText;
	public $secondAppointmentText;
	public $dateTimeText;
	public $locationText;
	public $thanksText;
	public $signHeadText;
    public $signBodyText;
	public $howToReachText;
	public $legalInfoText;
	public $user;
	public $secondAppointMentSubjectText;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $candidate, $appointment, $extrabody = null, $greetText, $subjectText, $firstLineText, $dateTimeText, $locationText, $thanksText, $signHeadText, $signBodyText,  $howToReachText, $legalInfoText, $secondAppointmentText,$secondAppointMentSubjectText)
    {
        $this->user = $user;
		$this->candidate = $candidate;
		$this->appointment = $appointment;
		$this->extrabody = $extrabody;
		$this->greetText = $greetText;
		$this->firstLineText = $firstLineText;
		$this->dateTimeText = $dateTimeText;
		$this->locationText = $locationText;
		$this->thanksText = $thanksText;
		$this->signHeadText = $signHeadText;
		$this->signBodyText = $signBodyText;
		$this->howToReachText = $howToReachText;
		$this->legalInfoText = $legalInfoText;
		$this->subjectText = $subjectText;
		$this->secondAppointmentText= $secondAppointmentText;
		$this->secondAppointMentSubjectText = $secondAppointMentSubjectText;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
		if($this->appointment->number === 1) {
			$subject = $this->subjectText;
			$view = 'candidate_1st_appointment';
		}
		else {
			$subject = $this->secondAppointMentSubjectText;
			$view = 'candidate_2nd_appointment';
		}

		$from = config('application.app.mail_settings.' . session('database') . '.from');

        return $this
			->subject($subject)
	        ->from($from['address'], $from['name'])
			->view('emails.' . $view);
    }
}
