<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CandidateDoesNotAnswerPhone extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

	public $candidate;
	public $user;

	/**
	 * Create a new message instance.
	 *
	 * @return void
	 */
	public function __construct($user, $candidate)
	{
		$this->user = $user;
		$this->candidate = $candidate;
	}

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
		$subject = 'Invitación entrevista de trabajo';
		$from = config('application.app.mail_settings.' . session('database') . '.from');

        return $this
			->subject($subject)
	        ->from($from['address'], $from['name'])
			->view('emails.candidate_does_not_answer_phone');
    }
}
