<?php


namespace App\Mail;


use Mail;
use Swift_Mailer;
use Swift_SmtpTransport;

class MailSender
{

	public static function send($to, $class, $queue = false)
	{
		$mailSettings = config('application.app.mail_settings.' . session('database'));

		if (!$mailSettings) {
			logger('we didnt hit mail_settings for: ' . session('database'));
			return null;
		}

		$transport = (new Swift_SmtpTransport($mailSettings['host'], $mailSettings['port']))
			->setUsername($mailSettings['username'])
			->setPassword($mailSettings['password']);

		if (isset($mailSettings['encryption'])) {
			$transport->setEncryption($mailSettings['encryption']);
		}

		Mail::setSwiftMailer(new Swift_Mailer($transport));

		$pendingMail = Mail::to($to);

		if ($queue) {
			$pendingMail->queue($class);
		} else {
			$pendingMail->send($class);
		}
	}
}
