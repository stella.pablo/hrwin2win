<?php

namespace App\Models;

use App\Models\External\TGerencias;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Management
 *
 * @property int                                  $id
 * @property int                                  $company_id
 * @property string                               $name
 * @property string                               $external_id
 * @property string|null                          $address
 * @property string|null                          $address_link
 * @property string|null                          $workhours
 * @property string|null                          $phone
 * @property string|null                          $url
 * @property int                                  $monthly_objective
 * @property int                                  $productivity_low_days
 * @property \Illuminate\Support\Carbon|null      $created_at
 * @property \Illuminate\Support\Carbon|null      $updated_at
 * @property-read \App\Models\Company             $company
 * @property-read \App\Models\External\TGerencias $external
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Management newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Management newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Management query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Management whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Management whereAddressLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Management whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Management whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Management whereExternalId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Management whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Management whereMonthlyObjective($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Management whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Management wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Management whereProductivityLowDays($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Management whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Management whereUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Management whereWorkhours($value)
 * @mixin \Eloquent
 */
class Management extends InternalModel
{

	use SoftDeletes;

	protected $guarded = [];

	protected $table='managements';

	// protected $with = ['company'];

	public function getNameAttribute($value)
	{
		return ucwords(mb_strtolower(trim($value)));
	}

	public function company()
	{
		return $this->belongsTo(Company::class);
	}

	public function external()
	{
		return $this->belongsTo(TGerencias::class, 'external_id', 'id_gerencia');
	}

}
