<?php

namespace App\Models;

/**
 * App\Models\EmailsType
 *
 * @property int                             $id
 * @property string                          $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmailsType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmailsType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmailsType query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmailsType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmailsType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmailsType whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmailsType whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class EmailsType extends InternalModel
{

	//
}
