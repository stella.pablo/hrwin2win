<?php

namespace App\Models;

/**
 * App\Models\ReportsStatus
 *
 * @property int                             $id
 * @property int                             $company_id
 * @property string                          $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReportsStatus newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReportsStatus newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReportsStatus query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReportsStatus whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReportsStatus whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReportsStatus whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReportsStatus whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReportsStatus whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ReportsStatus extends InternalModel
{

	protected $fillable = ['company_id', 'name'];
}
