<?php

namespace App\Models;

/**
 * App\Models\CandidatesStatus
 *
 * @property int                             $id
 * @property string                          $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CandidatesStatus newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CandidatesStatus newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CandidatesStatus query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CandidatesStatus whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CandidatesStatus whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CandidatesStatus whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CandidatesStatus whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class CandidatesStatus extends InternalModel
{

	protected $guared = [];
}
