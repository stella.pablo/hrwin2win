<?php

namespace App\Models;

/**
 * App\Models\ReportsMotive
 *
 * @property int                             $id
 * @property int                             $company_id
 * @property string                          $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReportsMotive newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReportsMotive newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReportsMotive query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReportsMotive whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReportsMotive whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReportsMotive whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReportsMotive whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReportsMotive whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ReportsMotive extends InternalModel
{

	//
}
