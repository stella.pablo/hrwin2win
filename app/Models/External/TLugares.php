<?php

namespace App\Models\External;

// use GeneaLabs\LaravelModelCaching\Traits\Cachable;

/**
 * App\Models\External\TLugares
 *
 * @property int                             $id
 * @property string                          $lugar
 * @property int                             $id_poblacion
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TLugares newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TLugares newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TLugares query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TLugares whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TLugares whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TLugares whereIdPoblacion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TLugares whereLugar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TLugares whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class TLugares extends ExternalModel
{

	// use Cachable;

	protected $dateFormat = 'Y-m-d H:i:s';
	protected $guarded = [];
	protected $table = 't_lugares_inscripcion';
}
