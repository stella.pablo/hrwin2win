<?php

namespace App\Models\External;

// use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use App\Models\Management;
use DB;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\External\TGerencias
 *
 * @property string                                                                            $id_empresa
 * @property string                                                                            $id_gerencia
 * @property string|null                                                                       $gerencia
 * @property int|null                                                                          $id_provincia
 * @property string|null                                                                       $tipo_gerencia
 * @property string|null                                                                       $id_comercial_liquidacion
 * @property int|null                                                                          $id_tipo_relacion
 * @property string|null                                                                       $tiene_rappel
 * @property int|null                                                                          $cantidad_para_rappel
 * @property float|null                                                                        $importe_por_contrato
 * @property int|null                                                                          $id_estado
 * @property string|null                                                                       $fecha_baja
 * @property int|null                                                                          $id_organizacion
 * @property string|null                                                                       $id_gerencia_externa
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\External\TComerciales[] $commercials
 * @property-read \App\Models\Management                                                       $internal
 * @property-read \App\Models\External\TComerciales|null                                       $manager
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\External\TEstrategias[] $strategies
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TGerencias newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TGerencias newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TGerencias query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TGerencias whereCantidadParaRappel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TGerencias whereFechaBaja($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TGerencias whereGerencia($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TGerencias whereIdComercialLiquidacion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TGerencias whereIdEmpresa($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TGerencias whereIdEstado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TGerencias whereIdGerencia($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TGerencias whereIdGerenciaExterna($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TGerencias whereIdOrganizacion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TGerencias whereIdProvincia($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TGerencias whereIdTipoRelacion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TGerencias whereImportePorContrato($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TGerencias whereTieneRappel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TGerencias whereTipoGerencia($value)
 * @mixin \Eloquent
 */
class TGerencias extends ExternalModel
{

	// use Cachable;
	use SoftDeletes;

	const CREATED_AT = null;
	const DELETED_AT = 'fecha_baja';
	const UPDATED_AT = null;

	protected $casts = [
		'id_empresa'  => 'string',
		'id_gerencia' => 'string',
	];
	protected $dates = ['fecha_alta', 'fecha_baja'];
	protected $guarded = [];
	protected $hidden = [
		'cantidad_para_rappel',
		'id_tipo_relacion',
		'importe_por_contrato',
		'tiene_rappel',
	];
	public $incrementing = false;
	protected $primaryKey = 'id_gerencia';
	protected $table = 't_gerencias';
	protected $withCount = ['commercials'];

	public function getCampaignsAttribute()
	{
		$commercialsIds = TComerciales
			::select('id_comercial')
			->where('id_gerencia', $this->id_gerencia)
			->get()
			->pluck('id_comercial')
			->toArray();

		$campaignsIds = TComercialesCampanyas
			::select('id_campanya')
			->distinct()
			->whereIn('id_comercial', $commercialsIds)
			->get()
			->pluck('id_campanya')
			->toArray();

		return TCampanyas::whereIn('id', $campaignsIds)->get();
	}

	public function resolveRouteBinding($value)
	{
		return $this->withTrashed()->find($value) ?? abort(404);
	}

	public function commercials()
	{
		return $this->hasMany(TComerciales::class, 'id_gerencia', 'id_gerencia')->where('id_estado', 0);
	}

	public function internal()
	{
		return $this
			->belongsTo(Management::class, 'id_gerencia', 'external_id')
			->where('company_id', session('company_id'));
	}

	public function manager()
	{
		return $this->belongsTo(TComerciales::class, 'id_comercial_liquidacion','id_comercial');
	}

	public function organization()
	{
		return $this->belongsTo(TOrganizaciones::class, 'id_organizacion');
	}

	public function strategies()
	{
		return $this->hasMany(TEstrategias::class, 'id_gerencia');
	}

	public function getGerenciaAttribute($value)
	{
		return ucwords(mb_strtolower($value));
	}

	public function structures()
	{
		return $this->hasMany(TGerenciasEstructuras::class, 'id_gerencia', 'id_gerencia');
	}

	public function feeGroups()
	{
		return $this->belongsToMany(TGruposComisionamientoGerencias::class, 'id_gerencia', 'id_gerencia');
	}


	public function getDefaultRateAttribute()
	{
		$ret = DB::connection(session('database'))->select('select id_tarifa from  t_gerencias where id_gerencia = ?', [$this->id_gerencia]);
		if (!isset($ret[0])) {
			return '';
		}
		if (!isset($ret[0]->id_tarifa)) {
			return '';
		}

		return $ret[0]->id_tarifa;
	}

	public function deliveries()
	{

		return $this->hasmany(TEntregas::class, 'id_gerencia', 'id_gerencia');

	}

	public function deliveryNotes()
	{

		return $this->hasMany(TAlbaranes::class, 'id_gerencia', 'id_gerencia');

	}

	public function addresses()
	{
		return $this->hasMany(TDireccionesEntregaGerencias::class, 'id_gerencia', 'id_gerencia');
	}

	public function paymentMethod()
	{
		return $this->hasOne(TFormasPago::class, 'id_forma_pago', 'id_forma_pago');
	}

	public function iva()
	{
		return $this->hasOne(TIvas::class, 'id_iva', 'id_iva');
	}

	public function itau()
	{
		return $this->hasOne(TItaus::class, 'id_itau', 'id_itau');
	}

	public function storage()
	{
		return $this->hasOne(TAlmacenes::class, 'id_almacen', 'id_almacen');
	}

	public function locationsDef()
	{

		return $this->hasMany(TUbicacionesDef::class, ['id_gerencia', 'id_almacen'], ['id_gerencia', 'id_almacen']);
	}

	public function rate()
	{

		return $this->hasOne(TTarifas::class, 'id_tarifa', 'id_tarifa');

	}
}
