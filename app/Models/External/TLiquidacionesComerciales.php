<?php

namespace App\Models\External;

use Awobaz\Compoships\Compoships;


use DB;

class TLiquidacionesComerciales extends ExternalModel
{

	use compoships;
	public const CREATED_AT = null;
	public const UPDATED_AT = null;
	public const DELETED_AT = null;
	protected $casts = [
		'id_liquidacion_comercial' => 'int'
	];
	protected $guarded = [];
	protected $table = 't_liquidaciones_comerciales';
	protected $appends= ['total'];

	public function settlement()
	{
		return $this->belongsTo(TLiquidaciones::class, 'id_liquidacion', 'id');
	}

	public function commercial()
	{
		return $this->belongsTo(TComerciales::class, 'id_comercial', 'id_comercial');
	}

	public function concepts()
	{
		return $this->hasMany(TLiquidacionesComercialesConceptos::class, 'id_liquidacion_comercial', 'id');
	}

	public function getTotalAttribute() {
		return TLiquidacionesComercialesConceptos::where('id_liquidacion_comercial',$this->id)->sum('importe');
	}

	public function getDistinctSalesAttribute(){
		return TLiquidacionesComercialesConceptos::where('id_liquidacion_comercial',$this->id)
			->where('id_comercial',$this->id_comercial)
			->distinct('id_venta')
			->count();
	}

	public function getDistinctProductsAttribute(){
        return TLiquidacionesComercialesConceptos::where('id_liquidacion_comercial',$this->id)
            ->where('id_comercial',$this->id_comercial)
            ->distinct('id_contrato_producto')
            ->count();
    }

	public function getTotalArticlesAttribute(){
		$total = 0;
		$data =  TLiquidacionesComercialesConceptos::where('id_liquidacion_comercial',$this->id)
			->where('concepto',1)
			->where('id_comercial',$this->id_comercial)
			->with('line')->get();
		foreach($data as $concept){
			$total += $concept->line->cantidad;
		}
		return $total;

	}

	public function getTotalProductsAttribute(){
		$total = 0;
		$data =  TLiquidacionesComercialesConceptos::where('id_liquidacion_comercial',$this->id)
			->where('concepto',9)
			->where('id_comercial',$this->id_comercial)
			->with('product')->get();
		foreach($data as $concept){
			$total ++;
		}
		return $total;

	}
}
