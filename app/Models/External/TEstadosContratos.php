<?php

namespace App\Models\External;

// use GeneaLabs\LaravelModelCaching\Traits\Cachable;

/**
 * App\Models\External\TEstadosContratos
 *
 * @property string      $id_empresa
 * @property string      $id_estado
 * @property string|null $estado
 * @property int|null    $tipo
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TEstadosContratos newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TEstadosContratos newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TEstadosContratos query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TEstadosContratos whereEstado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TEstadosContratos whereIdEmpresa($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TEstadosContratos whereIdEstado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TEstadosContratos whereTipo($value)
 * @mixin \Eloquent
 */
class TEstadosContratos extends ExternalModel
{

	// use Cachable;

	protected $casts = [
		'id_empresa' => 'string',
		'id_estado'  => 'string',
	];
	protected $primaryKey = 'id_estado';
	protected $table = 't_estados_contratos';
}
