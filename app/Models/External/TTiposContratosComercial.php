<?php

namespace App\Models\External;


class TTiposContratosComercial extends ExternalModel
{

	public const CREATED_AT = null;
	public const UPDATED_AT = null;

	protected $casts = [
		'id_tipo_contrato_comercial' => 'int',
		'id_empresa'   => 'string',
	];
	protected $guarded = [];

	protected $primaryKey = 'id_tipo_contrato_comercial';
	protected $table = 't_tipos_contratos_comercial';



	public function irpf() {
		return $this->hasOne(TIrpfs::class,'id_irpf','id_irpf');
	}

	public function iva() {
		return $this->hasOne(TIvas::class,'id_iva','id_iva');
	}

	public function stretches(){
		return $this->hasMany(TTiposContratosComercialTramos::class,'id_tipo_contrato_comercial','id_tipo_contrato_comercial');
	}
}
