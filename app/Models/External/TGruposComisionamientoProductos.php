<?php

namespace App\Models\External;
use App\Models\External\ExternalModel;
use Awobaz\Compoships\Compoships;
use Illuminate\Database\Eloquent\Model;
use \App\Models\External\TProductosTramos;
use \App\Models\External\TProductos;

class TGruposComisionamientoProductos extends ExternalModel{
	use compoships;
	const CREATED_AT = null;
	const UPDATED_AT = null;


	protected $guarded = [];
	protected $table = 't_grupos_comisionamiento_productos';


	public function feeGroup(){
		return $this->BelongsTo(TGruposComisionamiento::class,'id_grupo','id');
	}

	public function product(){
		return $this->hasOne(TProductos::class,'id_producto','id_producto');
	}

	public function stretch(){
		return $this->hasOne(\App\Models\External\TproductosTramos::class,'id_tramo','id');
	}

	public function fees(){
		return $this->hasMany(TGruposComisionamientoProductosComisiones::class,'id_comision_producto','id');
	}


}
