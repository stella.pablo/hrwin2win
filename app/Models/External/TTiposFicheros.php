<?php

namespace App\Models\External;

// use GeneaLabs\LaravelModelCaching\Traits\Cachable;

/**
 * App\Models\External\TTiposFicheros
 *
 * @property string      $id_empresa
 * @property int         $id_tipo_fichero
 * @property string|null $tipo_fichero
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TTiposFicheros newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TTiposFicheros newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TTiposFicheros query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TTiposFicheros whereIdEmpresa($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TTiposFicheros whereIdTipoFichero($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TTiposFicheros whereTipoFichero($value)
 * @mixin \Eloquent
 */
class TTiposFicheros extends ExternalModel
{

	// use Cachable;

	public const CREATED_AT = null;
	public const UPDATED_AT = null;

	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'id_empresa' => 'string',
	];
	protected $guarded = [];
	protected $primaryKey = 'id_tipo_fichero';
	protected $table = 't_tipos_ficheros';
}
