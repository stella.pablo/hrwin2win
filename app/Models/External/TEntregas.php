<?php

namespace App\Models\External;

use Akaunting\Money\Money;


class TEntregas extends ExternalModel
{

	const CREATED_AT = 'fecha';
	const UPDATED_AT = null;


	//protected $connection = 'sqlsrv';
	protected $casts = [
		'id_empresa' => 'string',
		'id_entrega' => 'string',
	];

	protected $dateFormat = 'Y-m-d H:i:s';
	protected $guarded = [];
	protected $primaryKey = 'id_entrega';
	protected $table = 't_entregas_gerencias_cab';
	public $incrementing = false;

	public function management(){
	 return	$this->belongsTo(TGerencias::class,'id_gerencia','id_gerencia');

	}

	public function lines(){
		return $this->hasMany(TEntregas::class,'id_entrega','id_entrega');
	}

	public function settlementConcepts(){
		return $this->hasMany(TLiquidacionesComercialesConceptos::class,'id_entrega','id_entrega');
	}



}
