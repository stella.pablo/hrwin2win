<?php

namespace App\Models\External;
use Awobaz\Compoships\Compoships;
use App\Models\External\ExternalModel;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ClientsIva
 *
 * @property string                                                             $id_empresa ID EMPRESA
 * @property string                                                             $id_iva     IVA
 * @property float|null                                                         $iva
 * @property string|null                                                           $descripcion
 * @property string|null                                                           $iva_de_articulo
 * @property string|null                                                           $cuenta_contable_repercutido
 * @property string|null                                                           $cuenta_contable_soportado
 * @property string|null                                                           $intracomunitario
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\TClientes[] $client
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClientsIva whereCuentaContableRepercutido($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClientsIva whereCuentaContableSoportado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClientsIva whereDescripcion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClientsIva whereIdEmpresa($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClientsIva whereIdIva($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClientsIva whereIntracomunitario($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClientsIva whereIva($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClientsIva whereIvaDeArticulo($value)
 * @mixin \Eloquent
 */
class TIvas extends ExternalModel{
	use compoships;
	const CREATED_AT = null;
	const UPDATED_AT = null;

	//protected $connection = 'sqlsrv';


	protected $casts = [
		'id_iva' => 'string',
	];

	protected $guarded = [];
	protected $primaryKey = 'id_iva';
	protected $table = 't_iva';

	public function client() {
		return $this->belongsToMany(TClientes::class, 'id_cliente', 'id_cliente');
	}
	public function management() {
		return $this->belongsToMany(TGerencias::class, 'id_gerencia', 'id_gerencia');
	}
	public function article(){
		return $this->belonsToMany(TArticulos::class,'id_articulo','id_articulo');
	}
	public function commercialContract(){
		return	$this->belongsToMany(TComercialesContratos::class,'id_iva','id_iva');
	}
	public function contractsTypes(){
		return	$this->belongsToMany(TTiposContratosComercial::class,'id_iva','id_iva');

	}

}
