<?php

namespace App\Models\External;

// use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class TVentasEstructuras extends ExternalModel
{

	// use Cachable;
	public const CREATED_AT = null;
	public const UPDATED_AT = null;
	public const DELETED_AT = null;
	protected $casts = [
		'id_empresa'  => 'string',
		'id_venta' => 'int',
		'id_estructura' => 'int'
	];
	protected $guarded = [];
	protected $primaryKey = 'id_venta';
	protected $table = 't_ventas_estructuras';


	public function sales()
	{

		return $this->belongsTo(TVentas::class, 'id_venta', 'id_venta');
	}

}
