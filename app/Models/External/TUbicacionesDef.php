<?php

namespace App\Models\External;

use Awobaz\Compoships\Compoships;
/**
 * App\Models\Client
 *
 * @property string                                                                       $id_empresa      Código de la Empresa
 * @property string                                                                       $id_cliente      Codigo del Cliente
 * @property string|null                                                                  $nombre          Nombre del cliente
 * @property string|null                                                                  $nif             Número del Nif /Cif
 * @property string|null                                                                  $contacto        Nombre el contacto
 * @property string|null                                                                  $direccion       Dirección del Cliente
 * @property string|null                                                                  $direccion2
 * @property string|null                                                                  $codigo_postal   Código Postal del Cliente
 * @property int|null                                                                     $id_provincia    Codigo de la Provincia
 * @property string|null                                                                  $telefono_fijo
 * @property string|null                                                                  $telefono_movil
 * @property string|null                                                                  $fax
 * @property string|null                                                                  $email
 * @property string|null                                                                  $pag_web
 * @property string|null                                                                  $id_comercial    Codigo del comercial
 * @property string|null                                                                  $ccc             Numero de cuenta bancaria
 * @property float|null                                                                   $dto_fra         descuento aplicado al cliente
 * @property float|null                                                                   $dto_rappel
 * @property float|null                                                                   $dto_pp
 * @property float|null                                                                   $prest_servicios
 * @property int|null                                                                     $id_forma_pago   Codigo de forma de pago
 * @property int|null                                                                     $dias_vencimineto
 * @property int|null                                                                     $dia_pago1
 * @property int|null                                                                     $dia_pago2
 * @property int|null                                                                     $dia_pago3
 * @property int|null                                                                     $num_vencimientos
 * @property float|null                                                                   $primer_vencimiento
 * @property int|null                                                                     $intervalo_1vencimiento
 * @property int|null                                                                     $intervalo_entre_vencimientos
 * @property string|null                                                                  $fecha_alta      fecha alta del cliente
 * @property string|null                                                                  $fecha_baja      fecha baja del cliente
 * @property int|null                                                                     $id_estado       Codigo del estado del cliente
 * @property string|null                                                                  $observaciones   Compo de observaciones
 * @property string|null                                                                  $posible_cliente Posible cliente
 * @property string|null                                                                  $bloqueado       Cliente bloqueado
 * @property string|null                                                                  $motivo_bloqueo
 * @property string|null                                                                  $poblacion
 * @property string|null                                                                  $id_usuario      ID. USUARIO
 * @property string|null                                                                  $id_tarifa
 * @property string|null                                                                  $req             Con Recargo de Equivalencia (S/N)
 * @property string|null                                                                  $motivo_baja
 * @property int|null                                                                     $tipo_tarjeta
 * @property string|null                                                                  $nombre_tarjeta
 * @property string|null                                                                  $num_tarjeta
 * @property string|null                                                                  $caducidad_tarjeta
 * @property string|null                                                                  $num_control_tarjeta
 * @property int|null                                                                     $id_sector
 * @property string|null                                                                  $id_actividad
 * @property string|null                                                                  $facturacion
 * @property string|null                                                                  $pagos
 * @property string|null                                                                  $rentabilidad
 * @property string|null                                                                  $entrega
 * @property string|null                                                                  $cuenta_contable
 * @property string|null                                                                  $oficina_bancaria_nombre
 * @property string|null                                                                  $oficina_bancaria_direccion
 * @property string|null                                                                  $f_ult_modif
 * @property string|null                                                                  $usuario_ult_modfi
 * @property string|null                                                                  $observaciones_ped
 * @property string|null                                                                  $observaciones_alb
 * @property string|null                                                                  $observaciones_fact
 * @property string|null                                                                  $albaran_valorado
 * @property string|null                                                                  $tipo_facturacion
 * @property string|null                                                                  $alias
 * @property float|null                                                                   $coste_portes
 * @property float|null                                                                   $coste_comercial
 * @property float|null                                                                   $coste_financiacion
 * @property float|null                                                                   $beneficio
 * @property float|null                                                                   $coste_varios
 * @property string|null                                                                  $descripcion_varios
 * @property float|null                                                                   $coste_administracion
 * @property string|null                                                                  $freq_facturacion
 * @property int|null                                                                     $num_copias_fact
 * @property string|null                                                                  $alb_por_ped
 * @property string|null                                                                  $asegurado
 * @property float|null                                                                   $importe_asegurado
 * @property float|null                                                                   $importe_otorgado
 * @property string|null                                                                  $fecha_alta_riesgo
 * @property string|null                                                                  $fecha_resolucion_riesgo
 * @property string|null                                                                  $cod_asegurado
 * @property string|null                                                                  $id_tipo_albaran
 * @property string|null                                                                  $id_tipo_iva
 * @property int|null                                                                     $id_iva
 * @property int|null                                                                     $id_tipo_irpf
 * @property string|null                                                                  $id_banco_pago
 * @property string|null                                                                  $id_ruta
 * @property string|null                                                                  $notas_lectura
 * @property string|null                                                                  $notas_contables
 * @property string|null                                                                  $notas_riesgo
 * @property string|null                                                                  $id_tipo_dto
 * @property string|null                                                                  $id_tipo_cliente
 * @property string|null                                                                  $id_cadena
 * @property string|null                                                                  $fra_especial
 * @property string|null                                                                  $descripcion_especial
 * @property float|null                                                                      $saldo_cliente
 * @property string|null                                                                     $facturae_una_linea
 * @property string|null                                                                     $observaciones_alb_int
 * @property string|null                                                                     $cliente_stock
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Budget[]              $budgets
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\TDireccionesEntrega[] $directions
 * @property-read \App\Models\ClientsIva                                                     $iva
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\TPedidos[]            $purchases
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereAlbPorPed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereAlbaranValorado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereAlias($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereAsegurado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereBeneficio($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereBloqueado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereCaducidadTarjeta($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereCcc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereClienteStock($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereCodAsegurado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereCodigoPostal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereContacto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereCosteAdministracion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereCosteComercial($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereCosteFinanciacion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereCostePortes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereCosteVarios($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereCuentaContable($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereDescripcionEspecial($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereDescripcionVarios($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereDiaPago1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereDiaPago2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereDiaPago3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereDiasVencimineto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereDireccion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereDireccion2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereDtoFra($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereDtoPp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereDtoRappel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereEntrega($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereFUltModif($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereFacturacion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereFacturaeUnaLinea($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereFax($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereFechaAlta($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereFechaAltaRiesgo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereFechaBaja($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereFechaResolucionRiesgo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereFraEspecial($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereFreqFacturacion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereIdActividad($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereIdBancoPago($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereIdCadena($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereIdCliente($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereIdComercial($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereIdEmpresa($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereIdEstado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereIdFormaPago($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereIdIva($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereIdProvincia($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereIdRuta($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereIdSector($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereIdTarifa($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereIdTipoAlbaran($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereIdTipoCliente($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereIdTipoDto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereIdTipoIrpf($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereIdTipoIva($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereIdUsuario($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereImporteAsegurado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereImporteOtorgado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereIntervalo1vencimiento($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereIntervaloEntreVencimientos($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereMotivoBaja($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereMotivoBloqueo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereNif($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereNombre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereNombreTarjeta($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereNotasContables($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereNotasLectura($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereNotasRiesgo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereNumControlTarjeta($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereNumCopiasFact($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereNumTarjeta($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereNumVencimientos($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereObservaciones($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereObservacionesAlb($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereObservacionesAlbInt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereObservacionesFact($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereObservacionesPed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereOficinaBancariaDireccion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereOficinaBancariaNombre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes wherePagWeb($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes wherePagos($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes wherePoblacion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes wherePosibleCliente($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes wherePrestServicios($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes wherePrimerVencimiento($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereRentabilidad($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereReq($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereSaldoCliente($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereTelefonoFijo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereTelefonoMovil($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereTipoFacturacion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereTipoTarjeta($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TClientes whereUsuarioUltModfi($value)
 * @mixin \Eloquent
 */
class TUbicacionesDef extends ExternalModel {

	const CREATED_AT = null;
	const UPDATED_AT = null;
	use compoships;
	// protected $appends = ['state'];

	//protected $connection = 'sqlsrv';

	protected $casts = [
		'id_empresa' => 'string',
		'id_ubicacion' => 'id_ubicacion',
	];

	protected $guarded = [];
	protected $primaryKey = 'id_ubicacion';
	protected $table = 't_ubicaciones_def';


	public function warehouse() {
		return $this->belongsTo(TAlmacenes::class, 'id_almacen', 'id_almacen');
	}

	public function locations() {

		return $this->hasMany(TUbicaciones::class,['id_ubicacion','id_almacen'],['id_ubicacion','id_almacen']);
	}

	public function management() {
		return $this->belongsTo(TGerencias::class,'id_gerencia','id_gerencia');
	}






}
