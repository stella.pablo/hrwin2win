<?php

namespace App\Models\External;

use \Awobaz\Compoships\Compoships;
class TTiposContratosComercialTramos extends ExternalModel
{
	use compoships;
	public const CREATED_AT = null;
	public const UPDATED_AT = null;

	protected $casts = [
		'id_tramo' => 'int',
		'id_empresa'   => 'string',
	];
	protected $guarded = [];
	public $incrementing = false;
	protected $primaryKey = 'id_tramo';
	protected $table = 't_tipos_contratos_comercial_tramos';



	public function commercialContractsType() {
		return $this->belongsTo(TTiposContratosComercial::class,'id_tipo_contrato_comercial','id_tipo_contrato_comercial');
	}

}
