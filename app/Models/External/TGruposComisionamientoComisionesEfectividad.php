<?php

namespace App\Models\External;
use App\Models\External\ExternalModel;
use Awobaz\Compoships\Compoships;
use Illuminate\Database\Eloquent\Model;

class TGruposComisionamientoComisionesEfectividad extends ExternalModel{
	use compoships;
	const CREATED_AT = null;
	const UPDATED_AT = null;


	protected $guarded = [];
	protected $table = 't_grupos_comisionamiento_comisionesEfectividad';

	public function feeGroup(){

		return $this->BelongsTo(TGruposComisionamiento::class,'id_grupo','id');

	}


}
