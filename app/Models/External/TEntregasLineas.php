<?php

namespace App\Models\External;

use Akaunting\Money\Money;

use Awobaz\Compoships\Compoships;

class TEntregasLineas extends ExternalModel
{

	use compoships;
	const CREATED_AT = null;
	const UPDATED_AT = null;


	//protected $connection = 'sqlsrv';
	protected $casts = [
		'id_empresa' => 'string',
		'id_entrega' => 'string',
	];

	protected $dateFormat = 'Y-m-d H:i:s';
	protected $guarded = [];
	protected $primaryKey = 'lin_entrega';
	protected $table = 't_entregas_gerencias_lin';
	public $incrementing = false;

	public function deliver(){
		return	$this->belongsTo(TEntregas::class,'id_entrega','id_entrega');

	}

	public function article(){
		return $this->hasOne(TArticulos::class,'id_articulo','id_articulo');
	}

	public function package(){
		return $this->hasOne(TArticulosPaquetes::class,['id_articulo','unidades_lote'],['id_articulo','unidades_lote']);
	}




}
