<?php

namespace App\Models\External;

use Akaunting\Money\Money;
use DB;
use Illuminate\Database\Eloquent\Model;


class TEmpresas extends ExternalModel {

	const CREATED_AT = 'fecha_alta';
	const UPDATED_AT = 'f_ult_modificacion';


	//protected $connection = 'sqlsrv';
	protected $casts = [
		'id_empresa'  => 'string',
	];
	protected $dateFormat = 'Y-m-d H:i:s';
	protected $guarded = [];
	protected $primaryKey = 'id_empresa';
	protected $table = 't_empresas';



	public function settlements() {
		return $this->hasMany(TLiquidaciones::class, 'id_empresa', 'id_empresa');
	}


}
