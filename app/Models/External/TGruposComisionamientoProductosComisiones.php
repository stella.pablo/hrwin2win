<?php

namespace App\Models\External;
use Awobaz\Compoships\Compoships;


class TGruposComisionamientoProductosComisiones extends ExternalModel{
	use compoships;
	const CREATED_AT = null;
	const UPDATED_AT = null;


	protected $guarded = [];
	protected $table = 't_grupos_comisionamiento_productos_comisiones';


	public function feeProduct(){
		return $this->BelongsTo(TGruposComisionamientoProductos::class,'id_producto_comision','id');
	}




}
