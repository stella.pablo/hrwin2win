<?php

namespace App\Models\External;

use Illuminate\Database\Eloquent\Model;

class TContratosProductos extends ExternalModel
{
    public const CREATED_AT = null;
    public const UPDATED_AT = null;
    protected $casts = [
        'id_empresa' => 'string',
    ];
    protected $guarded=[];
    protected $primaryKey ='id_producto';
    protected $table = 't_contratos_productos';

    public function product()
    {
        return $this->hasOne(TProductos::class, 'id_producto', 'id_producto');
    }

    public function stretch()
    {
        return $this->hasOne(TproductosTramos::class, 'id_tramo', 'id_tramo');
    }

    public function contract()
    {
        return $this->belongsTo(TContratos::class, 'id_contrato', 'id_contrato');
    }


    public function structures(){
        return $this->hasMany(TContratosProductosEstructuras::class,'id_contrato_producto','id');

    }

    public function settlementConcepts(){
        return $this->hasMany(TLiquidacionesComercialesConceptos::class,'id_contrato_producto','id');
    }
}
