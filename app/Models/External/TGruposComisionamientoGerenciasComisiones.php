<?php

namespace App\Models\External;
use Awobaz\Compoships\Compoships;


class TGruposComisionamientoGerenciasComisiones extends ExternalModel{
	use compoships;
	const CREATED_AT = null;
	const UPDATED_AT = null;


	protected $guarded = [];
	protected $table = 't_grupos_comisionamiento_gerencias_comisiones';


	public function feeManagement(){
		return $this->BelongsTo(TGruposComisionamientoGerencias::class,'id_comision_gerencia','id');
	}




}
