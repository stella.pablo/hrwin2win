<?php

namespace App\Models\External;

// use GeneaLabs\LaravelModelCaching\Traits\Cachable;

/**
 * App\Models\External\TProvincias
 *
 * @property string                                                                            $id_empresa
 * @property int                                                                               $id_provincia
 * @property string|null                                                                       $provincia
 * @property string|null                                                                       $zona
 * @property int|null                                                                          $estado_tsigo
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\External\TPoblaciones[] $cities
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TProvincias newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TProvincias newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TProvincias query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TProvincias whereEstadoTsigo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TProvincias whereIdEmpresa($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TProvincias whereIdProvincia($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TProvincias whereProvincia($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TProvincias whereZona($value)
 * @mixin \Eloquent
 */
class TProvincias extends ExternalModel
{

	// use Cachable;

	protected $casts = [
		'id_empresa' => 'string',
	];
	protected $primaryKey = 'id_provincia';
	protected $table = 't_provincias';

	public function cities()
	{
		return $this->hasMany(TPoblaciones::class, 'id_provincia', 'id_provincia')->orderBy('poblacion');
	}
}
