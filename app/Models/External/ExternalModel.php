<?php

namespace App\Models\External;

use Illuminate\Database\Eloquent\Model;


class ExternalModel extends Model
{

	protected $dateFormat = 'Y-m-d H:i:s';

	public function __construct(array $attributes = [])
	{
		parent::__construct($attributes);

		$this->setConnection(session('database'));
//		$this->setConnection('mexico');
	}
}
