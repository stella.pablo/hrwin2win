<?php

namespace App\Models\External;

/**
 * App\Models\External\TApadrinados
 *
 * @property int                                       $id
 * @property string                                    $codigo
 * @property string                                    $nombre
 * @property int|null                                  $id_contrato
 * @property \Illuminate\Support\Carbon|null           $created_at
 * @property \Illuminate\Support\Carbon|null           $updated_at
 * @property-read \App\Models\External\TContratos|null $contract
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TApadrinados newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TApadrinados newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TApadrinados query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TApadrinados whereCodigo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TApadrinados whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TApadrinados whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TApadrinados whereIdContrato($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TApadrinados whereNombre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TApadrinados whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class TApadrinados extends ExternalModel
{

	protected $dateFormat = 'Y-m-d H:i:s';
	protected $guarded = [];
	protected $table = 't_apadrinados';

	public function contract()
	{
		return $this->belongsTo(TContratos::class, 'id_contrato', 'id_contrato');
	}
}
