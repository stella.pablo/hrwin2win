<?php

namespace App\Models\External;

use Awobaz\Compoships\Compoships;
use App\Models\External\TGruposComisionamientoGerenciasComisiones;

class TGruposComisionamientoGerencias extends ExternalModel
{

	use compoships;
	const CREATED_AT = null;
	const UPDATED_AT = null;


	protected $guarded = [];
	protected $table = 't_grupos_comisionamiento_gerencias';


	public function management()
	{
		return $this->hasOne(TGerencias::class, 'id_gerencia', 'id_gerencia');
	}

	public function feeGroup()
	{
		return $this->belongsTo(TGruposComisionamiento::class, 'id_grupo', 'id');
	}

	public function fees()
	{
		return $this->hasMany(TGruposComisionamientoGerenciasComisiones::class, 'id_comision_gerencia', 'id');
	}


}
