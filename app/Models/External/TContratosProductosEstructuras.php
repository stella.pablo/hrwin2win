<?php

namespace App\Models\External;

/**
 * App\Models\External\TContratosFicheros
 *
 * @property string                                   $id_empresa
 * @property int                                      $id_contrato
 * @property int                                      $id_fichero
 * @property int|null                                 $id_tipo_fichero
 * @property string|null                              $notas
 * @property string|null                              $ruta
 * @property string|null                              $observaciones
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratosFicheros newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratosFicheros newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratosFicheros query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratosFicheros whereIdContrato($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratosFicheros whereIdEmpresa($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratosFicheros whereIdFichero($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratosFicheros whereIdTipoFichero($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratosFicheros whereNotas($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratosFicheros whereObservaciones($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratosFicheros whereRuta($value)
 * @mixin \Eloquent
 * @property-read \App\Models\External\TTiposFicheros $type
 */
class TContratosProductosEstructuras extends ExternalModel
{

	public const CREATED_AT = null;
	public const UPDATED_AT = null;

	protected $casts = [
		'id_empresa'      => 'string',
		'id_contrato' => 'int',
        'id_estructura' => 'int',
	];
	protected $guarded = [];
	protected $table = 't_contratos_productos_estructuras';

    protected $primaryKey ='id';
	public function contracts()
	{
		return $this->belongsTo(TContratos::class, 'id_contrato', 'id_contrato');
	}


	public function contractProduct(){
	    return $this->belongsTo(TContratosProductos::class, 'id_contrato_producto','id');
    }

}
