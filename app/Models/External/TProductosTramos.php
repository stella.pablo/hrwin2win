<?php

namespace App\Models\External;

// use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class TproductosTramos extends ExternalModel
{

    // use Cachable;
    public const CREATED_AT = null;
    public const UPDATED_AT = null;
    public const DELETED_AT = null;
    protected $casts = [
        'id_empresa' => 'string',
    ];
    protected $guarded=[];
    protected $primaryKey ='id';
    protected $table = 't_productos_tramos';

    public function product()
    {
        return $this->belongsTo(Tproductos::class, 'id_producto', 'id_producto');
    }
}
