<?php

namespace App\Models\External;
use Awobaz\Compoships\Compoships;
use App\Models\External\ExternalModel;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ClientsIva
 *
 * @property string                                                             $id_empresa ID EMPRESA
 * @property string                                                             $id_iva     IVA
 * @property float|null                                                         $iva
 * @property string|null                                                           $descripcion
 * @property string|null                                                           $iva_de_articulo
 * @property string|null                                                           $cuenta_contable_repercutido
 * @property string|null                                                           $cuenta_contable_soportado
 * @property string|null                                                           $intracomunitario
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\TClientes[] $client
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClientsIva whereCuentaContableRepercutido($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClientsIva whereCuentaContableSoportado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClientsIva whereDescripcion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClientsIva whereIdEmpresa($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClientsIva whereIdIva($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClientsIva whereIntracomunitario($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClientsIva whereIva($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ClientsIva whereIvaDeArticulo($value)
 * @mixin \Eloquent
 */
class TItaus extends ExternalModel{

	const CREATED_AT = null;
	const UPDATED_AT = null;

	//protected $connection = 'sqlsrv';
	use compoships;

	protected $casts = [
		'id_itau' => 'string',
	];

	protected $guarded = [];
	protected $primaryKey = 'id_itau';
	protected $table = 't_itau';

	public function client() {
		return $this->belongsToMany(TClientes::class, 'id_itau', 'id_itau');
	}
	public function management() {
		return $this->belongsToMany(TGerencias::class, 'id_itau', 'id_itau');
	}
	public function provider() {
		return $this->belongsToMany(TProveedores::class, 'id_itau', 'id_itau');
	}

	public function article(){
		return $this->belonsToMany(TArticulos::class,'id_itau','id_itau');
	}

}
