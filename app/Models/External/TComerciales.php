<?php

namespace App\Models\External;

// use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use App\Models\CandidatesFile;
use App\Models\CommercialsAbsence;
use App\Models\CommercialsReport;
use App\Models\Management;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

/**
 * App\Models\External\TComerciales
 *
 * @property string                                                                          $id_empresa
 * @property string                                                                          $id_comercial
 * @property string|null                                                                     $id_comercial_externo
 * @property string|null                                                                     $id_comercial_externo2
 * @property string|null                                                                     $nombre
 * @property string|null                                                                     $apellido1
 * @property string|null                                                                     $apellido2
 * @property string|null                                                                     $nif
 * @property string|null                                                                     $fecha_nacimiento
 * @property string|null                                                                     $id_gerencia
 * @property string|null                                                                     $id_nivel_funcional
 * @property string|null                                                                     $telefono_fijo
 * @property string|null                                                                     $telefono_movil
 * @property string|null                                                                     $fax
 * @property string|null                                                                     $direccion
 * @property string|null                                                                     $cod_postal
 * @property string|null                                                                     $poblacion
 * @property int|null                                                                        $id_provincia
 * @property string|null                                                                     $id_zona
 * @property string|null                                                                     $email
 * @property \Illuminate\Support\Carbon|null                                                 $fecha_alta
 * @property \Illuminate\Support\Carbon|null                                                 $fecha_baja
 * @property int|null                                                                        $id_estado
 * @property string|null                                                                     $iban
 * @property string|null                                                                     $acceso_mobile
 * @property string|null                                                                     $password
 * @property string|null                                                                     $id_captador
 * @property string|null                                                                     $id_captador_comision
 * @property float|null                                                                      $sueldo_base
 * @property float|null                                                                      $costes_laborales
 * @property float|null                                                                      $otras_retenciones
 * @property string|null                                                                     $fecha_laboral
 * @property string|null                                                                     $fecha_acreditacion_pet
 * @property string|null                                                                     $fecha_acreditacion_rec
 * @property string|null                                                                     $observaciones
 * @property int|null                                                                        $id_irpf
 * @property float|null                                                                      $irpf
 * @property int|null                                                                        $id_iva
 * @property float|null                                                                      $iva
 * @property string|null                                                                     $fact_nombre
 * @property string|null                                                                     $fact_nif
 * @property string|null                                                                     $fact_direccion
 * @property string|null                                                                     $fact_codigo_postal
 * @property string|null                                                                     $fact_poblacion
 * @property int|null                                                                        $fact_id_provincia
 * @property string|null                                                                     $alta_sistema_externo
 * @property string|null                                                                     $fecha_acumulado_ini
 * @property string|null                                                                     $fecha_acumulado_fin
 * @property float|null                                                                      $porcentaje_acumulado
 * @property float|null                                                                      $importe_liquidado
 * @property string|null                                                                     $id_usuario
 * @property \Illuminate\Support\Carbon|null                                                 $fecha
 * @property string|null                                                                     $url_imagen_perfil
 * @property string|null                                                                     $nombre_completo
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\External\TCampanyas[] $campaigns
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\External\TContratos[] $contracts
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\CandidatesFile[]      $files
 * @property-read mixed                                                                      $reports
 * @property-read mixed                                                                      $stats_daily
 * @property-read mixed                                                                      $stats_monthly
 * @property-read \App\Models\External\TGerencias|null                                       $management
 * @property-read \App\Models\Management|null                                                $management_internal
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComerciales newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComerciales newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\External\TComerciales onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComerciales query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComerciales whereAccesoMobile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComerciales whereAltaSistemaExterno($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComerciales whereApellido1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComerciales whereApellido2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComerciales whereCodPostal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComerciales whereCostesLaborales($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComerciales whereDireccion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComerciales whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComerciales whereFactCodigoPostal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComerciales whereFactDireccion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComerciales whereFactIdProvincia($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComerciales whereFactNif($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComerciales whereFactNombre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComerciales whereFactPoblacion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComerciales whereFax($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComerciales whereFecha($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComerciales whereFechaAcreditacionPet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComerciales whereFechaAcreditacionRec($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComerciales whereFechaAcumuladoFin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComerciales whereFechaAcumuladoIni($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComerciales whereFechaAlta($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComerciales whereFechaBaja($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComerciales whereFechaLaboral($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComerciales whereFechaNacimiento($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComerciales whereIban($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComerciales whereIdCaptador($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComerciales whereIdCaptadorComision($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComerciales whereIdComercial($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComerciales whereIdComercialExterno($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComerciales whereIdComercialExterno2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComerciales whereIdEmpresa($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComerciales whereIdEstado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComerciales whereIdGerencia($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComerciales whereIdIrpf($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComerciales whereIdIva($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComerciales whereIdNivelFuncional($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComerciales whereIdProvincia($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComerciales whereIdUsuario($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComerciales whereIdZona($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComerciales whereImporteLiquidado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComerciales whereIrpf($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComerciales whereIva($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComerciales whereNif($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComerciales whereNombre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComerciales whereNombreCompleto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComerciales whereObservaciones($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComerciales whereOtrasRetenciones($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComerciales wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComerciales wherePoblacion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComerciales wherePorcentajeAcumulado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComerciales whereSueldoBase($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComerciales whereTelefonoFijo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComerciales whereTelefonoMovil($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComerciales whereUrlImagenPerfil($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\External\TComerciales withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\External\TComerciales withoutTrashed()
 * @mixin \Eloquent
 */
class TComerciales extends Authenticatable
{

	// use Cachable;
	use SoftDeletes;

	public function __construct(array $attributes = [])
	{
		parent::__construct($attributes);

		$this->setConnection(session('database'));
	}

	public const CREATED_AT = 'fecha';
	public const UPDATED_AT = null;
	public const DELETED_AT = 'fecha_baja';
	public $incrementing = false;
	protected $appends = ['nombre_completo'];
	protected $casts = [
		'id_comercial' => 'string',
		'id_empresa'   => 'string',
	];
	protected $dateFormat = 'Y-m-d H:i:s';
	protected $dates = ['fecha', 'fecha_alta', 'fecha_baja'];
	protected $guarded = [];
	protected $hidden = [
		'acceso_mobile',
		'alta_sistema_externo',
		'costes_laborales',
		'fact_codigo_postal',
		'fact_direccion',
		'fact_id_provincia',
		'fact_nif',
		'fact_nombre',
		'fact_poblacion',
		'fax',
		'fecha_acreditacion_pet',
		'fecha_acreditacion_rec',
		'fecha_acumulado_fin',
		'fecha_acumulado_ini',
		'fecha_laboral',
		'iban',
		'id_captador',
		'id_captador_comision',
		'id_irpf',
		'id_iva',
		'id_usuario',
		'id_zona',
		'importe_liquidado',
		'irpf',
		'iva',
		'observaciones',
		'otras_retenciones',
		'password',
		'porcentaje_acumulado',
		'sueldo_base',
	];
	protected $primaryKey = 'id_comercial';
	protected $table = 't_comerciales';

	public function resolveRouteBinding($value)
	{
		return $this->withTrashed()->find($value) ?? abort(404);
	}

	public function absences() {

		return $this->hasMany(CommercialsAbsence::class, 'external_id');
	}

	public function campaigns()
	{
		return $this->hasManyThrough(
			TCampanyas::class,
			TComercialesCampanyas::class,
			'id_comercial',
			'id',
			'id_comercial',
			'id_campanya'
		)->orderBy('abreviatura');
	}

	public function company_contracts()
	{
		return $this->hasMany(TComercialesContratos::class, 'id_comercial', 'id_comercial');
	}

	public function current_company_contract()
	{
		$ret =  $this->hasOne(TComercialesContratos::class, 'id_comercial', 'id_comercial')
			->where('fecha_desde', '<=', Carbon::now())
			->where('fecha_hasta', '>=', Carbon::now())
			->orWhereNull('fecha_hasta')
			->orderBy('id_comercial_contrato', 'desc');



		if ($ret->count() > 0 ){

			return $ret->with('contractType');


		}else{
			return null;
		}

		//	->with('contractType');
	}

/*	public function getContractTypeAttribute(){


       return TComercialesContratos::where( 'id_comercial', 'id_comercial')
			->where('fecha_desde', '<=', Carbon::now())
            ->where('fecha_hasta', '>=', Carbon::now())
            ->orWhereNull('fecha_hasta')
            ->orderBy('id_comercial_contrato', 'desc')
            ->first('id_tipo_contrato_comercial');

    }*/

	public function contract_last()
	{
		return $this->hasOne(TContratos::class, 'id_comercial', 'id_comercial')->latest('fecha_registro');
	}

	public function contracts()
	{
		return $this
			->hasMany(TContratos::class, 'id_comercial', 'id_comercial');
	}

	public function contracts_ok()
	{
		return $this
			->hasMany(TContratos::class, 'id_comercial', 'id_comercial')
			->whereBetween('fecha_registro', [Carbon::now()->startOfMonth(), Carbon::tomorrow()])
			->whereIn('id_estado', [3, 6, 9]);
	}

	public function contracts_p()
	{
		return $this
			->hasMany(TContratos::class, 'id_comercial', 'id_comercial')
			->whereBetween('fecha_registro', [Carbon::now()->startOfMonth(), Carbon::tomorrow()])
			->whereIn('id_estado', [0, 1, 2]);
	}

	public function contracts_ko()
	{
		return $this
			->hasMany(TContratos::class, 'id_comercial', 'id_comercial')
			->whereBetween('fecha_registro', [Carbon::now()->startOfMonth(), Carbon::tomorrow()])
			->whereIn('id_estado', [4, 5, 7, 8]);
	}

	public function files()
	{

		$company_id = session('company_id');

		return	$this->hasMany(CandidatesFile::class, 'external_id', 'id_comercial')
				->whereHas('user', function ($query) use ($company_id) {
					$query->where('company_id', $company_id);
				});

	}

	public function management()
	{
		return $this->belongsTo(TGerencias::class, 'id_gerencia', 'id_gerencia');
	}

    public function management_internal()
    {
        $company_id = session('company_id');
        return $this
            ->belongsTo(Management::class, 'id_gerencia', 'external_id')
            ->where('company_id', $company_id);
    }


	public function strategy()
	{
		return $this->belongsTo(TEstrategias::class, 'id_estrategia');
	}
	public function structures(){
		return $this->hasMany(TComercialesEstructuras::class,'id_comercial','id_comercial');
	}


	public function getReportsAttribute()
	{
		return CommercialsReport::where('external_id', $this->id_comercial)->get();
	}

	public function getNameAttribute() {
		return $this->nombre_completo;
	}



	public function getNombreAttribute($value) {
		return ucwords(mb_strtolower($value));
	}

	public function getApellido1Attribute($value) {
		return ucwords(mb_strtolower($value));
	}

	public function getApellido2Attribute($value) {
		return ucwords(mb_strtolower($value));
	}

	public function getNombreCompletoAttribute()
	{
		return property_exists($this, 'nombre_completo')
			? ucwords(mb_strtolower($this->nombre_completo))
			: trim(preg_replace('/\s+/', ' ', $this->nombre . ' ' . $this->apellido1 . ' ' . $this->apellido2));
	}

	public function getUrlImagenPerfilAttribute()
	{
		return env('APP_URL') . ($this->attributes['url_imagen_perfil'] ?? 'images/avatar.png');
	}

	public function getIsManagerAttribute() {
		return $this->id_comercial === $this->management->id_comercial_liquidacion;
	}

	public function commercialSettlements()
	{
		return $this->hasMany(TLiquidacionesComerciales::class, 'id_comercial', 'id_comercial');
	}

	public function settlementConcepts()
	{
		return $this->hasMany(TLiquidacionesComercialesConceptos::class, 'id_comercial', 'id_comercial');
	}


	public function getUserTreeAttribute()
	{
		$id_comercial = $this->id_comercial;

		$userTree = DB::connection(session('database'))->select('select * from  [dbo].f_get_estructura_comercial(?) as result', [$id_comercial]);

		$RetTree = [];

		foreach ($userTree as &$Management) {
			if ($Management->nivel === 1) {
				$RetTree[] = $Management;
			}
		}


		$Tree = [];


		foreach ($RetTree as $main) {
			$found = false;
			$main->management = TGerencias::find($main->id_gerencia);
			$main->commercial = TComerciales::find($main->id_comercial);

			$this->getChildrenSingle($userTree, $main);

			foreach ($Tree as &$item) {

				//si la gerencia de nivel 1 comparte  la id con otra, es que es la misma, deshojo los hijos y se los añado
				if ($item->id_comercial === $main->id_comercial) {

					foreach ($main->child as $children) {

						$item->child[] = $children;
					}
					$found = true;
				}
			}
			if (!$found) {
				$Tree[] = $main;
			}


		}


		return $Tree;


	}

	function getChildrenSingle(&$userTree, &$parent)
	{


		foreach ($userTree as $item) {


			//si item es hijo de parent y no es comercial
			if ($item->id_comercial === $parent->id_comercial_hijo AND
				$item->nivel === $parent->nivel + 1 AND
				$item->id_nivel_funcional === '001' AND
				$item->id_gerencia === $parent->id_gerencia AND
				$item->id_gerencia_ref <> $parent->id_gerencia
			) {

				//busco los hijos de ese parent
				$item->management = TGerencias::find($item->id_gerencia_ref);
				$item->commercial = TComerciales::find($item->id_comercial_hijo);

				$this->getChildrenSingle($userTree, $item);

				$parent->child[] = $item;


			} elseif ($item->id_comercial === $parent->id_comercial_hijo AND
				$item->nivel === $parent->nivel + 1 AND
				$item->id_nivel_funcional === '001' AND
				$item->id_gerencia <> $parent->id_gerencia AND
				$item->id_gerencia_ref <> $parent->id_gerencia
			) {

				//busco los hijos de ese parent sub
				$item->management = TGerencias::find($item->id_gerencia_ref);
				$item->commercial = TComerciales::find($item->id_comercial_hijo);

				$this->getChildrenSingle($userTree, $item);

				$parent->child[] = $item;


			} elseif
			(
				$item->id_comercial === $parent->id_comercial AND
				$item->nivel === $parent->nivel + 1 AND
				$item->id_nivel_funcional === '003' AND
				$item->id_gerencia === $parent->id_gerencia AND
				$item->id_gerencia_ref === $parent->id_gerencia_ref


			) {


				$item->management = TGerencias::find($item->id_gerencia_ref);
				$item->commercial = TComerciales::find($item->id_comercial_hijo);
				$parent->child[] = $item;


			} elseif (
				$item->id_comercial === $parent->id_comercial_hijo AND
				$item->nivel === $parent->nivel + 1 AND
				$item->id_nivel_funcional === '003' AND
				$parent->id_nivel_funcional === '001' AND
				$item->id_gerencia === $item->id_gerencia_ref AND
				$item->id_gerencia_ref === $parent->id_gerencia_ref

			) {
				$item->management = TGerencias::find($item->id_gerencia_ref);
				$item->commercial = TComerciales::find($item->id_comercial_hijo);
				$parent->child[] = $item;

			}


		}
	}

	public function paymentproofs () {
		return $this->hasMany(TComprobantesPago::class, 'id_comercial', 'id_comercial');
	}


	public	function getCleanTreeAttribute()
	{

		$first = Arr::pluck(DB::connection(session('database'))->select('select distinct id_comercial_hijo from  [dbo].f_get_estructura_comercial(?) as result', [
			$this->id_comercial,
		]),
			'id_comercial_hijo'
		);


		if (sizeof($first) ===0 ){

			$first[] = $this->id_comercial;

		}

		return $first;

	}




	function getAvailableManagementsAttribute()
	{
		$id_comercial = $this->id_comercial;

		if ($this->id_nivel_funcional <> '001') {
			return [];
		}

		$userTree = DB::connection(session('database'))->select('select * from  [dbo].f_get_estructura_comercial(?) as result', [$id_comercial]);


		$managements = array_unique(array_column($userTree, 'id_gerencia'));

		return TGerencias::wherein('id_gerencia', $managements)->with('commercials')->get();


	}


}
