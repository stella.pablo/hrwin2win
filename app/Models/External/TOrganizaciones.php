<?php

namespace App\Models\External;

use Cache;

/**
 * App\Models\External\TOrganizaciones
 *
 * @property int                                                                             $id
 * @property string                                                                          $organizacion
 * @property \Illuminate\Support\Carbon|null                                                 $created_at
 * @property \Illuminate\Support\Carbon|null                                                 $updated_at
 * @property string|null                                                                     $id_comercial
 * @property-read mixed                                                                      $stats
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\External\TGerencias[] $managements
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TOrganizaciones newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TOrganizaciones newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TOrganizaciones query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TOrganizaciones whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TOrganizaciones whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TOrganizaciones whereIdComercial($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TOrganizaciones whereOrganizacion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TOrganizaciones whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class TOrganizaciones extends ExternalModel
{

	protected $guarded = [];
	protected $table = 't_organizaciones';

	public function getStatsAttribute()
	{
		$request = request();

		$managementsIds = Cache::remember('organization:' . $this->id . ':managementsIds', 5 * 60, function () {
			return $this->managements->pluck('id_gerencia')->toArray();
		});

		$commercialsCount = Cache::remember('organization:' . $this->id . ':commercials', 5 * 60, function () use ($managementsIds) {
			return TComerciales::whereIn('id_gerencia', $managementsIds)->count();
		});

		$commercialsIds = TComerciales::whereIn('id_gerencia', $managementsIds)->get()->pluck('id_comercial')->toArray();

		$c_ok = TContratos
			::whereIn('id_estado', [6, 9])
			->whereIn('id_comercial', $commercialsIds)
			->whereBetween('fecha_registro', [$request->date_from, $request->date_to])
			->count();

		$c_p = TContratos
			::whereIn('id_estado', [0, 1, 2, 3])
			->whereIn('id_comercial', $commercialsIds)
			->whereBetween('fecha_registro', [$request->date_from, $request->date_to])
			->count();

		$c_ko = TContratos
			::whereIn('id_estado', [4, 5, 7, 8])
			->whereIn('id_comercial', $commercialsIds)
			->whereBetween('fecha_registro', [$request->date_from, $request->date_to])
			->count();

		return [
			'c_ok'        => intval($c_ok),
			'c_p'         => intval($c_p),
			'c_ko'        => intval($c_ko),
			'c_total'     => intval($c_ok) + intval($c_p) + intval($c_ko),
			'managements' => count($managementsIds),
			'commercials' => intval($commercialsCount),
		];
	}

	public function managements()
	{
		return $this->hasMany(TGerencias::class, 'id_organizacion');
	}
}
