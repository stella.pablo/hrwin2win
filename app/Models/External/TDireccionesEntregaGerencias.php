<?php

namespace App\Models\External;
use Awobaz\Compoships\Compoships;
/**
 * App\Models\ClientsDirection
 *
 * @property string                  $id_empresa
 * @property string                  $id_cliente
 * @property string                  $id_direccion
 * @property int|null                $id_tipo_direccion
 * @property string|null             $nombre_direccion
 * @property string|null             $contacto_direccion
 * @property string|null             $direccion
 * @property string|null             $codigo_postal
 * @property int|null                $id_poblacion
 * @property string|null             $telefono_fijo
 * @property string|null             $telefono_movil
 * @property string|null             $fax
 * @property string|null             $email
 * @property string|null             $email2
 * @property string|null             $id_ruta
 * @property string|null             $observaciones
 * @property string|null             $poblacion
 * @property int|null                $id_provincia
 * @property string|null             $hora_lunes_m_ini
 * @property string|null             $hora_lunes_m_fin
 * @property string|null             $hora_lunes_t_ini
 * @property string|null             $hora_lunes_t_fin
 * @property string|null             $hora_martes_m_ini
 * @property string|null             $hora_martes_m_fin
 * @property string|null             $hora_martes_t_ini
 * @property string|null             $hora_martes_t_fin
 * @property string|null             $hora_miercoles_m_ini
 * @property string|null             $hora_miercoles_m_fin
 * @property string|null             $hora_miercoles_t_ini
 * @property string|null             $hora_miercoles_t_fin
 * @property string|null             $hora_jueves_m_ini
 * @property string|null             $hora_jueves_m_fin
 * @property string|null             $hora_jueves_t_ini
 * @property string|null             $hora_jueves_t_fin
 * @property string|null             $hora_viernes_m_ini
 * @property string|null             $hora_viernes_m_fin
 * @property string|null             $hora_viernes_t_ini
 * @property string|null             $hora_viernes_t_fin
 * @property string|null             $hora_sabado_m_ini
 * @property string|null             $hora_sabado_m_fin
 * @property string|null             $hora_sabado_t_ini
 * @property string|null             $hora_sabado_t_fin
 * @property string|null             $hora_domingo_m_ini
 * @property string|null             $hora_domingo_m_fin
 * @property string|null             $hora_domingo_t_ini
 * @property string|null             $hora_domingo_t_fin
 * @property string|null             $hora_todos_m_ini
 * @property string|null             $hora_todos_m_fin
 * @property string|null             $hora_todos_t_ini
 * @property string|null                $hora_todos_t_fin
 * @property float|null                 $orden_ruta ORDEN DE VISITA DENTRO DE UNA RUTA
 * @property string|null                $vacaciones_desde_1
 * @property string|null                $vacaciones_hasta_1
 * @property string|null                $vacaciones_desde_2
 * @property string|null                $vacaciones_hasta_2
 * @property string|null                $atencion
 * @property string|null                $division
 * @property int|null                   $id_pais
 * @property-read \App\Models\TClientes $client
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TDireccionesEntrega whereAtencion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TDireccionesEntrega whereCodigoPostal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TDireccionesEntrega whereContactoDireccion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TDireccionesEntrega whereDireccion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TDireccionesEntrega whereDivision($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TDireccionesEntrega whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TDireccionesEntrega whereEmail2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TDireccionesEntrega whereFax($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TDireccionesEntrega whereHoraDomingoMFin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TDireccionesEntrega whereHoraDomingoMIni($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TDireccionesEntrega whereHoraDomingoTFin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TDireccionesEntrega whereHoraDomingoTIni($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TDireccionesEntrega whereHoraJuevesMFin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TDireccionesEntrega whereHoraJuevesMIni($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TDireccionesEntrega whereHoraJuevesTFin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TDireccionesEntrega whereHoraJuevesTIni($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TDireccionesEntrega whereHoraLunesMFin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TDireccionesEntrega whereHoraLunesMIni($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TDireccionesEntrega whereHoraLunesTFin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TDireccionesEntrega whereHoraLunesTIni($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TDireccionesEntrega whereHoraMartesMFin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TDireccionesEntrega whereHoraMartesMIni($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TDireccionesEntrega whereHoraMartesTFin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TDireccionesEntrega whereHoraMartesTIni($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TDireccionesEntrega whereHoraMiercolesMFin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TDireccionesEntrega whereHoraMiercolesMIni($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TDireccionesEntrega whereHoraMiercolesTFin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TDireccionesEntrega whereHoraMiercolesTIni($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TDireccionesEntrega whereHoraSabadoMFin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TDireccionesEntrega whereHoraSabadoMIni($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TDireccionesEntrega whereHoraSabadoTFin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TDireccionesEntrega whereHoraSabadoTIni($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TDireccionesEntrega whereHoraTodosMFin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TDireccionesEntrega whereHoraTodosMIni($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TDireccionesEntrega whereHoraTodosTFin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TDireccionesEntrega whereHoraTodosTIni($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TDireccionesEntrega whereHoraViernesMFin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TDireccionesEntrega whereHoraViernesMIni($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TDireccionesEntrega whereHoraViernesTFin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TDireccionesEntrega whereHoraViernesTIni($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TDireccionesEntrega whereIdCliente($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TDireccionesEntrega whereIdDireccion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TDireccionesEntrega whereIdEmpresa($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TDireccionesEntrega whereIdPais($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TDireccionesEntrega whereIdPoblacion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TDireccionesEntrega whereIdProvincia($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TDireccionesEntrega whereIdRuta($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TDireccionesEntrega whereIdTipoDireccion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TDireccionesEntrega whereNombreDireccion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TDireccionesEntrega whereObservaciones($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TDireccionesEntrega whereOrdenRuta($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TDireccionesEntrega wherePoblacion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TDireccionesEntrega whereTelefonoFijo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TDireccionesEntrega whereTelefonoMovil($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TDireccionesEntrega whereVacacionesDesde1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TDireccionesEntrega whereVacacionesDesde2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TDireccionesEntrega whereVacacionesHasta1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TDireccionesEntrega whereVacacionesHasta2($value)
 * @mixin \Eloquent
 */
class TDireccionesEntregaGerencias extends ExternalModel {

	const CREATED_AT = null;
	const UPDATED_AT = null;
	use compoships;
	//protected $connection = 'sqlsrv';


	protected $casts = [
		'id_direccion' => 'int',
	];

	protected $guarded = [];
	protected $primaryKey = 'id_direccion';
	protected $table = 't_direcciones_entrega_gerencias';

	public function management() {
		return $this->belongsTo(TGerencias::class, 'id_gerencia', 'id_gerencia');
	}

	public function province() {
		return $this->hasOne(TProvincias::class, 'id_provincia', 'id_provincia');
	}


}
