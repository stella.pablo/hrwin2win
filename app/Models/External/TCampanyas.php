<?php

namespace App\Models\External;

// use GeneaLabs\LaravelModelCaching\Traits\Cachable;

/**
 * App\Models\External\TCampanyas
 *
 * @property int         $id
 * @property string      $id_empresa
 * @property string|null $abreviatura
 * @property string|null $nombre
 * @property string|null $fecha_alta
 * @property string|null $fecha_baja
 * @property int|null    $estado
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TCampanyas newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TCampanyas newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TCampanyas query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TCampanyas whereAbreviatura($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TCampanyas whereEstado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TCampanyas whereFechaAlta($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TCampanyas whereFechaBaja($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TCampanyas whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TCampanyas whereIdEmpresa($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TCampanyas whereNombre($value)
 * @mixin \Eloquent
 */
class TCampanyas extends ExternalModel
{

	// use Cachable;

	protected $casts = [
		'id_empresa' => 'string',
	];
	protected $table = 't_campanyas';
}
