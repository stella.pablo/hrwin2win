<?php

namespace App\Models\External;

// use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\External\TComercialesCampanyas
 *
 * @property int                                                                               $id
 * @property string                                                                            $id_empresa
 * @property int                                                                               $id_campanya
 * @property string                                                                            $id_comercial
 * @property \Illuminate\Support\Carbon|null                                                   $fecha_alta
 * @property \Illuminate\Support\Carbon|null                                                   $fecha_baja
 * @property bool|null                                                                         $estado
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\External\TComerciales[] $commercials
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComercialesCampanyas newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComercialesCampanyas newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\External\TComercialesCampanyas onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComercialesCampanyas query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComercialesCampanyas whereEstado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComercialesCampanyas whereFechaAlta($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComercialesCampanyas whereFechaBaja($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComercialesCampanyas whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComercialesCampanyas whereIdCampanya($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComercialesCampanyas whereIdComercial($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComercialesCampanyas whereIdEmpresa($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\External\TComercialesCampanyas withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\External\TComercialesCampanyas withoutTrashed()
 * @mixin \Eloquent
 */
class TComercialesCampanyas extends ExternalModel
{

	// use Cachable;
	use SoftDeletes;

	public const CREATED_AT = 'fecha_alta';
	public const UPDATED_AT = null;
	public const DELETED_AT = 'fecha_baja';
	public $incrementing = true;
	protected $casts = [
		'id_comercial' => 'string',
		'id_empresa'   => 'string',
		'estado'       => 'boolean',
	];
	protected $guarded = [];
	protected $table = 't_comerciales_campanyas';

	protected static function boot()
	{
		parent::boot();

		self::creating(function ($tComercialesCampanyas) {
			$tComercialesCampanyas->id_empresa = '01';
			$tComercialesCampanyas->estado = true;
		});
	}

	public function commercials()
	{
		return $this->hasMany(TComerciales::class, 'id_comercial', 'id_comercial');
	}
}
