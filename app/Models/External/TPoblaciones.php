<?php

namespace App\Models\External;

// use GeneaLabs\LaravelModelCaching\Traits\Cachable;

/**
 * App\Models\External\TPoblaciones
 *
 * @property string                                                                        $id_empresa
 * @property int                                                                           $id_poblacion
 * @property string|null                                                                   $poblacion
 * @property int|null                                                                      $id_provincia
 * @property string|null                                                                   $codigo_postal
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\External\TLugares[] $places
 * @property-read \App\Models\External\TProvincias|null                                    $province
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TPoblaciones newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TPoblaciones newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TPoblaciones query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TPoblaciones whereCodigoPostal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TPoblaciones whereIdEmpresa($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TPoblaciones whereIdPoblacion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TPoblaciones whereIdProvincia($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TPoblaciones wherePoblacion($value)
 * @mixin \Eloquent
 */
class TPoblaciones extends ExternalModel
{

	// use Cachable;

	protected $casts = [
		'id_empresa' => 'string',
	];
	protected $primaryKey = 'id_poblacion';
	protected $table = 't_poblaciones';

	public function places()
	{
		return $this->hasMany(TLugares::class, 'id_poblacion', 'id_poblacion')->orderBy('lugar');
	}

	public function province()
	{
		return $this->belongsTo(TProvincias::class, 'id_provincia', 'id_provincia');
	}
}
