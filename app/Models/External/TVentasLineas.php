<?php

namespace App\Models\External;

// use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class TVentasLineas extends ExternalModel
{

    // use Cachable;
    public const CREATED_AT = null;
    public const UPDATED_AT = null;
    public const DELETED_AT = null;
	protected $casts = [
		'id_empresa'  => 'string',
		'id_articulo' => 'string',
	];
    protected $guarded=[];
    protected $primaryKey ='id_linea';
    protected $table = 't_ventas_lineas';
	protected $with= ['article'];



    public function sale()
    {
        return $this->belongsTo(TVentas::class, 'id_venta', 'id_venta');
    }

    public function article()
	{
		return $this-> HasOne(TArticulos::class,'id_articulo', 'id_articulo');
	}

	public function article_package()
    {
    	return $this->hasOne(TarticulosPaquetes::class,'unidades_lote','unidades_lote');
	}

    public function settlementConcepts()
    {
        return $this->hasMany(TLiquidacionesComercialesConceptos::class,'id_linea_venta','id_linea');
    }

    public function deliveryLine()
    {
        return $this->hasOne(TEntregasLineas::class,['id_entrega','lin_entrega'],['id_entrega','lin_entrega']);
    }

    public function deliveryNoteLine()
    {
        return $this->hasOne(TAlbaranesLineas::class,['id_albaran','lin_albaran'],['id_albaran','lin_albaran']);
    }

}
