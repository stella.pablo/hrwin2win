<?php

namespace App\Models\External;

/**
 * App\Models\External\TEstrategias
 *
 * @property int                             $id
 * @property string                          $id_gerencia
 * @property string                          $id_estrategia
 * @property string                          $estrategia
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TEstrategias newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TEstrategias newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TEstrategias query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TEstrategias whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TEstrategias whereEstrategia($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TEstrategias whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TEstrategias whereIdEstrategia($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TEstrategias whereIdGerencia($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TEstrategias whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class TEstrategias extends ExternalModel
{

	protected $guarded = [];
	protected $table = 't_estrategias';
}
