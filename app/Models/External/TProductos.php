<?php

namespace App\Models\External;

// use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class TProductos extends ExternalModel
{

    // use Cachable;
    public const CREATED_AT = null;
    public const UPDATED_AT = null;
    public const DELETED_AT = null;
    protected $casts = [
    	'id_producto' => 'int',
        'id_empresa' => 'string',
    ];
    protected $guarded=[];
    protected $primaryKey ='id_producto';
    protected $table = 't_productos';

    public function stretches()
    {
        return $this->hasMany(TProductosTramos::class, 'id_producto', 'id_producto');
    }
}
