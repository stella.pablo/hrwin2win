<?php

namespace App\Models\External;

use Akaunting\Money\Money;

use Awobaz\Compoships\Compoships;

class TAlbaranes extends ExternalModel
{

	const CREATED_AT = 'fecha';
	const UPDATED_AT = null;

	use compoships;
	//protected $connection = 'sqlsrv';
	protected $casts = [
		'id_empresa' => 'string',
		'id_albaran' => 'string',
	];

	protected $dateFormat = 'Y-m-d H:i:s';
	protected $guarded = [];
	protected $primaryKey = 'id_albaran';
	protected $table = 't_albaranes_cab';
	public $incrementing = false;

	public function management(){
		return	$this->belongsTo(TGerencias::class,'id_gerencia','id_gerencia');

	}

	public function lines(){
		return $this->hasMany(TAlbaranesLineas::class,'id_albaran','id_albaran');
	}

	public function settlementConcepts(){
		return $this->hasMany(TLiquidacionesComercialesConceptos::class,'id_albaran','id_albaran');
	}





}
