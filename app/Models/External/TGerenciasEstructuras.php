<?php

namespace App\Models\External;

// use GeneaLabs\LaravelModelCaching\Traits\Cachable;

/**
 * App\Models\External\TProvincias
 *
 * @property string                                                                            $id_empresa
 * @property int                                                                               $id_provincia
 * @property string|null                                                                       $provincia
 * @property string|null                                                                       $zona
 * @property int|null                                                                          $estado_tsigo
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\External\TPoblaciones[] $cities
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TProvincias newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TProvincias newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TProvincias query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TProvincias whereEstadoTsigo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TProvincias whereIdEmpresa($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TProvincias whereIdProvincia($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TProvincias whereProvincia($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TProvincias whereZona($value)
 * @mixin \Eloquent
 */
class TGerenciasEstructuras extends ExternalModel
{

	// use Cachable;
	const CREATED_AT = null;
	const UPDATED_AT = null;

	protected $casts = [
		'id_gerencia' => 'string',
		'id_gerencia_estructura' =>'string'
	];
	public $incrementing = false;
	protected $primaryKey = 'id_gerencia_estructura';
	protected $table = 't_gerencias_estructuras';
	protected $guarded = [];
	public function management()
	{
		return $this->belongsTo(TGerencias::class, 'id_gerencia', 'id_gerencia');
	}
}
