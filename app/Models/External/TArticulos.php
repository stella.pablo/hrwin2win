<?php

namespace App\Models\External;

use Akaunting\Money\Money;
use DB;
use Illuminate\Database\Eloquent\Model;

use \Awobaz\Compoships\Compoships;

class TArticulos extends ExternalModel {

	const CREATED_AT = 'fecha_alta';
	const UPDATED_AT = 'f_ult_modificacion';
/*
	protected $appends = ['available','quantity'];*/
	//protected $connection = 'sqlsrv';
	protected $casts = [
		'id_empresa'  => 'string',
		'id_articulo' => 'string',
	];
	protected $dateFormat = 'Y-m-d H:i:s';
	protected $guarded = [];
	protected $primaryKey = 'id_articulo';
	protected $table = 't_articulos';
	protected $with = ['iva','category', 'itau'];

	//use compoships;

	public function category() {
		return $this->belongsTo(TFamilias::class, 'id_familia', 'id_familia');
	}

/*	public function getQuantityAttribute() {
		//PARCHE UNITARIOS

		$quantity = TGerenciasInventarios::where('id_articulo', $this->id_articulo)->where('unidades_lote', $this->unidades_lote)->where('id_gerencia',auth()->user()->id_gerencia)->sum('cantidad');
		//$quantity = TGerenciasInventarios::where('id_articulo', $this->id_articulo)->where('unidades_lote', 1)->where('id_gerencia',auth()->user()->id_gerencia)->sum('cantidad');
		return $quantity ?? 0;
	}*/

/*	public function getAvailableAttribute() {

		return !!$this->quantity;
	}*/




	//
	// public function photos()
	// {
	// 	return $this->hasMany(ProductPhoto::class)->orderBy('order', 'asc');
	// }
	//


	//



	public function iva(){
		return $this->HasOne(TIvas::class,'id_iva','id_iva')->where('id_empresa',auth()->user()->id_empresa);
	}
	public function itau() {
		return $this->hasOne(TItaus::class, 'id_itau', 'id_itau')->where('id_empresa',auth()->user()->id_empresa);
	}
	public function packages(){
		return $this->hasMany(TArticulosPaquetes::class,'id_articulo','id_articulo');

	}

/*	public function managementInventories(){

		//return $this->hasMany(TGerenciasInventarios::class,['id_articulo','unidades_lote'],['id_articulo','unidades_lote']);
		return $this->hasMany(TGerenciasInventarios::class,'id_articulo','id_articulo');

	}*/

	public function sales() {
		return $this->belongsToMany(TVentas::class, 't_ventas_lineas', 'id_articulo', 'id_venta')
			->take(5)
			->orderBy('id_linea', 'desc');
	}



	/*
	 * Laratables
	 */

	public static function laratablesOrderName() {
		return 'id_articulo';
	}



//	public function laratablesUnidadPrecio() {
//		return Money::EUR($this->unidad_precio);
//	}

}
