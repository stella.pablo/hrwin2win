<?php

namespace App\Models\External;

use Awobaz\Compoships\Compoships;


class TLiquidacionesComercialesConceptos extends ExternalModel
{

	use compoships;
	public const CREATED_AT = null;
	public const UPDATED_AT = null;
	public const DELETED_AT = null;
	protected $casts = [
		'id' => 'int',
	];
	protected $guarded = [];
	protected $table = 't_liquidaciones_comerciales_conceptos';


	public function commercialSettlement()
	{
		return $this->belongsTo(TLiquidacionesComerciales::class, 'id_liquidacion_comercial', 'id');
	}

	public function contract()
	{
		return $this->belongsTo(TContratos::class, 'id_contrato', 'id_contrato');
	}

	public function sale()
	{
		return $this->belongsTo(TVentas::class, 'id_venta', 'id_venta');
	}

	public function line()
	{
		return $this->belongsTo(TVentasLineas::class, 'id_linea_venta', 'id_linea');
	}

	public function group()
	{
		return $this->belongsTo(TGruposComisionamiento::class, 'id_grupo', 'id');
	}

	public function delivery()
	{

		return $this->belongsTo(TEntregas::class, 'id_entrega', 'id_entrega');
	}

	public function deliveryNote()
	{

		return $this->belongsTo(TAlbaranes::class, 'id_albaran', 'id_albaran');
	}

	public function deliveryLine()
	{

		return $this->hasOne(TEntregasLineas::class, ['id_entrega', 'lin_entrega'], ['id_entrega', 'lin_entrega']);
	}

	public function deliveryNoteLine()
	{

		return $this->hasOne(TAlbaranesLineas::class, ['id_albaran', 'lin_albaran'], ['id_albaran', 'lin_albaran']);
	}

	public function product()
	{
		return $this->belongsTo(TContratosProductos::class, 'id_contrato_producto', 'id');
	}

}
