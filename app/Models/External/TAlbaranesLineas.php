<?php

namespace App\Models\External;

use Akaunting\Money\Money;

use Awobaz\Compoships\Compoships;
class TAlbaranesLineas extends ExternalModel
{

	use compoships;
	const CREATED_AT = 'fecha';
	const UPDATED_AT = null;


	//protected $connection = 'sqlsrv';
	protected $casts = [
		'id_empresa'  => 'string',
		'lin_albaran' => 'int',
	];

	protected $dateFormat = 'Y-m-d H:i:s';
	protected $guarded = [];
	protected $primaryKey = 'lin_albaran';
	protected $table = 't_albaranes_lin';
	public $incrementing = false;

	public function management()
	{
		return $this->belongsTo(TGerencias::class, 'id_gerencia', 'id_gerencia');
	}

	public function deliveryNote()
	{
		return $this->belongsTo(TAlbaranes::class, 'id_albaran', 'id_albaran');
	}


}
