<?php

namespace App\Models\External;

use DB;

use \Awobaz\Compoships\Compoships;
class TArticulosPaquetes extends ExternalModel
{

	// use Cachable;
	use compoships;
	public const CREATED_AT = null;
	public const UPDATED_AT = null;
	public const DELETED_AT = null;
	protected $casts = [
		'id_empresa'    => 'string',
		'id_linea'      => 'int',
		'unidades_lote' => 'numeric',
		'id_articulo'   => 'varchar',
	];
	protected $guarded = [];
	protected $primaryKey = 'id_linea';
	protected $table = 't_articulos_paquetes';
	/*protected $with = ['providers'];*/



	public function article()
	{
		return $this->BelongsTo(TArticulos::class, 'id_articulo', 'id_articulo');
	}

	public function salelin()
	{
		return $this->belongsTo(TVentasLineas::class, 'unidades_lote', 'unidades_lote');
	}

	public function orderlin()
	{
		return $this->belongsTo(TPedidosLin::class, 'unidades_lote', 'unidades_lote');
	}



}
