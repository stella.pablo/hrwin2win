<?php

namespace App\Models\External;

use App\Models\User;
use Carbon\Carbon;

/**
 * App\Models\External\TContratos
 *
 * @property string                                                                                  $id_empresa
 * @property int                                                                                     $id_contrato
 * @property string|null                                                                             $id_tipo_contrato
 * @property string|null                                                                             $tipo_contrato
 * @property string|null                                                                             $codigo_verificacion
 * @property string|null                                                                             $codigo_cig
 * @property string|null                                                                             $ps_tipo_via
 * @property string|null                                                                             $ps_direccion
 * @property string|null                                                                             $ps_numero
 * @property string|null                                                                             $ps_duplicador
 * @property string|null                                                                             $ps_esc
 * @property string|null                                                                             $ps_piso
 * @property string|null                                                                             $ps_letra
 * @property string|null                                                                             $ps_codigo_postal
 * @property string|null                                                                             $ps_poblacion
 * @property int|null                                                                                $ps_id_provincia
 * @property string|null                                                                             $ps_cups_electricidad
 * @property string|null                                                                             $ps_cups_gas
 * @property string|null                                                                             $ps_cnae
 * @property string|null                                                                             $cliente_nombre
 * @property string|null                                                                             $cliente_apellidos
 * @property string|null                                                                             $cliente_tlf1
 * @property string|null                                                                             $cliente_tlf2
 * @property string|null                                                                             $cliente_email
 * @property string|null                                                                             $cliente_direccion_suministro
 * @property string|null                                                                             $cliente_tipo_via
 * @property string|null                                                                             $cliente_direccion
 * @property string|null                                                                             $cliente_numero
 * @property string|null                                                                             $cliente_duplicador
 * @property string|null                                                                             $cliente_esc
 * @property string|null                                                                             $cliente_piso
 * @property string|null                                                                             $cliente_letra
 * @property string|null                                                                             $cliente_poblacion
 * @property int|null                                                                                $cliente_id_provincia
 * @property int|null                                                                                $id_representante_tipo
 * @property string|null                                                                             $representante_nombre
 * @property string|null                                                                             $representante_apellidos
 * @property string|null                                                                             $representante_nif
 * @property string|null                                                                             $marca_colaboradora
 * @property int|null                                                                                $id_emp_colaboradora
 * @property string|null                                                                             $emp_colaboradora_tarjeta
 * @property string|null                                                                             $contrata_luz
 * @property float|null                                                                              $potencia_elec1
 * @property float|null                                                                              $potencia_elec2
 * @property float|null                                                                              $potencia_elec3
 * @property int|null                                                                                $id_tarifa_elec
 * @property string|null                                                                             $cbo_cmrclzdr_elec
 * @property string|null                                                                             $cbo_cmrclzdr_con_cbos
 * @property string|null                                                                             $cambio_titular_elec
 * @property string|null                                                                             $alta_directa_elec
 * @property float|null                                                                              $descuento_elec
 * @property float|null                                                                              $consumo_estimado
 * @property string|null                                                                             $contrata_gas
 * @property int|null                                                                                $id_tarifa_gas
 * @property string|null                                                                             $cbo_cmrclzdr_gas
 * @property string|null                                                                             $cambio_titular_gas
 * @property string|null                                                                             $alta_directa_gas
 * @property float|null                                                                              $descuento_gas
 * @property string|null                                                                             $contrata_servicios
 * @property string|null                                                                             $tipo_servicio
 * @property string|null                                                                             $servicio_normal
 * @property string|null                                                                             $servicio_luz
 * @property string|null                                                                             $servicio_plus
 * @property string|null                                                                             $factura_electronica
 * @property string|null                                                                             $plan_ahorro
 * @property int|null                                                                                $id_marca_caldera
 * @property string|null                                                                             $iban
 * @property string|null                                                                             $envio_fra_dir_suministro
 * @property string|null                                                                             $envio_fra_tipo_via
 * @property string|null                                                                             $envio_fra_direccion
 * @property string|null                                                                             $envio_fra_numero
 * @property string|null                                                                             $envio_fra_duplicador
 * @property string|null                                                                             $envio_fra_esc
 * @property string|null                                                                             $envio_fra_piso
 * @property string|null                                                                             $envio_fra_letra
 * @property string|null                                                                             $envio_fra_codigo_postal
 * @property string|null                                                                             $envio_fra_poblacion
 * @property int|null                                                                                $envio_fra_id_provincia
 * @property int|null                                                                                $id_estado
 * @property string|null                                                                             $revision_visual
 * @property string|null                                                                             $observaciones_visual
 * @property string|null                                                                             $revision_call_center
 * @property string|null                                                                             $observaciones_call_center
 * @property int|null                                                                                $incidencia
 * @property int|null                                                                                $motivo
 * @property string|null                                                                             $descripcion
 * @property string|null                                                                             $id_comercial
 * @property string|null                                                                             $id_comercial_externo
 * @property string|null                                                                             $id_gerencia
 * @property string|null                                                                             $fecha_registro
 * @property string|null                                                                             $hora_registro
 * @property string|null                                                                             $fecha_firma
 * @property string|null                                                                             $fecha_baja
 * @property string|null                                                                             $latitud
 * @property string|null                                                                             $longitud
 * @property int|null                                                                                $id_iva
 * @property float|null                                                                              $iva
 * @property string|null                                                                             $fecha_ok_comision
 * @property string|null                                                                             $estado_liquidacion
 * @property string|null                                                                             $contrato_fisico
 * @property string|null                                                                             $error_cig
 * @property string|null                                                                             $validado_fichero_agrupado
 * @property string|null                                                                             $id_lote
 * @property string|null                                                                             $id_contrato_destino
 * @property string|null                                                                             $id_contrato_procedencia
 * @property string|null                                                                             $fecha_envio
 * @property string|null                                                                             $fecha_recepcion
 * @property string|null                                                                             $fecha_ok_retrocesion
 * @property string|null                                                                             $estado_retrocesion
 * @property int|null                                                                                $valor
 * @property string|null                                                                             $cliente_menor
 * @property string|null                                                                             $cliente_fecha_nacimiento
 * @property string|null                                                                             $cliente_banco_titular
 * @property string|null                                                                             $cliente_banco
 * @property string|null                                                                             $cliente_sexo
 * @property float|null                                                                              $donativo
 * @property string|null                                                                             $cliente_tlf_movil
 * @property string|null                                                                             $cliente_bloque
 * @property string|null                                                                             $cliente_lote
 * @property string|null                                                                             $periodo
 * @property string|null                                                                             $cliente_balcon
 * @property string|null                                                                             $regular_permanente
 * @property string|null                                                                             $donativo_unico
 * @property string|null                                                                             $publi
 * @property string|null                                                                             $numero_contrato
 * @property string|null                                                                             $codigo_validacion
 * @property string|null                                                                             $cliente_otra_facturacion
 * @property string|null                                                                             $cliente_codigo_postal
 * @property string|null                                                                             $observaciones_contrato
 * @property string|null                                                                             $fecha_envio_callcenter
 * @property string|null                                                                             $observaciones_generales
 * @property string|null                                                                             $cliente_nif_cif
 * @property string|null                                                                             $canal_ingreso
 * @property int|null                                                                                $id_provincia
 * @property int|null                                                                                $id_poblacion
 * @property string|null                                                                             $tipo_documento
 * @property string|null                                                                             $id_pagador
 * @property string|null                                                                             $saludo
 * @property \Illuminate\Support\Carbon|null                                                         $fecha_empieza
 * @property string|null                                                                             $cliente_profesion
 * @property string|null                                                                             $cliente_empresa
 * @property string|null                                                                             $cliente_cargo
 * @property string|null                                                                             $cliente_estado
 * @property string|null                                                                             $medio_pago
 * @property string|null                                                                             $numero_cuenta
 * @property string|null                                                                             $nif_cif_dom
 * @property \Illuminate\Support\Carbon|null                                                         $fecha_acaba
 * @property int|null                                                                                $cliente_id_poblacion
 * @property string|null                                                                             $tipo_donacion
 * @property \Illuminate\Support\Carbon|null                                                         $fecha_vencimiento_tarjeta
 * @property string|null                                                                             $autoriza_correo
 * @property string|null                                                                             $autoriza_mensaje
 * @property string|null                                                                             $tipo_doc_dom
 * @property int|null                                                                                $programa_donaciones
 * @property string|null                                                                             $id_dom_org
 * @property \Illuminate\Support\Carbon|null                                                         $fecha_ptesa
 * @property int|null                                                                                $motivo_ko
 * @property int|null                                                                                $id_campanya
 * @property string|null                                                                             $tipo_contratacion
 * @property string|null                                                                             $cliente_barrio
 * @property string|null                                                                             $tipo_tarjeta
 * @property string|null                                                                             $numero_apadrinados
 * @property string|null                                                                             $apadrinado_nombre
 * @property int|null                                                                                $id_apadrinado t_apadrinados.id
 * @property int|null                                                                                $id_lugar_inscripcion
 * @property int|null                                                                                $numero_hijos
 * @property int|null                                                                                $cliente_numero_hijos
 * @property string|null                                                                             $tipo_documento_fra
 * @property string|null                                                                             $cliente_fra_nif_cif
 * @property string|null                                                                             $cliente_fra_nombre
 * @property string|null                                                                             $cliente_fra_tlf
 * @property int|null                                                                                $id_ultimo_operador
 * @property-read \App\Models\External\TCampanyas|null                                               $campaign
 * @property-read \App\Models\External\TPoblaciones                                                  $city
 * @property-read \App\Models\External\TPoblaciones                                                  $city_client
 * @property-read \App\Models\External\TComerciales|null                                             $commercial
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\External\TContratosFicheros[] $files
 * @property-read mixed                                                                              $nombre_completo
 * @property-read \App\Models\User|null                                                              $operator
 * @property-read \App\Models\External\TLugares                                                      $place
 * @property-read \App\Models\External\TProvincias                                                   $province
 * @property-read \App\Models\External\TProvincias                                                   $province_client
 * @property-read \App\Models\External\TApadrinados                                                  $sponsored
 * @property-read \App\Models\External\TEstadosContratos                                             $state
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereAltaDirectaElec($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereAltaDirectaGas($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereApadrinadoNombre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereAutorizaCorreo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereAutorizaMensaje($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereCambioTitularElec($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereCambioTitularGas($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereCanalIngreso($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereCboCmrclzdrConCbos($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereCboCmrclzdrElec($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereCboCmrclzdrGas($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereClienteApellidos($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereClienteBalcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereClienteBanco($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereClienteBancoTitular($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereClienteBarrio($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereClienteBloque($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereClienteCargo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereClienteCodigoPostal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereClienteDireccion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereClienteDireccionSuministro($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereClienteDuplicador($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereClienteEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereClienteEmpresa($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereClienteEsc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereClienteEstado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereClienteFechaNacimiento($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereClienteFraNifCif($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereClienteFraNombre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereClienteFraTlf($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereClienteIdPoblacion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereClienteIdProvincia($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereClienteLetra($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereClienteLote($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereClienteMenor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereClienteNifCif($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereClienteNombre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereClienteNumero($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereClienteNumeroHijos($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereClienteOtraFacturacion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereClientePiso($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereClientePoblacion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereClienteProfesion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereClienteSexo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereClienteTipoVia($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereClienteTlf1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereClienteTlf2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereClienteTlfMovil($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereCodigoCig($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereCodigoValidacion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereCodigoVerificacion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereConsumoEstimado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereContrataGas($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereContrataLuz($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereContrataServicios($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereContratoFisico($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereDescripcion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereDescuentoElec($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereDescuentoGas($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereDonativo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereDonativoUnico($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereEmpColaboradoraTarjeta($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereEnvioFraCodigoPostal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereEnvioFraDirSuministro($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereEnvioFraDireccion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereEnvioFraDuplicador($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereEnvioFraEsc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereEnvioFraIdProvincia($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereEnvioFraLetra($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereEnvioFraNumero($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereEnvioFraPiso($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereEnvioFraPoblacion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereEnvioFraTipoVia($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereErrorCig($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereEstadoLiquidacion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereEstadoRetrocesion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereFacturaElectronica($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereFechaAcaba($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereFechaBaja($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereFechaEmpieza($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereFechaEnvio($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereFechaEnvioCallcenter($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereFechaFirma($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereFechaOkComision($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereFechaOkRetrocesion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereFechaPtesa($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereFechaRecepcion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereFechaRegistro($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereFechaVencimientoTarjeta($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereHoraRegistro($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereIban($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereIdApadrinado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereIdCampanya($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereIdComercial($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereIdComercialExterno($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereIdContrato($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereIdContratoDestino($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereIdContratoProcedencia($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereIdDomOrg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereIdEmpColaboradora($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereIdEmpresa($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereIdEstado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereIdGerencia($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereIdIva($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereIdLote($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereIdLugarInscripcion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereIdMarcaCaldera($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereIdPagador($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereIdPoblacion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereIdProvincia($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereIdRepresentanteTipo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereIdTarifaElec($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereIdTarifaGas($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereIdTipoContrato($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereIdUltimoOperador($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereIncidencia($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereIva($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereLatitud($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereLongitud($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereMarcaColaboradora($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereMedioPago($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereMotivo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereMotivoKo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereNifCifDom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereNumeroApadrinados($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereNumeroContrato($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereNumeroCuenta($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereNumeroHijos($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereObservacionesCallCenter($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereObservacionesContrato($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereObservacionesGenerales($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereObservacionesVisual($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos wherePeriodo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos wherePlanAhorro($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos wherePotenciaElec1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos wherePotenciaElec2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos wherePotenciaElec3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereProgramaDonaciones($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos wherePsCnae($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos wherePsCodigoPostal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos wherePsCupsElectricidad($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos wherePsCupsGas($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos wherePsDireccion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos wherePsDuplicador($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos wherePsEsc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos wherePsIdProvincia($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos wherePsLetra($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos wherePsNumero($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos wherePsPiso($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos wherePsPoblacion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos wherePsTipoVia($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos wherePubli($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereRegularPermanente($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereRepresentanteApellidos($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereRepresentanteNif($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereRepresentanteNombre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereRevisionCallCenter($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereRevisionVisual($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereSaludo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereServicioLuz($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereServicioNormal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereServicioPlus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereTipoContratacion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereTipoContrato($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereTipoDocDom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereTipoDocumento($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereTipoDocumentoFra($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereTipoDonacion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereTipoServicio($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereTipoTarjeta($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereValidadoFicheroAgrupado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TContratos whereValor($value)
 * @mixin \Eloquent
 */
class TContratos extends ExternalModel
{

	public const CREATED_AT = null;
	public const UPDATED_AT = null;
	protected $appends = ['nombre_completo'];
	protected $casts = [
		'donativo'   => 'real',
		'id_empresa' => 'string',
	];
	protected $dates = [
		'fecha_acaba',
		'fecha_empieza',
		'fecha_ptesa',
		'fecha_vencimiento_tarjeta',
	];
	protected $hidden = [
		'alta_directa_elec',
		'alta_directa_gas',
		'apadrinado_nombre',
		'cambio_titular_elec',
		'cambio_titular_gas',
		'cbo_cmrclzdr_con_cbos',
		'cbo_cmrclzdr_elec',
		'cbo_cmrclzdr_gas',
		'cliente_balcon',
		'cliente_bloque',
		'cliente_codigo_postal',
		'cliente_direccion_suministro',
		'cliente_duplicador',
		'cliente_esc',
		'cliente_letra',
		'cliente_lote',
		'cliente_menor',
		'cliente_numero',
		'cliente_otra_facturacion',
		'cliente_piso',
		'cliente_poblacion',
		'cliente_tipo_via',
		'codigo_cig',
		'codigo_verificacion',
		'consumo_estimado',
		'contrata_gas',
		'contrata_luz',
		'contrata_servicios',
		'contrato_fisico',
		'descripcion',
		'descuento_elec',
		'descuento_gas',
		'donativo_unico',
		'emp_colaboradora_tarjeta',
		'envio_fra_codigo_postal',
		'envio_fra_dir_suministro',
		'envio_fra_direccion',
		'envio_fra_duplicador',
		'envio_fra_esc',
		'envio_fra_id_provincia',
		'envio_fra_letra',
		'envio_fra_numero',
		'envio_fra_piso',
		'envio_fra_poblacion',
		'envio_fra_tipo_via',
		'error_cig',
		'estado_retrocesion',
		'factura_electronica',
		'fecha_baja',
		'fecha_envio',
		'fecha_envio_callcenter',
		'fecha_ok_comision',
		'fecha_ok_retrocesion',
		'fecha_recepcion',
		'iban',
		'id_apadrinado',
		'id_contrato_destino',
		'id_contrato_procedencia',
		'id_emp_colaboradora',
		'id_iva',
		'id_lote',
		'id_marca_caldera',
		'id_representante_tipo',
		'id_tarifa_elec',
		'id_tarifa_gas',
		'incidencia',
		'iva',
		'marca_colaboradora',
		'motivo',
		'motivo_ko',
		'nif_cif_dom',
		'numero_apadrinados',
		'numero_hijos',
		'observaciones_generales',
		'observaciones_visual',
		'plan_ahorro',
		'potencia_elec1',
		'potencia_elec2',
		'potencia_elec3',
		'ps_cnae',
		'ps_codigo_postal',
		'ps_cups_electricidad',
		'ps_cups_gas',
		'ps_direccion',
		'ps_duplicador',
		'ps_esc',
		'ps_id_provincia',
		'ps_letra',
		'ps_numero',
		'ps_piso',
		'ps_poblacion',
		'ps_tipo_via',
		'publi',
		'regular_permanente',
		'representante_apellidos',
		'representante_nif',
		'representante_nombre',
		'revision_call_center',
		'revision_visual',
		'servicio_luz',
		'servicio_normal',
		'servicio_plus',
		'tipo_doc_dom',
		'validado_fichero_agrupado',
		'valor',
	];
	protected $guarded = [];
	protected $primaryKey = 'id_contrato';
	protected $table = 't_contratos';
	protected $with = ['campaign', 'commercial.management', 'state'];

	public function campaign()
	{
		return $this->belongsTo(TCampanyas::class, 'id_campanya');
	}

	public function city()
	{
		return $this->hasOne(TPoblaciones::class, 'id_poblacion', 'id_poblacion');
	}

	public function city_client()
	{
		return $this->hasOne(TPoblaciones::class, 'id_poblacion', 'cliente_id_poblacion');
	}

	public function commercial()
	{
		return $this->belongsTo(TComerciales::class, 'id_comercial', 'id_comercial');
	}

	public function files()
	{
		return $this->hasMany(TContratosFicheros::class, 'id_contrato', 'id_contrato');
	}

	public function operator()
	{
		return $this->belongsTo(User::class, 'id_ultimo_operador');
	}

	public function place()
	{
		return $this->hasOne(TLugares::class, 'id', 'id_lugar_inscripcion');
	}

	public function province()
	{
		return $this->hasOne(TProvincias::class, 'id_provincia', 'id_provincia');
	}

	public function province_client()
	{
		return $this->hasOne(TProvincias::class, 'id_provincia', 'cliente_id_provincia');
	}

	public function sponsored()
	{
		return $this->hasOne(TApadrinados::class, 'id_contrato', 'id_contrato');
	}
    public function paymentStructures()
    {

        return $this->hasMany(TContratosProductosEstructuras::class, 'id_contrato', 'id_contrato');
    }

    public function getFechaUltimoProductoAttribute(){
        $product =  TContratosProductos::where('id_contrato',$this->id_contrato)->orderBy('fecha_alta','desc')->first();
        if ($product){
            return $product->fecha_alta;
        }else{
            return null;
        }


    }

    public function products()
    {
        return $this->hasmany(TContratosProductos::class, 'id_contrato','id_contrato');
    }
	public function state()
	{
		return $this->hasOne(TEstadosContratos::class, 'id_estado', 'id_estado');
	}

	public function setIdTipoContratoAttribute($value)
	{
		$this->attributes['id_tipo_contrato'] = strtolower($value);
	}

	public function getFechaFirmaAttribute($value)
	{
		return $value ? Carbon::parse($value)->format('Y-m-d') : null;
	}

	public function getFechaRegistroAttribute($value)
	{
		return $value ? Carbon::parse($value)->format('Y-m-d') : null;
	}

	public function getNombreCompletoAttribute()
	{
		return trim(preg_replace('/\s+/', ' ', $this->cliente_nombre . ' ' . $this->cliente_apellidos));
	}
}
