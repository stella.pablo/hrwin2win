<?php

namespace App\Models\External;

/**
 * App\Models\External\TComercialesContratos
 *
 * @property string      $id_empresa
 * @property string      $id_comercial
 * @property int         $id_comercial_contrato
 * @property string|null $id_tipo_contrato_comercial
 * @property string|null $id_tramo
 * @property string|null $fecha_desde
 * @property string|null $fecha_hasta
 * @property string|null $devolucion_comercial
 * @property string|null $devolucion_estructura
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComercialesContratos newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComercialesContratos newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComercialesContratos query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComercialesContratos whereDevolucionComercial($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComercialesContratos whereDevolucionEstructura($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComercialesContratos whereFechaDesde($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComercialesContratos whereFechaHasta($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComercialesContratos whereIdComercial($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComercialesContratos whereIdComercialContrato($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComercialesContratos whereIdEmpresa($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComercialesContratos whereIdTipoContratoComercial($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TComercialesContratos whereIdTramo($value)
 * @mixin \Eloquent
 */
class TComercialesEstructuras extends ExternalModel
{

	public const CREATED_AT = null;
	public const UPDATED_AT = null;

	protected $casts = [
		'id_comercial' => 'string',
		'id_empresa'   => 'string',
	];
	protected $appends = ['commercial_name'];
	protected $guarded = [];
	public $incrementing = false;
	protected $primaryKey = 'id_estructura'; // Yeah, strange right?
	protected $table = 't_comerciales_estructuras';

	public function getCommercialNameAttribute()
	{

		$commercial = TComerciales::find($this->id_comercial_estructura);
		if ($commercial) {
			return $commercial->nombre_completo;
		} else {
			return 'Todos';
		}

	}
	public function commercial(){
		$this->belongsTo(TComerciales::class,'id_comercial','id_comercial');
	}


}
