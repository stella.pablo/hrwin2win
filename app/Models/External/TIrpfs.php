<?php

namespace App\Models\External;
use Awobaz\Compoships\Compoships;
use App\Models\External\ExternalModel;
use Illuminate\Database\Eloquent\Model;

class TIrpfs extends ExternalModel{
	use compoships;
	const CREATED_AT = null;
	const UPDATED_AT = null;

	//protected $connection = 'sqlsrv';


	protected $casts = [
		'id_irpf' => 'string',
	];

	protected $guarded = [];
	protected $primaryKey = 'id_irpf';
	protected $table = 't_irpfs';

	public function commercialContract(){
		return	$this->belongsToMany(TComercialesContratos::class,'id_irpf','id_irpf');
	}
	public function contractsTypes(){
		return	$this->belongsToMany(TTiposContratosComercial::class,'id_irpf','id_irpf');

	}


}
