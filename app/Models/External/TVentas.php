<?php

namespace App\Models\External;

// use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use \Awobaz\Compoships\Compoships;

class TVentas extends ExternalModel
{
	use compoships;
	// use Cachable;
	public const CREATED_AT = null;
	public const UPDATED_AT = null;
	public const DELETED_AT = null;
	protected $casts = [
		'id_empresa'  => 'string',
		'id_producto' => 'string',
	];
	protected $guarded = [];
	protected $primaryKey = 'id_venta';
	protected $table = 't_ventas';

//	protected $appends =['totals'];
	protected $appends = ['totals'];

	//protected $appends = ['totals','NumberOfRows','TotalTax','Taxes'];


	public function commercial()
	{

		return $this->belongsTo(TComerciales::class, 'id_comercial', 'id_comercial');
	}

	public function management()
	{

		return $this->belongsTo(TGerencias::class, 'id_gerencia', 'id_gerencia');
	}

	public function articles()
	{
		return $this->hasMany(TVentasLineas::class, 'id_venta', 'id_venta');
	}

	public function getTotalsAttribute()
	{

		//	return DB::connection('sqlsrv')->select("SELECT sum(total) from [dbo].t_ventas_lineas where id_venta=?",[$this->id_venta] );

		return $this->hasMany(TVentasLineas::class, 'id_venta', 'id_venta')->sum('total');
	}

	public function getNumberOfRowsAttribute()
	{
		return $this->hasMany(TVentasLineas::class, 'id_venta', 'id_venta')->count('id_venta');
	}

	public function getTaxAttribute()
	{
		return $this->hasMany(TVentasLineas::class, 'id_venta', 'id_venta')->sum('precio_impuestos');


	}

	public function getTaxesAttribute()
	{
		return $this->hasMany(TVentasLineas::class, 'id_venta', 'id_venta')->groupBy('iva')->selectRaw('sum (precio_impuestos) AS sum, sum (total) as sum_imp, iva')->get();

	}

	public function paymentStructures()
	{

		return $this->hasMany(TVentasEstructuras::class, 'id_venta', 'id_venta');
	}

	public function company()
	{
		return $this->belongsTo(TEmpresas::class, 'id_empresa', 'id_empresa');
	}

}
