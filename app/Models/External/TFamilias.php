<?php

namespace App\Models\External;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Category
 *
 * @property string                                                              $id_empresa
 * @property string                                                              $id_familia
 * @property string|null                                                         $familia
 * @property string|null                                                         $id_tipo_articulo
 * @property string|null                                                         $id_subtipo_articulo
 * @property float|null                                                          $rappel
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Product[] $products
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TFamilias whereFamilia($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TFamilias whereIdEmpresa($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TFamilias whereIdFamilia($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TFamilias whereIdSubtipoArticulo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TFamilias whereIdTipoArticulo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\External\TFamilias whereRappel($value)
 * @mixin \Eloquent
 */
class TFamilias extends ExternalModel {

	const CREATED_AT = null;
	const UPDATED_AT = null;

	// protected $appends = ['state'];

	//protected $connection = session('database');

	protected $casts = [
		'id_empresa' => 'string',
		'id_familia' => 'string',
	];

#  protected $dateFormat = 'Y-m-d H:i:s';

	protected $guarded = [];
	protected $primaryKey = 'id_familia';
	protected $table = 't_familias';

	protected $with = [];

	public function articles() {
		return $this->hasMany(TArticulos::class, 'id_familia', 'id_familia');
	}

}



/*
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Sale extends Model
{
    const CREATED_AT = 'fecha';
    const UPDATED_AT = 'fecha_ult_modif';

    protected $appends = ['state'];

    protected $connection = 'sqlsrv';

    protected $casts = [
      'id_empresa' => 'string',
      'id_pedido' => 'string'
    ];

    protected $dateFormat = 'Y-m-d H:i:s';

    protected $guarded = [];
    protected $primaryKey = 'id_pedido';
    protected $table = 't_pedidos_cab';

    protected $with = [];

    public function getStateAttribute()
    {
        $stateDB = DB::connection('sqlsrv')->select('SELECT dbo.F_VENTAS_PEDIDO_ESTADO_CAB(?, ?) as state', [$this->id_empresa, $this->id_pedido]);
        $state = $stateDB[0]->state;

        if ($state === 'PDE') {
            $state = 'PENDIENTE';
        } elseif ($state === 'SER') {
            $state = 'SERVIDO';
        } else {
            $state = 'N';
        }

        return $state;
    }

    public function client()
    {
        return $this->belongsTo(Client::class, 'id_cliente', 'id_cliente');
    }

    public function products()
    {
        return $this->hasMany(SalesProduct::class, 'id_pedido', 'id_pedido');
    }
}
*/
