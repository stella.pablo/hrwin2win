<?php

namespace App\Models\External;
use App\Models\External\ExternalModel;
use Awobaz\Compoships\Compoships;
use Illuminate\Database\Eloquent\Model;

class TGruposComisionamiento extends ExternalModel{
	use compoships;
	const CREATED_AT = null;
	const UPDATED_AT = null;


	protected $guarded = [];
	protected $table = 't_grupos_comisionamiento';


	public function feeManagements(){
		return	$this->hasMany(TGruposComisionamientoGerencias::class,'id_grupo','id');
	}

	public function feeProducts(){
		return $this->hasMany(TGruposComisionamientoProductos::class,'id_grupo','id');
	}

	public function fees(){
		return $this->hasMany(TGruposComisionamientoComisiones::class,'id_grupo','id');
	}
	public function feeArticles()
	{
		return $this->hasMany(TGruposComisionamientoArticulos::class, 'id_grupo', 'id');
	}
	public function efectivityFees(){
		return $this->hasMany(TGruposComisionamientoComisionesEfectividad::class,'id_grupo','id');
	}
}
