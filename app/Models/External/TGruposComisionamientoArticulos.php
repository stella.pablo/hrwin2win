<?php

namespace App\Models\External;
use App\Models\External\ExternalModel;

use App\Models\External\TArticulos;
use Awobaz\Compoships\Compoships;
use Illuminate\Database\Eloquent\Model;
use \App\Models\External\TArticulosPaquetes;


class TGruposComisionamientoArticulos extends ExternalModel{
	use compoships;
	const CREATED_AT = null;
	const UPDATED_AT = null;


	protected $guarded = [];
	protected $table = 't_grupos_comisionamiento_articulos';


	public function feeGroup(){
		return $this->BelongsTo(TGruposComisionamiento::class,'id_grupo','id');
	}

	public function article(){
		return $this->hasOne(TArticulos::class,'id_articulo','id_articulo');
	}

	public function package(){
		return $this->hasOne(TArticulosPaquetes::class,'id_tramo','id');
	}


}
