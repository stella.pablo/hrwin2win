<?php

namespace App\Models\External;

use Awobaz\Compoships\Compoships;


class TTarifas extends ExternalModel
{

	use compoships;
	const CREATED_AT = null;
	const UPDATED_AT = null;



	protected $casts = [
		'id_tarifa' => 'string',
	];

	protected $guarded = [];
	protected $primaryKey = 'id_tarifa';
	protected $table = 't_tarifas_cab';


	public function management()
	{
		return $this->belongsToMany(TGerencias::class, 'id_gerencia', 'id_gerencia');
	}


}
