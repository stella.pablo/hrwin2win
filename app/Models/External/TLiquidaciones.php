<?php

namespace App\Models\External;

use Awobaz\Compoships\Compoships;


class TLiquidaciones extends ExternalModel
{

	use compoships;
	public const CREATED_AT = null;
	public const UPDATED_AT = null;
	public const DELETED_AT = null;
	public $incrementing = true;
	protected $casts = [
		'id'         => 'int',
		'id_empresa' => 'string',
	];
	protected $dates = [
		'fecha_desde',
		'fecha_hasta',
		'fecha_liquidacion',
	];
	protected $guarded = [];
	protected $table = 't_liquidaciones';
	protected $appends= ['total'];


	public function commercialSettlements()
	{
		return $this->hasMany(TLiquidacionesComerciales::class, 'id_liquidacion', 'id');
	}

	public function company()
	{
		return $this->belongsTo(TEmpresas::class, 'id_empresa', 'id_empresa');
	}

	public function getTotalAttribute() {
		return TLiquidacionesComercialesConceptos::where('id_liquidacion',$this->id)->sum('importe');
	}

	public function contractType(){
		return $this->hasOne(TTiposContratosComercial::class,'id_tipo_contrato_comercial','id_tipo_contrato_comercial');
	}

}
