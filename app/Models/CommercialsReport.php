<?php

namespace App\Models;

use App\Models\External\TComerciales;

/**
 * App\Models\CommercialsReport
 *
 * @property int                                    $id
 * @property int                                    $management_id
 * @property string                                 $external_id
 * @property int                                    $motive_id
 * @property int|null                               $status_id
 * @property \Illuminate\Support\Carbon|null        $created_at
 * @property \Illuminate\Support\Carbon|null        $updated_at
 * @property-read \App\Models\External\TComerciales $commercial
 * @property-read \App\Models\ReportsMotive         $motive
 * @property-read \App\Models\ReportsStatus|null    $status
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CommercialsReport newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CommercialsReport newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CommercialsReport query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CommercialsReport whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CommercialsReport whereExternalId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CommercialsReport whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CommercialsReport whereManagementId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CommercialsReport whereMotiveId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CommercialsReport whereStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CommercialsReport whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class CommercialsReport extends InternalModel
{

	protected $guarded = [];
	protected $with = ['commercial', 'motive', 'status'];

	public function commercial()
	{
		return $this->belongsTo(TComerciales::class, 'external_id');
	}

	public function motive()
	{
		return $this->belongsTo(ReportsMotive::class);
	}

	public function status()
	{
		return $this->belongsTo(ReportsStatus::class);
	}
}
