<?php

namespace App\Models;

/**
 * App\Models\CandidatesFilesType
 *
 * @property int                             $id
 * @property int                             $company_id
 * @property string                          $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CandidatesFilesType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CandidatesFilesType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CandidatesFilesType query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CandidatesFilesType whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CandidatesFilesType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CandidatesFilesType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CandidatesFilesType whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CandidatesFilesType whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class CandidatesFilesType extends InternalModel
{

	protected $fillable = ['company_id', 'name', 'required'];
}
