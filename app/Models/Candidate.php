<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Candidate
 *
 * @property int                                                                        $id
 * @property string                                                                     $name
 * @property string|null                                                                $surname
 * @property string|null                                                                $lastname
 * @property string|null                                                                $email
 * @property string|null                                                                $phone
 * @property int|null                                                                   $phone_not_answered
 * @property string|null                                                                $petitioned_at
 * @property string|null                                                                $comment
 * @property int|null                                                                   $preapproved
 * @property string|null                                                                $preapproved_at
 * @property int|null                                                                   $preapproved_user_id
 * @property int|null                                                                   $approved
 * @property string|null                                                                $approved_at
 * @property int|null                                                                   $approved_user_id
 * @property int|null                                                                   $hired
 * @property string|null                                                                $hired_at
 * @property int|null                                                                   $hired_user_id
 * @property string|null                                                                $external_id
 * @property int                                                                        $management_id
 * @property int|null                                                                   $origin_id
 * @property int                                                                        $status_id
 * @property int                                                                        $user_id
 * @property \Illuminate\Support\Carbon|null                                            $deleted_at
 * @property \Illuminate\Support\Carbon|null                                            $created_at
 * @property \Illuminate\Support\Carbon|null                                            $updated_at
 * @property-read \App\Models\CandidatesAppointment                                     $appointmentfirst
 * @property-read \App\Models\CandidatesAppointment                                     $appointmentsecond
 * @property-read \App\Models\User|null                                                 $approvedUser
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\EmailsSent[]     $emails
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\CandidatesFile[] $files
 * @property-read mixed                                                                 $fullname
 * @property-read \App\Models\User|null                                                 $hiredUser
 * @property-read \App\Models\Management                                                $management
 * @property-read \App\Models\CandidatesOrigin|null                                     $origin
 * @property-read \App\Models\User|null                                                 $preapprovedUser
 * @property-read \App\Models\CandidatesStatus                                          $status
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Candidate newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Candidate newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Candidate onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Candidate query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Candidate whereApproved($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Candidate whereApprovedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Candidate whereApprovedUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Candidate whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Candidate whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Candidate whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Candidate whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Candidate whereExternalId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Candidate whereHired($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Candidate whereHiredAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Candidate whereHiredUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Candidate whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Candidate whereLastname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Candidate whereManagementId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Candidate whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Candidate whereOriginId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Candidate wherePetitionedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Candidate wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Candidate wherePhoneNotAnswered($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Candidate wherePreapproved($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Candidate wherePreapprovedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Candidate wherePreapprovedUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Candidate whereStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Candidate whereSurname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Candidate whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Candidate whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Candidate withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Candidate withoutTrashed()
 * @mixin \Eloquent
 */
class Candidate extends InternalModel
{

	use SoftDeletes;

	protected $appends = ['fullname'];
	protected $dates = ['deleted_at'];
	protected $guarded = [];
	protected $with = [
		'appointmentfirst',
		'appointmentsecond',
		'origin',
		'status',
		'preapprovedUser',
		'approvedUser',
		'hiredUser',
	];

	public function appointmentfirst()
	{
		return $this->hasOne(CandidatesAppointment::class)->where('number', 1)->orderBy('date', 'desc');
	}

	public function appointmentsecond()
	{
		return $this->hasOne(CandidatesAppointment::class)->where('number', 2)->orderBy('date', 'desc');
	}

	public function emails()
	{
		return $this->hasMany(EmailsSent::class);
	}

	public function management()
	{
		return $this->belongsTo(Management::class);
	}

	public function files()
	{
		return $this->hasMany(CandidatesFile::class);
	}

	public function origin()
	{
		return $this->belongsTo(CandidatesOrigin::class);
	}

	public function status()
	{
		return $this->belongsTo(CandidatesStatus::class);
	}

	# Users commiting transactions

	public function preapprovedUser()
	{
		return $this->belongsTo(User::class);
	}

	public function approvedUser()
	{
		return $this->belongsTo(User::class);
	}

	public function hiredUser()
	{
		return $this->belongsTo(User::class);
	}

	public function getNameAttribute($value) {
		return ucwords(mb_strtolower($value));
	}

	public function getSurnameAttribute($value) {
		return ucwords(mb_strtolower($value));
	}

	public function getLastnameAttribute($value) {
		return ucwords(mb_strtolower($value));
	}

	public function getFullnameAttribute()
	{
		$fullName = $this->name;

		if ($this->surname) {
			$fullName .= ' ' . $this->surname;
		}

		if ($this->lastname) {
			$fullName .= ' ' . $this->lastname;
		}

		# @TODO: Convert ucwords to mb_convert_case
		return ucwords(mb_strtolower(trim($fullName)));
	}
}
