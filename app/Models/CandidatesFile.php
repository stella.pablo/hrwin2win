<?php

namespace App\Models;

/**
 * App\Models\CandidatesFile
 *
 * @property int                                  $id
 * @property int|null                             $candidate_id
 * @property string|null                          $external_id
 * @property int                                  $type_id
 * @property int                                  $user_id
 * @property string                               $filename
 * @property string                               $path
 * @property int|null                             $absence_id
 * @property \Illuminate\Support\Carbon|null      $created_at
 * @property \Illuminate\Support\Carbon|null      $updated_at
 * @property-read \App\Models\CandidatesFilesType $type
 * @property-read \App\Models\User                $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CandidatesFile newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CandidatesFile newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CandidatesFile query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CandidatesFile whereAbsenceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CandidatesFile whereCandidateId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CandidatesFile whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CandidatesFile whereExternalId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CandidatesFile whereFilename($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CandidatesFile whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CandidatesFile wherePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CandidatesFile whereTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CandidatesFile whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CandidatesFile whereUserId($value)
 * @mixin \Eloquent
 */
class CandidatesFile extends InternalModel
{

	protected $guarded = [];
	protected $with = ['type', 'user'];

	public function type()
	{
		return $this->hasOne(CandidatesFilesType::class, 'id', 'type_id');
	}

	public function user()
	{
		return $this->belongsTo(User::class);
	}
}
