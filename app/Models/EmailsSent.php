<?php

namespace App\Models;

/**
 * App\Models\EmailsSent
 *
 * @property int                             $id
 * @property int|null                        $candidate_id
 * @property string|null                     $external_id
 * @property int                             $user_id
 * @property int                             $email_type_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\EmailsType     $type
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmailsSent newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmailsSent newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmailsSent query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmailsSent whereCandidateId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmailsSent whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmailsSent whereEmailTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmailsSent whereExternalId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmailsSent whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmailsSent whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EmailsSent whereUserId($value)
 * @mixin \Eloquent
 */
class EmailsSent extends InternalModel
{

	protected $table = 'emails_sent';
	protected $guarded = [];
	protected $with = ['type'];

	public function type()
	{
		return $this->belongsTo(EmailsType::class, 'email_type_id', 'id');
	}

	public function user() {
		return $this->belongsTo(User::class);
	}

}
