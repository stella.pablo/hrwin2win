<?php

namespace App\Models;

/**
 * App\Models\CandidatesAppointment
 *
 * @property int                             $id
 * @property int                             $candidate_id
 * @property int                             $number
 * @property \Illuminate\Support\Carbon      $date
 * @property int                             $status
 * @property string|null                     $reason
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CandidatesAppointment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CandidatesAppointment newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CandidatesAppointment query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CandidatesAppointment whereCandidateId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CandidatesAppointment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CandidatesAppointment whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CandidatesAppointment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CandidatesAppointment whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CandidatesAppointment whereReason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CandidatesAppointment whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CandidatesAppointment whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class CandidatesAppointment extends InternalModel
{

	protected $dates = ['date'];
	protected $guarded = [];
}
