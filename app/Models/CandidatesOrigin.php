<?php

namespace App\Models;

/**
 * App\Models\CandidatesOrigin
 *
 * @property int                             $id
 * @property string                          $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CandidatesOrigin newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CandidatesOrigin newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CandidatesOrigin query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CandidatesOrigin whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CandidatesOrigin whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CandidatesOrigin whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CandidatesOrigin whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class CandidatesOrigin extends InternalModel
{

	//
}
