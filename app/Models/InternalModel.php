<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class InternalModel extends Model
{

	protected $dateFormat = 'Y-m-d H:i:s';

	public function __construct(array $attributes = [])
	{
		parent::__construct($attributes);

		$this->setConnection('mysql');
	}
}
