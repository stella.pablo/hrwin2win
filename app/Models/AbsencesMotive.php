<?php

namespace App\Models;

/**
 * App\Models\AbsencesMotive
 *
 * @property int                             $id
 * @property int                             $company_id
 * @property string                          $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AbsencesMotive newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AbsencesMotive newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AbsencesMotive query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AbsencesMotive whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AbsencesMotive whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AbsencesMotive whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AbsencesMotive whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AbsencesMotive whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class AbsencesMotive extends InternalModel
{

	protected $fillable = ['company_id', 'name'];
}
