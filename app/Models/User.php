<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

/**
 * App\Models\User
 *
 * @property int                                                                                                            $id
 * @property int                                                                                                            $management_id
 * @property int                                                                                                            $company_id
 * @property string                                                                                                         $name
 * @property string                                                                                                         $email
 * @property string                                                                                                         $username
 * @property string                                                                                                         $password
 * @property int                                                                                                            $level
 * @property string                                                                                                         $pin
 * @property string|null                                                                                                    $remember_token
 * @property \Illuminate\Support\Carbon|null                                                                                $created_at
 * @property \Illuminate\Support\Carbon|null                                                                                $updated_at
 * @property-read \App\Models\Company                                                                                       $company
 * @property-read \App\Models\Management                                                                                    $management
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Management[]                                         $managements
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereLevel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereManagementId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User wherePin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereUsername($value)
 * @mixin \Eloquent
 */
class User extends Authenticatable
{

	use Notifiable;

	protected $connection = 'mysql';

	protected $casts = [
		'level' => 'integer',
	];

	protected $fillable = [
		'management_id',
		'company_id',
		'name',
		'email',
		'username',
		'level',
		'pin',
		'password',
	];

	protected $hidden = [
		'password',
		'remember_token',
	];

	protected $with = ['company'];

	public function company()
	{
		return $this->belongsTo(Company::class);
	}

	public function management()
	{
		return $this->belongsTo(Management::class);
	}

	public function managements()
	{
		return $this
			->belongsToMany(Management::class, 'user_management')
			->where('company_id', session('company_id'))
			->orderBy('name');
	}

	public function managementsAll()
	{
		return $this
			->belongsToMany(Management::class, 'user_management')
			->orderBy('name');
	}
}
