<?php

namespace App\Models;

use App\Models\External\TComerciales;

/**
 * App\Models\CommercialsAbsence
 *
 * @property int                                    $id
 * @property string                                 $external_id
 * @property string                                 $date
 * @property int                                    $motive_id
 * @property \Illuminate\Support\Carbon|null        $created_at
 * @property \Illuminate\Support\Carbon|null        $updated_at
 * @property-read \App\Models\External\TComerciales $commercial
 * @property-read \App\Models\AbsencesMotive        $motive
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CommercialsAbsence newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CommercialsAbsence newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CommercialsAbsence query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CommercialsAbsence whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CommercialsAbsence whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CommercialsAbsence whereExternalId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CommercialsAbsence whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CommercialsAbsence whereMotiveId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CommercialsAbsence whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class CommercialsAbsence extends InternalModel
{

	protected $guarded = [];
	protected $with = ['motive'];

	public function commercial()
	{
		return $this->belongsTo(TComerciales::class, 'external_id')->with(['management.manager']);
	}

	public function motive()
	{
		return $this->belongsTo(AbsencesMotive::class);
	}
}
