<?php

namespace App\Models;

/**
 * App\Models\UserManagement
 *
 * @property int                             $id
 * @property int                             $user_id
 * @property int                             $management_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserManagement newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserManagement newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserManagement query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserManagement whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserManagement whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserManagement whereManagementId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserManagement whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserManagement whereUserId($value)
 * @mixin \Eloquent
 */
class UserManagement extends InternalModel
{

	protected $guarded = [];
	protected $table = 'user_management';
}
