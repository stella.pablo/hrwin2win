<?php

namespace App\Models;

/**
 * App\Models\Commercial
 *
 * @property int                             $id
 * @property int|null                        $candidate_id
 * @property string                          $external_id
 * @property int|null                        $status_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Commercial newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Commercial newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Commercial query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Commercial whereCandidateId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Commercial whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Commercial whereExternalId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Commercial whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Commercial whereStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Commercial whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Commercial extends InternalModel
{

	protected $guarded = [];
}
