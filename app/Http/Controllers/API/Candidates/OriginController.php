<?php

namespace App\Http\Controllers\API\Candidates;

use App\Http\Controllers\Controller;
use App\Models\CandidatesOrigin;
use Illuminate\Http\Request;

class OriginController extends Controller
{

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		return CandidatesOrigin
			::where('company_id', session('company_id'))
			->orderBy('name', 'asc')
			->get();
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param \App\CandidatesOrigin    $candidatesOrigin
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, CandidatesOrigin $candidatesOrigin)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param \App\CandidatesOrigin $candidatesOrigin
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(CandidatesOrigin $candidatesOrigin)
	{
		//
	}
}
