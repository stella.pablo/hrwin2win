<?php

namespace App\Http\Controllers\API\Candidates;

use App\Http\Controllers\Controller;
use App\Models\Candidate;
use App\Models\Management;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Redis;

class StatsController extends Controller
{

	public function __invoke(Request $request)
	{
		$managementsIds = $request->filled('management_id')
			? [$request->management_id]
			: auth()
				->user()
				->managements
				->where('company_id', session('company_id'))
				->pluck('id')
				->toArray();

		$managements = Management::whereIn('id', $managementsIds)->get();

		$months = [];
		$stats = [];

		// Include the last 12 months and this current month (thus -1)
		for ($i = 12; $i > -1; $i--) {
			$months[] = Carbon::parse('first day of ' . $i . ' months ago')->format('Ym');
		}

		foreach ($months as $month) {
			$data = [];

			foreach ($managements as $management) {
				$path = 'rrhh:c' . $management->company_id . ':m' . $management->id . ':candidates:stats:' . $month;

				$init = Redis::hgetall($path);

				$data[$management->id] = $this->prepare($init ? $init : $this->init($path, $month, $management->id));
				$data[$management->id]['month'] = $month;
			}


			$stats[] = $this->sum($data);
		}


		return $stats;
	}

	private function calculate($candidates, &$stats)
	{
		foreach ($candidates as $candidate) {
			$stats['new'] += 1;

			if ($candidate->appointmentfirst) {
				$stats['a1'] += 1;

				if ($candidate->appointmentfirst->stats === 2) {
					$stats['a1d'] += 1;
				}
			}

			if ($candidate->appointmentsecond) {
				$stats['a1'] += 1;

				if ($candidate->appointmentsecond->stats === 2) {
					$stats['a1d'] += 1;
				}
			}

			if ($candidate->preapproved AND $candidate->preapproved_user_id) {
				$stats['pre'] += 1;
			}

			if ($candidate->approved AND $candidate->approved_user_id) {
				$stats['apr'] += 1;
			}

			if ($candidate->hired AND $candidate->hired_user_id) {
				$stats['hired'] += 1;
			}
		}
	}

	private function init($path, $month, $managementId)
	{
		$stats = [
			'new'   => 0,
			'a1'    => 0,
			'a1d'   => 0,
			'a2'    => 0,
			'a2d'   => 0,
			'pre'   => 0,
			'apr'   => 0,
			'hired' => 0,
		];

		$monthStart = Carbon::parse($month . '01')->startOfMonth();
		$monthEnd = Carbon::parse($month . '01')->endOfMonth();

		$candidates = Candidate
			::whereBetween('updated_at', [$monthStart, $monthEnd])
			->where('management_id', $managementId)
			->get();

		if ($candidates->isNotEmpty()) {
			$this->calculate($candidates, $stats);
		}

		Redis::pipeline(function ($pipe) use ($path, $stats) {
			foreach ($stats as $stat => $count) {
				$pipe->hset($path, $stat, $count);
			}
		});

		return $stats;
	}

	private function prepare($data)
	{
		foreach ($data as &$set) {
			$set = intval($set);
		}

		return $data;
	}

	private function sum($managementsStats) {
		$sum = [
			'new'   => 0,
			'a1'    => 0,
			'a1d'   => 0,
			'a2'    => 0,
			'a2d'   => 0,
			'pre'   => 0,
			'apr'   => 0,
			'hired' => 0,
		];

		foreach ($managementsStats as $stats) {
			// Remove key 'month' from summable data
			$month = Arr::pull($stats, 'month');

			foreach($stats as $key => $stat) {
				$sum[$key] += $stat;
			}

			$sum['month'] = $month;
		}

		return $sum;
	}
}
