<?php

namespace App\Http\Controllers\API\Candidates;

use App\Http\Controllers\Controller;
use App\Models\CandidatesFile;
use Illuminate\Http\Request;
use Storage;

class FileController extends Controller
{

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$file = $request->file;

		$filename = $file->getClientOriginalName();
		$path = $file->store('public/documents');

		return CandidatesFile::create([
			'candidate_id' => $request->candidate_id,
			'external_id'  => $request->external_id,
			'filename'     => $filename,
			'path'         => $path,
			'type_id'      => $request->type_id,
			'user_id'      => auth()->user()->id,
		])->load('type', 'user');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param \App\CandidatesFile $file
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show(CandidatesFile $file)
	{
		return response()->download(storage_path('app/' . $file->path), $file->filename);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param \App\CandidatesFile      $file
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, CandidatesFile $file)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param \App\CandidatesFile $file
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(CandidatesFile $file)
	{
		$filePath = $file->path;
		$delete = $file->delete();

		if ($delete) {
			Storage::delete($filePath);
		}

		return ['status' => $delete];
	}
}
