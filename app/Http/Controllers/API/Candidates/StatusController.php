<?php

namespace App\Http\Controllers\API\Candidates;

use App\Http\Controllers\Controller;
use App\Models\CandidatesStatus;
use Illuminate\Http\Request;

class StatusController extends Controller
{

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		return CandidatesStatus::orderBy('name', 'asc')->get();
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param \App\Models\CandidatesStatus $candidatesStatus
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show(CandidatesStatus $candidatesStatus)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param \App\Models\CandidatesStatus $candidatesStatus
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit(CandidatesStatus $candidatesStatus)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param \Illuminate\Http\Request     $request
	 * @param \App\Models\CandidatesStatus $candidatesStatus
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, CandidatesStatus $candidatesStatus)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param \App\Models\CandidatesStatus $candidatesStatus
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(CandidatesStatus $candidatesStatus)
	{
		//
	}
}
