<?php

namespace App\Http\Controllers\API\Candidates;

use App\Http\Controllers\Controller;
use App\Models\CandidatesFile;
use App\Models\CandidatesFilesType;
use Illuminate\Http\Request;

class FilesTypeController extends Controller
{

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request)
	{
		$company_id = $request->has('company_id') ? $request->company_id : session('company_id');

		return CandidatesFilesType::where('company_id', $company_id)->orderBy('name')->get();
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		return CandidatesFilesType::create($request->all());
	}

	/**
	 * Display the specified resource.
	 *
	 * @param \App\CandidatesFilesType $type
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show(CandidatesFilesType $type)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param \App\CandidatesFilesType $type
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, CandidatesFilesType $type)
	{
		return ['status' => $type->update($request->all())];
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param \App\CandidatesFilesType $type
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(CandidatesFilesType $type)
	{
		$files = CandidatesFile::where('type_id', $type->id)->count();

		return ['status' => $files ? false : $type->delete()];
	}
}
