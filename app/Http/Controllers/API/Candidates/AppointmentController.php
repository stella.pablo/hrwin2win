<?php

namespace App\Http\Controllers\API\Candidates;

use App\Http\Controllers\Controller;
use App\Models\CandidatesAppointment;
use Illuminate\Http\Request;

class AppointmentController extends Controller
{


	/**
	 * Store a newly created resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$data = $request->validate([
			'candidate_id' => 'required|exists:candidates,id',
			'number'       => 'required',
			'date'         => 'required',
			'status'       => 'required',
			'reason'       => 'sometimes',
		]);

		return CandidatesAppointment::create($data);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param \Illuminate\Http\Request   $request
	 * @param \App\CandidatesAppointment $appointment
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, CandidatesAppointment $appointment)
	{
		$data = $request->validate([
			'candidate_id' => 'required|exists:candidates,id',
			'number'       => 'required',
			'date'         => 'required',
			'status'       => 'required',
			'reason'       => 'sometimes',
		]);

		$appointment->update($data);

		return $appointment;
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param \App\CandidatesAppointment $appointment
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(CandidatesAppointment $appointment)
	{
		//
	}
}
