<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Company;
use Illuminate\Http\Request;

class CompanyController extends Controller
{

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request)
	{
		$managements = auth()->user()->managementsAll;

		$companiesIds = [];
		$managementsIds = [];

		if ($request->filled('company_id')) {
			$companiesIds[] = session('company_id');
			$managementsIds = $managements->pluck('id')->toArray();
		} else {
			foreach ($managements as $management) {
				$companiesIds[] = $management->company_id;
				$managementsIds[] = $management->id;
			}

			$companiesIds = array_unique($companiesIds);
			$managementsIds = array_unique($managementsIds);
		}

		$companies = Company
			//COLOMBIA IS OUT OF BUSINESS ATM, WILL NOT BE SHOWN TO PREVENT ERRORS
			::where('database','!=','colombia')->whereIn('id', $companiesIds)
			->with([
				'managements' => function ($q) use ($managementsIds) {
					$q->whereIn('id', $managementsIds);
				},
			])
			->get();

		return $companies;
	}
}
