<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{

	public function index()
	{
		return auth()->user()->load(['management.external', 'managements.external']);
	}

	public function update(Request $request, User $user)
	{
		$user->company_id = $request->company_id;
		$user->save();

		$userFresh = $user->fresh();

		session([
			'company_id' => $userFresh->company_id,
			'database'   => $userFresh->company->database,
		]);

		return $userFresh;
	}
}
