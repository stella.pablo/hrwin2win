<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\EmailsType;
use Illuminate\Http\Request;

class EmailsTypeController extends Controller
{

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		return EmailsType::orderBy('name')->get();
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param \App\EmailsType $emailsType
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show(EmailsType $emailsType)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param \App\EmailsType $emailsType
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit(EmailsType $emailsType)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param \App\EmailsType          $emailsType
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, EmailsType $emailsType)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param \App\EmailsType $emailsType
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(EmailsType $emailsType)
	{
		//
	}
}
