<?php

namespace App\Http\Controllers\API\Administration;

use App\Http\Controllers\Controller;
use App\Models\External\TAlmacenes;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class StoragesController extends Controller
{

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		return TAlmacenes::get();
	}

	/**
	 * Display the specified resource.
	 *
	 * @param \App\Models\TAlmacenes $tarifa
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show(TAlmacenes $storage)
	{



	}

	public function store(Request $request)
	{
	}


	public function update(Request $request, TAlmacenes $storage)
	{



	}




}
