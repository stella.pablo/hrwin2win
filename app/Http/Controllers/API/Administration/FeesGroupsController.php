<?php

namespace App\Http\Controllers\API\Administration;

use App\Http\Controllers\Controller;
use App\Models\External\TGruposComisionamiento;
use App\Models\External\TGruposComisionamientoArticulos;
use App\Models\External\TGruposComisionamientoComisiones;
use App\Models\External\TGruposComisionamientoComisionesEfectividad;
use App\Models\External\TGruposComisionamientoGerencias;
use App\Models\External\TGruposComisionamientoGerenciasComisiones;
use App\Models\External\TGruposComisionamientoProductos;
use App\Models\External\TGruposComisionamientoProductosComisiones;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class FeesGroupsController extends Controller
{

    public function index()
    {

        return TGruposComisionamiento::with('feeManagements.management', 'efectivityFees', 'feeManagements.fees', 'feeProducts.fees', 'feeArticles', 'feeProducts.product', 'fees')->get();
    }

    public function show(TGruposComisionamiento $group)
    {
        return $group->load('feeManagements.management.structures', 'efectivityFees', 'feeManagements.fees', 'feeProducts.fees', 'feeArticles', 'feeProducts.product', 'fees');


    }

    public function update(Request $request, TGruposComisionamiento $group)
    {
        $data = $request->validate([
            'id_empresa' => 'required',
            'id_tipo' => 'required',
            'nombre_grupo' => 'required',
            'fecha_desde' => 'required',
            'fecha_hasta' => 'sometimes',
            'id' => 'required',
            'fee_managements' => 'required|array',
            'fee_managements.*.id_grupo' => 'sometimes',
            'fee_managements.*.id_gerencia' => 'required',
            'fee_managements.*.fees' => 'sometimes|array',
            'fee_managements.*.fees.*.id_comision_gerencia' => 'sometimes',
            'fee_managements.*.fees.*.zonas_desde' => 'required',
            'fee_managements.*.fees.*.zonas_hasta' => 'required',
            'fee_managements.*.fees.*.cantidad_desde' => 'required',
            'fee_managements.*.fees.*.cantidad_hasta' => 'required',
            'fee_managements.*.fees.*.importe_comision' => 'required',
            'fee_products' => 'sometimes|array',
            'fee_products.*.id_grupo' => 'sometimes',
            'fee_products.*.id_producto' => 'required',
            'fee_products.*.id_tramo' => 'required',
            'fee_products.*.fees' => 'sometimes|array',
            'fee_products.*.fees.*.id_comision_producto' => 'sometimes',
            'fee_products.*.fees.*.desde_promotores' => 'required',
            'fee_products.*.fees.*.hasta_promotores' => 'required',
            'fee_products.*.fees.*.importe_comision' => 'required',
            'fee_products.*.fees.*.importe_comision_estructura' => 'required',
            'fee_articles' => 'sometimes|array',
            'fee_articles.*.id_grupo' => 'sometimes',
            'fee_articles.*.id_articulo' => 'required',
            'fee_articles.*.unidades_lote' => 'required',
            'fees' => 'required|array',
            'fees.*.id_grupo' => 'sometimes',
            'fees.*.id_estructura' => 'required',
            'fees.*.id_tipo_contrato_comercial' => 'required',
            'fees.*.id_tipo_importe_comercial' => 'required',
            'fees.*.importe_comercial' => 'required',
            'fees.*.id_tipo_importe_estructura' => 'required',
            'fees.*.importe_estructura' => 'required',
            'fees.*.id_tipo_importe_gerencia' => 'required',
            'fees.*.importe_gerencia' => 'required',
            'fees.*.cantidad_desde' => 'required',
            'fees.*.cantidad_hasta' => 'required',
            'efectivity_fees' => 'sometimes|array',
            'efectivity_fees.*.id_grupo' => 'sometimes',
            'efectivity_fees.*.efectividad_desde' => 'required',
            'efectivity_fees.*.efectividad_hasta' => 'required',
            'efectivity_fees.*.importe_promotor' => 'required',
            'efectivity_fees.*.importe_estructura' => 'required',
            'efectivity_fees.*.importe_gerencia' => 'required',

        ]);

        $feeGroupData = Arr::except($data, [
            'fee_managements',
            'fee_products',
            'fees',
            'management',
            'fee_articles',
            'efectivity_fees',
        ]);
        foreach ($data['fee_managements'] as $management) {
            if (isset($management['id'])) {


                TGruposComisionamientoGerencias::where('id', $management['id'])->update(Arr::except($management, ['id', 'management', 'fees']));

            } else {
                //crear si es nuevo
                $management['id_grupo'] = $feeGroupData['id'];

                $retmanagement = TGruposComisionamientoGerencias::create(Arr::except($management, ['fees']));
                $management['id'] = $retmanagement['id'];

            }

            if (isset($management['fees'])) {

                foreach ($management['fees'] as $feeManagement) {


                    if (isset($feeManagement['id'])) {

                        TGruposComisionamientoGerenciasComisiones::where('id', $feeManagement['id'])->update(Arr::except($feeManagement, ['id']));

                    } else {
                        //crear si es nuevo

                        $feeManagement['id_comision_gerencia'] = $management['id'];

                        TGruposComisionamientoGerenciasComisiones::create($feeManagement);

                    }


                }
            }


        }

        foreach ($data['fee_products'] as $product) {


            if (isset($product['id'])) {

                TGruposComisionamientoProductos::where('id', $product['id'])->update(Arr::except($product, ['id', 'product', 'fees']));

            } else {
                //crear si es nuevo

                $product['id_grupo'] = $feeGroupData['id'];
                $product = TGruposComisionamientoProductos::create(Arr::except($product, ['fees']));
            }

            if (isset($product['fees'])) {

                foreach ($product['fees'] as $feeProduct) {


                    if (isset($feeProduct['id'])) {

                        TGruposComisionamientoProductosComisiones::where('id', $feeProduct['id'])->update(Arr::except($feeProduct, ['id']));

                    } else {
                        //crear si es nuevo
                        $feeProduct['id_comision_producto'] = $product['id'];
                        TGruposComisionamientoProductosComisiones::create($feeProduct);
                    }


                }
            }
        }


        foreach ($data['fees'] as $fee) {

            if (isset($fee['id'])) {

                TGruposComisionamientoComisiones::where('id', $fee['id'])->update(Arr::except($fee, ['id']));
            } else {
                //crear si es nuevo
                $fee['id_grupo'] = $feeGroupData['id'];
                TGruposComisionamientoComisiones::create($fee);
            }
        }

        foreach ($data['efectivity_fees'] as $fee) {

            if (isset($fee['id'])) {

                TGruposComisionamientoComisionesEfectividad::where('id', $fee['id'])->update(Arr::except($fee, ['id']));
            } else {
                //crear si es nuevo
                $fee['id_grupo'] = $feeGroupData['id'];
                TGruposComisionamientoComisionesEfectividad::create($fee);
            }
        }


        foreach ($data['fee_articles'] as $article) {

            if (isset($article['id'])) {

                TGruposComisionamientoArticulos::where('id', $article['id'])->update(Arr::except($article, ['id', 'article']));

            } else {
                //crear si es nuevo
                $article['id_grupo'] = $feeGroupData['id'];
                TGruposComisionamientoArticulos::create($article);
            }
        }


        $feeGroup = $group->update($feeGroupData);

        if ($feeGroup) {


            return response()->json([
                'updated' => true,
                'feeGroup' => $group->fresh()->load('feeManagements.management.structures', 'feeProducts.fees', 'feeProducts.product', 'feeArticles',
                    'efectivityFees', 'fees'),
            ]);

        } else {
            return response()->json(['updated' => false]);
        }


    }

    public function store(Request $request)
    {

        $data = $request->validate([
            'id_empresa' => 'sometimes',
            'id_tipo' => 'required',
            'nombre_grupo' => 'required',
            'fecha_desde' => 'required',
            'fecha_hasta' => 'sometimes',
            'fee_managements' => 'required|array',
            'fee_managements.*.id_grupo' => 'sometimes',
            'fee_managements.*.id_gerencia' => 'required',
            'fee_managements.*.fees' => 'sometimes|array',
            'fee_managements.*.fees.*.id_comision_gerencia' => 'sometimes',
            'fee_managements.*.fees.*.zonas_desde' => 'required',
            'fee_managements.*.fees.*.zonas_hasta' => 'required',
            'fee_managements.*.fees.*.cantidad_desde' => 'required',
            'fee_managements.*.fees.*.cantidad_hasta' => 'required',
            'fee_managements.*.fees.*.importe_comision' => 'required',
            'fee_products' => 'sometimes|array',
            'fee_products.*.id_grupo' => 'sometimes',
            'fee_products.*.id_producto' => 'required',
            'fee_products.*.id_tramo' => 'required',
            'fee_products.*.fees' => 'sometimes|array',
            'fee_products.*.fees.*.id_comision_producto' => 'sometimes',
            'fee_products.*.fees.*.desde_promotores' => 'required',
            'fee_products.*.fees.*.hasta_promotores' => 'required',
            'fee_products.*.fees.*.importe_comision' => 'required',
            'fee_products.*.fees.*.importe_comision_estructura' => 'required',
            'fee_articles' => 'sometimes|array',
            'fee_articles.*.id_grupo' => 'sometimes',
            'fee_articles.*.id_articulo' => 'required',
            'fee_articles.*.unidades_lote' => 'required',
            'fees' => 'required|array',
            'fees.*.id_grupo' => 'sometimes',
            'fees.*.id_estructura' => 'required',
            'fees.*.id_tipo_contrato_comercial' => 'required',
            'fees.*.id_tipo_importe_comercial' => 'required',
            'fees.*.importe_comercial' => 'required',
            'fees.*.id_tipo_importe_estructura' => 'required',
            'fees.*.importe_estructura' => 'required',
            'fees.*.id_tipo_importe_gerencia' => 'required',
            'fees.*.importe_gerencia' => 'required',
            'fees.*.cantidad_desde' => 'required',
            'fees.*.cantidad_hasta' => 'required',
            'efectivity_fees' => 'sometimes|array',
            'efectivity_fees.*.id_grupo' => 'sometimes',
            'efectivity_fees.*.efectividad_desde' => 'required',
            'efectivity_fees.*.efectividad_hasta' => 'required',
            'efectivity_fees.*.importe_promotor' => 'required',
            'efectivity_fees.*.importe_estructura' => 'required',
            'efectivity_fees.*.importe_gerencia' => 'required',
        ]);

        $feeGroupData = Arr::except($data, [
            'fee_managements',
            'fee_products',
            'management',
            'fees',
            'fee_articles',
            'efectivity_fees',
        ]);


        $feeGroup = TGruposComisionamiento::create($feeGroupData);

        return response()->json([
            'created' => true,
            'feeGroup' => $feeGroup->load('feeManagements.management.structures', 'efectivityFees', 'feeProducts.fees', 'feeProducts.product', 'feeArticles',
                'fees'),
        ]);


    }

    public function destroy(TGruposComisionamiento $group)
    {


        if ($group->delete()) {
            return response()->json(['deleted' => true]);
        } else {
            return response()->json(['deleted' => false]);
        }


    }

}
