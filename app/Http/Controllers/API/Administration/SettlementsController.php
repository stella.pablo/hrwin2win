<?php

namespace App\Http\Controllers\API\Administration;

use App\Http\Controllers\Controller;
use App\Models\External\TComerciales;
use App\Models\External\TGerencias;
use App\Models\External\TLiquidaciones;
use App\Models\External\TLiquidacionesComerciales;
use App\Models\External\TLiquidacionesComercialesConceptos;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use PDF;

class SettlementsController extends Controller
{

	public function index()
	{
		return TLiquidaciones::with('commercialSettlements.concepts')->get();
	}

	public function show(TLiquidaciones $settlement)
	{

		return $settlement->load('commercialSettlements.concepts.line.article', 'commercialSettlements.concepts.sale.articles', 'commercialSettlements.commercial','commercialSettlements.concepts.contract.products');


	}

	public function store(Request $request)
	{
		$data = $request->validate([
			'id_empresa'                 => 'sometimes',
			'observaciones'              => 'sometimes',
			'id_gerencia'                => 'sometimes',
			'id_comercial'               => 'sometimes',
			'fecha_liquidacion'          => 'required',
			'fecha_desde'                => 'required',
			'fecha_hasta'                => 'required',
			'id_tipo_contrato_comercial' => 'required',

		]);

		$settlement = TLiquidaciones::create($data);

		return response()->json(['created' => true, 'settlement' => $settlement->load('commercialSettlements.concepts')]);


	}


	public function update(Request $request, TLiquidaciones $settlement)
	{


		$data = $request->validate([
			'id'                                                           => 'required',
			'id_empresa'                                                   => 'sometimes',
			'id_gerencia'                                                  => 'sometimes',
			'observaciones'                                                => 'sometimes',
			'id_comercial'                                                 => 'sometimes',
			'fecha_liquidacion'                                            => 'required',
			'fecha_desde'                                                  => 'required',
			'fecha_hasta'                                                  => 'required',
			'id_tipo_contrato_comercial'                                   => 'required',
			'commercial_settlements'                                       => 'sometimes',
			'commercial_settlements.*.importe_otros'                       => 'sometimes',
			'commercial_settlements.*.id'                                  => 'required',
			'commercial_settlements.*.concepts'                            => 'sometimes',
			'commercial_settlements.*.concepts.*.id'                       => 'sometimes',
			'commercial_settlements.*.concetps.*.concepto'                 => 'required',
			'commercial_settlements.*.concepts.*.id_comercial'             => 'required',
			'commercial_settlements.*.concepts.*.id_contrato'              => 'sometimes',
			'commercial_settlements.*.concepts.*.id_grupo'                 => 'sometimes',
			'commercial_settlements.*.concepts.*.id_linea_venta'           => 'sometimes',
			'commercial_settlements.*.concepts.*.id_liquidacion'           => 'required',
			'commercial_settlements.*.concepts.*.id_liquidacion_comercial' => 'required',
			'commercial_settlements.*.concepts.*.id_venta'                 => 'sometimes',
			'commercial_settlements.*.concepts.*.importe'                  => 'required',
			'commercial_settlements.*.concepts.*.tipo_linea'               => 'required',
			'',

		]);
		//only header
		$settlementData = [
			'observaciones' => $data['observaciones'],
		];


		foreach ($data['commercial_settlements'] as $commercialSettlement) {
			$totalOthers = 0;
			foreach ($commercialSettlement['concepts'] as $concept) {
				//actualiza
				if (isset($concept['id'])) {
					//no es automática
					if ($concept['tipo_linea'] != 0) {

						TLiquidacionesComercialesConceptos::where('id', $concept['id'])->update(Arr::except($concept, ['id', 'line', 'sale']));
						$totalOthers += $concept['importe'];

					}

				} else {
					//crear si es nuevo
					TLiquidacionesComercialesConceptos::create($concept);
					$totalOthers += $concept['importe'];
				}
			}

			$dataCommercial = [

				'importe_otros' => $totalOthers,
			];

			TLiquidacionesComerciales::where('id', $commercialSettlement['id'])->update($dataCommercial);

		}

		$ret = $settlement->update($settlementData);
		if ($ret) {

			return response()->json([
				'updated'    => true,
				'settlement' => $settlement->fresh()->load('commercialSettlements.concepts.line.article', 'commercialSettlements.concepts.sale.articles', 'commercialSettlements.commercial'),
			]);

		} else {
			return response()->json(['updated' => false]);
		}
	}

	public function destroy(TLiquidaciones $settlement)
	{


		if ($settlement->delete()) {
			return response()->json(['deleted' => true]);
		} else {
			return response()->json(['deleted' => false]);
		}


	}


	public function getManagementSettlements(TGerencias $management)
	{


		$management_id = $management->id_gerencia;

		return TLiquidaciones::whereHas('commercialSettlements', function ($query) use ($management_id) {

			$query->where('id_gerencia', $management_id);

		})->with([
			'commercialSettlements' => function ($query) use ($management_id) {

				$query->where('id_gerencia', $management_id)->with('concepts.sale')->with('concepts.line')
					->with('concepts.deliveryLine.delivery')
					->with('concepts.deliveryNoteLine.deliveryNote')
					->with('commercial')
			;
			},

		])->with('company')->get();


	}








	public function getCommercialSettlements(TComerciales $commercial)
	{


		$commercial_id = $commercial->id_comercial;

		return TLiquidaciones::whereHas('commercialSettlements', function ($query) use ($commercial_id) {

			$query->where('id_comercial', $commercial_id);

		})->with([
			'commercialSettlements' => function ($query) use ($commercial_id) {

				$query->where('id_comercial', $commercial_id)->with('concepts.sale')->with('concepts.line');
			},

		])->get();


	}


	public function pdfSettlement(Request $request, TLiquidaciones $settlement)
	{


		$data = $settlement->append('totalTax','contractType');
		$view = 'templates._components.pdfSettlement';

		$date = $settlement->fecha_liquidacion->todateString();

		//return view($view,['data'=>$data]);
        $settlementName = 'Settlement #'. $settlement->id . ' on '. $date.".pdf";



		return PDF::loadView($view, compact('data'))->setPaper('a4', 'landscape')->download($settlementName);


	}


	public function pdfCommercialSettlement($commercialSettlement)
	{


		$data = TLiquidacionesComerciales::where('id',$commercialSettlement)
			->with('settlement.company', 'commercial', 'concepts.line.article','concepts.deliveryLine.delivery','concepts.deliveryNoteLine.deliveryNote','concepts.product.contract', 'concepts.product.product')
			->first();
		$view = 'templates._components.pdfCommercialSettlement';


		$date = $data->settlement->fecha_liquidacion->todateString();

        $settlementName = 'Settlement #'. $data->id_liquidacion . ' for '. $data->commercial->nombre. ' on '. $date.".pdf";



		return PDF::loadView($view, compact('data'))
			->setPaper('a4', 'landscape')
			->download($settlementName);


	}

	public function pdfManagementSettlement(TGerencias $management, $settlement)
	{

		/*$data = $this->getManagementSettlements($management);*/


		$total = 0;

		$data =  TLiquidaciones::whereHas('commercialSettlements', function ($query) use ($management) {
			$query->where('id_gerencia', $management->id_gerencia);
		})->with([
			'commercialSettlements' => function ($query) use ($management) {
				$query->where('id_gerencia', $management->id_gerencia)
					->with('concepts')
					->with('commercial')
			;
			},

		])
			->where('id',$settlement)
			->with('company')
			->first();



		foreach ($data['commercialSettlements'] as $commercial){

			$total += $commercial->total;
		}


		$date = $data->fecha_liquidacion->todateString();

        $settlementName = 'Settlement #'. $settlement . ' for '. $management->id_gerencia. ' on '. $date.".pdf";




		$data['pdfTotal'] =  $total;
		$data['pdfManagement'] = $management;
		$view = 'templates._components.pdfManagementSettlement';

		return PDF::loadView($view, compact('data'))
			->setPaper('a4', 'landscape')
			->download($settlementName);


	}


}


