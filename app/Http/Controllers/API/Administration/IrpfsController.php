<?php

namespace App\Http\Controllers\API\Administration;

use App\Http\Controllers\Controller;
use App\Models\External\TIrpfs;


class IrpfsController extends Controller
{

	public function index()
	{
		return TIrpfs::get();
	}

	public function show(TIrpfs $irpf)
	{
		return $irpf;


	}

}
