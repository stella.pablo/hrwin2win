<?php

namespace App\Http\Controllers\API\Administration;

use App\Http\Controllers\Controller;
use App\Models\External\TGruposComisionamientoComisiones;


class FeesGroupFeesController extends Controller
{

	public function index()
	{
		return TGruposComisionamientoComisiones::get();
	}

	public function show(TGruposComisionamientoComisiones $fee)
	{
		return $fee;


	}

	public function destroy(TGruposComisionamientoComisiones $fee){

		return ['status' => $delete = $fee->delete()];
	}

}
