<?php

namespace App\Http\Controllers\API\Administration;

use App\Http\Controllers\Controller;
use App\Models\External\TItaus;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class ItausController extends Controller
{

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		return TItaus::get();
	}

	/**
	 * Display the specified resource.
	 *
	 * @param \App\Models\TItaus $tarifa
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show(TItaus $itau)
	{



	}

	public function store(Request $request)
	{
	}


	public function update(Request $request, TItaus $itau)
	{



	}




}
