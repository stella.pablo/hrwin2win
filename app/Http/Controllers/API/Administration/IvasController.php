<?php

namespace App\Http\Controllers\API\Administration;

use App\Http\Controllers\Controller;
use App\Models\External\TIvas;


class IvasController extends Controller
{

	public function index()
	{
		return Tivas::get();
	}

	public function show(TIva $iva)
	{
		return $iva;


	}

}
