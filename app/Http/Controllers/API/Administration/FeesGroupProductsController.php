<?php

namespace App\Http\Controllers\API\Administration;

use App\Http\Controllers\Controller;
use App\Models\External\TGruposComisionamientoProductos;
use App\Models\External\TGruposComisionamientoProductosComisiones;


class FeesGroupProductsController extends Controller
{

	public function index()
	{
		return TGruposComisionamientoProductos::get();
	}

	public function show(TGruposComisionamientoProductos $product)
	{
		return $product;


	}

	public function update(Request $request, TGruposComisionamientoProductos $product)
	{

		return $product;
	}

	public function store(TGruposComisionamientoProductos $product)
	{

		return $product;
	}

	public function destroy(TGruposComisionamientoProductos $product)
	{

		//destroy fees

		TGruposComisionamientoProductosComisiones::where('id_comision_producto',$product['id'])->delete();

		return ['status' => $delete = $product->delete()];
	}

}
