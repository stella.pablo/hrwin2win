<?php

namespace App\Http\Controllers\API\Administration;

use App\Http\Controllers\Controller;
use App\Models\External\TTiposContratosComercial;
use App\Models\External\TTiposContratosComercialTramos;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class ContractsTypesController extends Controller
{

	public function index()
	{
		return TTiposContratosComercial::with('stretches','iva', 'irpf')->get();
	}

	public function show(TTiposContratosComercial $contractType)
	{
		return $contractType->load('stretches');


	}

	public function store(Request $request)
	{
		$data = $request->validate([
			'id_empresa'                             => 'sometimes',
			'tipo_contrato_comercial'                => 'required',
			'id_tipo_contrato_comercial'             => 'sometimes',
			'id_iva'                                 => 'sometimes',
			'id_irpf'                                => 'sometimes',
			'stretches'                              => 'required|array',
			'stretches.*.id_tramo'                   => 'sometimes',
			'stretches.*.id_empresa'                 => 'sometimes',
			'stretches.*.id_tipo_contrato_comercial' => 'sometimes',
			'stretches.*.descripcion'                => 'required',
			'stretches.*.sueldo_base' => 'required',
			'stretches.*.horas'       => 'required',

		]);

		$contractTypeData = Arr::except($data, [
			'stretches','iva','irpf'
		]);

		$contractType = TTiposContratosComercial::create($contractTypeData);

		return response()->json(['created' => true, 'contractType' => $contractType->load('stretches','iva','irpf')]);


	}

	public function update(Request $request, TTiposContratosComercial $contractType)
	{
		$data = $request->validate([
			'id_empresa'                             => 'required',
			'tipo_contrato_comercial'                => 'required',
			'id_tipo_contrato_comercial'             => 'required',
			'id_iva'                                 => 'sometimes',
			'id_irpf'                                => 'sometimes',
			'stretches'                              => 'required|array',
			'stretches.*.id_tramo'                   => 'sometimes',
			'stretches.*.id_empresa'                 => 'sometimes',
			'stretches.*.id_tipo_contrato_comercial' => 'sometimes',
			'stretches.*.descripcion'                => 'required',
			'stretches.*.sueldo_base' => 'required',
			'stretches.*.horas'       => 'required',

		]);


		$contractTypeData = Arr::except($data, [
			'stretches','iva','irpf'
		]);

		$contractType->update($contractTypeData);

		foreach ($data['stretches'] as $stretch) {

			if (isset($stretch['id_tramo'])) {

				TTiposContratosComercialTramos::where('id_tramo', $stretch['id_tramo'])->update(Arr::except($stretch, ['id_tramo', 'id_empresa']));
			} else {
				//crear si es nuevo
				$stretch['id_empresa'] = '01';
				$stretch['comision_negativa'] = 'S';
				$stretch['id_tipo_contrato_comercial'] = $contractType['id_tipo_contrato_comercial'];

				TTiposContratosComercialTramos::create($stretch);


			}


		}


		return response()->json(['updated' => true, 'contractType' => $contractType->fresh()->load('stretches','iva','irpf')]);

	}
    public function destroy(TTiposContratosComercial $contractType)
    {

		$contractType->load('stretches');

		if ($contractType->delete()){
			return response()->json(['deleted' => true]);
		}else{
			return response()->json(['deleted' => false]);
		}



    }


}
