<?php

namespace App\Http\Controllers\API\Administration;

use App\Http\Controllers\Controller;
use App\Models\External\TGruposComisionamientoArticulos;


class FeesGroupArticlesController extends Controller
{

	public function index()
	{
		return TGruposComisionamientoArticulos::get();
	}

	public function show(TGruposComisionamientoArticulos $article)
	{
		return $article;


	}

	public function update(Request $request, TGruposComisionamientoArticulos $article)
	{

		return $article;
	}

	public function store(TGruposComisionamientoArticulos $article)
	{

		return $article;
	}

	public function destroy(TGruposComisionamientoArticulos $article)
	{

		return ['status' => $delete = $article->delete()];
	}

}
