<?php

namespace App\Http\Controllers\API\Administration;

use App\Http\Controllers\Controller;
use App\Models\External\TTarifas;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class RatesController extends Controller
{

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		return TTarifas::get();
	}

	/**
	 * Display the specified resource.
	 *
	 * @param \App\Models\TTarifas $tarifa
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show(TTarifas $province)
	{



	}

	public function store(Request $request)
	{
	}


	public function update(Request $request, TTarifas $province)
	{



	}




}
