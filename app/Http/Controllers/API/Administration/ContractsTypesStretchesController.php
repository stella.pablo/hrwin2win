<?php

namespace App\Http\Controllers\API\Administration;

use Illuminate\Support\Facades\Request;
use App\Http\Controllers\Controller;
use App\Models\External\TTiposContratosComercialTramos;


class ContractsTypesStretchesController extends Controller
{

	public function index(Request $request)
	{
		return TTiposContratosComercialTramos::where('id_tipo_contrato_comercial','=',$request->id_tipo_contrato_comercial)->get();
	}
    public function store(Request $request)
    {

    	$contractType['id_empresa'] =$request['id_empresa'];
		$contractType['id_iva'] = $request['id_iva'];
		$contractType['id_irpf'] =$request['id_irpf'];
		$contractType['tipo_contrato_comercial'] =$request['tipo_contrato_comercial'];


    	return(TTiposContratosComercialTramos::create($contractType));

    }

    public function destroy($stretch)
    {

		$contractType = TTiposContratosComercialTramos::where('id_tramo',$stretch)->first();
		if ($contractType){
			return ['status' => $contractType->delete()];
		}else{
			return response()->json(['deleted' => false]);
		}



    }
    public function update(Request $request){


    	$contractType = TTiposContratosComercialTramos::where('id_tramo',$request['id_tramo'])->first();
		$contractType['tipo_contrato_comercial'] =$request['tipo_contrato_comercial'];
		$contractType['id_iva'] =$request['id_iva'];
		$contractType['id_irpf'] =$request['id_irpf'];
    	$contractType->update();
		return response()->json(['updated' => true]);
	}



}
