<?php

namespace App\Http\Controllers\API\Administration;

use App\Http\Controllers\Controller;

use App\Models\External\TGruposComisionamientoComisionesEfectividad;


class FeesGroupEfectivityFeesController extends Controller
{

	public function index()
	{
		return TGruposComisionamientoComisionesEfectividad::get();
	}

	public function show(TGruposComisionamientoComisionesEfectividad $fee)
	{
		return $fee;


	}

	public function destroy(TGruposComisionamientoComisionesEfectividad $fee){

		return ['status' => $delete = $fee->delete()];
	}

}
