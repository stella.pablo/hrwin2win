<?php

namespace App\Http\Controllers\API\Administration;

use App\Http\Controllers\Controller;
use App\Models\External\TGruposComisionamientoGerenciasComisiones;


class FeesGroupManagementFeesController extends Controller
{

	public function index()
	{
		return TGruposComisionamientoGerenciasComisiones::get();
	}

	public function show(TGruposComisionamientoGerenciasComisiones $fee)
	{
		return $fee;


	}

	public function update(Request $request, TGruposComisionamientoGerenciasComisiones $fee)
	{

		return $fee;
	}

	public function store(TGruposComisionamientoGerenciasComisiones $fee)
	{

		return $fee;
	}

	public function destroy(TGruposComisionamientoGerenciasComisiones $fee)
	{

		return ['status' => $delete = $fee->delete()];
	}

}
