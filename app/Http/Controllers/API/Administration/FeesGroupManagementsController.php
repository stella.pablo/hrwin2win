<?php

namespace App\Http\Controllers\API\Administration;

use App\Http\Controllers\Controller;
use App\Models\External\TGruposComisionamientoGerencias;
use App\Models\External\TGruposComisionamientoGerenciasComisiones;
use Illuminate\Support\Facades\Request;


class FeesGroupManagementsController extends Controller
{

	public function index()
	{
		return TGruposComisionamientoGerencias::get();
	}

	public function show(TGruposComisionamientoGerencias $management)
	{
		return $management;


	}

	public function destroy(TGruposComisionamientoGerencias $management)
	{

		//destroy fees

		TGruposComisionamientoGerenciasComisiones::where('id_comision_gerencia',$management['id'])->delete();

		return ['status' => $delete = $management->delete()];

	}

	public function store(Request $request){

	}

	public function update (Request $request, TGruposComisionamientoGerencias $management){


	}

}
