<?php

namespace App\Http\Controllers\API\Administration;

use App\Http\Controllers\Controller;
use App\Models\External\TGerencias;
use App\Models\External\TGerenciasEstructuras;
use Illuminate\Http\Request;


class ManagementStructuresController extends Controller
{

	public function index(Request $request)
	{
		return TGerenciasEstructuras::where('id_gerencia', '=', $request->id_gerencia)->get();
	}



	public function store(Request $request)
	{


    	$structure['id_empresa'] ='01';
		$structure['id_gerencia'] = $request['id_gerencia'];
		$structure['nombre'] =$request['nombre'];
		$structure['id_tipo_estructura'] = $request['id_tipo_estructura'];
		$structure['id_gerencia_estructura']= str_pad(TGerenciasEstructuras::where('id_gerencia','=',$structure['id_gerencia'])->count()+1, 3,'0', STR_PAD_LEFT);

		return TGerenciasEstructuras::create($structure);

	}

	public function destroy($management, $structure)
	{




		$struct = TGerenciasEstructuras::where('id_gerencia', '=', $management)->where('id_gerencia_estructura', '=', $structure)->first();




		return ['status' => !isset($struct['id_gerencia_estructura']) ? false : $struct->delete()];
	}
	public function update(TGerencias $management, TGerenciasEstructuras $structure, Request $request){

		$data=[
			'nombre'=>$request->nombre,
			'id_tipo_estructura' => $request->id_tipo_estructura

		];

		return TGerenciasEstructuras::where('id_gerencia',$management->id_gerencia)->where('id_gerencia_estructura',$structure->id_gerencia_estructura)->update($data);




	}
}
