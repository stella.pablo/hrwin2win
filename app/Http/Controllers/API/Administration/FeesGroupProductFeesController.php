<?php

namespace App\Http\Controllers\API\Administration;

use App\Http\Controllers\Controller;
use App\Models\External\TGruposComisionamientoProductosComisiones;


class FeesGroupProductFeesController extends Controller
{

	public function index()
	{
		return TGruposComisionamientoProductosComisiones::get();
	}

	public function show(TGruposComisionamientoProductosComisiones $fee)
	{
		return $fee;


	}

	public function update(Request $request, TGruposComisionamientoProductosComisiones $fee)
	{

		return $fee;
	}

	public function store(TGruposComisionamientoProductosComisiones $fee)
	{

		return $fee;
	}

	public function destroy(TGruposComisionamientoProductosComisiones $fee)
	{

		return ['status' => $delete = $fee->delete()];
	}

}
