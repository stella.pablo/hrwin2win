<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Candidate;
use App\Models\CandidatesFile;
use App\Models\CandidatesFilesType;
use App\Search\CandidateSearch;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Validation\Rule;


class CandidateController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return CandidateSearch::apply($request);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'comment' => 'sometimes',
            'email' => 'sometimes|email',
            'email_type_id' => 'sometimes|numeric',
            'lastname' => 'sometimes',
            'management_id' => Rule::requiredIf(function () use ($request) {
                return auth()->user()->level < 3;
            }),
            'name' => 'required',
            'origin_id' => 'required',
            'petitioned_at' => 'required|date',
            'phone' => 'sometimes',
            'surname' => 'sometimes',
        ]);

        $candidateData = Arr::except($data, ['email_type_id']);

        if (auth()->user()->level >= 3) {
            $candidateData['management_id'] = auth()->user()->management_id;
        }
        $candidateData['user_id'] = auth()->user()->id;

        return Candidate::create($candidateData);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Candidate $candidate
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Candidate
            ::withTrashed()
            ->findOrFail($id)
            ->load('files', 'emails.user', 'management.external');
    }

    public function checkIsValid($commercial, $type, $phone)
    {

        if ($type == 'store') {

            return response()->json(['valid' => Candidate::where('phone', '=', $phone)->count() < 1]);

        } else {

            return response()->json(['valid' => Candidate::where('phone', '=', $phone)->where('id', '<>', $commercial)->count() < 1]);

        }

    }

    public function checkHireAvailability(Candidate $candidate)
    {

        $requiredFileTypes = CandidatesFilesType::where('company_id', auth()->user()->company_id)->where('required', 1)->get();

        foreach ($requiredFileTypes as $fileType) {



            if (!CandidatesFile::where('candidate_id', $candidate->id)->where('type_id',$fileType->id)->first()) {

                return response()->json(['available' => false, 'error' => $fileType->name . ' not available!']);

            }

        }

        return response()->json(['available' => true]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Candidate $candidate
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Candidate $candidate)
    {
        $data = $request->validate([
            'approved' => 'sometimes',
            'approved_at' => 'sometimes',
            'approved_user_id' => 'sometimes',
            'comment' => 'sometimes',
            'email' => 'sometimes',
            'email_type_id' => 'sometimes',
            'external_id' => 'sometimes',
            'hired' => 'sometimes',
            'hired_at' => 'sometimes',
            'hired_user_id' => 'sometimes',
            'lastname' => 'sometimes',
            'management_id' => 'sometimes|exists:managements,id',
            'name' => 'sometimes',
            'origin_id' => 'sometimes',
            'petitioned_at' => 'sometimes|date',
            'phone' => 'sometimes',
            'preapproved' => 'sometimes',
            'preapproved_at' => 'sometimes',
            'preapproved_user_id' => 'sometimes',
            'surname' => 'sometimes',

        ]);

        $candidate->update($data);

        return $candidate->fresh()->load('files', 'emails', 'management');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Candidate $candidate
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Candidate $candidate)
    {
        $candidate->delete();

        return $candidate->fresh()->load('files', 'emails');
    }

    public function external($external_id)
    {
        return Candidate::where('external_id', $external_id)->first();
    }
}
