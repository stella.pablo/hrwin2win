<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\External\TproductosTramos;
use Illuminate\Http\Request;


class ProductsStretchesController extends Controller
{
	public function index(Request $request)
	{
		return TproductosTramos::where('id_producto','=',$request->id_producto)->get();
	}
    public function store(Request $request)
    {

    	$stretch['id_empresa'] =$request['id_empresa'];
		$stretch['descripcion'] = $request['descripcion'];
		$stretch['tipo_tramo'] =$request['tipo_tramo'];
		$stretch['precio'] =$request['precio'];
		$stretch['id_producto'] =$request['id_producto'];


    	return(TTiposContratosComercialTramos::create($stretch));

    }

    public function destroy($stretch)
    {


		$stretch = TproductosTramos::where('id',$stretch)->first();
		if ($stretch){

			return response()->json(['status' => $delete = $stretch->delete()]);

		}else{
			return response()->json(['status' => false]);
		}



    }
    public function update(Request $request){


    	$stretch = TproductosTramos::where('id',$request['id'])->first();
		$stretch['descripcion'] =$request['descripcion'];
		$stretch['tipo_tramo'] =$request['tipo_tramo'];
		$stretch['precio'] =$request['precio'];
    	$stretch->update();
		return response()->json(['updated' => true]);
	}

}


