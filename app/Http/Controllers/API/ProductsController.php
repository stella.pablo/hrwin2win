<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\External\TProductos;
use App\Models\External\TProductosTramos;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class ProductsController extends Controller
{

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		return Tproductos::with('stretches')->get();
	}

	/**
	 * Display the specified resource.
	 *
	 * @param \App\Models\Tproductos $producto
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show(TProductos $product)
	{
		return $product->load('stretches');


	}

	public function store(Request $request)
	{
		$data = $request->validate([
			'id_empresa'              => 'sometimes',
			'id_producto'             => 'sometimes',
			'producto'                => 'required',
			'id_tipo_producto'        => 'required',
			'tipo_contrato'           => 'required',
			'maximo_lineas'           => 'required',
			'activo'                  => 'required',
			'stretches'               => 'required|array',
			'stretches.*.id'          => 'sometimes',
			'stretches.*.id_empresa'  => 'sometimes',
			'stretches.*.id_producto' => 'sometimes',
			'stretches.*.descripcion' => 'required',
			'stretches.*.tipo_tramo'  => 'required',
			'stretches.*.precio'      => 'sometimes',

		]);

		$productData = Arr::except($data, [
			'stretches',
		]);

		$product = TProductos::create($productData);

		return response()->json(['created' => true, 'product' => $product->load('stretches')]);


	}


	public function update(Request $request, Tproductos $product)
	{


		$data = $request->validate([
			'id_empresa'              => 'required',
			'id_producto'             => 'required',
			'producto'                => 'required',
			'id_tipo_producto'        => 'required',
			'activo'                  => 'required',
			'tipo_contrato'           => 'required',
			'maximo_lineas'           => 'required',
			'stretches'               => 'required|array',
			'stretches.*.id'          => 'sometimes',
			'stretches.*.id_empresa'  => 'sometimes',
			'stretches.*.id_producto' => 'sometimes',
			'stretches.*.descripcion' => 'required',
			'stretches.*.tipo_tramo'  => 'required',
			'stretches.*.precio'      => 'sometimes',

		]);


		$productData = Arr::except($data, [
			'stretches',

		]);

		$product->update($productData);

		foreach ($data['stretches'] as $stretch) {

			if (isset($stretch['id'])) {

				TproductosTramos::where('id', $stretch['id'])->update(Arr::except($stretch, ['id', 'id_empresa']));
			} else {
				//crear si es nuevo
				$stretch['id_empresa'] = '01';
				$stretch['id_producto'] = $product['id_producto'];

				TproductosTramos::create($stretch);


			}


		}


		return response()->json(['updated' => true, 'product' => $product->fresh()->load('stretches')]);


	}
	public function destroy(TProductos $product)
	{


		$product->load('stretches');

		return ['deleted' => $delete = $product->delete()];


	}

}


















