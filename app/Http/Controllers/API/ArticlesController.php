<?php

namespace App\Http\Controllers\API;


use App\Http\Controllers\Controller;
use App\Models\External\TArticulos;
use App\Models\External\TVentasLineas;
use Carbon\Carbon;
use DB;
use Freshbitsweb\Laratables\Laratables;
use Illuminate\Http\Request;

class ArticlesController extends Controller
{

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request)
	{


		return TArticulos::with('packages')->get();
	}


//	return TArticulos::get();

	/*[dbo].[F_VENTAS_PRECIO_VENTA_GERENCIA] (@ID_EMPRESA VARCHAR(2),@ID_ARTICULO VARCHAR(100),@CANTIDAD NUMERIC(18,2),@UNIDADES_LOTE NUMERIC(18,6), @ID_DIVISA VARCHAR(50),  @FECHA DATETIME)
			return TArticulos::select( [DB::raw('ISNULL(SUM(t_ubicaciones.cantidad), 0) as quantity')])
				->where('t_articulos.descripcion', 'LIKE', '%' . $request->search . '%')
				->leftJoin('t_ubicaciones', 't_ubicaciones.id_articulo', '=', 't_articulos.id_articulo')
				->groupBy($select)
				->get();*/


	public function datatables()
	{
		return Laratables::recordsOf(TArticulos::class);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param \App\Models\TArticulo $article
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show(TArticulos $article)
	{


		return $article->load('packages', 'sales');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param \App\Models\TArticulo    $article
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update(
		Request $request,
		TArticulos $article
	) {
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param \App\Models\TArticulo $article
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(
		TArticulos $article
	) {
		//
	}


	public function top(Request $request)
	{
		if ($request->has('id_comercial')) {

			return DB::connection(session('database'))->select('SELECT tpl.id_articulo as product_id, ta.descripcion as title, SUM(tpl.cantidad) as qty FROM t_pedidos_lin tpl, t_articulos ta, 
			t_pedidos_cab	tpc WHERE tpl.id_articulo = ta.id_articulo AND tpc.id_comercial = ? AND tpc.id_pedido = tpl.id_pedido GROUP BY tpl.id_articulo, ta.descripcion', [$request->id_comercial]);

		} elseif ($request->has('id_gerencia')) {

			return DB::connection(session('database'))->select('SELECT tpl.id_articulo as product_id, ta.descripcion as title, SUM(tpl.cantidad) as qty FROM t_pedidos_lin tpl, t_articulos ta, t_pedidos_cab tpc WHERE tpl.id_articulo = ta.id_articulo AND tpc.id_gerencia = ? AND tpc.id_pedido = tpl.id_pedido GROUP BY tpl.id_articulo, ta.descripcion',
				[$request->id_gerencia]);

		} else {

			return DB::connection(session('database'))->select('SELECT tpl.id_articulo as product_id, ta.descripcion as title, SUM(tpl.cantidad) as qty FROM t_pedidos_lin tpl, t_articulos ta WHERE tpl.id_articulo = ta.id_articulo  GROUP BY tpl.id_articulo, ta.descripcion');
		}
	}


	public function topSales(Request $request)
	{


		$time = Carbon::now();

		if (request()->has('timeLength')) {


			if (request()->timeLength === 'today') {
				$start = Carbon::now()->startOfDay()->format('Y-m-d H:i:s');
				$end = Carbon::now()->endOfDay()->format('Y-m-d H:i:s');

			} elseif (request()->timeLength === 'week') {
				$start = $time->startOfWeek()->format('Y-m-d H:i:s');
				$end = $time->endOfWeek()->format('Y-m-d H:i:s');

			} elseif (request()->timeLength === 'month') {
				$start = $time->startOfMonth()->format('Y-m-d H:i:s');
				$end = $time->endOfMonth()->format('Y-m-d H:i:s');
			} elseif (request()->has('year')) {

				$start = $time->startOfYear()->format('Y-m-d H:i:s');
				$end = $time->endOfYear()->format('Y-m-d H:i:s');
			} elseif (request()->timeLength === 'with_trashed' AND request()->has('date_from') AND request()->has('date_to')) {

				$start = new Carbon(request()->date_from);
				$start = $start->startOfDay()->format('Y-m-d H:i:s');

				$end = new Carbon(request()->date_to);
				$end = $end->startOfDay()->format('Y-m-d H:i:s');


			} else {
				$start = $time->startOfMonth()->format('Y-m-d H:i:s');
				$end = $time->endOfMonth()->format('Y-m-d H:i:s');
			}
		} else {
			$start = $time->startOfMonth()->format('Y-m-d H:i:s');
			$end = $time->endOfMonth()->format('Y-m-d H:i:s');

		}
		$dateString = ' AND tv.fecha Between Cast(\'' . $start . '\' As DATE) AND Cast(\'' . $end . '\' As DATE)';


		if ($request->has('id_comercial')) {

			//dd("SELECT tvl.id_articulo as product_id, ta.descripcion as title, SUM(tvl.cantidad) as qty FROM t_ventas_lineas tvl, t_articulos ta, t_ventas tv WHERE tvl.id_articulo = ta
			//.id_articulo AND tv.id_comercial = ? AND tv.id_venta = tvl.id_venta \' . $dateString . \' GROUP BY tvl.id_articulo, ta.descripcion\'");
			return DB::connection(session('database'))->select('SELECT tvl.id_articulo as product_id, ta.descripcion as title, SUM(tvl.cantidad) as qty FROM t_ventas_lineas tvl, t_articulos ta, t_ventas tv WHERE tvl.id_articulo = ta.id_articulo AND tv.id_comercial = ? AND tv.id_venta = tvl.id_venta ' . $dateString . ' GROUP BY tvl.id_articulo, ta.descripcion',
				[$request->id_comercial]);


		} elseif ($request->has('id_gerencia')) {
			/*dd("SELECT tvl.id_articulo as product_id, ta.descripcion as title, SUM(tvl.cantidad) as qty FROM t_ventas_lineas tvl, t_articulos ta, t_ventas tv WHERE tvl.id_articulo = ta.id_articulo AND tv.id_comercial = ? AND tv.id_venta = tvl.id_venta \' . $dateString . \' GROUP BY tvl.id_articulo, ta.descripcion'");*/
			return DB::connection(session('database'))->select('SELECT tvl.id_articulo as product_id, ta.descripcion as title, SUM(tvl.cantidad) as qty FROM t_ventas_lineas tvl, t_articulos ta, t_ventas tv WHERE tvl.id_articulo = ta.id_articulo AND tv.id_gerencia = ? AND tv.id_venta = tvl.id_venta ' . $dateString . ' GROUP BY tvl.id_articulo, ta.descripcion',
				[$request->id_gerencia]);


		} else {

			//$stats = TVentasLineas::get()->load('sale')->whereIn('sale.id_comercial', auth()->user()->clean_tree)->whereBetween('fecha', [$start, $end])->groupBy('id_articulo');
			$stats = TVentasLineas::get()->load('sale')->whereIn('sale.id_comercial', auth()->user()->clean_tree)->whereBetween('sale.fecha', [$start, $end])->groupBy('id_articulo');


			$return = [];

			foreach ($stats as $stat) {
				$rt = [];
				$rt['product_id'] = $stat[0]['id_articulo'];
				$rt['title'] = $stat[0]['article']['descripcion'];

				$q = 0;
				$t = 0;
				foreach ($stat as $micro) {

					$q += $micro['cantidad'];
					$t += $micro['total'];

				}
				$rt['qty'] = $q;
				$rt['total'] = $t;

				$return[] = $rt;
			}


			return $return;

		}
	}


}
