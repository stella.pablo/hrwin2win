<?php

namespace App\Http\Controllers\API\Commercials;

use App\Http\Controllers\Controller;
use App\Models\CommercialsAbsence;
use Illuminate\Http\Request;

class AbsenceController extends Controller
{

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request)
	{
		$absences = CommercialsAbsence
			::where('company_id', session('company_id'))
			->with(['commercial'])
			->orderBy('date', 'desc');

		if ($request->filled('date_from') AND $request->filled('date_to')) {
			$absences->whereBetween('date', [$request->date_from, $request->date_to]);
		}

		return $absences->get();
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$data = $request->all();
		$data['company_id'] = session('company_id');

		$store = CommercialsAbsence::create($data);

		return $store->load('motive');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param \App\CommercialsAbsence $absence
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show(CommercialsAbsence $absence)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param \App\CommercialsAbsence  $absence
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, CommercialsAbsence $absence)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param \App\CommercialsAbsence $absence
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(CommercialsAbsence $absence)
	{
		return ['status' => $absence->delete()];
	}
}
