<?php

namespace App\Http\Controllers\API\Commercials;

use App\Http\Controllers\Controller;
use App\Models\External\TComerciales;
use App\Models\External\TEstrategias;
use App\Models\External\TGerencias;
use Illuminate\Http\Request;

class StrategyController extends Controller
{

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request)
	{
		if (session('database') !== 'mexico') {
			return [];
		}

		$managementsIds = auth()->user()->managements->pluck('external_id')->toArray();
		$strategies = TEstrategias::query();

		if ($request->filled('management_id')) {
			$strategies->where('id_gerencia', $request->management_id);
		}

		if ($request->filled('organization_id')) {
			$managementsIds = TGerencias
				::where('id_organizacion', $request->organization_id)
				->get()
				->pluck('id_gerencia')
				->toArray();
		}

		return $strategies
			->whereIn('id_gerencia', $managementsIds)
			->orderBy('id_estrategia')
			->get();
	}

	public function store(Request $request)
	{
		$data = $request->validate([
			'id_gerencia'   => 'required',
			'id_estrategia' => 'required',
			'estrategia'    => 'required',
		]);

		return TEstrategias::create($data);
	}

	public function update(Request $request, TEstrategias $strategy)
	{
		$data = $request->validate([
			'id_gerencia'   => 'required',
			'id_estrategia' => 'required',
			'estrategia'    => 'required',
		]);

		$strategy->update($data);

		return $strategy->fresh();
	}

	public function destroy(TEstrategias $strategy)
	{
		TComerciales
			::where('id_estrategia', $strategy->id)
			->update(['id_estrategia' => null]);

		return ['status' => $strategy->delete()];
	}
}
