<?php

namespace App\Http\Controllers\API\Commercials;

use App\Http\Controllers\Controller;
use App\Models\External\TCampanyas;

class CampaignController extends Controller
{

	public function index()
	{
		return TCampanyas::orderBy('abreviatura')->get();
	}
}
