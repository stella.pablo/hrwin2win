<?php

namespace App\Http\Controllers\API\Commercials;


use App\Http\Controllers\Controller;
use App\Models\External\TComerciales;
use App\Models\External\TComercialesEstructuras;
use App\Models\Management;
use App\Models\User;
use App\Models\UserManagement;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;


class CommercialStructureController extends Controller
{

	public function store(Request $request)
	{

		$structure['id_empresa'] = $request['id_empresa'];
		$structure['id_estructura'] = $request['suggestedId'];
		$structure['id_comercial'] = $request['id_comercial'];
		$structure['id_comercial_estructura'] = $request['id_comercial_estructura'];
		$structure['fecha_desde'] = $request['fecha_desde'];
		$structure['fecha_hasta'] = $request['fecha_hasta'];
		$structure['id_gerencia'] = $request['id_gerencia'];
		$structure['id_gerencia_estructura'] = $request['id_gerencia_estructura'];
		$structure['porcentaje_cobro'] = $request['porcentaje_cobro'];

		return (TComercialesEstructuras::create($structure));

	}

	public function destroy($id_comercial, $id_estructura)
	{

		$structure = TComercialesEstructuras::where('id_comercial', $id_comercial)->where('id_estructura', $id_estructura)->first();


		if ($structure) {
			return ['status' => $structure->delete()];
		} else {
			return response()->json(['deleted' => false]);
		}


	}

	public function update(Request $request)
	{
		$data = $request;


		$structure = TComercialesEstructuras::where('id_comercial', $data['id_comercial'])->where('id_estructura', $data['id_estructura'])->first();

		$structure['fecha_desde'] = $request['fecha_desde'];
		$structure['fecha_hasta'] = $request['fecha_hasta'];
		$structure['porcentaje_cobro'] = $request['porcentaje_cobro'];
		$structure->update();

		return response()->json(['updated' => true]);
	}
}
