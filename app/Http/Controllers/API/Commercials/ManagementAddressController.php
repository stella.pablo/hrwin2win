<?php

namespace App\Http\Controllers\API\Commercials;

use App\Http\Controllers\Controller;
use App\Models\External\TGerencias;
use App\Models\External\TDireccionesEntregaGerencias;
use App\Traits\CreateAccess;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class ManagementAddressController extends Controller
{

	use CreateAccess;

	public function index(Request $request)
	{


		return TDireccionesEntregaGerencias::where('id_gerencia', $request->id_gerencia)->get();
	}

	public function store(Request $request)
	{
		$data = $request->validate([
			'id_empresa'         => 'sometimes',
			'id_direccion'       => 'sometimes',
			'id_gerencia'        => 'required',
			'id_tipo_direccion'  => 'required',
			'nombre_direccion'   => 'required',
			'contacto_direccion' => 'required',
			'id_provincia'       => 'required',
			'poblacion'          => 'required',
			'direccion'          => 'required',
			'codigo_postal'      => 'required',

		]);



		if(!isset($data['id_empresa'])){
			$data['id_empresa'] = auth()->user()->company()->id_empresa;

		}
		if(!isset($data['id_direccion'])){

			$data['id_direccion'] = TDireccionesEntregaGerencias::where('id_gerencia', '=', $data['id_gerencia'])->count()+1;


		}


		$created =  TDireccionesEntregaGerencias::create($data);
		if ($created){

			return $data;

		}else{

			return response(json(['error' => true]));

		}



	}

	public function show($address_id, $management_id)
	{

		return TDireccionesEntregaGerencias::where('id_gerencia','=',$management_id)->where('id_direccion', '=', $address_id)->first();

	}

	public function update(Request $request, TDireccionesEntregaGerencias $address)
	{

		$management = $request->id_gerencia;
		$id_direccion = $request->id_direccion;

		$rqcopy = clone $request;
		unset($rqcopy['id_empresa']);
		unset($rqcopy['id_gerencia']);
		unset($rqcopy['id_direccion']);

		$add =  TDireccionesEntregaGerencias::where('id_gerencia','=',$management)->where('id_direccion','=',$id_direccion)->first();

		$add->update($rqcopy->all());

		if($add){

			return response()->json(['updated' => true, 'address'=> $add->fresh()->load('province')]);

		}




		//return $address->update($request->all());

	}




}
