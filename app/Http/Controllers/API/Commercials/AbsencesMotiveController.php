<?php

namespace App\Http\Controllers\API\Commercials;

use App\Http\Controllers\Controller;
use App\Models\AbsencesMotive;
use App\Models\CommercialsAbsence;
use Illuminate\Http\Request;

class AbsencesMotiveController extends Controller
{

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request)
	{
		$company_id = $request->has('company_id') ? $request->company_id : session('company_id');

		return AbsencesMotive::where('company_id', $company_id)->orderBy('name')->get();
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		return AbsencesMotive::create($request->all());
	}

	/**
	 * Display the specified resource.
	 *
	 * @param \App\Models\AbsencesMotive $motive
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show(AbsencesMotive $motive)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param \Illuminate\Http\Request   $request
	 * @param \App\Models\AbsencesMotive $motive
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, AbsencesMotive $motive)
	{
		return ['status' => $motive->update($request->all())];
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param \App\Models\AbsencesMotive $motive
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(AbsencesMotive $motive)
	{
		$absences = CommercialsAbsence::where('motive_id', $motive->id)->count();

		return ['status' => $absences ? false : $motive->delete()];
	}
}
