<?php

namespace App\Http\Controllers\API\Commercials;

use App\Http\Controllers\Controller;
use App\Models\CommercialsReport;
use Illuminate\Http\Request;

class ReportController extends Controller
{

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request)
	{
		$managementsIds = auth()->user()->managements->pluck('id')->toArray();
		$reports = CommercialsReport::whereIn('management_id', $managementsIds);

		if ($request->has('date_from') AND $request->has('date_to')) {
			$reports->whereBetween('created_at', [$request->date_from, $request->date_to]);
		}

		return $reports->latest()->get();
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$data = $request->all();
		$data['management_id'] = auth()->user()->management_id;
		$report = CommercialsReport::create($data);

		return $report->fresh();
	}

	/**
	 * Display the specified resource.
	 *
	 * @param \App\CommercialsReport $report
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show(CommercialsReport $report)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param \App\CommercialsReport   $report
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, CommercialsReport $report)
	{
		$data = $request->all();
		unset($data['commercial']);
		unset($data['motive']);
		unset($data['status']);

		$report->update($data);

		return $report->fresh();
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param \App\CommercialsReport $report
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(CommercialsReport $report)
	{
		//
	}
}
