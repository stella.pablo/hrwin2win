<?php

namespace App\Http\Controllers\API\Commercials;


use App\Models\External\TComercialesContratos;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;



class CommercialCompanyContractsController extends Controller
{

    public function store(Request $request)
    {

    	$contract['id_empresa'] =$request['id_empresa'];
    	$contract['id_comercial'] = $request['id_comercial'];
		$contract['id_tipo_contrato_comercial'] = $request['id_tipo_contrato_comercial'];
		$contract['id_tramo'] =$request['id_tramo'];
		$contract['fecha_desde'] =$request['fecha_desde'];
		$contract['fecha_hasta'] =$request['fecha_hasta'];

		$contract['devolucion_comercial'] ='N';
		$contract['devolucion_estructura'] = 'N';

		$contract = TComercialesContratos::create($contract);

    	return($contract->load('contractType','contractStretch'));

    }

    public function destroy($id_comercial, $id_comercial_contrato)
    {

		$contract = TComercialesContratos::where('id_comercial',$id_comercial)->where('id_comercial_contrato',$id_comercial_contrato)->first();

		if ($contract){
			return ['status' => $contract->delete()];
		}else{
			return response()->json(['deleted' => false]);
		}



    }
    public function update(Request $request){
    	$data = $request;


    	$contract = TComercialesContratos::where('id_comercial',$data['id_comercial'])->where('id_comercial_contrato',$data['id_comercial_contrato'])->first();

		$contract['fecha_desde'] =$request['fecha_desde'];
		$contract['fecha_hasta'] =$request['fecha_hasta'];
		$contract['id_tipo_contrato_comercial'] =$request['id_tipo_contrato_comercial'];
		$contract['id_tramo'] =$request['id_tramo'];

    	$contract->update();
		return response()->json(['updated' => true]);
	}
}
