<?php

namespace App\Http\Controllers\API\Commercials;

use App\Models\External\TComerciales;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Storage;

class CommercialAvatarController extends Controller
{

    public function store(Request $request, TComerciales $commercial)
    {
        $request->validate([
        	'file' => 'required|file',
        ]);

        $file = $request->file;
	    $path = $file->store('public/avatar');

	    $commercial->url_imagen_perfil = str_replace('public/', 'storage/', $path);
	    $commercial->save();

	    return $commercial->fresh();
    }

    public function destroy(TComerciales $commercial)
    {
	    Storage::delete($commercial->url_imagen_perfil);

	    $commercial->url_imagen_perfil = null;
	    $commercial->save();

	    return $commercial->fresh();
    }
}
