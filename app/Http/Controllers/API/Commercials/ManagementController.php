<?php

namespace App\Http\Controllers\API\Commercials;

use App\Http\Controllers\Controller;
use App\Models\External\TComerciales;
use App\Models\External\TGerencias;
use App\Models\User;
use App\Traits\CreateAccess;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class ManagementController extends Controller
{

	use CreateAccess;

	public function index(Request $request)
	{
		// Get the managements this user has access to in this current company
		$managementsIds = auth()->user()->managements->pluck('external_id')->toArray();

		$relations = [
			'internal',
			'structures',
			'manager:id_comercial,apellido1,apellido2,nombre,nombre_completo,id_estado',
		];


		if (session('database') === 'mexico' AND $request->has('with_organization')) {
			$relations[] = 'organization';
		}


		$managements = TGerencias
			::with($relations)
			->whereIn('id_gerencia', $managementsIds)
			->whereHas('manager', function($query) use ($request) {
                if ($request->has('with_trashed')) {
                    $query->withTrashed();
                }
            })
			->orderBy('gerencia');

		if ($request->has('with_commercials_count')) {
			$managements->withCount('commercials');
		}

		if ($request->has('with_trashed')) {
			$managements->withTrashed();
		}

		if (session('database') === 'mexico') {
			if ($request->filled('organization_id')) {
				$managements->where('id_organizacion', $request->organization_id);
			}

			if ($request->filled('strategy_id')) {
				$managements->whereHas('strategies', function ($query) use ($request) {
					$query->where('id', $request->strategy_id);
				});
			}
		}

		return $managements->get();
	}

	public function store(Request $request)
	{
		$data = $request->validate([
			'gerencia'                 => 'required',
			'id_comercial_liquidacion' => 'required',
			'id_organizacion'          => session('database') === 'mexico' ? 'required' : 'sometimes',
			'id_provincia'             => 'sometimes',
			'id_estado'                => 'sometimes',
			'id_tipo_iva'              => 'sometimes',
			'importe_alquiler'         => 'sometimes',
			'id_iva'                   => 'sometimes',
			'id_almacen'               => 'sometimes',
			'id_itau'                  => 'sometimes',
			'id_forma_pago'            => 'sometimes',
			'id_tarifa'                => 'sometimes',
		]);

		$data['id_estado'] = 1;
		$data['id_tipo_albaran'] = 'F';
		$data['freq_facturacion'] = 'M';
		$data['facturacion'] = 'S';
		$data['entrega'] = 'S';
		$data['albaran_valorado'] = 'S';
		$data['alb_por_ped'] = 'S';
		$data['asegurado'] = 'N';
		$data['albaran_valorado'] ='S';
		$data['tipo_facturacion'] = 'P';
		$data['id_tipo_dto'] = 'N';
		$data['idioma'] = 'ES';
		$data['nombre'] = $data['gerencia'];


		//compruebo que el correo no esté ya en users
		$email = TComerciales::where('id_comercial', '=', $data['id_comercial_liquidacion'])->first('email');
		if (!$email) {
			return response()->json(['error' => 'el comercial de liquidación no tiene email asociado']);
		}

		$repeatedMail = User::where('email', $email->email)->exists();

		if ($repeatedMail) {
			return response()->json(['error' => 'El email del comercial de liquidación ya está asociado a otro usuario de recursos humanos']);
		}

		$management = TGerencias::create($data)->load(['internal']);

		if ($request->filled('create_access_hr')) {
			$this->create_access_hr(TComerciales::find($management->manager->id_comercial));
		}

		return $management;
	}

	public function show(TGerencias $management)
	{
		$this->authorize('view', $management);

		$relations = [
			'internal',
			'structures',
			'manager:id_comercial,id_comercial_externo,apellido1,apellido2,nombre,nombre_completo,email',
		];

		if (session('database') === 'mexico') {
			$relations[] = 'organization';
			$relations[] = 'strategies';
		}

		return $management
			->load($relations)
			->append('campaigns');
	}

	public function update(Request $request, TGerencias $management)
	{
		$data = $request->validate([
			'fecha_baja'               => 'sometimes',
			'gerencia'                 => 'required',
			'id_comercial_liquidacion' => 'required',
			'id_organizacion'          => session('database') === 'mexico' ? 'required' : 'sometimes',
			'id_provincia'             => 'sometimes',
			'importe_alquiler'         => 'sometimes',
			'id_tipo_iva'              => 'sometimes',
			'importe_alquiler'         => 'sometimes',
			'id_iva'                   => 'sometimes',
			'id_almacen'               => 'sometimes',
			'id_itau'                  => 'sometimes',
			'id_forma_pago'            => 'sometimes',
			'id_tarifa'                => 'sometimes',


		]);

		//CREO QUE ESTO LO TOCO DENIS, COMENTO Y PONGO LO MIO, SI FALLA MIRAR POR QUE


		/*
		if ($data['id_comercial_liquidacion'] != $management->id_comercial_liquidacion) {
			//compruebo que el VIEJO no tenga otra gerencia a su nombre
			$count = TGerencias::where('id_comercial_liquidacion', $management->id_comercial_liquidacion)->count();

			if ($count < 2) {
				TComerciales::where('id_comercial', $management->id_comercial_liquidacion)->update(['id_nivel_funcional' => '003']);
			}

			//actualizo al nuevo comercial con nivel fx gerente
			TComerciales::where('id_comercial', $data['id_comercial_liquidacion'])->update(['id_nivel_funcional' => '001']);
		}*/


		if ($data['id_comercial_liquidacion'] != $management->id_comercial_liquidacion) {
			//compruebo que el VIEJO no tenga otra gerencia a su nombre
			$count = TGerencias::where('id_comercial_liquidacion', $data['id_comercial_liquidacion'])->count();

			if ($count < 2) {


				$email = TComerciales::where('id_comercial', '=', $data['id_comercial_liquidacion'])->first('email');
				if (!$email) {

					return response()->json(['error' => 'el comercial de liquidación no tiene email asociado']);

				}
				/*
				 * Ahora supuestamente se aceptan varios email repetidos
				$repeatedMail = User::where('email', '=', $email->email)->first();


				if ($repeatedMail) {


					return response()->json(['error' => 'El email del nuevo comercial de liquidación ya está asociado a otro usuario de recursos humanos']);

				}*/


				TComerciales::where('id_comercial', $management->id_comercial_liquidacion)->update(['id_nivel_funcional' => '003']);

			} else {
				return response()->json(['error' => 'El comercial ya es Diamante de otra gerencia']);
			}

			//actualizo al nuevo comercial con nivel fx gerente
			TComerciales::where('id_comercial', $data['id_comercial_liquidacion'])->update(['id_nivel_funcional' => '001']);

		}


		$management->update($data);


		if ($request->filled('create_access_hr')) {
			$this->create_access_hr(TComerciales::find($management->manager->id_comercial));
		}

		return $management->fresh();
	}


	// Extra

	public function stats(Request $request)
	{
		$cacheKey = 'c' . session('company_id') . ':contracts:stats';
		$managementId = '';
		$stats = [];

		if (auth()->user()->level >= 3) {
			$managementId = auth()->user()->management->external_id;
		} elseif ($request->has('manager')) {
			$managementId = $request->manager;
		}

		if ($managementId) {
			$cacheKey = str_replace(':contracts:', ':m' . $managementId . ':contracts:', $cacheKey);
		}

		$stats = Cache::remember($cacheKey, 10000, function () use ($managementId) {
			return DB::connection(session('database'))->select("SELECT * FROM [dbo].[f_get_comerciales_contratos_meses_completo](?, '')", [$managementId]);
		});

		return $stats;
	}

	public function types_contracts()
	{
		return DB
			::connection(session('database'))
			->table('t_tipos_contratos_comercial')
			->get();
	}

	public function types_contracts_subtype(Request $request)
	{
		return DB
			::connection(session('database'))
			->table('t_tipos_contratos_comercial_tramos')
			->where('id_tipo_contrato_comercial', $request->id_tipo_contrato_comercial)
			->get();
	}

	public function leaders(Request $request, TGerencias $management)
	{

		$leaders = TComerciales::where('id_nivel_funcional', '002')->where('fecha_baja', null)->where('id_gerencia', $request->id_gerencia)->get();


		return $leaders->map(function ($item) {
			$item['enabled'] = true;

			return $item;
		});
	}

}
