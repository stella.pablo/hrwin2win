<?php

namespace App\Http\Controllers\API\Commercials;

use App\Http\Controllers\Controller;
use App\Models\ReportsMotive;
use Illuminate\Http\Request;

class ReportsMotiveController extends Controller
{

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request)
	{
		$company_id = $request->has('company_id') ? $request->company_id : session('company_id');

		return ReportsMotive::where('company_id', $company_id)->orderBy('name')->get();
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param \App\Models\ReportsMotive $reportsMotive
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show(ReportsMotive $reportsMotive)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param \App\Models\ReportsMotive $reportsMotive
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit(ReportsMotive $reportsMotive)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param \Illuminate\Http\Request  $request
	 * @param \App\Models\ReportsMotive $reportsMotive
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, ReportsMotive $reportsMotive)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param \App\Models\ReportsMotive $reportsMotive
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(ReportsMotive $reportsMotive)
	{
		//
	}
}
