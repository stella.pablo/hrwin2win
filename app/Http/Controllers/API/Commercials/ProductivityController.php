<?php

namespace App\Http\Controllers\API\Commercials;

use App\Http\Controllers\Controller;
use App\Models\External\TCampanyas;
use App\Models\External\TComerciales;
use App\Models\External\TContratos;
use App\Models\External\TGerencias;
use App\Models\Management;
use App\Search\CommercialSearch;
use Cache;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

class ProductivityController extends Controller
{

	public function index(Request $request)
	{
		$campaignId = null;
		$filters = $request->all();
		unset($filters['date_from']);
		unset($filters['date_to']);
		$commercials = CommercialSearch::get($filters)->toArray();

		if ($request->filled('campaign_id')) {
			$campaign = TCampanyas::where('abreviatura', $request->campaign_id)->first();
			$campaignId = $campaign->id;
		}

		foreach ($commercials as &$commercial) {
			$contracts = TContratos
				::whereBetween('fecha_registro', [$request->date_from, $request->date_to])
				->where('id_comercial', $commercial['id_comercial']);

			if ($request->filled('campaign_id')) {
				$contracts->where('id_campanya', $campaignId);
			}

			$c_ok = clone $contracts;
			$c_p = clone $contracts;
			$c_ko = clone $contracts;

			$contractsPerType = [
				'numero_contratos_ok' => $c_ok->whereIn('id_estado', [6, 9])->count(),
				'numero_contratos_p'  => $c_p->whereIn('id_estado', [0, 1, 2, 3])->count(),
				'numero_contratos_ko' => $c_ko->whereIn('id_estado', [4, 5, 7, 8])->count(),
			];

			$commercial = array_merge($commercial, $contractsPerType);
		}

		return $commercials;
	}

	public function low(Request $request)
	{
		$campaignId = null;

		if ($request->filled('campaign_id')) {
			$campaign = TCampanyas::where('abreviatura', $request->campaign_id)->first();
			$campaignId = $campaign->id;
		}

		$filters = $request->all();
		unset($filters['date_from']);
		unset($filters['date_to']);

		$commercials = CommercialSearch::apply($filters);
		$commercials
			->with('contract_last')
			->withCount('contracts')
			->whereDoesntHave('contracts', function ($query) use ($request, $campaignId) {
				$query->whereBetween('fecha_registro', [$request->date_from, $request->date_to]);

				if ($campaignId) {
					$query->where('id_campanya', $campaignId);
				}
			});

		return $commercials->get();
	}

	public function commercial(Request $request, $commercial)
	{
		$commercial = TComerciales::findOrFail($commercial);

		$date = Carbon::now()->startOfMonth();
		$daysInMonth = $date->daysInMonth;

		// Contracts per day
		$contractsPerDay = $commercial
			->contracts()
			->where('fecha_registro', '>=', $date)
			->get()
			->groupBy(function ($value) {
				return Carbon::parse($value->fecha_registro)->format('Ymd');
			});

		$contractsPerDayReady = [];

		foreach ($contractsPerDay as $day => $contracts) {
			$contractsPerDayReady[$day] = ['ok' => 0, 'p' => 0, 'ko' => 0, 'total' => 0];

			foreach ($contracts as $contract) {
				if (in_array($contract->id_estado, [0, 1, 2, 3])) {
					$contractsPerDayReady[$day]['p'] += 1;
				} elseif (in_array($contract->id_estado, [6, 9])) {
					$contractsPerDayReady[$day]['ok'] += 1;
				} elseif (in_array($contract->id_estado, [4, 5, 7, 8])) {
					$contractsPerDayReady[$day]['ko'] += 1;
				}
			}

			$contractsPerDayReady[$day]['total'] += count($contracts);
		}

		for ($i = 1; $i !== $daysInMonth + 1; $i++) {
			$key = $date->setDay($i)->format('Ymd');

			if (!isset($contractsPerDayReady[$key])) {
				$contractsPerDayReady[$key] = ['ok' => 0, 'p' => 0, 'ko' => 0, 'total' => 0];
			}
		}

		ksort($contractsPerDayReady);

		// Contracts per month
		$dateLastYear = Carbon::now()->subYear();

		$contractsPerMonth = $commercial
			->contracts()
			->where('fecha_registro', '>', $dateLastYear)
			->get()
			->groupBy(function ($value) {
				return Carbon::parse($value->fecha_registro)->format('Ym');
			});

		$contractsPerMonthReady = [];

		foreach ($contractsPerMonth as $month => $contracts) {
			$contractsPerMonthReady[$month] = ['ok' => 0, 'p' => 0, 'ko' => 0, 'total' => 0];

			foreach ($contracts as $contract) {
				if (in_array($contract->id_estado, [0, 1, 2, 3])) {
					$contractsPerMonthReady[$month]['p'] += 1;
				} elseif (in_array($contract->id_estado, [6, 9])) {
					$contractsPerMonthReady[$month]['ok'] += 1;
				} elseif (in_array($contract->id_estado, [4, 5, 7, 8])) {
					$contractsPerMonthReady[$month]['ko'] += 1;
				}
			}

			$contractsPerMonthReady[$month]['total'] += count($contracts);
		}

		for ($i = 1; $i !== 13; $i++) {
			$key = $dateLastYear->addMonth()->format('Ym');

			if (!isset($contractsPerMonthReady[$key])) {
				$contractsPerMonthReady[$key] = ['ok' => 0, 'p' => 0, 'ko' => 0, 'total' => 0];
			}
		}

		ksort($contractsPerMonthReady);

		return [
			'daily'   => $contractsPerDayReady,
			'monthly' => $contractsPerMonthReady,
		];
	}

	public function commercials(Request $request)
	{
		$commercials = CommercialSearch::get($request->all())->toArray();
		$this->fill($request, $commercials, __FUNCTION__);

		return $commercials;
	}

	public function managements(Request $request)
	{
		$managements = TGerencias
			::with(['internal', 'manager:id_comercial,apellido1,apellido2,nombre,nombre_completo'])
			->has('manager')
			->withCount('commercials')
			->orderBy('gerencia');

		if ($request->filled('organization_id')) {
			$managements->where('id_organizacion', $request->organization_id);
		}

		if ($request->filled('management_id')) {
			$managements->where('id_gerencia', $request->management_id);
		} else {
			$managementsIds = auth()->user()->managements->pluck('external_id')->toArray();
			$managements->whereIn('id_gerencia', $managementsIds);
		}

		$managementsData = $managements->get()->toArray();

		$this->fill($request, $managementsData, __FUNCTION__);

		return $managementsData;
	}

	private function fill(Request $request, &$container, $what)
	{
		foreach ($container as &$c) {
			$campaigns = TCampanyas::orderBy('abreviatura');

			if ($request->filled('campaign')) {
				$campaigns->where('abreviatura', $request->campaign);
			}

			$campaignsData = $campaigns->get();
			$contracts = TContratos::whereBetween('fecha_registro', [$request->date_from, $request->date_to]);

			if ($what === 'managements') {
				$whatId = $c['id_gerencia'];
				$contracts->where('id_gerencia', $whatId);
			} elseif ($what === 'commercials') {
				$whatId = $c['id_comercial'];
				$contracts->where('id_comercial', $whatId);
			}

			$c['campaigns'] = [];

			foreach ($campaignsData as $campaign) {
				$c_ok = clone $contracts;
				$c_p = clone $contracts;
				$c_ko = clone $contracts;

				$c_ok_count = $c_ok->where('id_campanya', $campaign->id)->whereIn('id_estado', [6, 9])->count();
				$c_p_count = $c_p->where('id_campanya', $campaign->id)->whereIn('id_estado', [0, 1, 2, 3])->count();
				$c_ko_count = $c_ko->where('id_campanya', $campaign->id)->whereIn('id_estado', [4, 5, 7, 8])->count();

				$c['campaigns'][$campaign->abreviatura] = [
					'c_ok'    => intval($c_ok_count),
					'c_p'     => intval($c_p_count),
					'c_ko'    => intval($c_ko_count),
					'c_total' => intval($c_ok_count) + intval($c_p_count) + intval($c_ko_count),
				];
			}
		}
	}

	public function stats(Request $request)
	{
		$filters = $request->all();

		if (!isset($filters['date_from'])) {
			$filters['date_from'] = Carbon::now()->subYear();
		}

		if (!isset($filters['date_to'])) {
			$filters['date_to'] = Carbon::now();
		}

		if($request->has('daily')) {
			return [
				'daily' => $this->getStatsContractsDaily($filters)
			];
		}

		return [
			'contracts'          => $this->getStatsContracts($filters),
			'commercials'        => $this->getStatsCommercials($filters),
			'commercialsNew'     => $this->getStatsCommercialsNew($filters),
			'commercialsRemoved' => $this->getStatsCommercialsRemoved($filters),
		];
	}

	private function getStatsContracts($filters)
	{
		$managementsIds = auth()->user()->managements->pluck('external_id')->toArray();

		$contracts = TContratos
			::select([
				DB::raw('COUNT(*) AS contracts'),
				'id_estado',
				DB::raw('CONVERT(NVARCHAR(10), DATEADD(MONTH, DATEDIFF(MONTH, 0, fecha_registro), 0), 120) AS month'),
			])
			->whereBetween('fecha_registro', [$filters['date_from'], $filters['date_to']])
			->whereIn('id_gerencia', $managementsIds)
			->groupBy([
				'id_estado',
				DB::raw('DATEADD(MONTH, DATEDIFF(MONTH, 0, fecha_registro), 0)'),
			])
			->orderByRaw('DATEADD(MONTH, DATEDIFF(MONTH, 0, fecha_registro), 0), id_estado');

		if (isset($filters['commercial'])) {
			$contracts->where('id_comercial', $filters['commercial']);
		}

		if (isset($filters['management_id'])) {
			$contracts->where('id_gerencia', $filters['management_id']);
		}

		$contractsData = $contracts
			->get()
			->groupBy(function ($value) {
				return Carbon::parse($value->month)->format('Ym');
			});

		$contractsReady = [];

		foreach ($contractsData as $month => $contractsInMonth) {
			$contractsReady[$month] = ['ok' => 0, 'p' => 0, 'ko' => 0, 'total' => 0];

			foreach ($contractsInMonth as $contract) {
				if (in_array($contract->id_estado, [0, 1, 2, 3])) {
					$contractsReady[$month]['p'] += $contract->contracts;
				} elseif (in_array($contract->id_estado, [6, 9])) {
					$contractsReady[$month]['ok'] += $contract->contracts;
				} elseif (in_array($contract->id_estado, [4, 5, 7, 8])) {
					$contractsReady[$month]['ko'] += $contract->contracts;
				}

				$contractsReady[$month]['total'] += $contract->contracts;
			}
		}

		return $contractsReady;
	}

	private function getStatsContractsDaily($filters)
	{
		$managementsIds = auth()->user()->managements->pluck('external_id')->toArray();

		$contracts = TContratos
			::select([
				DB::raw('COUNT(*) AS contracts'),
				'id_estado',
				DB::raw('CONVERT(NVARCHAR(10), DATEADD(DAY, DATEDIFF(DAY, 0, fecha_registro), 0), 120) AS month'),
			])
			->whereBetween('fecha_registro', [$filters['date_from'], $filters['date_to']])
			->whereIn('id_gerencia', $managementsIds)
			->groupBy([
				'id_estado',
				DB::raw('DATEADD(DAY, DATEDIFF(DAY, 0, fecha_registro), 0)'),
			])
			->orderByRaw('DATEADD(DAY, DATEDIFF(DAY, 0, fecha_registro), 0), id_estado');

		if (isset($filters['commercial'])) {
			$contracts->where('id_comercial', $filters['commercial']);
		}

		if (isset($filters['management_id'])) {
			$contracts->where('id_gerencia', $filters['management_id']);
		}

		$contractsData = $contracts
			->get()
			->groupBy(function ($value) {
				return Carbon::parse($value->month)->format('Ymd');
			});

		$contractsReady = [];

		$startDay = Carbon::now()->startOfMonth();
		$endDay = Carbon::now();
		$daysDiff = $startDay->diffInDays($endDay) + 1;

		$curDay = $startDay->clone();
		for($i = 1; $i <= $daysDiff; $i++) {
			$curDayFormatted = $curDay->format('Ymd');
			$contractsReady[$curDayFormatted] = ['ok' => 0, 'p' => 0, 'ko' => 0, 'total' => 0];

			if(isset($contractsData[$curDayFormatted])) {
				foreach ($contractsData[$curDayFormatted] as $contract) {
					if (in_array($contract->id_estado, [0, 1, 2, 3])) {
						$contractsReady[$curDayFormatted]['p'] += $contract->contracts;
					} elseif (in_array($contract->id_estado, [6, 9])) {
						$contractsReady[$curDayFormatted]['ok'] += $contract->contracts;
					} elseif (in_array($contract->id_estado, [4, 5, 7, 8])) {
						$contractsReady[$curDayFormatted]['ko'] += $contract->contracts;
					}

					$contractsReady[$curDayFormatted]['total'] += $contract->contracts;
				}
			}

			$curDay->addDay();
		}

		return $contractsReady;
	}

	private function getStatsCommercials($filters)
	{
		$managementsIds = auth()->user()->managements->pluck('external_id')->toArray();
		$commercials = [];

		for ($i = 12; $i != -1; $i--) {
			$dateStart = Carbon::now()->startOfMonth()->subMonths($i);
			$dateEnd = Carbon::now()->startOfMonth()->subMonths($i - 1);

			$key = $dateStart->format('Ym');

			$commercialsObj = TComerciales
				::withTrashed()
				->where('fecha_alta', '<=', $dateEnd)
				->whereIn('id_gerencia', $managementsIds)
				->where(function ($query) use ($dateEnd) {
					$query
						->whereNull('fecha_baja')
						->orWhere('fecha_baja', '>', $dateEnd);
				});

			if (isset($filters['management_id'])) {
				$commercialsObj->where('id_gerencia', $filters['management_id']);
			}

			$commercialsData = $commercialsObj->get();

			$commercials[$key] = [
				'commercials' => $commercialsData->count(),
				'objective'   => 0,
			];

			foreach ($commercialsData as $commercial) {
				$commercials[$key]['objective'] += $this->getMonthlyObjective($commercial->id_gerencia);
			}
		}

		return $commercials;
	}

	private function getMonthlyObjective($management_id)
	{
		return Cache::remember('c:' . session('company_id') . ':m:' . $management_id . ':monthly_objective', 600, function () use ($management_id) {
			$management = Management
				::where('company_id', session('company_id'))
				->where('external_id', $management_id)
				->first();

			return $management ? $management->monthly_objective : 0;
		});
	}

	private function getStatsCommercialsNew($filters)
	{
		$managementsIds = auth()->user()->managements->pluck('external_id')->toArray();
		$commercials = [];

		for ($i = 12; $i != -1; $i--) {
			$dateStart = Carbon::now()->startOfMonth()->subMonths($i);
			$dateEnd = Carbon::now()->startOfMonth()->subMonths($i - 1);

			$key = $dateStart->format('Ym');

			$commercialsObj = TComerciales
				::withTrashed()
				->whereBetween('fecha_alta', [$dateStart, $dateEnd])
				->whereIn('id_gerencia', $managementsIds)
				->where(function ($query) use ($dateEnd) {
					$query
						->whereNull('fecha_baja')
						->orWhere('fecha_baja', '>', $dateEnd);
				});

			if (isset($filters['management_id'])) {
				$commercialsObj->where('id_gerencia', $filters['management_id']);
			}

			$commercials[$key] = $commercialsObj->count();
		}

		return $commercials;
	}

	private function getStatsCommercialsRemoved($filters)
	{
		$managementsIds = auth()->user()->managements->pluck('external_id')->toArray();
		$commercials = [];

		for ($i = 12; $i != -1; $i--) {
			$dateStart = Carbon::now()->startOfMonth()->subMonths($i);
			$dateEnd = Carbon::now()->startOfMonth()->subMonths($i - 1);

			$key = $dateStart->format('Ym');

			$commercialsObj = TComerciales
				::withTrashed()
				->whereIn('id_gerencia', $managementsIds)
				->whereNotNull('fecha_baja')
				->whereBetween('fecha_baja', [$dateStart, $dateEnd]);

			if (isset($filters['management_id'])) {
				$commercialsObj->where('id_gerencia', $filters['management_id']);
			}

			$commercials[$key] = $commercialsObj->count();
		}

		return $commercials;
	}
}
