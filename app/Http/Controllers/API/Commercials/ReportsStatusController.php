<?php

namespace App\Http\Controllers\API\Commercials;

use App\Http\Controllers\Controller;
use App\Models\CommercialsReport;
use App\Models\ReportsStatus;
use Illuminate\Http\Request;

class ReportsStatusController extends Controller
{

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request)
	{
		$company_id = $request->has('company_id') ? $request->company_id : session('company_id');

		return ReportsStatus::where('company_id', $company_id)->orderBy('name')->get();
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		return ReportsStatus::create($request->all());
	}

	/**
	 * Display the specified resource.
	 *
	 * @param \App\Models\ReportsStatus $status
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show(ReportsStatus $status)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param \Illuminate\Http\Request  $request
	 * @param \App\Models\ReportsStatus $status
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, ReportsStatus $status)
	{
		return ['status' => $status->update($request->all())];
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param \App\Models\ReportsStatus $status
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(ReportsStatus $status)
	{
		$reports = CommercialsReport::where('status_id', $status->id)->count();

		return ['status' => $reports ? false : $status->delete()];
	}
}
