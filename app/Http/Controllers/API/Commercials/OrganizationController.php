<?php

namespace App\Http\Controllers\API\Commercials;

use App\Http\Controllers\Controller;
use App\Models\External\TOrganizaciones;
use Illuminate\Http\Request;

class OrganizationController extends Controller
{

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request)
	{
		if(session('database') !== 'mexico' && session('database') !=='mexico_test') {
			return [];
		}

		$organizations = TOrganizaciones::orderBy('organizacion')->get();

		if ($request->has('stats')) {
			$organizations->each->append('stats');
		}

		return $organizations;
	}
}
