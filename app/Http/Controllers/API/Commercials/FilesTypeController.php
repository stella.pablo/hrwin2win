<?php

namespace App\Http\Controllers\API\Commercials;

use App\Http\Controllers\Controller;
use App\Models\External\TTiposFicheros;
use Illuminate\Http\Request;

class FilesTypeController extends Controller
{

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		return TTiposFicheros::orderBy('tipo_fichero')->get();
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$data = $request->validate([
			'tipo_fichero' => 'required',
			'obligatorio'  => 'sometimes',
		]);

		// Set default params
		$data['id_empresa'] = '01';

		// Get latest id (because no autoinr ID)
		$latest = TTiposFicheros::latest('id_tipo_fichero')->first();
		$data['id_tipo_fichero'] = $latest ? $latest->id_tipo_fichero + 1 : 1;

		return TTiposFicheros::create($data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param \Illuminate\Http\Request            $request
	 * @param \App\Models\External\TTiposFicheros $type
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, TTiposFicheros $type)
	{
		$data = $request->validate([
			'tipo_fichero' => 'required',
			'obligatorio'  => 'sometimes',
		]);

		if (!isset($data['obligatorio']) OR intval($data['obligatorio']) === 0) {
			$data['obligatorio'] = null;
		}

		$type->update($data);

		return $type->fresh();
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param \App\Models\External\TTiposFicheros $type
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(TTiposFicheros $type)
	{
		return ['status' => $type->delete()];
	}
}
