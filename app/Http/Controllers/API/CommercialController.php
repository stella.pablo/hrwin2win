<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Commercial;
use App\Models\External\TComerciales;
use App\Models\External\TComercialesCampanyas;
use App\Search\CommercialProductivity;
use App\Search\CommercialSearch;
use App\Traits\CreateAccess;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class CommercialController extends Controller
{

	use CreateAccess;

	public function index(Request $request)
	{
		if (auth()->user()->level > 4) {
			return ['error' => true];
		}

		return CommercialSearch::get($request->all());
	}

	public function store(Request $request)
	{
		$data = $request->validate([
			'apellido1'                                      => 'required',
			'apellido2'                                      => 'required',
			'campaigns'                                      => 'required|array',
			'candidate_id'                                   => 'sometimes',
			'cod_postal'                                     => 'sometimes',
			'company_contracts'                              => 'required|array',
			'company_contracts.*.id_tipo_contrato_comercial' => 'required',
			'company_contracts.*.id_tramo'                   => 'required',
			'curp'                                           => 'sometimes',
			'direccion'                                      => 'sometimes',
			'email'                                          => 'required',
			'fm12'                                           => 'sometimes',
			'id_comercial_externo'                           => 'sometimes',
			'id_estrategia'                                  => 'sometimes',
			'id_gerencia'                                    => 'sometimes',
			'nacionalidad'                                   => 'sometimes',
			'nif'                                            => 'sometimes',
			'nombre'                                         => 'required',
			'observaciones'                                  => 'sometimes',
			'pasaporte'                                      => 'sometimes',
			'permiso'                                        => 'sometimes',
			'poblacion'                                      => 'sometimes',
			'rfc'                                            => 'sometimes',
			'telefono_fijo'                                  => 'sometimes',
			'telefono_movil'                                 => 'required',
			'id_nivel_funcional'                             => 'sometimes',
		]);

		$commercialData = Arr::except($data, [
			'campaigns',
			'candidate_id',
			'company_contracts',
		]);
		if (isset($commercialData['id_comercial_externo'])) {


			$repeatedExternal = TComerciales::where('id_comercial_externo', '=', $data['id_comercial_externo'])->first();
			if ($repeatedExternal <> null) {

				return response(['error' => 'El id_externo del comercial está en uso y debe ser único']);

			}


		}
		$commercial = TComerciales::create($commercialData);

		if ($request->filled('create_access_mobile') AND $commercial->email) {
			$this->create_access_mobile($commercial);
		}

		return $commercial;
	}

	public function show(TComerciales $commercial)
	{
		$this->authorize('view', $commercial);

		return $commercial
			->load([
				'absences',
				'campaigns',
				'company_contracts.contractType',
				'company_contracts.contractStretch',
				'files',
				'management.manager',
				'management_internal',
				'structures',
			])
			->loadCount([
				'contracts_ok',
				'contracts_p',
				'contracts_ko',
			]);
	}

	public function update(Request $request, TComerciales $commercial)
	{
		$data = $request->validate([
			'apellido1'                                      => 'sometimes',
			'apellido2'                                      => 'sometimes',
			'campaigns'                                      => 'sometimes|nullable|array',
			'candidate_id'                                   => 'sometimes',
			'cod_postal'                                     => 'sometimes',
			'company_contracts'                              => 'sometimes|nullable|array',
			'company_contracts.*.id_tipo_contrato_comercial' => 'sometimes',
			'company_contracts.*.id_tramo'                   => 'sometimes',
			'curp'                                           => 'sometimes',
			'direccion'                                      => 'sometimes',
			'email'                                          => 'sometimes',
			'fecha_alta'                                     => 'sometimes|nullable|date',
			'fecha_baja'                                     => 'sometimes|nullable|date',
			'fm12'                                           => 'sometimes',
			'id_comercial_externo'                           => 'sometimes',
			'id_estado'                                      => 'sometimes',
			'id_estrategia'                                  => 'sometimes',
			'id_gerencia'                                    => 'sometimes',
			'nacionalidad'                                   => 'sometimes',
			'nif'                                            => 'sometimes',
			'nombre'                                         => 'sometimes',
			'observaciones'                                  => 'sometimes',
			'pasaporte'                                      => 'sometimes',
			'permiso'                                        => 'sometimes',
			'poblacion'                                      => 'sometimes',
			'rfc'                                            => 'sometimes',
			'telefono_fijo'                                  => 'sometimes',
			'telefono_movil'                                 => 'sometimes',
			'id_nivel_funcional'                             => 'sometimes',
		]);

		$commercialData = Arr::except($data, [
			'campaigns',
			'candidate_id',
			'company_contracts',
		]);

		$commercialData['fecha'] = Carbon::now();

		$commercial->update($commercialData);

		if ($request->filled('create_access_mobile')) {
			$this->create_access_mobile($commercial);
		}

		return $commercial->fresh();
	}

	public function update_external(Request $request, $external_id)
	{
		$commercial = Commercial::where('external_id', $external_id)->first();

		if ($commercial) {
			$commercial->update($request->all());
		} else {
			$commercial = Commercial::create([
				'external_id' => $external_id,
				'status_id'   => $request->status_id,
			]);
		}

		return $commercial->fresh();
	}

	public function campaigns(Request $request, TComerciales $commercial)
	{
		$data = $request->validate([
			'campaigns' => 'required|array',
		]);
		$campaignsWants = $data['campaigns'];

		TComercialesCampanyas::where('id_comercial', $commercial->id_comercial)->forceDelete();

		foreach ($campaignsWants as $c) {
			TComercialesCampanyas::create([
				'id_campanya'  => $c,
				'id_comercial' => $commercial->id_comercial,
			]);
		}

		return $data;
	}

	public function contracts($commercial_id, Request $request)
	{
		$contracts = DB::connection(auth()->user()->company->database)->select(
			"SELECT * FROM f_get_contratos_lista(:id_comercial, :tecnicos, :nivel_usuario, :desde, :hasta, :solo_mios, :estados, :tipo_contrato, '')",
			[
				'id_comercial'  => '',
				'tecnicos'      => $commercial_id,
				'nivel_usuario' => '',
				'desde'         => $request->desde,
				'hasta'         => $request->hasta,
				'solo_mios'     => 'N',
				'estados'       => '',
				'tipo_contrato' => 'TODOS',
			]
		);

		return $contracts;
	}

	public function internal($external_id)
	{
		return Commercial::where('external_id', $external_id)->first();
	}

	public function productivity(Request $request)
	{
		return CommercialProductivity::apply($request);
	}

	public function checkIsValid($commercial,$type,$phone){

		if ($type=='store'){

			return response()->json(['valid' => TComerciales::where('telefono_movil','=',$phone)->count() < 1]);

		}else{

			return response()->json(['valid' => TComerciales::where('telefono_movil','=',$phone)->where('id_comercial', '<>',$commercial)->count() < 1]);

		}

	}

	public function managers()
	{

		$managers = TComerciales::where('id_nivel_funcional', '001')->get();

		return $managers->map(function ($item) {
			$item['enabled'] = true;

			return $item;
		});
	}
}
