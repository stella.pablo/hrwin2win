<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Management;
use Illuminate\Http\Request;

class ManagementController extends Controller
{

	private $fields = [
		'name'              => 'required',
		'monthly_objective' => 'required',
		'phone'             => 'sometimes',
		'workhours'         => 'sometimes',
		'address'           => 'sometimes',
		'address_link'      => 'sometimes',
		'url'               => 'sometimes',
	];

	public function index()
	{

		return auth()->user()->managements;
	}

	public function update(Request $request, Management $management)
	{
		$management->update($request->validate($this->fields));

		return $management->fresh();
	}
}
