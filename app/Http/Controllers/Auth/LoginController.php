<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Auth;
use Hash;
use Illuminate\Http\Request;

class LoginController extends Controller
{

	public function login(Request $request)
	{
		$this->validate($request, [
			'username' => 'required',
			'password' => 'required',
		]);

		if (Auth::attempt(['username' => $request->username, 'password' => $request->password], $request->rememberme)) {
			// Rehash bcrypt passwords to argon2id if needed
			$user = Auth::user();
			if (Hash::needsRehash($user->password)) {
				$user->password = Hash::make($request->password);
				$user->save();
			}

			session([
				'company_id' => auth()->user()->company_id,
				'database'   => auth()->user()->company->database,
			]);

			return response()->json(['user' => true]);
		} else {
			return response()->json(['user' => false]);
		}
	}

	public function logout()
	{
		Auth::logout();

		return redirect('/');
	}
}
