<?php

namespace App\Http\Middleware;

use App\Models\Company;
use App\Models\External\TComerciales;
use Auth;
use Closure;

class CustomApiLogin
{

	/**
	 * Handle an incoming request.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param \Closure                 $next
	 *
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		if ($bearerToken = $request->header('Authorization')) {
			$request->header('Authorization', null);

			if (substr_count($bearerToken, '@') === 1) {
				$fullAuthArr = explode('@', $bearerToken);
				$database = $fullAuthArr[0];
				$token = $fullAuthArr[1];

				$commercial = TComerciales::on($database)->where('observaciones', $token)->first();

				if ($commercial) {
					Auth::shouldUse('commercials');
					Auth::login($commercial);

					$company = Company::where('database', $database)->firstOrFail();
					$sessionData = [
						'company_id' => $company->id,
						'database'   => $company->database,
					];
					session($sessionData);

					return $next($request);
				}
			}
		}

		return response()->json([
			'message' => 'Not a valid API request.',
		]);
	}
}
