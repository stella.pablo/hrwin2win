<?php

namespace App\Traits;

use App\Mail\MailSender;
use App\Mail\NewLoginData;
use App\Models\External\TComerciales;
use App\Models\User;
use App\Models\UserManagement;
use Hash;
use Illuminate\Support\Str;

trait CreateAccess
{

	public $DATABASE_TO_LOGIN = [
		'mexico'   => 'mx',
		'portugal' => 'pt',
        'mexico_test' => 'mx_test'
	];

	public function create_access_mobile(TComerciales $commercial)
	{
		if (!$commercial->email) {
			return null;
		}

		// Generate access data
		$username = $this->generateUsername($commercial);
		$password = rand(1111, 9999);

		// Set API key
		$commercial->update([
			'acceso_mobile' => 'S',
			'observaciones' => Str::random(50),
			'password'      => bcrypt($password),
		]);

		$commercial = $commercial->fresh();

		// Send email
		logger('creating new mobile access for: ' . $commercial->nombre_completo);
//		Mail::to($commercial->email)->send(new NewLoginData($commercial->nombre_completo, $username, $password, 'mobile'));
		MailSender::send($commercial->email, (new NewLoginData($commercial->nombre_completo, $username, $password, 'mobile')));
	}

	public function create_access_hr(TComerciales $commercial)
	{
		if (!$commercial->email OR !$commercial->id_comercial_externo) {
			return null;
		}

		$username = $this->generateUsername($commercial);
		$password = rand(1111, 9999);

		$user = User::firstOrCreate(['username' => $username], [
			'management_id' => $commercial->management_internal->id,
			'company_id'    => auth()->user()->company_id,
			'name'          => $commercial->nombre_completo,
			'email'         => $commercial->email,
			'username'      => $username,
			'password'      => Hash::make(strval($password)),
			'level'         => 3,
			'pin'           => rand(1111, 9999),
		]);

		// In case we're finding it, update name and email
		$user->name = $commercial->nombre_completo;
		$user->email = $commercial->email;
		$user->password = Hash::make(strval($password));
		$user->save();

		UserManagement::where('user_id', $user->id)->delete();

		UserManagement::create([
			'user_id'       => $user->id,
			'management_id' => $user->management_id,
		]);

		// Send email
		logger('creating new hr access for: ' . $user);
//		Mail::to($user->email)->send(new NewLoginData($user->name, $username, $password, 'hr'));
		MailSender::send($commercial->email, (new NewLoginData($commercial->nombre_completo, $username, $password, 'hr')));
	}

	private function generateUsername($commercial)
	{
		$username = strtolower(str_replace(' ', '', $commercial->id_comercial_externo));
		return $this->DATABASE_TO_LOGIN[session('database')] . '@' . $username;
	}
}
