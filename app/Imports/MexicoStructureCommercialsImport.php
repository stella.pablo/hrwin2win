<?php

namespace App\Imports;

use App\Models\External\TComerciales;
use App\Models\External\TComercialesCampanyas;
use App\Models\External\TEstrategias;
use App\Models\External\TGerencias;
use Carbon\Carbon;
use DB;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithProgressBar;

class MexicoStructureCommercialsImport implements ToModel, WithHeadingRow, WithProgressBar
{

	use Importable;

	/**
	 * @param array $row
	 *
	 * @return \Illuminate\Database\Eloquent\Model|null
	 */
	public function model(array $row)
	{
		$managerName = $row['gerente'];
		$management = TGerencias::where('gerencia', $managerName)->first();
		$managementId = $management->id_gerencia;

		$commercialName = preg_replace('/\s+/', ' ', $row['nombre_promotores']);
		$commercialNameArr = explode(' ', $commercialName);
		$commercialNameFixed = count($commercialNameArr) < 4 ?
			$commercialNameArr[count($commercialNameArr) - 1] :
			$commercialNameArr[count($commercialNameArr) - 2] . ' ' . $commercialNameArr[count($commercialNameArr) - 1];
		$commercialNameFixed .= ' ' . $commercialNameArr[0] . ' ' . $commercialNameArr[1];
		$commercialNameFixedArr = explode(' ', $commercialNameFixed);

		$nextCommercialId = $this->getNextCommercialId();
		$this->saveCommercialCampaign($nextCommercialId);

		$strategy = TEstrategias::where('id_estrategia', strval($row['estrategia_de_celula']))->first();
		$strategyId = $strategy ? $strategy->id : null;

		$commercial = [
			'apellido1'            => $commercialNameFixedArr[count($commercialNameFixedArr) - 2],
			'apellido2'            => $commercialNameFixedArr[count($commercialNameFixedArr) - 1],
			'fecha_alta'           => Carbon::now(),
			'id_comercial'         => $nextCommercialId,
			'id_comercial_externo' => $row['clave_del_promotor'],
			'id_empresa'           => '01',
			'id_estrategia'        => $strategyId,
			'id_estado'            => 0,
			'id_gerencia'          => $managementId,
			'nombre'               => count($commercialNameFixedArr) < 4 ? $commercialNameFixedArr[0] : $commercialNameFixedArr[0] . ' ' . $commercialNameFixedArr[1],
		];

		return new TComerciales($commercial);
	}

	private function getNextCommercialId()
	{
		$lastId = DB
			::connection(session('database'))
			->table('t_num_series_emp')
			->where('codigo', 'COMER')
			->first()
			->ult_num;

		$nextId = $lastId + 1;

		while (strlen($nextId) < 9) {
			$nextId = '0' . $nextId;
		}

		DB
			::connection(session('database'))
			->table('t_num_series_emp')
			->where('cod_emp', '01')
			->where('codigo', 'COMER')
			->update([
				'fecha_ult_num' => Carbon::now(),
				'ult_num'       => $nextId,
			]);

		return $nextId;
	}

	private function saveCommercialCampaign($commercialId)
	{
		TComercialesCampanyas::create([
			'id_empresa'   => '01',
			'id_campanya'  => 1,
			'id_comercial' => $commercialId,
			'fecha_alta'   => Carbon::now(),
			'estado'       => 1,
		]);
	}
}
