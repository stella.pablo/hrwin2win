<?php

namespace App\Imports;

use App\Models\External\TOrganizaciones;
use Log;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithProgressBar;

class MexicoStructureOrganizationsImport implements ToModel, WithHeadingRow, WithProgressBar
{

	use Importable;

	/**
	 * @param array $row
	 *
	 * @return \Illuminate\Database\Eloquent\Model|null
	 */
	public function model(array $row)
	{
		$organization = $row['organizacion'];

		if (TOrganizaciones::where('organizacion', $organization)->exists()) {
			Log::info('!!! ' . $organization . ' already exists, skipping...');

			return null;
		}

		if (empty($organization)) {
			return null;
		}

		Log::info('~ [ORGANIZATION] Creating: ' . $organization);

		return new TOrganizaciones(['organizacion' => $organization]);
	}
}
