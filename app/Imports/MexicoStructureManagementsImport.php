<?php

namespace App\Imports;

use App\Models\External\TComerciales;
use App\Models\External\TComercialesCampanyas;
use App\Models\External\TEstrategias;
use App\Models\External\TGerencias;
use App\Models\External\TOrganizaciones;
use Carbon\Carbon;
use DB;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithProgressBar;

class MexicoStructureManagementsImport implements ToModel, WithHeadingRow, WithProgressBar
{

	use Importable;

	/**
	 * @param array $row
	 *
	 * @return \Illuminate\Database\Eloquent\Model|null
	 */
	public function model(array $row)
	{
		if (empty($row['gerente'])) {
			return null;
		}

		$managerName = $row['gerente'];
		$managerNameArr = explode(' ', $managerName);
		$strategyId = strval($row['estrategia_de_celula']);
		$strategyName = $row['nombre_estrategia'];

		if ($management = TGerencias::where('gerencia', $managerName)->first()) {
			$managementId = $management->id_gerencia;

			if (TEstrategias::where('id_gerencia', $managementId)->where('id_estrategia', $strategyId)->doesntExist()) {
				$this->saveStrategy($managementId, $strategyId, $strategyName);
			}

			return null;
		}

		$organizationId = TOrganizaciones::where('organizacion', $row['organizacion'])->first()->id;
		$managementId = $this->getNextManagementId();

		$strategy = $this->saveStrategy($managementId, $strategyId, $strategyName);

		$commercial = TComerciales::create([
			'apellido1'     => $managerNameArr[count($managerNameArr) - 2],
			'apellido2'     => $managerNameArr[count($managerNameArr) - 1],
			'fecha_alta'    => Carbon::now(),
			'id_comercial'  => $this->getNextCommercialId(),
			'id_empresa'    => '01',
			'id_estado'     => 0,
			'id_estrategia' => $strategy->id,
			'id_gerencia'   => $managementId,
			'nombre'        => count($managerNameArr) < 4 ? $managerNameArr[0] : $managerNameArr[0] . ' ' . $managerNameArr[1],
		]);

		if ($row['gerente'] === $row['jefe_de_organizacion']) {
			TOrganizaciones::where('organizacion', $row['organizacion'])->update([
				'id_comercial' => $commercial->id_comercial,
			]);
		}

		$this->saveCommercialCampaign($commercial->id_comercial);

		$management = [
			'id_empresa'               => '01',
			'id_gerencia'              => $managementId,
			'id_comercial_liquidacion' => $commercial->id_comercial,
			'id_organizacion'          => $organizationId,
			'gerencia'                 => $managerName,
		];

		return new TGerencias($management);
	}

	private function saveCommercialCampaign($commercialId)
	{
		TComercialesCampanyas::create([
			'id_empresa'   => '01',
			'id_campanya'  => 1,
			'id_comercial' => $commercialId,
			'fecha_alta'   => Carbon::now(),
			'estado'       => 1,
		]);
	}

	private function saveStrategy($managementId, $strategyId, $strategy)
	{
		return TEstrategias::create([
			'id_gerencia'   => $managementId,
			'id_estrategia' => $strategyId,
			'estrategia'    => $strategy,
		]);
	}

	private function getNextManagementId()
	{
		$lastId = DB
			::connection(session('database'))
			->table('t_num_series_emp')
			->where('codigo', 'GERENC')
			->first()
			->ult_num;

		$nextId = $lastId + 1;

		while (strlen($nextId) < 4) {
			$nextId = '0' . $nextId;
		}

		DB
			::connection(session('database'))
			->table('t_num_series_emp')
			->where('cod_emp', '01')
			->where('codigo', 'GERENC')
			->update([
				'fecha_ult_num' => Carbon::now(),
				'ult_num'       => $nextId,
			]);

		return $nextId;
	}

	private function getNextCommercialId()
	{
		$lastId = DB
			::connection(session('database'))
			->table('t_num_series_emp')
			->where('codigo', 'COMER')
			->first()
			->ult_num;

		$nextId = $lastId + 1;

		while (strlen($nextId) < 9) {
			$nextId = '0' . $nextId;
		}

		DB
			::connection(session('database'))
			->table('t_num_series_emp')
			->where('cod_emp', '01')
			->where('codigo', 'COMER')
			->update([
				'fecha_ult_num' => Carbon::now(),
				'ult_num'       => $nextId,
			]);

		return $nextId;
	}
}
