<?php

namespace App\Imports;

use App\Models\External\TComerciales;
use App\Models\External\TContratos;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithProgressBar;
use PhpOffice\PhpSpreadsheet\Cell\Cell;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Cell\DefaultValueBinder;
use PhpOffice\PhpSpreadsheet\Exception;

class MexicoContractsImport extends DefaultValueBinder implements ToModel, WithBatchInserts, WithChunkReading, WithHeadingRow, WithProgressBar
{

	use Importable;

	/**
	 * @param array $row
	 *
	 * @return \Illuminate\Database\Eloquent\Model|null
	 */
	public function model(array $row)
	{
		$commercial = TComerciales::where('id_comercial_externo', $row['promotor'])->first();
		if (!$commercial) {
			return null;
		}

		$contract = [
			'id_empresa'           => '01',
			'id_tipo_contrato'     => 'tmx',
			'tipo_contrato'        => 'H',
			'cliente_nombre'       => '',
			'cliente_apellidos'    => '',
			'cliente_tlf1'         => strval($row['telefono_asignado']),
			'cliente_email'        => mb_strtolower($row['correo_electronico']),
			'cliente_id_provincia' => 1,
			'cliente_poblacion'    => '',
			'id_estado'            => $row['estatus_pisa_multiorden'] === 'CANCELADA' ? 7 : 9,
			'id_comercial'         => $commercial->id_comercial,
			'id_gerencia'          => $commercial->management->id_gerencia,
			'fecha_registro'       => \Carbon\Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row['fecha_captura'])),
			'hora_registro'        => \Carbon\Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row['fecha_captura'])),
			'id_campanya'          => 1,
			'codigo_cig'           => strval($row['folio_siac']),
		];

		return new TContratos($contract);
	}

	public function bindValue(Cell $cell, $value)
	{
		try {
			$cell->setValueExplicit(strval($value), DataType::TYPE_STRING);
		} catch (Exception $e) {
		}

		return true;
	}

	/**
	 * @return int
	 */
	public function batchSize(): int
	{
		return 300;
	}

	/**
	 * @return int
	 */
	public function chunkSize(): int
	{
		return 300;
	}
}
