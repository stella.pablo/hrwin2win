<?php

namespace App\Observers;

use App\Models\CandidatesFile;
use App\Models\External\TComerciales;
use App\Models\External\TComercialesCampanyas;
use App\Models\External\TComercialesContratos;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

class CommercialObserver
{

	protected $request;
	private $data;

	/**
	 * ContractObserver constructor.
	 *
	 * @param $request
	 */
	public function __construct(Request $request)
	{
		$this->request = $request;
		$this->data = $this->request->all();
	}

	public function creating(TComerciales $commercial)
	{
		$commercial->id_empresa = '01';
		$commercial->id_nivel_funcional = '003';
		$commercial->acceso_mobile = 'S';
		$commercial->id_estado = '0';
		$commercial->fecha_alta = Carbon::now()->toDateTimeString();

		$lastComPos = DB
			::connection(session('database'))
			->table('t_num_series_emp')
			->where('cod_emp', '01')
			->where('codigo', 'COMER')
			->first();

		if (!$lastComPos) {
			return null;
		}

		$commercial->id_comercial = sprintf('%09d', intval($lastComPos->ult_num) + 1);
	}

	/**
	 * Handle the t comerciales "created" event.
	 *
	 * @param \App\Models\External\TComerciales $commercial
	 *
	 * @return void
	 */
	public function created(TComerciales $commercial)
	{
		// Campaigns

		$campaigns = $this->data['campaigns'];

		if (is_array($campaigns) AND count($campaigns)) {
			foreach ($campaigns as $campaign) {
				TComercialesCampanyas::create([
					'id_empresa'   => '01',
					'id_campanya'  => $campaign['id'],
					'id_comercial' => $commercial->id_comercial,
					'fecha_alta'   => Carbon::now(),
					'estado'       => true,
				]);
			}
		}

		// Candidate

		if (isset($this->data['candidate_id'])) {
			CandidatesFile
				::where('candidate_id', $this->data['candidate_id'])
				->update(['external_id' => $commercial->id_comercial]);
		}

		// Contract

		//	$contractId = TComercialesContratos::where('id_comercial', $commercial->id_comercial)->count() + 1;
		/*
				TComercialesContratos::create([
					'id_empresa'                 => '01',
					'id_comercial'               => $commercial->id_comercial,
					'id_comercial_contrato'      => $contractId,
					'id_tipo_contrato_comercial' => $this->data['company_contract']['id_tipo_contrato_comercial'],
					'id_tramo'                   => $this->data['company_contract']['id_tramo'],
					'fecha_desde'                => Carbon::now(),
					'devolucion_comercial'       => 'N',
					'devolucion_estructura'      => 'N',
				]);
		*/
		// Update last commercial ID


		foreach ($this->data['company_contracts'] as $contract) {

			unset($contract['contract_type']);
			unset($contract['contract_stretch']);
			$contract['id_empresa'] = $commercial->id_empresa;
			$contract['id_comercial'] = $commercial->id_comercial;
			$contract['devolucion_comercial'] = 'N';
			$contract['devolucion_estructura '] = 'N';

			TComercialesContratos::create($contract);

		}


		DB
			::connection(session('database'))
			->table('t_num_series_emp')
			->where('cod_emp', '01')
			->where('codigo', 'COMER')
			->update([
				'fecha_ult_num' => Carbon::now(),
				'ult_num'       => $commercial->id_comercial,
			]);
	}

	/**
	 * Handle the t comerciales "updating" event.
	 *
	 * @param \App\Models\External\TComerciales $commercial
	 *
	 * @return void
	 */
	public function updating(TComerciales $commercial)
	{
		/*
		if (isset($this->data['company_contract']) AND is_array($this->data['company_contract'])) {
			if ($commercial->company_contract) {
				TComercialesContratos
					::where('id_comercial', $commercial->id_comercial)
					->update([
						'id_tipo_contrato_comercial' => $this->data['company_contract']['id_tipo_contrato_comercial'],
						'id_tramo'                   => $this->data['company_contract']['id_tramo'],
					]);
			} else {
				$contractId = TComercialesContratos::where('id_comercial', $commercial->id_comercial)->count() + 1;

				TComercialesContratos::create([
					'id_empresa'                 => '01',
					'id_comercial'               => $commercial->id_comercial,
					'id_comercial_contrato'      => $contractId,
					'id_tipo_contrato_comercial' => $this->data['company_contract']['id_tipo_contrato_comercial'],
					'id_tramo'                   => $this->data['company_contract']['id_tramo'],
					'fecha_desde'                => Carbon::now(),
					'devolucion_comercial'       => 'N',
					'devolucion_estructura'      => 'N',
				]);
			}
		}
		*/
	}

	/**
	 * Handle the t comerciales "deleted" event.
	 *
	 * @param \App\Models\External\TComerciales $commercial
	 *
	 * @return void
	 */
	public function deleted(TComerciales $commercial)
	{
		//
	}

	/**
	 * Handle the t comerciales "restored" event.
	 *
	 * @param \App\Models\External\TComerciales $commercial
	 *
	 * @return void
	 */
	public function restored(TComerciales $commercial)
	{
		//
	}

	/**
	 * Handle the t comerciales "force deleted" event.
	 *
	 * @param \App\Models\External\TComerciales $commercial
	 *
	 * @return void
	 */
	public function forceDeleted(TComerciales $commercial)
	{
		//
	}
}
