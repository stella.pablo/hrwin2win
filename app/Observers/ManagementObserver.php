<?php

namespace App\Observers;

use App\Models\External\TComerciales;
use App\Models\External\TComercialesEstructuras;
use App\Models\External\TGerencias;
use App\Models\External\TGerenciasEstructuras;
use App\Models\Management;
use App\Models\User;
use App\Models\UserManagement;
use Carbon\Carbon;
use DB;

class ManagementObserver
{

	public function creating(TGerencias $management)
	{
		$management->fecha_alta = Carbon::now();
		$management->id_empresa = '01';
		$management->id_estado = 1;

		$lastMngComPos = DB
			::connection(session('database'))
			->table('t_num_series_emp')
			->where('cod_emp', '01')
			->where('codigo', 'GERENC')
			->first();

		if (!$lastMngComPos) {
			return null;
		}

		$management->id_gerencia = sprintf('%04d', intval($lastMngComPos->ult_num) + 1);
	}

	/**
	 * Handle the t gerencias "created" event.
	 *
	 * @param \App\Models\External\TGerencias $management
	 *
	 * @return void
	 */
	public function created(TGerencias $management)
	{
		// Change commercial's management
		TComerciales
			::where('id_comercial', $management->id_comercial_liquidacion)
			->update([
				'id_gerencia'        => $management->id_gerencia,
				'id_nivel_funcional' => '001',
			]);

		// Update latest created management id
		DB
			::connection(session('database'))
			->table('t_num_series_emp')
			->where('cod_emp', '01')
			->where('codigo', 'GERENC')
			->update([
				'fecha_ult_num' => Carbon::now(),
				'ult_num'       => $management->id_gerencia,
			]);

		// Create internal management
		$managementInternal = Management::create([
			'company_id'            => session('company_id'),
			'name'                  => $management->gerencia,
			'external_id'           => $management->id_gerencia,
			'monthly_objective'     => 10,
			'productivity_low_days' => 0,
		]);

		//we create the management_structures so the management will have the default structure.
		if (session('database') == 'mexico') {
			$level1 = 'Gerente';
			$level2 = 'Supervisor';
			$level3 = 'Promotor';
		} else {
			$level1 = 'Gerente';
			$level2 = 'Estructura';
			$level3 = 'Comercial';
		}

		TGerenciasEstructuras::create([
			'id_empresa'             => '01',
			'id_gerencia'            => $management->id_gerencia,
			'id_gerencia_estructura' => '001',
			'nombre'                 => $level1,
			'id_tipo_estructura'     => 'G',
		]);
		TGerenciasEstructuras::create([
			'id_empresa'             => '01',
			'id_gerencia'            => $management->id_gerencia,
			'id_gerencia_estructura' => '002',
			'nombre'                 => $level2,
			'id_tipo_estructura'     => 'S',
		]);
		TGerenciasEstructuras::create([
			'id_empresa'             => '01',
			'id_gerencia'            => $management->id_gerencia,
			'id_gerencia_estructura' => '003',
			'nombre'                 => $level3,
			'id_tipo_estructura'     => 'C',
		]);

		// Also we create the commercial payment Structure
		TComercialesEstructuras::create([
			'id_empresa'             => '01',
			'id_comercial'           => $management->id_comercial_liquidacion,
			'id_estructura'          => 1,
			'id_gerencia'            => $management->id_gerencia,
			'id_gerencia_estructura' => '003',
			'fecha_desde'            => Carbon::now(),

		]);


		// Give permission to high level users from same company
		// and also to Superuser
		$users = User
			::where('company_id', session('company_id'))
			->where(function ($query) {
				$query
					->whereIn('level', [1, 2])
					->orWhere('username', 'Superuser');
			})
			->get();

		foreach ($users as $user) {
			UserManagement::create([
				'management_id' => $managementInternal->id,
				'user_id'       => $user->id,
			]);
		}
	}

	/**
	 * Handle the t gerencias "updated" event.
	 *
	 * @param \App\Models\External\TGerencias $management
	 *
	 * @return void
	 */
	public function updated(TGerencias $management)
	{
		Management
			::where('company_id', session('company_id'))
			->where('external_id', $management->id_gerencia)
			->update([
				'name' => $management->gerencia,
			]);
	}

	/**
	 * Handle the t gerencias "deleted" event.
	 *
	 * @param \App\Models\External\TGerencias $management
	 *
	 * @return void
	 */
	public function deleted(TGerencias $management)
	{
		//
	}

	/**
	 * Handle the t gerencias "restored" event.
	 *
	 * @param \App\Models\External\TGerencias $management
	 *
	 * @return void
	 */
	public function restored(TGerencias $management)
	{
		//
	}

	/**
	 * Handle the t gerencias "force deleted" event.
	 *
	 * @param \App\Models\External\TGerencias $management
	 *
	 * @return void
	 */
	public function forceDeleted(TGerencias $management)
	{
		//
	}
}
