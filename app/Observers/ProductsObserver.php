<?php

namespace App\Observers;


use App\Models\External\TProductos;
use App\Models\External\TProductosTramos;
use Illuminate\Http\Request;

class ProductsObserver
{

	protected $request;
	private $data;

	/**
	 * ContractObserver constructor.
	 *
	 * @param $request
	 */
	public function __construct(Request $request)
	{

		$this->request = $request;
		$this->data = $this->request->all();
	}

	public function creating(TProductos $product)
	{
		$product->id_empresa = '01';

	}

	/**
	 * Handle the t comerciales "created" event.
	 *
	 * @param \App\Models\External\TProductos $product
	 *
	 * @return void
	 */
	public function created(TProductos $product)
	{


		foreach ($this->data['stretches'] as $stretch) {


			$stretch['id_empresa'] = $product['id_empresa'];
			$stretch['id_producto'] = $product['id_producto'];



			TProductosTramos::create($stretch);

		}


	}

	/**
	 * Handle the t comerciales "updating" event.
	 *
	 * @param \App\Models\External\TProductos $product
	 *
	 * @return void
	 */
	public function updated(TProductos $product)
	{


	}

	/**
	 * Handle the t comerciales "deleted" event.
	 *
	 * @param \App\Models\External\TProductos $product
	 *
	 * @return void
	 */
	public function deleted(TProductos $product)
	{
		//borrar los tramos que acompañen


		if (isset($product['stretches'])) {
			foreach ($product['stretches'] as $stretch) {

				TProductosTramos::where('id', $stretch['id'])->delete();
			}
		}


	}

	/**
	 * Handle the t comerciales "restored" event.
	 *
	 * @param \App\Models\External\TProductos $product
	 *
	 * @return void
	 */
	public function restored(TProductos $product)
	{
		//
	}

	/**
	 * Handle the t comerciales "force deleted" event.
	 *
	 * @param \App\Models\External\TProductos $product
	 *
	 * @return void
	 */
	public function forceDeleted(TProductos $product)
	{
		//
	}
}
