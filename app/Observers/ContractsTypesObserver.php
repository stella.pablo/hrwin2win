<?php

namespace App\Observers;


use App\Models\External\TTiposContratosComercial;
use App\Models\External\TTiposContratosComercialTramos;
use Illuminate\Http\Request;

class ContractsTypesObserver
{

	protected $request;
	private $data;

	/**
	 * ContractObserver constructor.
	 *
	 * @param $request
	 */
	public function __construct(Request $request)
	{

		$this->request = $request;
		$this->data = $this->request->all();
	}

	public function creating(TTiposContratosComercial $contractType)
	{
		$contractType->id_empresa = '01';

	}

	/**
	 * Handle the t comerciales "created" event.
	 *
	 * @param \App\Models\External\TTiposContratosComercial $contractType
	 *
	 * @return void
	 */
	public function created(TTiposContratosComercial $contractType)
	{


		foreach ($this->data['stretches'] as $stretch) {


			$stretch['id_empresa'] = $contractType['id_empresa'];
			$stretch['id_tipo_contrato_comercial'] = $contractType['id_tipo_contrato_comercial'];
			$stretch['comision_negativa'] = 'S';


			TTiposContratosComercialTramos::create($stretch);

		}


	}

	/**
	 * Handle the t comerciales "updating" event.
	 *
	 * @param \App\Models\External\TTiposContratosComercial $contractType
	 *
	 * @return void
	 */
	public function updated(TTiposContratosComercial $contractType)
	{


		//dd('wiwiiwiw');
		//update de tramos
		//no funciona aqui si no modifico el modelo principal así que tengo que hacerlo en el controlador
		/*foreach ($this->data['stretches'] as $stretch) {

			if (isset($stretch['id_tramo'])) {

				TTiposContratosComercialTramos::where('id_tramo', $stretch['id_tramo'])->update(Arr::except($stretch, ['id_tramo', 'id_empresa']));
			} else {
				//crear si es nuevo
				$stretch['id_empresa'] = '01';
				$stretch['comision_negativa'] = 'S';
				$stretch['id_tipo_contrato_comercial'] = $contractType['id_tipo_contrato_comercial'];

				TTiposContratosComercialTramos::create($stretch);


			}


		}*/


	}

	/**
	 * Handle the t comerciales "deleted" event.
	 *
	 * @param \App\Models\External\TTiposContratosComercial $contractType
	 *
	 * @return void
	 */
	public function deleted(TTiposContratosComercial $contractType)
	{
		//borrar los tramos que acompañen


		if (isset($contractType['stretches'])) {
			foreach ($contractType['stretches'] as $stretch) {

				TTiposContratosComercialTramos::where('id_tramo', $stretch['id_tramo'])->delete();
			}
		}


	}

	/**
	 * Handle the t comerciales "restored" event.
	 *
	 * @param \App\Models\External\TTiposContratosComercial $contractType
	 *
	 * @return void
	 */
	public function restored(TTiposContratosComercial $contractType)
	{
		//
	}

	/**
	 * Handle the t comerciales "force deleted" event.
	 *
	 * @param \App\Models\External\TTiposContratosComercial $contractType
	 *
	 * @return void
	 */
	public function forceDeleted(TTiposContratosComercial $contractType)
	{
		//
	}
}
