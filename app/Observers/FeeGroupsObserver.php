<?php

namespace App\Observers;


use App\Models\External\TGruposComisionamiento;
use App\Models\External\TGruposComisionamientoArticulos;
use App\Models\External\TGruposComisionamientoComisiones;
use App\Models\External\TGruposComisionamientoComisionesEfectividad;
use App\Models\External\TGruposComisionamientoGerencias;
use App\Models\External\TGruposComisionamientoGerenciasComisiones;
use App\Models\External\TGruposComisionamientoProductos;
use App\Models\External\TGruposComisionamientoProductosComisiones;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class FeeGroupsObserver
{

    protected $request;
    private $data;

    /**
     * ContractObserver constructor.
     *
     * @param $request
     */
    public function __construct(Request $request)
    {

        $this->request = $request;
        $this->data = $this->request->all();
    }

    public function creating(TGruposComisionamiento $feeGroup)
    {
        $feeGroup->id_empresa = '01';

    }

    /**
     * Handle the t comerciales "created" event.
     *
     * @param \App\Models\External\TGruposComisionamiento $feeGroup
     *
     * @return void
     */
    public function created(TGruposComisionamiento $feeGroup)
    {


        foreach ($this->data['fee_managements'] as $management) {


            $management['id_grupo'] = $feeGroup['id'];

            $createdManagement = TGruposComisionamientoGerencias::create(Arr::except($management, ['fees']));

            foreach ($management['fees'] as $fee) {
                $fee['id_comision_gerencia'] = $createdManagement['id'];

                TGruposComisionamientoGerenciasComisiones::create($fee);
            }

        }

        foreach ($this->data['fee_products'] as $product) {


            $product['id_grupo'] = $feeGroup['id'];

            $createdProduct = TGruposComisionamientoProductos::create(Arr::except($product, ['fees']));

            foreach ($product['fees'] as $fee) {

                $fee['id_comision_producto'] = $createdProduct['id'];

                TGruposComisionamientoProductosComisiones::create($fee);


            }

        }

        foreach ($this->data['fees'] as $fee) {


            $fee['id_grupo'] = $feeGroup['id'];
            TGruposComisionamientoComisiones::create($fee);

        }

        if (isset($this->data['efectivity_fees'])) {
            foreach ($this->data['efectivity_fees'] as $fee) {


                $fee['id_grupo'] = $feeGroup['id'];
                TGruposComisionamientoComisionesEfectividad::create($fee);

            }
        }


        foreach ($this->data['fee_articles'] as $article) {


            $article['id_grupo'] = $feeGroup['id'];
            TGruposComisionamientoArticulos::create($article);

        }

    }

    /**
     * Handle the t comerciales "updating" event.
     *
     * @param \App\Models\External\TGruposComisionamiento $feeGroup
     *
     * @return void
     */
    public function updated(TGruposComisionamiento $feeGroup)
    {


    }

    /**
     * Handle the t comerciales "deleted" event.
     *
     * @param \App\Models\External\TGruposComisionamiento $feeGroup
     *
     * @return void
     */
    public function deleted(TGruposComisionamiento $feeGroup)
    {
        //borrar los tramos que acompañen
        $feeGroup->load('feeManagements.fees', 'feeProducts', 'fees', 'feeArticles');


        if (isset($feeGroup['feeManagements'])) {

            foreach ($feeGroup['feeManagements'] as $feeManagement) {

                TGruposComisionamientoGerenciasComisiones::where('id_comision_gerencia', $feeManagement['id'])->delete();

            }

            TGruposComisionamientoGerencias::where('id_grupo', $feeGroup['id'])->delete();

        }
        if (isset($feeGroup['feeArticles'])) {

            TGruposComisionamientoArticulos::where('id_grupo', $feeGroup['id'])->delete();

        }

        if (isset($feeGroup['feeProducts'])) {


            foreach ($feeGroup['feeProducts'] as $product) {

                TGruposComisionamientoProductosComisiones::where('id_comision_producto', $product['id'])->delete();

            }

            TGruposComisionamientoProductos::where('id_grupo', $feeGroup['id'])->delete();


        }

        if (isset($feeGroup['fees'])) {


            TGruposComisionamientoComisiones::where('id_grupo', $feeGroup['id'])->delete();

        }

        if (isset($feeGroup['efectivity_fees'])) {


            TGruposComisionamientoComisionesEfectividad::where('id_grupo', $feeGroup['id'])->delete();

        }

    }

    /**
     * Handle the t comerciales "restored" event.
     *
     * @param \App\Models\External\TGruposComisionamiento $feeGroup
     *
     * @return void
     */
    public function restored(TGruposComisionamiento $feeGroup)
    {
        //
    }

    /**
     * Handle the t comerciales "force deleted" event.
     *
     * @param \App\Models\External\TGruposComisionamiento $feeGroup
     *
     * @return void
     */
    public function forceDeleted(TGruposComisionamiento $feeGroup)
    {
        //
    }
}
