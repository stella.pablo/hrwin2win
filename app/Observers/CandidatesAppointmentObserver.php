<?php

namespace App\Observers;

use App\Mail\CandidateAppointment as CandidateAppointmentMail;
use App\Mail\MailSender;
use App\Models\Candidate;
use App\Models\CandidatesAppointment;
use App\Models\EmailsSent;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Redis;

class CandidatesAppointmentObserver
{

    public function created(CandidatesAppointment $appointment)
    {

        $path = auth()->user()->redis_routes['candidates']['stats'] . date('Ym');

        $candidate = Candidate::find($appointment->candidate_id);
        $candidate->phone_not_answered = false;
        $candidate->status_id = 1;
        $candidate->save();
        if (session('database') == 'portugal') {
            $greetText = 'Olá ';
            $subjectText = 'Marcação de Entrevista';
            $firstLineText = 'Obrigado pelo seu interesse em nossa empresa, é hora de verificar se é a pessoa que estamos à procura! Estamos curiosos para conhecê-lo(a) melhor. No seguimento da nossa conversa telefónica, aqui ficam os dados para que possa participar de nosso processo de seleção:';
            $dateTimeText = 'Data e hora: ';
            $locationText = 'Localização:';
            $thanksText = 'Obrigado e boa sorte!';
            $signHeadText = 'Atenciosamente,';
            $signBodyText = 'Equipe de RH Portugal.';
            $howToReachText = 'Como chegar (Google Maps): ';
            $legalInfoText = '';
            $secondAppointMentText = 'No seguimento da nossa conversa telefónica, a sua 2ª entrevista foi agendada para:';
            $secondAppointMentSubjectText = 'Marcação de 2ª Entrevista';
        } else {
            $greetText = 'Buenos días ';
            $subjectText = 'Entrevista de trabajo';
            $firstLineText = 'Gracias por tu interés en nuestra empresa ¡Es hora de comprobar si tú eres la persona que estamos buscando! Tenemos curiosidad de conocerte mejor por lo que te recordamos los datos para que puedas asistir a nuestro proceso de selección: ';
            $dateTimeText = 'Fecha y hora:';
            $locationText = 'Localización:';
            $thanksText = '¡Gracias y mucha suerte!';
            $signHeadText = 'Atentamente,';
            $signBodyText = 'Equipo de RRHH.';
            $howToReachText = 'Cómo llegar:';
            $legalInfoText = 'La información contenida en este mensaje y/o archivo(s) adjunto(s) es confidencial/privilegiada y está destinada a ser leída sólo por la(s) persona(s) a la(s) que va dirigida. Si usted lee este mensaje y no es el destinatario señalado, el empleado o el agente responsable de entregar el mensaje al destinatario, o ha recibido esta comunicación por error, le informamos que está totalmente prohibida, y puede ser ilegal, cualquier divulgación, distribución o reproducción de esta comunicación, y le rogamos que nos lo notifique inmediatamente y nos devuelva el mensaje original a la dirección arriba mencionada. Gracias. ';
            $secondAppointMentText = 'Tal y como hemos hablado la entrevista será:';
            $secondAppointMentSubjectText = 'Segunda entrevista de trabajo';
        }

        if ($candidate->email && request()->has('sendMail') && request('sendMail')== 1 ) {
            $extrabody = request()->filled('extrabody') ? request()->extrabody : null;
            MailSender::send($candidate, new CandidateAppointmentMail(auth()->user(), $candidate, $appointment, $extrabody, $greetText, $subjectText, $firstLineText, $dateTimeText, $locationText, $thanksText, $signHeadText, $signBodyText,  $howToReachText, $legalInfoText, $secondAppointMentText,$secondAppointMentSubjectText));

            EmailsSent::create([
                'candidate_id' => $candidate->id,
                'user_id' => auth()->user()->id,
                'email_type_id' => 3,
            ]);
        }

        Redis::hincrby($path, 'a' . $appointment->number, 1);
    }

    public function updating(CandidatesAppointment $appointment)
    {
        $original = $appointment->getOriginal();
        $path = auth()->user()->redis_routes['candidates']['stats'] . date('Ym');

        if ($original['status'] !== $appointment->status) {
            if ($appointment->status === 2) {
                Redis::hincrby($path, 'a' . $appointment->number . 'd', 1);
            } elseif ($appointment->status === 0 AND $original['status'] === 2) {
                Redis::hincrby($path, 'a' . $appointment->number . 'd', -1);
            }

        }

        $candidate = Candidate::find($appointment->candidate_id);
        $candidate->touch();
    }
}
