<?php

namespace App\Observers;

use App\Mail\CandidateDoesNotAnswerPhone;
use App\Mail\MailSender;
use App\Mail\NewCandidate as NewCandidateMail;
use App\Models\Candidate;
use App\Models\EmailsSent;
use Illuminate\Support\Facades\Redis;

class CandidateObserver
{

	public function created(Candidate $candidate)
	{
		$path = 'rrhh:c' . $candidate->management->company->id . ':m' . $candidate->management->id . ':candidates:stats:' . date('Ym');
		Redis::hincrby($path, 'new', 1);

		if(request()->filled('email_type_id')) {
			MailSender::send($candidate, new NewCandidateMail(auth()->user(), $candidate));

			EmailsSent::create([
				'candidate_id'  => $candidate->id,
				'user_id'       => auth()->user()->id,
				'email_type_id' => request()->email_type_id,
			]);
		}
	}

	public function updating(Candidate $candidate)
	{
		$original = $candidate->getOriginal();
		$path = 'rrhh:c' . $candidate->management->company->id . ':m' . $candidate->management->id . ':candidates:stats:' . date('Ym');

		if ($original['status_id'] !== 3 AND $candidate->status_id === 3) {
			MailSender::send($candidate, new CandidateDoesNotAnswerPhone(auth()->user(), $candidate));

			EmailsSent::create([
				'candidate_id'  => $candidate->id,
				'user_id'       => auth()->user()->id,
				'email_type_id' => 2,
			]);
		}

		if (!$original['preapproved'] AND $candidate->preapproved) {
			Redis::hincrby($path, 'pre', 1);
		}

		if (!$original['approved'] AND $candidate->approved) {
			Redis::hincrby($path, 'apr', 1);
		}

		if (!$original['hired'] AND $candidate->hired) {
			Redis::hincrby($path, 'hired', 1);
		}
	}
}
