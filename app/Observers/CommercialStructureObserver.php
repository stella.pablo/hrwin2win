<?php

namespace App\Observers;

use App\Models\CandidatesFile;
use App\Models\External\TComercialesEstructuras;
use App\Models\External\TComerciales;
use App\Models\Management;
use App\Models\User;
use App\Models\UserManagement;
use Carbon\Carbon;
use Couchbase\UserSettings;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class CommercialStructureObserver
{

	protected $request;
	private $data;

	/**
	 * ContractObserver constructor.
	 *
	 * @param $request
	 */
	public function __construct(Request $request)
	{
		$this->request = $request;
		$this->data = $this->request->all();
	}

	public function creating(TComercialesEstructuras $structure)
	{


		if($structure['id_gerencia_estructura'] == '001') {
			$baseManagement = Management
				::where('external_id', '=', $structure['id_gerencia'])
				->where('company_id', session('company_id'))
				->first();

			if ($baseManagement) {
				//aplico permisos solo si existe el usuario en cuestión.
				//tengo el comercial al que tengo que darle los permisos en base

				$baseUser = User::where('management_id', $baseManagement['id'])->first();

				$commercialObjective = TComerciales::where('id_comercial', '=', $structure['id_comercial_estructura'])->first()->append('user_tree', 'cleanTree', 'available_managements');

				//busco el arbol de estructuras del comercial objetivo para añadir las estructuras que ya tiene a los permisos del comercial base

				if ($baseUser and $commercialObjective) {
					$managementsIdsArr = Arr::pluck($commercialObjective->available_managements, 'id_gerencia');
					$managementsAll = Management::whereIn('external_id', $managementsIdsArr)->where('company_id', session('company_id'))->get();

					if ($managementsAll->count() > 0) {
						foreach ($managementsAll as $management) {
							$userMgmtArr = [
								'user_id'       => $baseUser->id,
								'management_id' => $management->id,
							];


							UserManagement::firstOrCreate($userMgmtArr, $userMgmtArr);
						}
					}
				}
			}

		}



	}

	/**
	 * Handle the t comerciales "created" event.
	 *
	 * @param \App\Models\External\TComercialesEstructuras $structure
	 *
	 * @return void
	 */
	public function created(TComercialesEstructuras $structure)
	{
		// Campaigns

	}

	/**
	 * Handle the t comerciales "updating" event.
	 *
	 * @param \App\Models\External\TComercialesEstructuras $structure
	 *
	 * @return void
	 */
	public function updating(TComercialesEstructuras $structure)
	{

	}

	/**
	 * Handle the t comerciales "deleted" event.
	 *
	 * @param \App\Models\External\TComercialesEstructuras $structure
	 *
	 * @return void
	 */
	public function deleted(TComercialesEstructuras $structure)
	{
		//CLEAN PERMISSIONS

		$baseManagement = Management
			::where('external_id', $structure->id_gerencia)
			->where('company_id', session('company_id'))
			->first();

		$baseUser = User::where('management_id', $baseManagement->id)->first();

		if ($baseUser) {
			$allStructures = TComercialesEstructuras
				::where('id_comercial', $structure->id_comercial)
				->where('id_estructura', '<>', $structure->id_estructura)
				->where('id_gerencia_estructura', '001')
				->get();

			$otherManagersIds = Arr::pluck($allStructures, 'id_comercial_estructura');
			$otherManagementsIds = Arr::pluck(
				TComerciales
					::whereIn('id_comercial', $otherManagersIds)
					->distinct('id_gerencia')
					->get()
					->toArray(),
				'id_gerencia'
			);

			$otherManagementsIds[] = $structure->id_gerencia;


			$internalManagementsIds = Arr::pluck(
				Management
					::whereIn('external_id', $otherManagementsIds)
					->where('company_id', session('company_id'))
					->get()
					->toArray(),
				'id'
			);

			$internalManagementsIds[] = $baseManagement->id;

			UserManagement
				::whereNotIn('management_id', $internalManagementsIds)
				->where('user_id', $baseUser->id)
				->delete();
		}
	}

	/**
	 * Handle the t comerciales "restored" event.
	 *
	 * @param \App\Models\External\TComercialesEstructuras $structure
	 *
	 * @return void
	 */
	public function restored(TComercialesEstructuras $structure)
	{
		//
	}

	/**
	 * Handle the t comerciales "force deleted" event.
	 *
	 * @param \App\Models\External\TComercialesEstructuras $structure
	 *
	 * @return void
	 */
	public function forceDeleted(TComercialesEstructuras $structure)
	{
		//
	}
}
