<?php

namespace App\Observers;


use App\Models\External\TAlbaranes;
use App\Models\External\TArticulosPaquetes;
use App\Models\External\TComerciales;
use App\Models\External\TContratos;
use App\Models\External\TContratosProductos;
use App\Models\External\TContratosProductosEstructuras;
use App\Models\External\TEntregas;
use App\Models\External\TGerencias;
use App\Models\External\TGruposComisionamiento;
use App\Models\External\TGruposComisionamientoComisiones;
use App\Models\External\TGruposComisionamientoProductosComisiones;
use App\Models\External\TLiquidaciones;
use App\Models\External\TLiquidacionesComerciales;
use App\Models\External\TLiquidacionesComercialesConceptos;
use App\Models\External\TVentasLineas;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;

class SettlementsObserver
{

	protected $request;
	private $data;

	/**
	 * ContractObserver constructor.
	 *
	 * @param $request
	 */
	public function __construct(Request $request)
	{

		$this->request = $request;
		$this->data = $this->request->all();
	}

	public function creating(TLiquidaciones $settlement)
	{
		$settlement->id_empresa = '01';

	}

	/**
	 * Handle the t comerciales "created" event.
	 *
	 * @param \App\Models\External\TTiposContratosComercial $contractType
	 *
	 * @return void
	 */


	public function created(TLiquidaciones $settlement)
	{


		$referedCommercial = '';
		$referedManagement = '';

		$commercials = TComerciales::query();

		$contractType = $this->data['id_tipo_contrato_comercial'];


		if (isset($this->data['id_gerencia'])) {

			$referedManagement = $this->data['id_gerencia'];

		}
		if (isset($this->data['id_comercial'])) {

			$referedCommercial = $this->data['id_comercial'];

		}


		$commercials = $commercials->when(isset($this->data['id_comercial']), function ($q) use ($referedCommercial) {

			return $q->where('id_comercial', $referedCommercial);


		});


		$commercials = $commercials->when(!isset($this->data['id_comercial']) and isset($this->data['id_gerencia']), function ($q) use ($referedManagement) {


			return $q->where('id_gerencia', $referedManagement);

		});


		$commercials = $commercials->when(isset($this->data['id_estructura_comercial']), function ($q, $structure) {

			return $q->where('id_nivel_funcional', $structure);

		});


		$commercials = $commercials->with('management', 'current_company_contract')->get();

		$commercialsToSettle = [];

		foreach ($commercials as $commercial) {
			if ($commercial->current_company_contract) {
				if ($commercial->current_company_contract->id_tipo_contrato_comercial == $contractType) {
					$commercialsToSettle[] = $commercial;
				}
			}
		}


		foreach ($commercialsToSettle as $commercial) {


			//solo liquido aquellos que están con contrato


			$importe_car = 0;
			$commercial_amount = 0;
			$salesNum = 0;
			$salesCount = 0;
			//creo la línea intermedia del comercial
			$commercialSettlementData = [
				'id_liquidacion'   => $settlement['id'],
				'id_comercial'     => $commercial['id_comercial'],
				'id_gerencia'      => $commercial['id_gerencia'],
				'tipo_registro'    => 1,
				'numero_ventas'    => 0,
				'numero_articulos' => 0,
				'fecha_pago'       => $settlement['fecha_liquidacion'],
				'importe_inm'      => $commercial_amount,
				'importe_car'      => $importe_car,
				'iva'              => $commercial->current_company_contract->contractType->iva,
				'irpf'             => $commercial->current_company_contract->contractType->irpf,


			];

			$commercialSettlement = TLiquidacionesComerciales::create($commercialSettlementData);


			$groups = TGruposComisionamiento::whereHas('feeManagements', function ($q) use ($commercial) {

				$q->where('id_gerencia', $commercial['id_gerencia']);

			})->with([
				'feeManagements' => function ($q) use ($commercial) {
					$q->where('id_gerencia', $commercial['id_gerencia'])->with('fees');
				},
			])
				->get()
				->load('feeArticles', 'feeProducts', 'fees', 'efectivityFees');

			if (!isset($commercial->management)) {
				$manager = null;
				$management = null;
			} else {
				$manager = $commercial->management->id_comercial_liquidacion;
				$management = $commercial->management;
			}


			$fecha_desde = Carbon::createFromDate($this->data['fecha_desde'])->endOfDay()->toDateTime();
			$fecha_hasta = Carbon::createFromDate($this->data['fecha_hasta'])->endOfDay()->toDateTime();

			//recorro los grupos del comercial

			foreach ($groups as $group) {


				//liquidacion de artículos para grupos tipo 2 o 3 (ARTICULO)

				if ($group->id_tipo == 2 or $group->id_tipo == 3) {

					$articles = [];
					//tomo las id de loas artículos en un array

					foreach ($group->feeArticles as $article) {
						$articles[] = $article->id_articulo;
					}


					$sales = TVentasLineas::whereHas('sale', function ($q) use ($fecha_desde, $fecha_hasta) {

						$q->whereBetween('fecha', [$fecha_desde, $fecha_hasta]);
					}

					)->whereDoesntHave('settlementConcepts', function ($q) use ($commercial) {

						$q->where('id_comercial', $commercial['id_comercial']);

					})->whereIn('id_articulo', $articles);


					$sales = $sales->with('sale.paymentStructures')->get();


					$commercialSales = TVentasLineas::whereHas('sale', function ($q) use ($commercial, $fecha_desde, $fecha_hasta) {

						$q->whereBetween('fecha', [$fecha_desde, $fecha_hasta])->where('id_comercial', '=', $commercial['id_comercial']);
					}
					)->whereDoesntHave('settlementConcepts', function ($q) use ($commercial) {

						$q->where('id_comercial', $commercial['id_comercial']);


					})->whereIn('id_articulo', $articles);


					$salesNum = $commercialSales->sum('cantidad');
					$salesCount = $commercialSales->count();

					$commercialSales = $commercialSales->get();

					$fee = TGruposComisionamientoComisiones::where('id_grupo', $group->id)
						->when(isset($this->data['id_estructura_comercial']), function ($q, $structure) {
							return $q->where('id_estructura', $structure);
						})
						->where('cantidad_desde', '<=', $salesNum)
						->where('cantidad_hasta', '>=', $salesNum)
						->first();


					if ($fee) {

						$commercial_amount = $salesNum * $fee->importe_comercial;

						foreach ($commercialSales as $sale) {

							//pago directo por venta
							$commercialSettlementConcept = [
								'id_liquidacion'           => $settlement['id'],
								'id_grupo'                 => $group['id'],
								'id_comercial'             => $commercial['id_comercial'],
								'id_liquidacion_comercial' => $commercialSettlement['id'],
								'concepto'                 => 1,
								'importe'                  => $sale->cantidad * $fee->importe_comercial,
								'id_venta'                 => $sale['id_venta'],
								'id_linea_venta'           => $sale['id_linea'],
								'tipo_linea'               => 0,

							];


							TLiquidacionesComercialesConceptos::create($commercialSettlementConcept);


						}


					}


					foreach ($sales as $sale) {


						//recorro las estructuras dwe venta buscando las que pertenezcan a esta venta y llamen a este comercial como gerente

						$baseCommercial = null;
						$structuretoPay = null;

						foreach ($sale->sale->paymentStructures as $structure) {

							if ($structure->id_tipo_estructura == 'C') {
								//recojo la info del comercial que ha hecho la venta
								$baseCommercial = TComerciales::where('id_comercial', '=', $structure->id_comercial)->first();

								//el propio gerente ha hecho la venta en efectivo, se la cobro


								if ($sale->sale['id_metodo_pago'] == 2 and $manager == $commercial['id_comercial'] and $structure->id_comercial == $commercial['id_comercial']) {

									// and $manager == $structure->id_comercial and $sale->sale['id_gerencia'] == $commercial['id_gerencia']
									//cobro si es gerente el importe de la linea


									$commercialSettlementConcept = [
										'id_liquidacion'           => $settlement['id'],
										'id_grupo'                 => null,
										'id_liquidacion_comercial' => $commercialSettlement['id'],
										'id_comercial'             => $manager,
										'concepto'                 => 2,
										'importe'                  => -$sale->total,
										'id_venta'                 => $sale['id_venta'],
										'id_linea_venta'           => $sale['id_linea'],
										'tipo_linea'               => 0,

									];
									TLiquidacionesComercialesConceptos::create($commercialSettlementConcept);
									$commercial_amount -= $sale->total;


								}

								if ($manager == $commercial['id_comercial'] and $structure->id_comercial == $commercial['id_comercial']) {
									//abono del dinero que ha pagado el comercial por el artículo
									$baseprice = $this->BuyPrice($management->id_gerencia, $sale->sale->id_empresa, $sale->id_articulo, $sale->unidades_lote, $sale->cantidad,
										$management->defaultRate);
									if ($baseprice > 0) {
										$commercialSettlementConcept = [
											'id_liquidacion'           => $settlement['id'],
											'id_grupo'                 => null,
											'id_liquidacion_comercial' => $commercialSettlement['id'],
											'id_comercial'             => $manager,
											'concepto'                 => 5,
											'importe'                  => $baseprice * $sale->cantidad,
											'id_venta'                 => $sale['id_venta'],
											'id_linea_venta'           => $sale['id_linea'],
											'tipo_linea'               => 0,

										];

										TLiquidacionesComercialesConceptos::create($commercialSettlementConcept);

										$commercial_amount += $baseprice * $sale->cantidad;
									}
								}

							}
						}

						foreach ($sale->sale->paymentStructures as $structure) {

							//	dd($sale->sale->paymentStructures);

							$structuretoPay = null;
							if ($structure->id_comercial == $commercial['id_comercial'] and $structure->id_tipo_estructura == 'G') {

								//estoy liquidando venta referenciada
								$structuretoPay = $structure;

							}


							//un comercial del gerente ha hecho la venta, en la linea de gerente imputo el cobro de la venta en efectivo
							//si es efectivo, si el comercial que liquido es el gerente de la venta y coincide con el comercial de la estructura
							if ($sale->sale['id_metodo_pago'] == 2 and $manager == $commercial['id_comercial'] and $sale->sale['id_gerencia'] == $commercial['id_gerencia'] and $structuretoPay) {
								//and $manager == $commercial['id_comercial']
								//cobro si es gerente el importe de la linea

								$commercialSettlementConcept = [
									'id_liquidacion'           => $settlement['id'],
									'id_grupo'                 => null,
									'id_liquidacion_comercial' => $commercialSettlement['id'],
									'id_comercial'             => $manager,
									'concepto'                 => 2,
									'importe'                  => -$sale->total,
									'id_venta'                 => $sale['id_venta'],
									'id_linea_venta'           => $sale['id_linea'],
									'tipo_linea'               => 0,

								];


								TLiquidacionesComercialesConceptos::create($commercialSettlementConcept);

								$commercial_amount -= $sale->total;


								//abono del importe de precio de compra de los artículos. Para simplificar, tomo el precio de compra actual


							}


							//si el tengo un comercial base y tengo que pagar a la estructura
							if ($structuretoPay and $baseCommercial) {
								//tengo que hacer un hallazo del comercial y sus ventas para saber la agrupación e importes

								$structureManagement = TGerencias::where('id_gerencia', $structuretoPay->id_gerencia)->first();


								$cgroup = TGruposComisionamiento::whereHas('feeManagements', function ($q) use ($baseCommercial) {

									$q->where('id_gerencia', $baseCommercial->id_gerencia);

								})->first()->load('feeArticles');


								//recorro los grupos del comercial


								$carticles = [];
								//tomo las id de loas artículos en un array

								foreach ($cgroup->feeArticles as $article) {
									$carticles[] = $article->id_articulo;
								}


								$csales = TVentasLineas::whereHas('sale', function ($q) use ($baseCommercial, $fecha_desde, $fecha_hasta) {

									$q->whereBetween('fecha', [$fecha_desde, $fecha_hasta])->where('id_comercial', $baseCommercial->id_comercial);
								}

								)->whereIn('id_articulo', $articles);


								$csalesNum = $csales->sum('cantidad');


								$cfee = TGruposComisionamientoComisiones::where('id_grupo', $cgroup->id)
									->where('cantidad_desde', '<=', $csalesNum)
									->where('cantidad_hasta', '>=', $csalesNum)
									->first();

								if ($cfee) {


									//pago al comercial actual lo que indico en la tarifa de gerencia de comercial base
									$importe_car += $sale->cantidad * $cfee->importe_estructura;
									$commercialSettlementConcept = [
										'id_liquidacion'            => $settlement['id'],
										'id_grupo'                  => $cgroup['id'],
										'id_liquidacion_comercial'  => $commercialSettlement['id'],
										'id_comercial'              => $commercial->id_comercial,
										'concepto'                  => 3,
										'importe'                   => $sale->cantidad * $cfee->importe_estructura,
										'id_venta'                  => $sale['id_venta'],
										'id_linea_venta'            => $sale['id_linea'],
										'tipo_linea'                => 0,
										'id_comercial_ref_directo'  => $structureManagement->id_comercial_liquidacion,
										'id_comercial_ref_original' => $sale->sale['id_comercial'],
										'id_gerencia_ref_directa'   => $structuretoPay->id_gerencia,
										'id_gerencia_ref_original'  => $sale->sale['id_gerencia'],

									];

									TLiquidacionesComercialesConceptos::create($commercialSettlementConcept);


								}

							}
						}


					}
				}
				//grupos de comisionamiento de tipo PRODUCTO
				if ($group->id_tipo == 1 or $group->id_tipo == 3) {


					$baseCommercial = null;
					$structuretoPay = null;

					$totalProducts = 0;
					$actualCommercial = $commercial->id_comercial;
					//recojo los productos con fecha de alta entre fechas de liquidacion junto a sus contratos
					$productList = (Arr::pluck($group->feeProducts->toArray(), 'id_producto'));
					$stretchesList = (Arr::pluck($group->feeProducts->toArray(), 'id_tramo'));

					$contracts = TContratos::whereHas('products', function ($q) use ($fecha_desde, $fecha_hasta, $productList, $stretchesList) {

						$q->whereBetween('fecha_alta', [$fecha_desde, $fecha_hasta])
							->whereIn('id_producto', $productList)
							->WhereIn('id_tramo', $stretchesList);


					})
						->with([
							'products' => function ($q) use (
								$fecha_desde,
								$fecha_hasta,
								$productList,
								$stretchesList,
								$actualCommercial
							) {
								$q->whereBetween('fecha_alta', [$fecha_desde, $fecha_hasta])
									->whereIn('id_producto', $productList)
									->WhereIn('id_tramo', $stretchesList)
									->whereDoesntHave('settlementConcepts', function ($q2) use ($actualCommercial) {
										$q2->where('id_comercial', $actualCommercial);
									})->with('structures');


							},
						])->get();


					foreach ($contracts as $contract) {

						$totalProducts += sizeof($contract->products);
					}


					$fee = $group->fees->where('id_grupo', $group->id)
						->where('cantidad_desde', '<=', $totalProducts)
						->where('cantidad_hasta', '>=', $totalProducts)
						->where('id_estructura', $commercial->id_nivel_funcional)
						->where('id_tipo_contrato_comercial', $contractType)
						->first();


					foreach ($contracts as $contract) {

						//recorro los contratos

						foreach ($contract->products as $product) {

							//cuento los productos totales
							//recorro los productos a liquidar


							foreach ($product->structures as $paymentStructure) {


								//PAYMENTTYPE
								//liquidacion de comision directa de comercial por venta


								// recorro las estructuras de los productos y busco las que me sean de pago
								// directo a comercial. He limpiado en la query las que están ya pagadas
								$baseCommercial = TComerciales::where('id_comercial', '=', $contract->id_comercial)->first();

								$structureManagement = TGerencias::where('id_gerencia', $paymentStructure->id_gerencia)->first();


								if ($fee AND $contract->id_comercial == $paymentStructure->id_comercial AND $paymentStructure->id_comercial == $actualCommercial) {


									//si hay comisión en el grupo, es el propio comercial que ha hecho la venta de producto
									//pago al comercial actual lo que indico en la tarifa de gerencia de comercial base

									$commercial_amount += $fee->importe_comercial;
									$commercialSettlementConcept = [
										'id_liquidacion'            => $settlement['id'],
										'id_grupo'                  => $group['id'],
										'id_liquidacion_comercial'  => $commercialSettlement['id'],
										'id_comercial'              => $commercial->id_comercial,
										'concepto'                  => 9,
										'importe'                   => $fee->importe_comercial,
										'id_contrato'               => $contract['id_contrato'],
										'id_contrato_producto'      => $product['id'],
										'tipo_linea'                => 0,
										'id_comercial_ref_directo'  => $structureManagement->id_comercial_liquidacion,
										'id_comercial_ref_original' => $contract['id_comercial'],
										'id_gerencia_ref_directa'   => $paymentStructure->id_gerencia,
										'id_gerencia_ref_original'  => $contract['id_gerencia'],

									];

									TLiquidacionesComercialesConceptos::create($commercialSettlementConcept);


									//pagos por porcentaje de oks


									$totalContracts = TContratos::query();
									$totalContracts = $totalContracts->whereHas('products', function ($q)
									use ($fecha_desde, $fecha_hasta) {
										$q->whereBetween('fecha_alta', [$fecha_desde, $fecha_hasta]);
									}
									)->where('id_comercial', $commercial->id_comercial)
										->withCount([
											'products' => function ($q) use ($fecha_desde, $fecha_hasta) {
												$q->whereBetween('fecha_alta', [$fecha_desde, $fecha_hasta]);
											},
										]);

									$totalContractsCount = $totalContracts->get()->sum('products_count');

									$totalContractsOkCount = $totalContracts->where('id_estado', 6)->orwhere('id_estado', 9)->get()->sum('products_count');


									if ($totalContractsCount && $totalContractsOkCount) {

										$percentaje = $totalContractsOkCount * 100 / $totalContractsCount;

										//hallo la linea de comisionamiento que me interesa

										$percentajeFee = $group->efectivityFees
											->where('efectividad_desde', '<=', $percentaje)
											->where('efectividad_hasta', '>=', $percentaje)
											->first();


										if ($percentajeFee) {
											if ($commercial->id_nivel_funcional == '001') {

												$amount = $percentajeFee->importe_gerencia;

											} elseif ($commercial->id_nivel_funcional === '002') {

												$amount = $percentajeFee->importe_estructura;

											} elseif ($commercial->id_nivel_funcional === '003') {

												$amount = $percentajeFee->importe_promotor;

											}

											$importe_car += $amount;
											$commercialSettlementConcept = [
												'id_liquidacion'            => $settlement['id'],
												'id_grupo'                  => $group['id'],
												'id_liquidacion_comercial'  => $commercialSettlement['id'],
												'id_comercial'              => $commercial->id_comercial,
												'concepto'                  => 13,
												'importe'                   => $amount,
												'id_contrato'               => null,
												'id_contrato_producto'      => null,
												'tipo_linea'                => 0,
												'id_comercial_ref_directo'  => null,
												'id_comercial_ref_original' => null,
												'id_gerencia_ref_directa'   => null,
												'id_gerencia_ref_original'  => null,

											];

											TLiquidacionesComercialesConceptos::create($commercialSettlementConcept);


										}


									}


									//liquidacion de comision de comercial por contrato finalizado ok

									if ($contract->id_estado == 6 or $contract->id_estado == 9) {
										$commercialCount = TGerencias::where('id_gerencia', $commercial->id_gerencia)->withCount('commercials')->first();

										$variableFee = TGruposComisionamientoProductosComisiones::
										where('id_comision_producto', $product->id)
											->where('desde_promotores', '<=', $commercialCount->commercials_count)
											->where('hasta_promotores', '>=', $commercialCount->commercials_count)
											->first();


										if ($variableFee) {


											$commercial_amount += $fee->importe_comision;
											$commercialSettlementConcept = [
												'id_liquidacion'            => $settlement['id'],
												'id_grupo'                  => $group['id'],
												'id_liquidacion_comercial'  => $commercialSettlement['id'],
												'id_comercial'              => $commercial->id_comercial,
												'concepto'                  => 11,
												'importe'                   => $variableFee->importe_comision,
												'id_contrato'               => $contract['id_contrato'],
												'id_contrato_producto'      => $product['id'],
												'tipo_linea'                => 0,
												'id_comercial_ref_directo'  => $structureManagement->id_comercial_liquidacion,
												'id_comercial_ref_original' => $contract['id_comercial'],
												'id_gerencia_ref_directa'   => $paymentStructure->id_gerencia,
												'id_gerencia_ref_original'  => $contract['id_gerencia'],

											];

											TLiquidacionesComercialesConceptos::create($commercialSettlementConcept);


										}


									}

								}


							}


						}


					}


					//pago a gerente en base a zonas
					if ($commercial->id_nivel_funcional == '001') {


						$totalContracts = TContratos::query();
						$totalContracts = $totalContracts->whereHas('products', function ($q)
						use ($fecha_desde, $fecha_hasta) {
							$q->whereBetween('fecha_alta', [$fecha_desde, $fecha_hasta]);
						}
						)->where('id_gerencia', $commercial->id_gerencia)
							->withCount([
								'products' => function ($q) use ($fecha_desde, $fecha_hasta) {
									$q->whereBetween('fecha_alta', [$fecha_desde, $fecha_hasta]);
								},
							]);

						$totalZonesCount = TContratos::whereHas('products', function ($q)
						use ($fecha_desde, $fecha_hasta) {
							$q->whereBetween('fecha_alta', [$fecha_desde, $fecha_hasta]);
						}
						)->where('id_gerencia', $commercial->id_gerencia)
							->distinct('id_tipo_contrato')->count();


						$totalProductsCount = $totalContracts->get()->sum('products_count');


						$productionFee = $group->feeManagements->first()->fees
							->where('cantidad_desde', '<=', $totalProductsCount)
							->where('cantidad_hasta', '>=', $totalProductsCount)
							->where('zonas_desde', '<=', $totalZonesCount)
							->where('zonas_hasta', '>=', $totalZonesCount)->first();

						if ($productionFee) {


							$commercial_amount += $fee->importe_comision;
							$commercialSettlementConcept = [
								'id_liquidacion'            => $settlement['id'],
								'id_grupo'                  => $group['id'],
								'id_liquidacion_comercial'  => $commercialSettlement['id'],
								'id_comercial'              => $commercial->id_comercial,
								'concepto'                  => 14,
								'importe'                   => $productionFee->importe_comision,
								'id_contrato'               => null,
								'id_contrato_producto'      => null,
								'tipo_linea'                => 0,
								'id_comercial_ref_directo'  => null,
								'id_comercial_ref_original' => null,
								'id_gerencia_ref_directa'   => null,
								'id_gerencia_ref_original'  => null,

							];

							TLiquidacionesComercialesConceptos::create($commercialSettlementConcept);


						}


					}


				}

			}


			//fuera de grupos, busco por cada contrato el importe a pagar al comercial basándome en el id del comercial de los contratos que entran en liquidación
			//busco en las estrucutras de los contratos el comercial que estoy liquidando actualmente y que no hayan sido liquidadas anteriormente y no sea
			//el autor del contrato
			$contractProductsStructuresToSettle = TContratosProductosEstructuras::whereHas('contractProduct', function ($q)
			use ($fecha_desde, $fecha_hasta, $commercial) {

				$q->whereBetween('fecha_alta', [$fecha_desde, $fecha_hasta])
					->whereDoesntHave('settlementConcepts', function ($q2) use ($commercial) {
						$q2->where('id_comercial', $commercial->id_comercial);
					})
					->whereDoesntHave('contract', function ($q3) use ($commercial) {
						$q3->where('id_comercial', $commercial->id_comercial);

					});
			}
			)->where('id_comercial', $commercial->id_comercial)
				->with('contractProduct.contract')
				->get();


			//tengo las estructuras de todos los productos que tengo que pagarle.


			foreach ($contractProductsStructuresToSettle as $structureTosettle) {


				$structureManagement = TGerencias::where('id_gerencia', $structureTosettle->id_gerencia)->first();


				//busco en cada estructura el comercial original del contrato

				$originalCommercial = TComerciales::where('id_comercial', $structureTosettle->contractProduct->contract->id_comercial)->first();
				$originalCommercialCompanyContract = TContratosProductosEstructuras::where('id_contrato_producto', $structureTosettle->id_contrato_producto)->where('id_tipo_estructura', 'C')->first();


				//Para hacerlo perfecto tendría que buscar el id tipo contrato comercial en elas estructuras de pago donde el tipo


				//ahora  busco en el comercial original el grupo de comisionamiento que me interesa
				$cgroup = TGruposComisionamiento::whereHas('feeManagements', function ($q) use ($originalCommercial) {

					$q->where('id_gerencia', $originalCommercial->id_gerencia);

				})->where('id_tipo', 1)
					->orWhere('id_tipo', 3)
					->with([
						'feeManagements' => function ($q) use ($originalCommercial) {
							$q->where('id_gerencia', $originalCommercial->id_gerencia)->with('fees');
						},
					])
					->first()
					->load('feeProducts.fees', 'efectivityFees');


				//busco los productos que hay en cGroup si he encontrado un grupo que me valga

				if ($cgroup) {


					$cproducts = [];
					//tomo las id de loas artículos en un array

					foreach ($cgroup->feeProducts as $product) {
						$cproducts[] = $product->id_producto;
					}


					$cContractProducts = TContratosProductos::whereHas('contract', function ($q)
					use ($originalCommercial, $fecha_desde, $fecha_hasta) {

						$q->where('id_comercial', $originalCommercial->id_comercial);
					}

					)->whereBetween('fecha_alta', [$fecha_desde, $fecha_hasta])
						->whereIn('id_producto', $cproducts);


					$cProductsNum = $cContractProducts->count();
					//hallo la cantidad de productos que ha vendido el comercial BASE


					$cfee = TGruposComisionamientoComisiones::where('id_grupo', $cgroup->id)
						->where('cantidad_desde', '<=', $cProductsNum)
						->where('cantidad_hasta', '>=', $cProductsNum)
						->where('id_tipo_contrato_comercial', $originalCommercialCompanyContract->id_tipo_contrato_comercial)
						->where('id_estructura', $originalCommercialCompanyContract->id_gerencia_estructura)
						->first();

					if ($cfee) {


						//tengo el cfee que es la lina de comisiones que me interesa para pagar al comercial referido desde el comercial base


						//pago al comercial actual lo que indico en la tarifa de gerencia de
						// comercial base

						if ($commercial->id_nivel_funcional == '001') {
							$amount = $cfee->importe_gerencia;

						} else {
							$amount = $cfee->importe_estructura;
						}

						$importe_car += $amount;
						$commercialSettlementConcept = [
							'id_liquidacion'            => $settlement['id'],
							'id_grupo'                  => $cgroup['id'],
							'id_liquidacion_comercial'  => $commercialSettlement['id'],
							'id_comercial'              => $commercial->id_comercial,
							'concepto'                  => 10,
							'importe'                   => $amount,
							'id_contrato'               => $structureTosettle->contractProduct->contract->id_contrato,
							'id_contrato_producto'      => $structureTosettle->contractProduct->id,
							'tipo_linea'                => 0,
							'id_comercial_ref_directo'  => $structureManagement->id_comercial_liquidacion,
							'id_comercial_ref_original' => $structureTosettle->contractProduct->contract->id_comercial,
							'id_gerencia_ref_directa'   => $structureManagement->id_gerencia,
							'id_gerencia_ref_original'  => $structureTosettle->contractProduct->contract->id_gerencia,

						];

						TLiquidacionesComercialesConceptos::create($commercialSettlementConcept);

						//ahora comisiono el importe del producto si esta en finalizado y enviado
						if ($structureTosettle->contractProduct->contract->id_estado == 6 or $structureTosettle->contractProduct->contract->id_estado == 9) {
							$commercialCount = TGerencias::where('id_gerencia', $commercial->id_gerencia)->withCount('commercials')->first();

							$variableFee = TGruposComisionamientoProductosComisiones::
							where('id_comision_producto', $structureTosettle->contractProduct->id)
								->where('desde_promotores', '<=', $commercialCount->commercials_count)
								->where('hasta_promotores', '>=', $commercialCount->commercials_count)
								->first();


							if ($variableFee) {
								//liquidacion de comision de gerent por contrato finalizado ok

								$importe_car += $variableFee->importe_comision_estructura;
								$commercialSettlementConcept = [
									'id_liquidacion'            => $settlement['id'],
									'id_grupo'                  => $cgroup['id'],
									'id_liquidacion_comercial'  => $commercialSettlement['id'],
									'id_comercial'              => $commercial->id_comercial,
									'concepto'                  => 12,
									'importe'                   => $variableFee->importe_comision,
									'id_contrato'               => $structureTosettle->contractProduct->contract->id_contrato,
									'id_contrato_producto'      => $structureTosettle->contractProduct->id,
									'tipo_linea'                => 0,
									'id_comercial_ref_directo'  => $structureManagement->id_comercial_liquidacion,
									'id_comercial_ref_original' => $contract['id_comercial'],
									'id_gerencia_ref_directa'   => $paymentStructure->id_gerencia,
									'id_gerencia_ref_original'  => $contract['id_gerencia'],

								];

								TLiquidacionesComercialesConceptos::create($commercialSettlementConcept);


							}


						}
					}

					//en cgroup añado el importe por porcentaje de oks en el tramo de liquidacion de fecha


					$totalContracts = TContratos::query();
					$totalContracts = $totalContracts->whereHas('products', function ($q)
					use ($fecha_desde, $fecha_hasta) {
						$q->whereBetween('fecha_alta', [$fecha_desde, $fecha_hasta]);
					}
					)->where('id_comercial', $originalCommercial->id_comercial)
						->withCount([
							'products' => function ($q) use ($fecha_desde, $fecha_hasta) {
								$q->whereBetween('fecha_alta', [$fecha_desde, $fecha_hasta]);
							},
						]);

					$totalContractsCount = $totalContracts->get()->sum('products_count');

					$totalContractsOkCount = $totalContracts->where('id_estado', 6)->orwhere('id_estado', 9)->get()->sum('products_count');


					if ($totalContractsCount && $totalContractsOkCount) {

						$percentaje = $totalContractsOkCount * 100 / $totalContractsCount;

						//hallo la linea de comisionamiento que me interesa

						$percentajeFee = $cgroup->efectivityFees
							->where('efectividad_desde', '<=', $percentaje)
							->where('efectividad_hasta', '>=', $percentaje)
							->first();


						if ($percentajeFee) {
							if ($commercial->id_nivel_funcional == '001') {

								$amount = $percentajeFee->importe_gerencia;

							} elseif ($commercial->id_nivel_funcional === '002') {

								$amount = $percentajeFee->importe_estructura;

							} elseif ($commercial->id_nivel_funcional === '003') {

								$amount = $percentajeFee->importe_promotor;

							}

							$importe_car += $amount;
							$commercialSettlementConcept = [
								'id_liquidacion'            => $settlement['id'],
								'id_grupo'                  => $cgroup['id'],
								'id_liquidacion_comercial'  => $commercialSettlement['id'],
								'id_comercial'              => $commercial->id_comercial,
								'concepto'                  => 13,
								'importe'                   => $amount,
								'id_contrato'               => null,
								'id_contrato_producto'      => null,
								'tipo_linea'                => 0,
								'id_comercial_ref_directo'  => null,
								'id_comercial_ref_original' => null,
								'id_gerencia_ref_directa'   => null,
								'id_gerencia_ref_original'  => null,

							];

							TLiquidacionesComercialesConceptos::create($commercialSettlementConcept);


						}


					}


				}


			}


			//fuera de grupos, compruebo las entregas y los albaranes que tiene este GERENTE


			if ($commercial->id_nivel_funcional == '001') {


				$management_id = $commercial->id_gerencia;

				$settlementDateStart = Carbon::parse($settlement['fecha_liquidacion'])->startOfMonth();


				$settlementDateEnd = Carbon::parse($settlement['fecha_liquidacion'])->endOfMonth();


				$commercial_id = $commercial->id_comercial;
				//i search for the management  renting price


				$rentCount = TliquidacionesComerciales::whereHas('concepts', function ($q) use ($management_id) {

					$q->where('concepto', 8);
				})
					->where('id_comercial', $commercial_id)
					->whereBetween('fecha_pago', [$settlementDateStart, $settlementDateEnd])->count();

				if ($rentCount < 1 && $management->importe_alquiler > 0) {


					$commercialSettlementConcept = [
						'id_liquidacion'           => $settlement['id'],
						'id_grupo'                 => null,
						'id_liquidacion_comercial' => $commercialSettlement['id'],
						'id_comercial'             => $commercial->id_comercial,
						'concepto'                 => 8,
						'importe'                  => -$management->importe_alquiler,
						'tipo_linea'               => 0,

					];


					TLiquidacionesComercialesConceptos::create($commercialSettlementConcept);

					$commercial_amount -= $management->importe_alquiler;


				}

				$deliveriesToSettle = TEntregas::whereBetween('fecha', [$fecha_desde, $fecha_hasta])
					->where('id_gerencia', $management_id)
					->with('lines')
					->whereDoesntHave('settlementConcepts')
					->get();

				//not working doestnthave relations so i will iterate and check if exists

				foreach ($deliveriesToSettle as $delivery) {

					$count = TLiquidacionesComercialesConceptos::where('id_entrega', '=', $delivery->id_entrega)->count();

					if ($count == 0) {


						foreach ($delivery->lines as $deliveryLine) {

							$baseprice = $this->BuyPrice($management_id, $delivery->id_empresa, $deliveryLine->id_articulo, $deliveryLine->unidades_lote, $deliveryLine->cantidad,
								$management->defaultRate);

							$commercialSettlementConcept = [
								'id_liquidacion'           => $settlement['id'],
								'id_grupo'                 => 0,
								'id_liquidacion_comercial' => $commercialSettlement['id'],
								'id_comercial'             => $commercial->id_comercial,
								'concepto'                 => 6,
								'importe'                  => -$deliveryLine->cantidad * $baseprice,
								'id_entrega'               => $deliveryLine['id_venta'],
								'lin_entrega'              => $deliveryLine['lin_entrega'],
								'tipo_linea'               => 0,
							];

							TLiquidacionesComercialesConceptos::create($commercialSettlementConcept);
							$importe_car -= $deliveryLine->cantidad * $baseprice;
						}


					}


				}


				$deliveryNotesToSettle = TAlbaranes::whereBetween('fecha', [$fecha_desde, $fecha_hasta])
					->where('id_gerencia', '=', $management_id)
					->with('lines')
					->whereDoesntHave('settlementConcepts')
					->where('id_tipo_direccion', '=', 0)
					->orwhere('id_tipo_direccion', '=', 1)
					->get();

				//not working doestnthave relations so i will iterate and check if exists

				foreach ($deliveryNotesToSettle as $deliveryNote) {

					$count = TLiquidacionesComercialesConceptos::where('id_albaran', '=', $deliveryNote->id_albaran)->count();

					if ($count == 0) {


						foreach ($deliveryNote->lines as $deliveryNoteLine) {

							$baseprice = $this->BuyPrice($management_id, $deliveryNote->id_empresa, $deliveryNoteLine->id_articulo, $deliveryNoteLine->unidades_lote, $deliveryNoteLine->cantidad,
								$management->defaultRate);

							$commercialSettlementConcept = [
								'id_liquidacion'           => $settlement['id'],
								'id_grupo'                 => 0,
								'id_liquidacion_comercial' => $commercialSettlement['id'],
								'id_comercial'             => $commercial->id_comercial,
								'concepto'                 => 7,
								'importe'                  => -$deliveryNoteLine->cantidad * $baseprice,
								'id_albaran'               => $deliveryNoteLine['id_albaran'],
								'lin_albaran'              => $deliveryNoteLine['lin_albaran'],
								'tipo_linea'               => 0,
							];

							TLiquidacionesComercialesConceptos::create($commercialSettlementConcept);
							$importe_car -= $deliveryNoteLine->cantidad * $baseprice;

						}


					}


				}

				//luego comisión de la gerencia en caso de que tenga plus de ZONAS


			}


			$commercialSettlementData = [
				'numero_ventas'    => $salesCount,
				'numero_articulos' => $salesNum,
				'importe_inm'      => $commercial_amount,
				'importe_car'      => $importe_car,


			];
			$commercialSettlement->update($commercialSettlementData);


		}

	}

	/**
	 * Handle the t comerciales "updating" event.
	 *
	 * @param \App\Models\External\TTiposContratosComercial $settlement
	 *
	 * @return void
	 */
	public function updated(TLiquidaciones $settlement)
	{


	}

	/**
	 * Handle the t comerciales "deleted" event.
	 *
	 * @param \App\Models\External\TLiquidaciones $settlement
	 *
	 * @return void
	 */
	public function deleted(TLiquidaciones $settlement)
	{
		TLiquidacionesComercialesConceptos::where('id_liquidacion', $settlement->id)->delete();
		TLiquidacionesComerciales::where('id_liquidacion', $settlement->id)->delete();


	}

	/**
	 * Handle the t comerciales "restored" event.
	 *
	 * @param \App\Models\External\TLiquidaciones $settlement
	 *
	 * @return void
	 */
	public function restored(TLiquidaciones $settlement)
	{
		//
	}

	/**
	 * Handle the t comerciales "force deleted" event.
	 *
	 * @param \App\Models\External\TLiquidaciones $settlement
	 *
	 * @return void
	 */
	public function forceDeleted(TLiquidaciones $settlement)
	{
		//
	}


	private function BuyPrice($managemnt_id, $company_id, $article_id, $package, $quantity, $defaultRate)
	{


		$out = 0;
		$out2 = '';
		$precio_fijo = DB::connection(session('database'))->select("EXEC dbo.SP_VENTAS_PRECIO_ARTICULO_GERENCIA_PAQUETE ?,?,?,?,?,?,?", [
			$company_id,
			$article_id,
			$managemnt_id,
			1,
			$package,
			$out,
			$out2,
		]);


		if (isset($precio_fijo)) {
			if (isset($precio_fijo[0])) {

				$precio = $precio_fijo[0]->ret;
				$divisa = $precio_fijo[0]->id_divisa;
				if ($precio_fijo > 0) {
					return $precio;

				}
			}
		}
		//TARIFA DE COMPRA (de ventas en sql)


		$tarifa = $defaultRate;


		if ($tarifa) {

			$priceDB = DB::connection(session('database'))->select('SELECT dbo.F_VENTAS_PRECIO_TARIFA(?, ?, ?, ?, ?, ?) as price', [
				$company_id,
				$article_id,
				1,
				$tarifa,
				$quantity,
				$package,
			]);
			$priceDB = $priceDB[0]->price;

			if ($priceDB > 0) {
				return $priceDB;
			}


		}


		//no encuentra ninguna, intento introduciéndole el pack a mano

		$pack = TArticulosPaquetes::where('id_articulo', $article_id)->where('unidades_lote', '<>', $package)->orderBy('unidades_lote', 'desc')->first();

		$pack = $pack->unidades_lote;

		if ($pack) {


			$out = 0;
			$out2 = '';
			$precio_fijo = DB::connection(session('database'))->select("EXEC dbo.SP_VENTAS_PRECIO_ARTICULO_GERENCIA_PAQUETE ?,?,?,?,?,?,?", [
				$company_id,
				$article_id,
				$managemnt_id,
				1,
				$pack,
				$out,
				$out2,
			]);


			if (isset($precio_fijo)) {
				if (isset($precio_fijo[0])) {

					$precio = $precio_fijo[0]->ret;
					$divisa = $precio_fijo[0]->id_divisa;
					if ($precio_fijo > 0) {
						return ($precio / $pack) * $package;

					}
				}
			}


			if ($tarifa) {

				$priceDB = DB::connection(session('database'))->select('SELECT dbo.F_VENTAS_PRECIO_TARIFA(?, ?, ?, ?, ?, ?) as price', [
					$company_id,
					$article_id,
					1,
					$tarifa,
					$quantity,
					$pack,
				]);
				$priceDB = $priceDB[0]->price;

				if ($priceDB > 0) {
					return ($priceDB / $pack) * $package;
				}


			}


		}

		//nothing found
		return 0;

	}
}
