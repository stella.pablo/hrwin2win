<?php

namespace App\Policies;

use App\Models\User;
use App\Models\External\TComerciales;
use Illuminate\Auth\Access\HandlesAuthorization;

class CommercialPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any t comerciales.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the t comerciales.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\External\TComerciales  $commercial
     * @return mixed
     */
    public function view(User $user, TComerciales $commercial)
    {
	    return in_array($commercial->id_gerencia, $user->managements->pluck('external_id')->toArray()) || $user->level < 3;
    }

    /**
     * Determine whether the user can create t comerciales.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the t comerciales.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\External\TComerciales  $commercial
     * @return mixed
     */
    public function update(User $user, TComerciales $commercial)
    {
        //
    }

    /**
     * Determine whether the user can delete the t comerciales.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\External\TComerciales  $commercial
     * @return mixed
     */
    public function delete(User $user, TComerciales $commercial)
    {
        //
    }

    /**
     * Determine whether the user can restore the t comerciales.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\External\TComerciales  $commercial
     * @return mixed
     */
    public function restore(User $user, TComerciales $commercial)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the t comerciales.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\External\TComerciales  $commercial
     * @return mixed
     */
    public function forceDelete(User $user, TComerciales $commercial)
    {
        //
    }
}
