<?php

namespace App\Policies;

use App\Models\User;
use App\Models\External\TGerencias;
use Illuminate\Auth\Access\HandlesAuthorization;

class ManagementPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the t gerencias.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\External\TGerencias  $management
     * @return mixed
     */
    public function view(User $user, TGerencias $management)
    {
    	return in_array($management->id_gerencia, $user->managements->pluck('external_id')->toArray());
    }

    /**
     * Determine whether the user can create t gerencias.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the t gerencias.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\External\TGerencias  $management
     * @return mixed
     */
    public function update(User $user, TGerencias $management)
    {
        //
    }

    /**
     * Determine whether the user can delete the t gerencias.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\External\TGerencias  $management
     * @return mixed
     */
    public function delete(User $user, TGerencias $management)
    {
        //
    }

    /**
     * Determine whether the user can restore the t gerencias.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\External\TGerencias  $management
     * @return mixed
     */
    public function restore(User $user, TGerencias $management)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the t gerencias.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\External\TGerencias  $management
     * @return mixed
     */
    public function forceDelete(User $user, TGerencias $management)
    {
        //
    }
}
