<?php

namespace App\Console\Commands;

use Auth;
use Carbon\Carbon;
use DB;
use Elasticsearch;

use Illuminate\Console\Command;

class SearchIndexCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'search:index';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Index all commercials statistics into Elastic Search';

    private $search;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

//        Auth::loginUsingId(9);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
//        $contracts = DB::connection(auth()->user()->company->database)->table('t_contratos')->select(['id_comercial', 'id_gerencia', 'id_contrato', 'id_tipo_contrato', 'id_estado', 'hora_registro'])->get();
//		// dd($contracts[0]);
//		$contracts_es = [];
//
//        foreach ($contracts as $contract) {
//			$state = '';
//			$states_p = [0, 1, 2, 3];
//			$states_ok = [6, 9];
//			$states_ko = [4, 5, 7, 8];
//
//			if(in_array($contract->id_estado, $states_ok)) {
//				$state = 'ok';
//			}
//			elseif(in_array($contract->id_estado, $states_ko)) {
//				$state = 'ko';
//			}
//			else {
//				$state = 'p';
//			}
//
//			$contracts_es[] = [
//				'company_id' => auth()->user()->company_id,
//				'management_id' => auth()->user()->management_id,
//				'management_external_id' => $contract->id_gerencia,
//				'commercial_external_id' => $contract->id_comercial,
//				'campaign' => $contract->id_tipo_contrato,
//				'contract_id' => $contract->id_contrato,
//				'state' => $state,
//				'timestamp' => $contract->hora_registro
//			];
//        }
//
//		// $this->info('Creating index');
//		//
//		// Elasticsearch::indices()->create([
//		// 	'index' => 'contracts',
//		// 	'body' => [
//		// 		'mappings' => [
//		// 			'contracts' => [
//		// 				'_source' => [
//		// 					'enabled' => true
//		// 				],
//		// 				'properties' => [
//		// 					'company_id' => ['type' => 'integer'],
//		// 					'management_id' => ['type' => 'integer'],
//		// 					'contract_id' => ['type' => 'integer'],
//		// 					'timestamp' => ['type' => 'date', "format" => "yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||epoch_millis"],
//		// 				]
//		// 			]
//		// 		]
//		// 	]
//		// ]);
//
//		$this->info('Filling data...');
//
//		foreach($contracts_es as $contract_es) {
//			Elasticsearch::index([
//				'index' => 'contracts',
//				'type' => 'contracts',
//				'body' => $contract_es
//			]);
//
//			$this->line('~~~ Indexed: ' . $contract_es['contract_id']);
//		}
    }
}
