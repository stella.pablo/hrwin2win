<?php

namespace App\Console\Commands;

use App\Imports\MexicoContractsImport;
use Illuminate\Console\Command;

class ImportMexicoContracts extends Command
{

	protected $signature = 'import:mexicocontracts';
	protected $description = 'Command description';

	public function handle()
	{
		session(['database' => 'mexico']);

		$this->output->title('~ Importing contracts');
		(new MexicoContractsImport)->withOutput($this->output)->import('Contracts.xlsx');
		$this->output->success('~ Importing contracts');
	}
}
