<?php

namespace App\Console\Commands;

use App\Imports\MexicoStructureCommercialsImport;
use App\Imports\MexicoStructureManagementsImport;
use App\Imports\MexicoStructureOrganizationsImport;
use App\Models\External\TCampanyas;
use App\Models\External\TComerciales;
use App\Models\External\TComercialesCampanyas;
use App\Models\External\TComercialesContratos;
use App\Models\External\TEstrategias;
use App\Models\External\TGerencias;
use App\Models\External\TOrganizaciones;
use Carbon\Carbon;
use DB;
use Illuminate\Console\Command;

class ImportMexicoSructure extends Command
{

	protected $signature = 'import:mexicostructure';
	protected $description = 'Import places';

	public function handle()
	{
		session(['database' => 'mexico']);

		$this->reset();

		$this->output->title('~ Importing organizations');
		(new MexicoStructureOrganizationsImport)->withOutput($this->output)->import('MexicoStructure.xlsx');
		$this->output->success('~ Importing organizations');

		$this->output->title('~ Importing managements');
		(new MexicoStructureManagementsImport)->withOutput($this->output)->import('MexicoStructure.xlsx');
		$this->output->success('~ Importing managements');

		$this->output->title('~ Importing commercials');
		(new MexicoStructureCommercialsImport)->withOutput($this->output)->import('MexicoStructure.xlsx');
		$this->output->success('~ Importing commercials');

		return true;
	}

	private function reset() {
		TComerciales::truncate();
		TComercialesCampanyas::truncate();
		TComercialesContratos::truncate();
		TEstrategias::truncate();
		TGerencias::truncate();
		TOrganizaciones::getQuery()->delete();

//		dd('ok');

//		DBCC CHECKIDENT (t_estrategias, RESEED, 0);
//		DBCC CHECKIDENT (t_organizaciones, RESEED, 0);

		DB
			::connection(session('database'))
			->table('t_num_series_emp')
			->where('cod_emp', '01')
			->where('codigo', 'COMER')
			->update([
				'fecha_ult_num' => Carbon::now(),
				'ult_num'       => '000000000',
			]);

		DB
			::connection(session('database'))
			->table('t_num_series_emp')
			->where('cod_emp', '01')
			->where('codigo', 'GERENC')
			->update([
				'fecha_ult_num' => Carbon::now(),
				'ult_num'       => '0000',
			]);
	}
}
