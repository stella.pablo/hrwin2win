<?php

namespace App\Console\Commands;

use App\Models\Company;
use App\Models\External\TGerencias;
use App\Models\Management;
use App\Models\User;
use App\Models\UserManagement;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;

class ManagementsImportCommand extends Command
{

	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'import:managements';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Import managements and give permissions';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$databases = array_keys(Arr::except(config('database.connections'), 'mysql'));
		$database = $this->menu('Select database', $databases)->open();

		if ($database === null) {
			return true;
		}

		session(['database' => $databases[$database]]);

		$company = Company::where('database', $databases[$database])->firstOrFail();
		$users = User::orderBy('username')->get();
		$usersArr = [];

		foreach ($users as $user) {
			$usersArr[$user->id] = $user->username;
		}

		$userId = $this->menu('Select user to give permissions', $usersArr)->open();

		if ($userId === null) {
			return true;
		}

		$user = User::find($userId);

		$gerencias = TGerencias::all();

		foreach ($gerencias as $gerencia) {
			$this->line('- Searching for existing management id: ' . $gerencia->id_gerencia);
			$management = $company->managements()->where('external_id', $gerencia->id_gerencia)->first();

			if ($management) {
				if ($management->name !== $gerencia->gerencia) {
					$management->name = $gerencia->gerencia;
					$management->save();
				}
			} else {
				$this->info('~ Creating new management with id: ' . $gerencia->id_gerencia);
				$management = Management::create([
					'company_id'            => $company->id,
					'external_id'           => $gerencia->id_gerencia,
					'name'                  => $gerencia->gerencia,
					'address'               => 'DIRECCIÓN',
					'address_link'          => 'DIRECCIÓN_GOOGLE_MAPS',
					'workhours'             => '09:00 - 20:00',
					'phone'                 => '999666111',
					'url'                   => 'http://url.com/',
					'monthly_objective'     => 10,
					'productivity_low_days' => 4,
				]);
			}

			if (UserManagement::where('user_id', $user->id)->where('management_id', $management->id)->doesntExist()) {
				$this->info('~ Creating new permission for user `' . $user->name . '` and management id: ' . $gerencia->id_gerencia);
				UserManagement::create([
					'user_id'       => $user->id,
					'management_id' => $management->id,
				]);
			}
		}
	}
}
