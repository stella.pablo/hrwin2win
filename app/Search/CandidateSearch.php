<?php

namespace App\Search;

use App\Models\Candidate;
use Illuminate\Http\Request;

class CandidateSearch
{
    public static function apply(Request $filters)
    {
		$candidates = (new Candidate)->newQuery();

		if($filters->has('trashed')) {
			$candidates->withTrashed();
		}

		if($filters->has('date_from')) {
			$candidates->whereDate('petitioned_at', '>=', $filters->date_from);
		}

		if($filters->has('date_to')) {
			$candidates->whereDate('petitioned_at', '<=', $filters->date_to);
		}

		if($filters->has('origin_id')) {
			$candidates->where('origin_id', $filters->origin_id);
		}

		if($filters->has('status_id')) {
			$candidates->where('status_id', $filters->status_id);
		}

		if($filters->has('management_id')){
		    $candidates->where('management_id', $filters->management_id);
        }

		if($filters->has('appointmentfirst')) {
			if($filters->appointmentfirst == 1) {
				$candidates->doesntHave('appointmentfirst');
			}
			else {
				$candidates->whereHas('appointmentfirst', function($query) use ($filters) {
					$status = 1;
					# Did come
					if($filters->appointmentfirst == 3) {
						$status = 2;
					}
					# Did not come
					if($filters->appointmentfirst == 4) {
						$status = 0;
					}
					$query->where('status', $status);
				});
			}
		}

		if($filters->has('appointmentsecond')) {
			if($filters->appointmentsecond == 1) {
				$candidates->doesntHave('appointmentsecond');
			}
			else {
				$candidates->whereHas('appointmentsecond', function($query) use ($filters) {
					$status = 1;
					# Did come
					if($filters->appointmentsecond == 3) {
						$status = 2;
					}
					# Did not come
					if($filters->appointmentsecond == 4) {
						$status = 0;
					}
					$query->where('status', $status);
				});
			}
		}

		if($filters->has('preapproved')) {
			$boolean = $filters->preapproved == "true" ? true : false;
			$candidates->where('preapproved', $boolean);
		}

		if($filters->has('approved')) {
			$boolean = $filters->approved == "true" ? true : false;
			$candidates->where('approved', $boolean);
		}

		if($filters->has('hired')) {
			$boolean = $filters->hired == "true" ? true : false;
			$candidates->where('hired', $boolean);
		}

		if($filters->has('latest')) {
			$candidates->latest();
		}

		if($filters->has('take')) {
			$candidates->take($filters->take);
		}

		$managementsIds = [];

		if($filters->filled('management_id')) {
			$managementsIds[] = $filters->management_id;
		}
		else {
			$managementsIds = auth()->user()->managements
				->pluck('id')
				->toArray();
		}

		$candidates->whereIn('management_id', $managementsIds);

        return $candidates->get();
    }
}
