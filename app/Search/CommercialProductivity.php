<?php

namespace App\Search;

use DB;
use App\Models\CommercialsReport;
use Illuminate\Http\Request;

class CommercialProductivity
{
    public static function apply(Request $filters)
    {
		$commercials = DB::connection(auth()->user()->company->database)->select("SELECT * from f_get_cuenta_contratos (?, ?, ?)", [
			$filters->date_from,
			$filters->date_to,
			$filters->campaign
		]);

        if ($filters->has('productivitylow')) {
            $commercialsLow = DB::connection(auth()->user()->company->database)->select("SELECT id_comercial FROM f_get_comerciales_toxicos (?)", [$filters->date]);
			$commercialsLowIds = array_pluck($commercialsLow, 'id_comercial');

			$commercials = array_values(array_where($commercials, function($commercial, $key) use ($commercialsLowIds) {
				return in_array($commercial->comercial, $commercialsLowIds);
			}));
        }

		if($filters->has('manager')) {
			$manager = $filters->manager;
			$commercials = array_values(array_where($commercials, function($commercial, $key) use ($manager) {
				return $commercial->id_gerencia === $manager;
			}));
		}

		if($filters->user()->level === 3) {
			$manager = $filters->user()->management->external_id;
			$commercials = array_values(array_where($commercials, function($commercial, $key) use ($manager) {
				return $commercial->id_gerencia === $manager;
			}));
		}

        foreach ($commercials as $key => $commercial) {
			// $commercial->internal = Commercial::where('external_id', $commercial->comercial)->first();
			$commercial->report = CommercialsReport::where('external_id', $commercial->comercial)->latest()->first();
        };

        return $commercials;
    }
}
