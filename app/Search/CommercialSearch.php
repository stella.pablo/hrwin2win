<?php

namespace App\Search;

use App\Models\External\TComerciales;
use Carbon\Carbon;

class CommercialSearch
{

	public static function apply($filters)
	{
		$relations = [
			'campaigns:t_campanyas.id,abreviatura',
			'management.manager:id_comercial,nombre,apellido1,apellido2,nombre_completo',
		];

		if (session('database') === 'mexico') {
			$relations[] = 'management.organization:id,organizacion';
			$relations[] = 'strategy:id,id_estrategia';
			$relations[] = 'paymentproofs';

		}

		$commercials = TComerciales::with($relations)->orderBy('nombre_completo');

		$organizationId = $filters['organization_id'] ?? null;

		if (isset($filters['campaign_id'])) {
			$commercials->whereHas('campaigns', function ($query) use ($filters) {
				$query->where('abreviatura', $filters['campaign_id']);
			});
		}

		if (isset($filters['commercial_id'])) {
			$commercials->where('id_comercial', $filters['commercial_id']);
		}

		if (isset($filters['date_from']) AND isset($filters['date_to'])) {
			$commercials->whereBetween('fecha_alta', [$filters['date_from'], $filters['date_to']]);
		} elseif (isset($filters['date_from'])) {
			$commercials->whereDate('fecha_alta', '>=', $filters['date_from']);
		} elseif (isset($filters['date_to'])) {
			$commercials->whereDate('fecha_alta', '<=', $filters['date_to']);
		}

		if (isset($filters['latest'])) {
			$commercials->latest('fecha_alta');
		}

		if (isset($filters['strategy_id'])) {
			$commercials->where('id_estrategia', $filters['strategy_id']);
		}

		if (isset($filters['take'])) {
			$commercials->take($filters['take']);
		}

		if (isset($filters['with_trashed'])) {
			$commercials->withTrashed();
		}
		else {
			$commercials->where(function ($query) {
				$query
					->whereNull('fecha_baja')
					->orWhere('fecha_baja', '>', Carbon::now());
			});
		}


		// @TODO: Fix this shit...

		$managementsIds = [];

		// If we want a specific management
		if (isset($filters['management_id'])) {

			$managementsIds = [$filters['management_id']];
		} // Sometimes we want commercials without managements so we sent management_id empty
		elseif (!array_key_exists('management_id', $filters)) {
			$managementsIds = auth()->user()->managements->pluck('external_id')->toArray();
		}

		$commercials->where(function ($q) use ($managementsIds, $organizationId) {
			$q->whereHas('management', function ($query) use ($managementsIds, $organizationId) {
					$query->whereIn('id_gerencia', $managementsIds);

					if ($organizationId) {
						$query->where('id_organizacion', $organizationId);
					}
				})
				->orWhereNull('id_gerencia');;
		});

		return $commercials;
	}

	public static function get($filters)
	{
		return self::apply($filters)->get();
	}
}
